const { app, BrowserWindow, dialog, net, ipcMain } = require('electron');
const isDev = require('electron-is-dev');
const { autoUpdater } = require('electron-updater');
const path = require('path');
const db = require('./db');
const { UPDATE_MESSAGE, BIDV_PROXY_CHECK_PAYMENT, BIDV_RESPONSE } = require('./event/app');
const rootMainProcess = require('./main-processes/index');

let mainwindow;

function createWindow() {
  mainwindow = new BrowserWindow({
    show: false,
    backgroundColor: '#FFFFFF',
    webPreferences: {
      devTools: isDev,
      nodeIntegration: false,
      contextIsolation: true,
      preload: isDev
        ? path.join(app.getAppPath(), 'electron/preload')
        : path.join(app.getAppPath(), 'electron/preload'),
    }
  });

  const startUrl = isDev ? 'http://localhost:3000' : `file://${path.join(__dirname, '../build/index.html')}`;
  mainwindow.loadURL(startUrl);

  mainwindow.once('ready-to-show', () => { mainwindow.show() });
  mainwindow.maximize();
  mainwindow.on('closed', () => {
    mainwindow = null;
  });
}

app.whenReady().then(createWindow).then(() => {
  db.authenticate()
    .then(() => { console.log('Connect to sqlite successfully!') })
    .catch((err) => { console.log(err) });

  if (isDev) {
    autoUpdater.updateConfigPath = path.join(__dirname, 'app-update.yml');
    const { default: installExtension, REDUX_DEVTOOLS } = require('electron-devtools-installer');
    installExtension(REDUX_DEVTOOLS)
      .then((name) => console.log(`Added Extension:  ${name}`))
      .catch((err) => console.log('An error occurred: ', err));
  }
});

/* Begin Main Process */

rootMainProcess();

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
});

/* Check update processes */
autoUpdater.autoInstallOnAppQuit = false;

autoUpdater.on('error', () => {
  dialog.showErrorBox('Error', 'An error occurred while updating, please try again!');
  mainwindow.webContents.send(UPDATE_MESSAGE, 'update-error');
});

autoUpdater.on('update-not-available', () => {
  mainwindow.webContents.send(UPDATE_MESSAGE, 'update-not-available');
});

autoUpdater.on('update-downloaded', () => {
  dialog.showMessageBox({
    type: 'info',
    buttons: ['Restart', 'Later'],
    title: 'Application Update!',
    detail: 'A new version has been downloaded. Restart the application to apply the updates.'
  }).then(({ response }) => {
    if (response === 0) {
      autoUpdater.quitAndInstall();
    }
  });

  mainwindow.webContents.send(UPDATE_MESSAGE, 'update-downloaded');
})

ipcMain.handle(BIDV_PROXY_CHECK_PAYMENT, (e, { url }) => {
  const request = net.request({
    method: 'GET',
    url,
  });

  request.on('response', (response) => {
    console.log(`STATUS: ${response.statusCode}`);
    console.log(`HEADERS: ${JSON.stringify(response.headers)}`);

    response.on('data', (data) => {
      console.log(`BODY: ${data}`);
      //mainwindow.webContents.send(BIDV_RESPONSE, `Body: ${data}`);
      mainwindow.webContents.send(BIDV_RESPONSE, `${data}`);
    });
  });

  request.on('finish', () => {
    console.log('Request is Finished');
  });
  request.on('abort', () => {
    console.log('Request is Aborted')
  });
  request.on('error', (error) => {
    console.log(`ERROR: ${JSON.stringify(error)}`)
  });
  request.on('close', (error) => {
    console.log('Last Transaction has occured')
  });
  request.setHeader('Content-Type', 'application/json');
  request.end();
});

/* End Main Process */

app.disableHardwareAcceleration();