const { default: sequelize, Op, QueryTypes } = require("sequelize");
const db = require("../db");

const ReceiptDetails = db.define("receipt_details", {
  receipt_id: {
    type: sequelize.INTEGER,
    field: "receipt_id",
  },
  sku: {
    type: sequelize.INTEGER,
    field: "sku",
  },
  qty: {
    type: sequelize.INTEGER,
    field: "qty",
  },
  base_price: {
    type: sequelize.INTEGER,
    field: "base_price",
  },
  discount_price: {
    type: sequelize.INTEGER,
    field: "discount_price",
  },
}, {
  timestamps: false,
});

ReceiptDetails.getAllByReceiptId = async (receipt_id) => {
  const receipt_details = ReceiptDetails.findAll({
    where: { receipt_id },
    raw: true,
    attributes: ['receipt_id', 'sku', 'qty', 'base_price', 'discount_price'],
  });

  return receipt_details;
}

ReceiptDetails.addDatas = async ({ receipt_id, product }) => {
  const query = `
    INSERT INTO receipt_details ( receipt_id, sku, qty, base_price, discount_price )
    VALUES ( ${receipt_id}, ${product.sku}, ${product.qty}, 
      ${product.base_price}, ${product.discount} )
  `;

  const receipt_details = await db.query(query, {
    raw: true,
    type: QueryTypes.INSERT,
  });

  return receipt_details;
};

ReceiptDetails.getAllData = async () => {
  const query = "SELECT * FROM receipt_details";
  const data = await db.query(query, {
    raw: true,
    type: QueryTypes.SELECT,
  });

  return data;
};

ReceiptDetails.getDataByReceiptId = async ({ receipt_id }) => {
  const query = `SELECT * FROM receipt_details WHERE receipt_id = ${receipt_id}`;
  const data = await db.query(query, {
    raw: true,
    type: QueryTypes.SELECT,
  });

  return data;
};

ReceiptDetails.deleteDataById = async (receipt_id) => {
  const query = `DELETE FROM receipt_details WHERE receipt_id = ${receipt_id}`;
  const data = await db.query(query, {
    raw: true,
    type: QueryTypes.DELETE,
  });

  return data;
};

ReceiptDetails.clearData = async () => {
  const query = "DELETE FROM receipt_details";
  const data = await db.query(query, {
    raw: true,
    type: QueryTypes.DELETE,
  });

  return data;
};

module.exports = ReceiptDetails;
