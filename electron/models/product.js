const { default: sequelize, Op, QueryTypes } = require('sequelize');
const db = require("../db");

const Product = db.define('products', {
  product_sku: {
    type: sequelize.INTEGER,
    field: 'product_sku',
    // autoIncrement,
  },
  product_barcode: {
    type: sequelize.STRING,
    field: 'product_barcode',
  },
  product_name: {
    type: sequelize.STRING,
    field: 'product_name',
  },
  product_brand: {
    type: sequelize.STRING,
    field: 'product_brand',
  },
  product_type: {
    type: sequelize.INTEGER,
    field: 'product_type',
  },
  product_config: {
    type: sequelize.INTEGER,
    field: 'product_config',
  }
});

Product.searchProductsByText = async (search_text) => {
  const products = await Product.findAll({
    raw: true,
    attributes: ['product_sku', 'product_barcode', 'product_name', 'product_config', 'product_brand'],
    where: {
      [Op.or]: [
        {
          product_sku: {
            [Op.like]: `%${search_text}%`,
          }
        },
        {
          product_name: {
            [Op.like]: `%${search_text}%`,
          }
        },
        {
          product_barcode: {
            [Op.like]: `%${search_text}%`,
          }
        },
      ]
    },
  })

  return products;
}

Product.getProductList = async (sku_lists) => {
  const sortedSkuList = JSON.parse(JSON.stringify(sku_lists)).sort((firstParam, secondParam) => { return firstParam - secondParam });

  const whenThenQuery = sku_lists.map((item, index) => ` WHEN ${item} THEN ${sortedSkuList[index]} `).join('');

  const query = `
    select product_sku, product_name, product_barcode, product_config, product_brand from products
    where product_sku in (${sku_lists})
    order by CASE product_sku
    ${whenThenQuery}
    END
  `;

  const products = await db.query(query,
    {
      raw: true,
      type: QueryTypes.SELECT,
    }
  );

  return products;
}

module.exports = Product;