const dayjs = require("dayjs");
const { default: sequelize, Op, QueryTypes } = require("sequelize");
const db = require("../db");
const ReceiptDetails = require("./receipt-detail");

const Receipts = db.define(
  "receipts",
  {
    id: {
      type: sequelize.INTEGER,
      field: "id",
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    remote_receipt_id: {
      type: sequelize.INTEGER,
      field: "remote_receipt_id",
      allowNull: true,
    },
    remote_receipt_code: {
      type: sequelize.STRING,
      field: "remote_receipt_code",
      allowNull: true,
    },
    local_created_at: {
      type: sequelize.INTEGER,
      field: "local_created_at",
      allowNull: true,
    },
    local_updated_at: {
      type: sequelize.INTEGER,
      field: "local_updated_at",
      allowNull: true,
    },
    remote_cdate: {
      type: sequelize.INTEGER,
      field: "remote_cdate",
      allowNull: true,
    },
    remote_udate: {
      type: sequelize.INTEGER,
      field: "remote_udate",
      allowNull: true,
    },
    status: {
      type: sequelize.INTEGER,
      field: "status",
      allowNull: false,
    },
    isSync: {
      type: sequelize.INTEGER,
      field: "isSync",
      allowNull: false,
    },
    gift_code: {
      type: sequelize.STRING,
      field: "gift_code",
      allowNull: false,
    },
    gift_items: {
      type: sequelize.STRING,
      field: "gift_items",
      allowNull: false,
    },
    customer: {
      type: sequelize.STRING,
      field: "customer",
      allowNull: false,
    },
    note: {
      type: sequelize.STRING,
      field: "note",
      allowNull: false,
    },
    distNum: {
      type: sequelize.STRING,
      field: "distNum",
      allowNull: false,
    },
    distPercent: {
      type: sequelize.STRING,
      field: "distPercent",
      allowNull: false,
    },
    expired_items: {
      type: sequelize.STRING,
      field: "expired_items",
      allowNull: false,
    },
    codes: {
      type: sequelize.STRING,
      field: "codes",
      allowNull: false,
    },
    payment_summary: {
      type: sequelize.STRING,
      field: "payment_summary",
      allowNull: false,
    },
    gift_card_payment: {
      type: sequelize.STRING,
      field: "gift_card_payment",
      allowNull: false,
    },
    card_payment: {
      type: sequelize.STRING,
      field: "card_payment",
      allowNull: false,
    },
    qr_payment: {
      type: sequelize.STRING,
      field: "qr_payment",
      allowNull: false,
    },
    balance_payment: {
      type: sequelize.STRING,
      field: "balance_payment",
      allowNull: false,
    },
    vat: {
      type: sequelize.STRING,
      field: "vat",
      allowNull: false,
    },
    total_price: {
      type: sequelize.INTEGER,
      field: "total_price",
      allowNull: false,
    },
    user: {
      type: sequelize.STRING,
      field: "user",
      allowNull: false,
    },
  },
  {
    timestamps: false,
  }
);

const extractReceiptData = (receipt_data) => {
  const {
    user,
    posReceipt,
    product,
    customer,
    payment,
    mode,
    note,
    distNum,
    distPercent,
    total_price,
  } = receipt_data;

  const {
    remote_receipt_id,
    remote_receipt_code,
    receipt_udate,
    receipt_cdate,
    isComplete,
  } = posReceipt;
  const customerInfo = {
    ...customer.data,
    isNewCustomer: customer.isNewCustomer,
  };
  const {
    summary,
    type: { giftCard, card, qrPay, balance },
    vat,
  } = payment;
  const gift_code = product.gift.gift_code;
  const { gift_items, expired_items, codes } = product.productTable;

  return {
    user: JSON.stringify(user),
    remote_receipt_id,
    remote_receipt_code,
    receipt_udate,
    receipt_cdate,
    isComplete,
    total_price,
    note,
    distNum,
    distPercent,
    customerInfo: JSON.stringify(customerInfo),
    summary: JSON.stringify(summary),
    giftCard: JSON.stringify(giftCard),
    card: JSON.stringify(card),
    qrPay: JSON.stringify(qrPay),
    balance: JSON.stringify(balance),
    vat: JSON.stringify(vat),
    gift_code: JSON.stringify(gift_code),
    gift_items: JSON.stringify(gift_items),
    expired_items: JSON.stringify(expired_items),
    codes: JSON.stringify(codes),
  };
};

const RECEIPT_STATUS = {
  DRAFT: 0,
  PENDING: 1,
  COMPLETE: 2,
};

Receipts.createNew = async (new_data) => {
  const data = extractReceiptData(new_data);

  const time = dayjs().unix();

  const receipt_id = await Receipts.create({
    remote_receipt_id: data.remote_receipt_id,
    remote_receipt_code: data.remote_receipt_code,
    local_created_at: time,
    local_updated_at: time,
    remote_cdate: data.receipt_cdate,
    remote_udate: data.receipt_udate,
    status: RECEIPT_STATUS.DRAFT,
    isSync: false,
    gift_code: data.gift_code,
    gift_items: data.gift_items,
    customer: data.customerInfo,
    note: data.note,
    distNum: data.distNum,
    distPercent: data.distPercent,
    expired_items: data.expired_items,
    codes: data.codes,
    payment_summary: data.summary,
    gift_card_payment: data.giftCard,
    card_payment: data.card,
    qr_payment: data.qrPay,
    balance_payment: data.balance,
    vat: data.vat,
    user: data.user,
    total_price: data.total_price,
  }).then((result) => result.id);

  // const query = `
  //   INSERT INTO receipts ( code, total_price, discount, user_id, created_at, updated_at )
  //   VALUES ( ${code}, ${total_price}, ${discount},
  //         ${user_id}, ${created_at}, ${updated_at} )
  // `;

  // const receipts = await db.query(query, {
  //   raw: true,
  //   type: QueryTypes.INSERT,
  // });

  // const query_getID = `
  //   SELECT id from receipts WHERE created_at = ${created_at} AND user_id = ${user_id}
  // `;

  // const receipt_id = await db.query(query_getID, {
  //   raw: true,
  //   type: QueryTypes.SELECT,
  // });

  return receipt_id;
};

Receipts.getById = async (id) => {
  const receipt = Receipts.findOne({
    where: { id },
    raw: true,
  });

  return receipt;
};

Receipts.getChunkById = async (id, columns) => {
  const receipt = Receipts.findOne({
    where: { id },
    attributes: [...columns],
    raw: true,
  });

  return receipt;
};

Receipts.updateReceipt = async (id, column, data) => {
  const receipt = await Receipts.findOne({
    where: { id },
  });

  receipt[column] = data;
  await receipt.save();
};

Receipts.updateAttributes = async (id, data) => {
  const receipt = await Receipts.findOne({
    where: { id },
  });

  receipt.update(data);
  await receipt.save();
};

///////////////////////////////////////////////

Receipts.updateById = async (new_data) => {
  const {
    total_price,
    discount,
    updated_at,
    status,
    customer,
    receipt_id,
  } = new_data;

  let formatData = `
    ${total_price ? "total_price = " + total_price + "," : ""}
    ${discount ? "discount = " + discount + "," : ""}
    ${updated_at ? "updated_at = " + updated_at + "," : ""}
    ${status ? "status = " + status + "," : ""}
    ${
      customer
        ? "customer = '" + customer + "',"
        : customer === null
        ? "customer = null,"
        : ""
    }
  `;
  let i = formatData.lastIndexOf(",");
  formatData = formatData.substring(0, i);

  const query = `
      UPDATE receipts
      SET ${formatData}
      WHERE id = ${receipt_id};
  `;

  const returnResult = await db.query(query, {
    raw: true,
    type: QueryTypes.UPDATE,
  });

  return returnResult;
};

Receipts.getId = async (id) => {
  const query = `SELECT * FROM receipts WHERE id = ${id}`;

  const receipts = await db.query(query, {
    raw: true,
    type: QueryTypes.INSERT,
  });

  return receipts;
};

Receipts.deleteWithId = async (id) => {
  const query = `DELETE FROM receipts WHERE id = ${id}`;

  const receipts = await db.query(query, {
    raw: true,
    type: QueryTypes.INSERT,
  });

  return receipts;
};

Receipts.clearData = async () => {
  const query = "DELETE FROM receipts";
  const data = await db.query(query, {
    raw: true,
    type: QueryTypes.DELETE,
  });

  return data;
};

Receipts.getAllData = async () => {
  const query = "SELECT * FROM receipts";
  const data = await db.query(query, {
    raw: true,
    type: QueryTypes.SELECT,
  });

  return data;
};

Receipts.getDetail = async ({ code }) => {
  console.log("Receipts.getDetail");
  console.log(code);
  const query = `
    SELECT *
    FROM receipts
    JOIN receipt_details, products
    ON receipts.id = receipt_details.receipt_id AND receipt_details.sku = products.product_sku
    WHERE receipt_details.receipt_id = ${code}
  `;
  const data = await db.query(query, {
    raw: true,
    type: QueryTypes.SELECT,
  });

  return data;
};

Receipts.getList = async (params) => {
  const result = await Receipts.findAll({
    raw: true,
  });
  return result;
};

module.exports = Receipts;
