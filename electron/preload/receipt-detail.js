const { ipcRenderer } = require("electron");
const { RECEIPT_DETAIL } = require("../event/receipt-detail");

module.exports = {
  addNewDataIntoReceiptDetailTable: ({ receipt_id, products }) =>
    ipcRenderer.invoke(RECEIPT_DETAIL.ADD_NEW_DATA, {
      receipt_id,
      products,
    }),
  getAllDataFromReceiptDetailTable: (args) =>
    ipcRenderer.invoke(RECEIPT_DETAIL.GET_ALL_DATA, args),
  getDataByReceiptIdFromReceiptDetailTable: ({ receipt_id }) =>
    ipcRenderer.invoke(RECEIPT_DETAIL.GET_DATA_BY_RECEIPT_ID, { receipt_id }),
};
