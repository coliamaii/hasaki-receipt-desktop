const { ipcRenderer } = require("electron");
const {
  QUIT_APP,
  GET_VERSION,
  CHECK_UPDATE,
  UPDATE_MESSAGE,
  QUIT_AND_UPDATE,
  BIDV_PROXY_CHECK_PAYMENT,
  BIDV_RESPONSE,
} = require("../event/app");

module.exports = {
  quitApp: (args) => ipcRenderer.send(QUIT_APP, args),
  getVersion: (args) => ipcRenderer.invoke(GET_VERSION, args),
  checkUpdate: (args) => ipcRenderer.invoke(CHECK_UPDATE, args),
  quitAndUpdate: (args) => ipcRenderer.send(QUIT_AND_UPDATE, args),
  listenUpdateMessage: (callback) => ipcRenderer.on(UPDATE_MESSAGE, callback),
  bidvCheckPaymentStatus: ({ url }) => ipcRenderer.invoke(BIDV_PROXY_CHECK_PAYMENT, { url }),
  listenBidvResponse: (callback) => ipcRenderer.on(BIDV_RESPONSE, callback),
};
