const { contextBridge } = require("electron");

const app = require("./app");
const product = require("./product");
const receipt = require("./receipt");
const receipt_detail = require("./receipt-detail");

contextBridge.exposeInMainWorld("api", {
  ...app,
  ...product,
  ...receipt,
  ...receipt_detail,
});