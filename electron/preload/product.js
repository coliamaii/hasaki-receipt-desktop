const { ipcRenderer } = require("electron");
const {
  SEARCH_PRODUCTS,
  GET_PRODUCTS_RULE_BY_PRICE,
} = require("../event/product");

module.exports = {
  searchProducts: ({ search_text }) =>
    ipcRenderer.invoke(SEARCH_PRODUCTS, { search_text }),
  getProductPriceByRule: ({ items }) =>
    ipcRenderer.invoke(GET_PRODUCTS_RULE_BY_PRICE, { items }),
};
