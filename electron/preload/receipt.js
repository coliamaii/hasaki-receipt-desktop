const { ipcRenderer } = require("electron");
const { RECEIPT } = require("../event/receipt");

module.exports = {
  createNewReceipt: (data) => ipcRenderer.invoke(RECEIPT.CREATE_NEW, data),
  fetchAllReceiptInfoById: (id) => ipcRenderer.invoke(RECEIPT.FETCH_ALL_RECEIPT_INFO_BY_ID, id),
  getReceptChuckById: (id, columns) => ipcRenderer.invoke(RECEIPT.GET_RECEIPT_CHUNK_BY_ID, { id, columns }),
  updateReceiptById: (id, column, data) => ipcRenderer.invoke(RECEIPT.UPDATE_BY_ID, { id, column, data }),
  updateAttributesById: (id, data) => ipcRenderer.invoke(RECEIPT.UPDATE_ATTRIBUTE_BY_ID, { id, data }),

  getDataByIdFromReceiptTable: ({ id }) =>
    ipcRenderer.invoke(RECEIPT.GET_DATA_BY_ID, { id }),
  updateDataByIdFromReceiptTable: (new_data) =>
    ipcRenderer.invoke(RECEIPT.UPDATE_DATA_BY_ID, new_data),
  getAllDataFromReceiptTable: (args) =>
    ipcRenderer.invoke(RECEIPT.GET_ALL_DATA, args),
  getDetailReceipt: (params) => ipcRenderer.invoke(RECEIPT.GET_DATA_JOIN_RECEIPT_DETAIL_TABLE, params),
  getListReceipt: (params) => ipcRenderer.invoke(RECEIPT.GET_ALL_DATA_WITH_QTY, params),
};
