const { Sequelize } = require('sequelize');
const isDev = require('electron-is-dev');
const path = require('path');

const db = new Sequelize({
  dialect: 'sqlite',
  storage: isDev
    ? path.join(__dirname, '../db/hasaki.db')
    : path.join(process.resourcesPath, 'db/hasaki.db'),
  define: {
    timestamps: false,
  },
});

module.exports = db;