const { default: sequelize, QueryTypes } = require('sequelize');
const db = require("../db");

const Combo = db.define('combos', {
  combo_sku: {
    type: sequelize.NUMBER,
    field: 'combo_sku',
  },
  product_sku: {
    type: sequelize.NUMBER,
    field: 'product_sku',
  },
  qty: {
    type: sequelize.NUMBER,
    field: 'qty',
  },
});

Combo.getListCombo = async (raw) => {
  //const raw = [{ "sku": 100160062, "qty": 1 }, { "sku": 204900005, "qty": 2 }];

  const unionQuery = raw.map(item => `select * from combos c WHERE c.product_sku="${item.sku}" and c.qty <= ${item.qty}`).join(' UNION ');

  const query = `
    select x.combo_sku from (
    ${unionQuery}
    ) x 
    GROUP by x.combo_sku
    HAVING count(product_sku)=(select count(*) from combos where combo_sku=x.combo_sku)
  `;

  const combos = await db.query(query,
    {
      raw: true,
      type: QueryTypes.SELECT,
    }
  );

  return combos;
}

Combo.getComboDetails = async (list_sku) => {
  const query = `
    select c.combo_sku as combo_sku, p.product_name as combo_name, c.product_sku as product_sku, c.qty as qty 
    from combos c join products p on c.combo_sku=p.product_sku 
    where c.combo_sku in (${list_sku.join(',')})
  `;

  const combos = await db.query(query,
    {
      raw: true,
      type: QueryTypes.SELECT,
    }
  );

  const comboDetails = combos.reduce((arr, combo) => {
    const index = arr.findIndex(item => item.combo_sku === combo.combo_sku);
    const product_detail = { sku: combo.product_sku, qty: combo.qty };

    index === -1 ? arr.push({
      combo_sku: combo.combo_sku,
      combo_name: combo.combo_name,
      combo_details: [product_detail],
    }) : arr[index].combo_details.push(product_detail);

    return arr;
  }, []);

  return comboDetails;
}

module.exports = Combo;