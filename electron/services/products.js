const Combo = require('../models/combo');
const Product = require('../models/product');

const searchProducts = async (search_text) => {
  const products = await Product.searchProductsByText(search_text);
  return products;
}

const getProductsByRule = async (items) => {
  const sku_lists = items.map(item => item.sku);
  const products_info = await Product.getProductList(sku_lists);
  const products = products_info.map((product) => {
    const index = items.findIndex(item => item.sku == product.product_sku);
    return {
      ...product,
      qty: items[index].qty,
    }
  });

  const combos = await Combo.getListCombo(items);
  const comboDetails = await Combo.getComboDetails(combos.map(item => item.combo_sku));

  return { products, combos: comboDetails };
}

module.exports = {
  searchProducts,
  getProductsByRule,
}