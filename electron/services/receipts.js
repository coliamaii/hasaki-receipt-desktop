const Receipts = require("../models/receipt");

const createNewReceipt = async (new_data) => {
  // await Receipts.clearData();
  const receipt_id = await Receipts.createNew(new_data);

  return receipt_id;
};

const getReceiptById = async (id) => {
  const receipt = await Receipts.getById(id);
  return receipt;
}

const updateReceiptById = async (id, column, data) => {
  await Receipts.updateReceipt(id, column, data);
}

const getChunkById = async (id, columns) => {
  const receipt = await Receipts.getChunkById(id, columns);
  return receipt;
}

const updateAttributesById = async (id, data) => {
  console.log({data})
  await Receipts.updateAttributes(id, data);
}


////////////////////////////

const getIdReceipt = async (id) => {
  const receiptID = await Receipts.getId(id);

  return receiptID;
};

const getAllReceiptData = async () => {
  const allDataInReceiptTbl = await Receipts.getAllData();

  return allDataInReceiptTbl;
};

const getDetailReceipt = async (params) => {
  const allDataInReceiptTbl = await Receipts.getDetail(params);

  return allDataInReceiptTbl;
};

const updateDataByIdFromReceiptTable = async (new_data) => {
  const returnData = await Receipts.updateById(new_data);

  return returnData;
};

const getListReceipt = async (params) => {
  const allDataWithQtyInReceiptTbl = await Receipts.getList(params);

  return allDataWithQtyInReceiptTbl;
};

module.exports = {
  createNewReceipt,
  getReceiptById,
  getChunkById,
  updateReceiptById,
  updateAttributesById,

  getIdReceipt,
  getAllReceiptData,
  getDetailReceipt,
  updateDataByIdFromReceiptTable,
  getListReceipt,
};
