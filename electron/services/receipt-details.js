const ReceiptDetails = require("../models/receipt-detail");

const getAllReceiptDetailsById = async (receipt_id) => {
  const receipt_details = await ReceiptDetails.getAllByReceiptId(receipt_id);

  return receipt_details;
}

const addProductListToReceiptDetail = async ({ receipt_id, products }) => {
  // await ReceiptDetails.clearData();
  await ReceiptDetails.deleteDataById(receipt_id);

  products.map(async (product) => {
    await ReceiptDetails.addDatas({ receipt_id, product });
  });
  const allDataInReceiptDetail = await ReceiptDetails.getAllData();
};

const getAllDataInReceiptDetailTable = async () => {
  const allDataInReceiptDetail = await ReceiptDetails.getAllData();

  return allDataInReceiptDetail;
};

const getDataByReceiptIdFromReceiptDetailTable = async ({ receipt_id }) => {
  const returnData = await ReceiptDetails.getDataByReceiptId({ receipt_id });

  return returnData;
};

module.exports = {
  getAllReceiptDetailsById,
  addProductListToReceiptDetail,
  getAllDataInReceiptDetailTable,
  getDataByReceiptIdFromReceiptDetailTable,
};
