const { ipcMain } = require("electron");
const { RECEIPT } = require("../../event/receipt");
const { getAllReceiptData } = require("../../services/receipts");

const getAllDataProcess = () => {
  ipcMain.handle(RECEIPT.GET_ALL_DATA, (e, args) => {
    return new Promise((resolve, reject) => {
      try {
        getAllReceiptData()
          .then((data) => resolve(data))
          .catch((err) => {
            throw err;
          });
      } catch (err) {
        reject(err);
      }
    });
  });
}

module.exports = getAllDataProcess;
