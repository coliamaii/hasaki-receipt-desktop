const { ipcMain } = require("electron");
const { RECEIPT } = require('../../event/receipt');
const { updateReceiptById } = require('../../services/receipts');

const updateReceiptByIdProcess = async () => {
  ipcMain.handle(RECEIPT.UPDATE_BY_ID, async (e, { id, column, data }) => {
    try {
      await updateReceiptById(id, column, data);
    } catch (err) {
      throw err;
    }
  });
}

module.exports = updateReceiptByIdProcess;