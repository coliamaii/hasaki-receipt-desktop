const { ipcMain } = require("electron");
const { RECEIPT } = require("../../event/receipt");
const { createNewReceipt } = require("../../services/receipts");

const createNewReceiptProcess = () => {
  ipcMain.handle(RECEIPT.CREATE_NEW, (e, new_data) => {
    return new Promise((resolve, reject) => {
      try {
        createNewReceipt(new_data)
          .then((data) => resolve(data))
          .catch((err) => {
            throw err;
          });
      } catch (err) {
        reject(err);
      }
    });
  });
}

module.exports = createNewReceiptProcess;
