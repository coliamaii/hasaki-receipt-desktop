const { ipcMain } = require("electron");
const { getChunkById } = require('../../services/receipts');
const { getAllReceiptDetailsById } = require('../../services/receipt-details');
const { RECEIPT } = require('../../event/receipt');

const getReceiptChunkById = async () => {
  ipcMain.handle(RECEIPT.GET_RECEIPT_CHUNK_BY_ID, async (e, { id, columns }) => {
    try {
      const data = await getChunkById(id, columns);
      return data;
    } catch (err) {
      throw (err);
    }
  });
}

module.exports = getReceiptChunkById;