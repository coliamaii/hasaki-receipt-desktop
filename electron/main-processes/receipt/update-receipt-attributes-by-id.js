const { ipcMain } = require("electron");
const { RECEIPT } = require('../../event/receipt');
const { updateAttributesById } = require('../../services/receipts');

const updateReceiptAttributesByIdProcess = async () => {
  ipcMain.handle(RECEIPT.UPDATE_ATTRIBUTE_BY_ID, async (e, { id, data }) => {
    try {
      await updateAttributesById(id, data);
    } catch (err) {
      throw err;
    }
  });
}

module.exports = updateReceiptAttributesByIdProcess;