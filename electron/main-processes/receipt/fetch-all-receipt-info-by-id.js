const { ipcMain } = require("electron");
const { getReceiptById } = require('../../services/receipts');
const { getAllReceiptDetailsById } = require('../../services/receipt-details');
const { RECEIPT } = require('../../event/receipt');

const fetchAllReceiptInfoByIdProcess = async () => {
  ipcMain.handle(RECEIPT.FETCH_ALL_RECEIPT_INFO_BY_ID, async (e, id) => {
    try {
      const receipt = await getReceiptById(id);
      const receipt_details = await getAllReceiptDetailsById(receipt.id);
      return { receipt, receipt_details };
    } catch (err) {
      throw (err);
    }
  });
}

module.exports = fetchAllReceiptInfoByIdProcess;