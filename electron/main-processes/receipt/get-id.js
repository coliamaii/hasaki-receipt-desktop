const { ipcMain } = require("electron");
const { RECEIPT } = require("../../event/receipt");
const { getIdReceipt } = require("../../services/receipts");

const getIdProcess = () => {
  ipcMain.handle(RECEIPT.GET_DATA_BY_ID, (e, { id }) => {
    return new Promise((resolve, reject) => {
      try {
        getIdReceipt(id)
          .then((data) => resolve(data))
          .catch((err) => {
            throw err;
          });
      } catch (err) {
        reject(err);
      }
    });
  });
}

module.exports = getIdProcess;
