const { ipcMain } = require("electron");
const { RECEIPT } = require("../../event/receipt");
const { getDetailReceipt } = require("../../services/receipts");

const getDetailReceiptProcess = () => {
  ipcMain.handle(RECEIPT.GET_DATA_JOIN_RECEIPT_DETAIL_TABLE, (e, params) => {
    return new Promise((resolve, reject) => {
      try {
        getDetailReceipt(params)
          .then((data) => resolve(data))
          .catch((err) => {
            throw err;
          });
      } catch (err) {
        reject(err);
      }
    });
  });
};

module.exports = getDetailReceiptProcess;
