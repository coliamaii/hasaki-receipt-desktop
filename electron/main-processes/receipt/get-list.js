const { ipcMain } = require("electron");
const { RECEIPT } = require("../../event/receipt");
const { getListReceipt } = require("../../services/receipts");

const getListProcess = (params) => {
  ipcMain.handle(RECEIPT.GET_ALL_DATA_WITH_QTY, (e, params) => {
    return new Promise((resolve, reject) => {
      try {
        getListReceipt(params)
          .then((data) => resolve(data))
          .catch((err) => {
            throw err;
          });
      } catch (err) {
        reject(err);
      }
    });
  });
}

module.exports = getListProcess;
