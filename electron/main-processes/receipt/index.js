const createNewReceiptProcess = require("./create-new");
const getAllDataProcess = require("./get-all-data");
const getListProcess = require("./get-list");
const getDetailReceiptProcess = require("./get-detail");
const getIdProcess = require("./get-id");
const updateDataByIdProcess = require("./update-data-by-id");
const fetchAllReceiptInfoByIdProcess = require('./fetch-all-receipt-info-by-id');
const getReceiptChunkById = require('./get-receipt-chunk-by-id');
const updateReceiptByIdProcess = require('./update-receipt-by-id');
const updateReceiptAttributesByIdProcess=require('./update-receipt-attributes-by-id');

const receiptMainProcess = () => {
  createNewReceiptProcess();
  fetchAllReceiptInfoByIdProcess();
  updateReceiptByIdProcess();
  getReceiptChunkById();
  updateReceiptAttributesByIdProcess(),

  getListProcess();
  getAllDataProcess();
  getDetailReceiptProcess();
  getIdProcess();
  updateDataByIdProcess();
}

module.exports = receiptMainProcess;