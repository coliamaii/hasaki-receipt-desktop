const { ipcMain } = require("electron");
const { RECEIPT } = require("../../event/receipt");
const { updateDataByIdFromReceiptTable } = require("../../services/receipts");

const updateDataByIdProcess = () => {
  ipcMain.handle(RECEIPT.UPDATE_DATA_BY_ID, (e, new_data) => {
    return new Promise((resolve, reject) => {
      try {
        updateDataByIdFromReceiptTable(new_data)
          .then((data) => resolve(data))
          .catch((err) => {
            throw err;
          });
      } catch (err) {
        reject(err);
      }
    });
  });

}

module.exports = updateDataByIdProcess;