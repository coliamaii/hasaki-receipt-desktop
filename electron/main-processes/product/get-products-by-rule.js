const { ipcMain } = require('electron');
const { GET_PRODUCTS_RULE_BY_PRICE } = require('../../event/product');
const { getProductsByRule } = require('../../services/products');

const getProductByRuleProcess = () => {
  ipcMain.handle(GET_PRODUCTS_RULE_BY_PRICE, (e, { items }) => {
    return new Promise((resolve, reject) => {
      try {
        getProductsByRule(items)
          .then(data => resolve(data))
          .catch(err => { throw err });
      } catch (err) {
        reject(err);
      }
    })
  });
}

module.exports = getProductByRuleProcess;