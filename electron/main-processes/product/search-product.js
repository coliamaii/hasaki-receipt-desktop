const { ipcMain } = require('electron');
const { SEARCH_PRODUCTS } = require('../../event/product');
const { searchProducts } = require('../../services/products');

const searchProductProcess = () => {
  ipcMain.handle(SEARCH_PRODUCTS, (e, { search_text }) => {
    return new Promise((resolve, reject) => {
      try {
        searchProducts(search_text)
          .then(data => resolve(data))
          .catch(err => { throw err })
      } catch (err) {
        reject(err);
      }
    })
  });
}

module.exports = searchProductProcess;