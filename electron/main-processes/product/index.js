const getProductByRuleProcess = require("./get-products-by-rule");
const searchProductProcess = require("./search-product");

const productMainProcess = () => {
  getProductByRuleProcess();
  searchProductProcess();
}

module.exports = productMainProcess;