const productMainProcess = require("./product");
const appMainProcess = require('./app');
const receiptMainProcess = require("./receipt");
const receiptDetailMainProcess = require("./receipt-detail");

const rootMainProcess = () => {
  appMainProcess();
  productMainProcess();
  receiptMainProcess();
  receiptDetailMainProcess();
}

module.exports = rootMainProcess;
