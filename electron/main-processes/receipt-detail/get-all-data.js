const { ipcMain } = require("electron");
const { RECEIPT_DETAIL } = require("../../event/receipt-detail");
const {
  getAllDataInReceiptDetailTable,
} = require("../../services/receipt-details");

const getAllDataProcess = () => {
  ipcMain.handle(RECEIPT_DETAIL.GET_ALL_DATA, (e, arg) => {
    return new Promise((resolve, reject) => {
      try {
        getAllDataInReceiptDetailTable()
          .then((data) => resolve(data))
          .catch((err) => {
            throw err;
          });
      } catch (err) {
        reject(err);
      }
    });
  });

}

module.exports = getAllDataProcess;