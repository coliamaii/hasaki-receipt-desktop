const { ipcMain } = require("electron");
const { RECEIPT_DETAIL } = require("../../event/receipt-detail");
const {
  addProductListToReceiptDetail,
} = require("../../services/receipt-details");

const addReceiptListProcess = () => {
  ipcMain.handle(
    RECEIPT_DETAIL.ADD_NEW_DATA,
    (e, { receipt_id, products }) => {
      return new Promise((resolve, reject) => {
        try {
          addProductListToReceiptDetail({ receipt_id, products })
            .then((data) => resolve(data))
            .catch((err) => {
              throw err;
            });
        } catch (err) {
          reject(err);
        }
      });
    }
  );
}

module.exports = addReceiptListProcess;
