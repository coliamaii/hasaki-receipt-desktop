const { ipcMain } = require("electron");
const { RECEIPT_DETAIL } = require("../../event/receipt-detail");
const {
  getDataByReceiptIdFromReceiptDetailTable,
} = require("../../services/receipt-details");

module.exports = ipcMain.handle(
  RECEIPT_DETAIL.GET_DATA_BY_RECEIPT_ID,
  (e, { receipt_id }) => {
    return new Promise((resolve, reject) => {
      try {
        getDataByReceiptIdFromReceiptDetailTable({ receipt_id })
          .then((data) => resolve(data))
          .catch((err) => {
            throw err;
          });
      } catch (err) {
        reject(err);
      }
    });
  }
);
