const addReceiptListProcess = require("./add-receipt-list");
const getAllDataProcess = require("./get-all-data")

const receiptDetailMainProcess = () => {
  addReceiptListProcess();
  getAllDataProcess();
}

module.exports = receiptDetailMainProcess;
