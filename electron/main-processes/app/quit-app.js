const { app, ipcMain } = require('electron');
const { QUIT_APP } = require('../../event/app');

const quitAppProcess = () => {
  ipcMain.on(QUIT_APP, (e, arg) => {
    app.quit();
  });
}

module.exports = quitAppProcess;