const checkUpdateProcess = require("./check-update");
const getVersionProcess = require("./get-version");
const quitAndUpdateProcess = require("./quit-and-update");
const quitAppProcess = require("./quit-app");

const appMainProcess = () => {
  checkUpdateProcess();
  getVersionProcess();
  quitAndUpdateProcess();
  quitAppProcess();
}

module.exports = appMainProcess;