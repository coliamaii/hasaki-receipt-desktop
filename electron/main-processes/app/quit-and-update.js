const { app, ipcMain } = require('electron');
const { autoUpdater } = require('electron-updater');
const { QUIT_AND_UPDATE } = require('../../event/app');

const quitAndUpdateProcess = () => {
  ipcMain.on(QUIT_AND_UPDATE, (e, arg) => {
    autoUpdater.quitAndInstall();
  });
}

module.exports = quitAndUpdateProcess;