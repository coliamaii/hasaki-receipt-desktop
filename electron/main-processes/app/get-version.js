const { app, ipcMain } = require('electron');
const { GET_VERSION } = require('../../event/app');

const getVersionProcess = () => {
  ipcMain.handle(GET_VERSION, (e, arg) => {
    return app.getVersion();
  });
}

module.exports = getVersionProcess;