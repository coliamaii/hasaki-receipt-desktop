const { app, ipcMain } = require('electron');
const { autoUpdater } = require('electron-updater');
const { CHECK_UPDATE } = require('../../event/app');

const checkUpdateProcess = () => {
  ipcMain.handle(CHECK_UPDATE, (e, arg) => {
    autoUpdater.checkForUpdates();
  });
}

module.exports = checkUpdateProcess;