const { ipcMain, net } = require("electron");
const { BIDV_PROXY_CHECK_PAYMENT, BIDV_RESPONSE } = require("../../event/app");

module.exports = ipcMain.handle(BIDV_PROXY_CHECK_PAYMENT, async (e, { url }) => {
  const request = net.request({
    method: 'GET',
    url,
  });

  request.on('response', (response) => {
    console.log(`STATUS: ${response.statusCode}`);
    console.log(`HEADERS: ${JSON.stringify(response.headers)}`);

    response.on('data', (data) => {
      console.log(`BODY: ${data}`);
      win.webContents.send(BIDV_RESPONSE, data);
    });
  });

  request.on('finish', () => {
    console.log('Request is Finished');
  });
  request.on('abort', () => {
    console.log('Request is Aborted')
  });
  request.on('error', (error) => {
    console.log(`ERROR: ${JSON.stringify(error)}`)
  });
  request.on('close', (error) => {
    console.log('Last Transaction has occured')
  });
  request.setHeader('Content-Type', 'application/json');
  request.end();
});