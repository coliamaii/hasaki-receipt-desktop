const RECEIPT_DETAIL = {
  ADD_NEW_DATA: "add-new-data-into-receipt-detail-table",
  GET_ALL_DATA: "get-all-data-from-receipt-detail-table",
  GET_DATA_BY_RECEIPT_ID: "get-data-by-receipt-id-from-receipt-detail-table"
}

module.exports = {
  RECEIPT_DETAIL,
};
