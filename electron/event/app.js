module.exports = {
  QUIT_APP: "quit-app",
  GET_VERSION: 'get-version',
  CHECK_UPDATE: 'check-update',
  UPDATE_MESSAGE: 'update-message',
  QUIT_AND_UPDATE: 'quit-and-update',
  BIDV_PROXY_CHECK_PAYMENT: 'bidv-proxy-check-payment',
  BIDV_RESPONSE: 'bidv-response',
};
