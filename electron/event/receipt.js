const RECEIPT = {
  CREATE_NEW: "create-new-receipt",
  FETCH_ALL_RECEIPT_INFO_BY_ID: 'fetch-all-receipt-info-by-id',
  UPDATE_BY_ID: 'update-by-id',
  GET_RECEIPT_CHUNK_BY_ID: 'get-receipt-chunk-by-id',
  UPDATE_ATTRIBUTE_BY_ID: 'update-receipt-attribute-by-id',

  GET_DATA_BY_ID: "get-data-by-id-from-receipt-table",
  UPDATE_DATA_BY_ID: "update-data-by-id-from-receipt-table",
  GET_ALL_DATA: "get-all-data-from-receipt-table",
  GET_DATA_JOIN_RECEIPT_DETAIL_TABLE: "get-data-join-receipt-detail-table",
  GET_ALL_DATA_WITH_QTY: "get-data-with-qty-from-receipt-table",
};
module.exports = {
  RECEIPT,
};
