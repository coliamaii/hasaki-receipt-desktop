import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import auth from "./scenes/Auth/reducer";
import { reducer as formReducer } from "redux-form";
import api from "./share/api/reducer";
import mode from "./share/mode/reducer";
import websocket from './share/socket/reducer'

const rootReducer = () =>
  combineReducers({
    form: formReducer,
    //router: connectRouter(history),
    auth,
    api,
    mode,
    websocket,
  });

export default rootReducer;
