import { useBarcode } from "@createnextapp/react-barcode";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Row, Space } from "antd";
import logo_mini from "assets/images/logo_mini.png";
import dayjs from "dayjs";
import React from "react";
import { useSelector } from "react-redux";
import { Hr } from "../styled";
import ProductTable from "./ProductTable";
import QRCode_App_Img from "assets/images/QRCode_App.png";
import { BarcodeImg } from "../styled";

function PosBill({ productTblColumns }) {
  const { receipt, customer } = useSelector(
    (state) => state.api.profile.receipt.detail
  );

  const { receipt_code, receipt_user_id } = receipt
    ? receipt
    : { receipt_code: 0, receipt_user_id: 0 };

  const { inputRef } = useBarcode({
    value: { receipt_code },
    options: {
      displayValue: false,
      height: "40%",
    },
  });

  return (
    <Row>
      <Col xs={24} className="text-center no-padding">
        <img style={{ width: 50 }} src={logo_mini} />
      </Col>

      <Col xs={24} className="text-center">
        <span style={{ fontSize: 10 }}>
          <strong>CÔNG TY TNHH HASAKI BEAUTY &amp; S.P.A</strong>
          <br />
          <span>555 Đường 3 tháng 2, P.8, Q.10, HCM (MST: 0313612829)</span>
          <br />
          <span style={{ fontSize: 18 }}>hasaki.vn - NowFree 2h</span>
          <br />
          <strong>
            Khiếu nại - Góp ý: <span style={{ fontSize: 18 }}>1800 6310</span>
          </strong>{" "}
          -
        </span>
        <hr style={{ borderBottom: "2px dashed #333" }} />
      </Col>

      <Col xs={24} className="text-center">
        <h4>
          <strong>HÓA ĐƠN</strong>
        </h4>
      </Col>
      <Col xs={24} className="text-center">
        {customer && <>KH: {customer.customer_name ? customer.customer_name : "GUEST"}</>}
        <div className="text-center">
          <BarcodeImg ref={inputRef} />
        </div>
        <div className="text-center">
          {receipt_code} - {dayjs().format("DD/MM/YYYY HH:mm")} -{" "}
          {receipt_user_id}
        </div>
      </Col>

      <Col xs={24}>
        <ProductTable columns={productTblColumns} />
      </Col>

      <Col
        xs={24}
        className="print-line-height"
        style={{
          marginTop: "0px !important",
          marginBottom: "2px !important",
          fontSize: "85%",
        }}
      >
        Yêu cầu xuất VAT: https://go.hasaki.vn/EC9FD7868C
        <br />
        (**) Sản phẩm khuyến mãi không được tích điểm &amp; đổi trả
      </Col>

      <Col xs={24}>
        <Hr />
        <div className="text-left" style={{ fontSize: "95%" }}>
          <img
            className="pull-left"
            src={QRCode_App_Img}
            width="75"
            height="75"
          />
          <strong>Cài App Hasaki</strong>
          <br />
          <Space>
            <FontAwesomeIcon icon={faCheck} />
            NowFree2H miễn phí giao 2h Hà Nội-HCM
          </Space>

          <br />
          <Space>
            <FontAwesomeIcon icon={faCheck} />
            Miễn phí giao hàng từ 90k
          </Space>
          <br />
          <Space>
            <FontAwesomeIcon icon={faCheck} />
            Giảm 5% đơn hàng đầu tiên trên APP
          </Space>
          <br />
        </div>
      </Col>
      <Col xs={24}>
        <Hr />
        <p className="text-center print-line-height">
          <small>
            Cám ơn quý khách đã mua hàng tại HASAKI! Điểm tích lũy: 225,123
            <br />
            Vui lòng giữ hóa đơn để đổi trả trong vòng 14 ngày. Xem thêm quy
            định đổi trả hàng tại{" "}
            <span style={{ fontSize: 12 }}>hasaki.vn</span>
          </small>
        </p>
      </Col>
    </Row>
  );
}

PosBill.propTypes = {};

export default PosBill;
