import React from "react";
import PropTypes from "prop-types";
import { Col, Row } from "antd";
import formatMoney from "utils/money";
import { useSelector } from "react-redux";

function PaymentMethod({ payment_method, payment_amount }) {
  const { arrPaymentReceipts } = useSelector(
    (state) => state.api.profile.receipt.detail
  );

  const { payment_methods } = arrPaymentReceipts
    ? arrPaymentReceipts
    : { payment_methods: [] };

  payment_methods.map(({ key, name }) => {
    if (payment_method == key) {
      return (
        <Row>
          <Col xs={4} className="text-right">
            {name}:
          </Col>
          <Col xs={20} className="text-right">
            {formatMoney(payment_amount)}
          </Col>
        </Row>
      );
    }
  });
}

PaymentMethod.propTypes = {};

export default PaymentMethod;
