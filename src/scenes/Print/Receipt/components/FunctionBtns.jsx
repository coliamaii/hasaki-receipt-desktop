import { faFile, faPrint } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, Row, Space } from "antd";
import React from "react";
import { useSelector } from "react-redux";

function FunctionBtns({ handleClickNewBtn, handleClickPrintBtn }) {
  const { receipt } = useSelector((state) => state.api.profile.receipt.detail);

  const { receipt_code } = receipt ? receipt : { receipt_code: 0 };

  return (
    <Row>
      <Col xs={24}>
        <div className="box box-default">
          <div className="box-header with-border">
            <h3 className="box-title receipt_code">#{receipt_code}</h3>
          </div>

          <div className="box-body">
            <Space size="large">
              <Button
                type="primary"
                size="large"
                onClick={() => handleClickPrintBtn()}
                icon={<FontAwesomeIcon icon={faPrint} />}
              >
                &nbsp; Print
              </Button>
              <Button
                size="large"
                onClick={() => handleClickNewBtn()}
                icon={<FontAwesomeIcon icon={faFile} />}
              >
                &nbsp; New
              </Button>
            </Space>
          </div>
        </div>
      </Col>
    </Row>
  );
}

FunctionBtns.propTypes = {};

export default FunctionBtns;
