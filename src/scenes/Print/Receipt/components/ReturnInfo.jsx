import { css } from "@emotion/css";
import { Col, Row, Table } from "antd";
import dayjs from "dayjs";
import React from "react";
import { useSelector } from "react-redux";

function ReturnInfo(props) {
  const { arrReturn } = useSelector(
    (state) => state.api.profile.receipt.detail
  );

  const returnInfoColumns = [
    {
      title: "SKU",
      width: "5%",
      key: "0",
      align: "left",
      render: (text, { return_item_sku }, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{return_item_sku}</>,
        };
      },
    },
    {
      title: "Qty",
      key: "1",
      align: "left",
      render: ({ return_item_qty }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{return_item_qty}</>,
        };
      },
    },
    {
      title: "Date",
      key: "2",
      align: "left",
      render: ({ return_ctime }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>{dayjs(return_ctime * 1000).format("DD/MM/YYYY HH:mm")}</>
          ),
        };
      },
    },
  ];
  const tableCSS = css({
    "& table": {
      borderCollapse: "collapse",
    },
    "& thead > tr > th": {
      backgroundColor: "#E2E4EA !important",
      fontWeight: "bold",
      textAlign: "center",
    },

    "& .ant-table-placeholder": {
      display: "none",
    },

    "& thead > tr > th, & tbody > tr > td, & tfoot > tr > td": {
      padding: 8,
    },
  });

  return (
    <Row>
      <Col xs={24}>
        <div className="box box-default">
          <div className="box-header with-border">
            <h3 className="box-title">Return info</h3>
          </div>
          <div className="box-body">
            <Row>
              <Col xs={24}>
                <Table
                  bordered
                  className={tableCSS}
                  columns={returnInfoColumns}
                  dataSource={arrReturn}
                  pagination={false}
                />
              </Col>
            </Row>
          </div>
        </div>
      </Col>
    </Row>
  );
}

ReturnInfo.propTypes = {};

export default ReturnInfo;
