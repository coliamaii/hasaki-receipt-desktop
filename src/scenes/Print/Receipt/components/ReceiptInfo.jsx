import { Col, Row } from "antd";
import dayjs from "dayjs";
import React from "react";
import { useSelector } from "react-redux";

function ReceiptInfo(props) {
  const { stores } = useSelector((state) => state.auth.info);
  const { receipt, user } = useSelector(
    (state) => state.api.profile.receipt.detail
  );

  const { receipt_cdate, receipt_store_id } = receipt
    ? receipt
    : { receipt_cdate: 0, receipt_store_id: 0 };

  return (
    <Row>
      <Col xs={24}>
        <div className="box box-default">
          <div className="box-header with-border">
            <h3 className="box-title">Receipt info</h3>
          </div>
          <div className="box-body">
            <Row>
              <Col xs={24}>
                <p>
                  Created At:{" "}
                  <strong>
                    {dayjs(receipt_cdate * 1000).format("DD/MM/YYYY HH:mm")}
                  </strong>
                </p>
                <p>
                  Store:{" "}
                  <strong>
                    {stores.map(({ store_id, store_name }) => {
                      if (store_id === receipt_store_id) {
                        return store_name;
                      }
                    })}
                  </strong>
                </p>
                <p>
                  Cashier: <strong>{user?.name}</strong>
                </p>
                <p>
                  {receipt_cdate > dayjs().unix() - 15 * 86400 ? (
                    <strong className="text-red">Cho phép đổi trả hàng</strong>
                  ) : (
                    <strong className="text-red" style={{ fontSize: 20 }}>
                      Quá hạn đổi trả hàng
                    </strong>
                  )}
                </p>
              </Col>
            </Row>
          </div>
        </div>
      </Col>
    </Row>
  );
}

ReceiptInfo.propTypes = {};

export default ReceiptInfo;
