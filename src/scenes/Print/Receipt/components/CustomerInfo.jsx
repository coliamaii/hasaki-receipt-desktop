import { Col, Row } from "antd";
import React from "react";
import { useSelector } from "react-redux";
import number_format from "utils/money";

function CustomerInfo(props) {
  const {
    customer_name,
    customer_phone,
    customer_email,
    customer_order_point,
    customer_receipt_point,
    customer_init_order_point,
    customer_init_receipt_point,
    used_point,
  } = useSelector((state) => state.api.profile.receipt.detail.customer);

  return (
    <Row>
      <Col xs={24}>
        <div className="box box-default">
          <div className="box-header with-border">
            <h3 className="box-title">Customer Info</h3>
          </div>
          <div className="box-body">
            <Row>
              <Col xs={24}>
                <p>Name: {customer_name}</p>
                <p>Phone: {customer_phone}</p>
                <p>Email: {customer_email}</p>
                <p>
                  Point:{" "}
                  {number_format(
                    Math.floor(
                      (customer_order_point +
                        customer_receipt_point +
                        customer_init_order_point +
                        customer_init_receipt_point -
                        used_point) /
                        1000
                    )
                  )}
                </p>
              </Col>
            </Row>
          </div>
        </div>
      </Col>
    </Row>
  );
}

CustomerInfo.propTypes = {};

export default CustomerInfo;
