import { css } from "@emotion/css";
import { Col, Row, Table } from "antd";
import _map from "lodash/map";
import React from "react";
import { useSelector } from "react-redux";
import formatMoney from "utils/money";

function trans(values) {
  let result = values;
  switch (values) {
    case 1:
      result = "Tiền mặt";
      break;
    case 2:
      result = "Chuyển khoảng ngân hàng";
      break;
    case 3:
      result = "Thẻ";
      break;
    case 4:
      result = "VOUCHER";
      break;
    case 5:
      result = "Thẻ quà tặng";
      break;
    case 6:
      result = "BALANCE";
      break;
    case 7:
      result = "Kiểm tra quà tặng";
      break;
    case 8:
      result = "Qùa tặng di động";
      break;
    case 9:
      result = "Kế toán";
      break;
    default:
      break;
  }

  return result;
}

function ProductTable({ columns }) {
  const { receipt } = useSelector((state) => state.api.profile.receipt.detail);

  const {
    receipt_subtotal,
    receipt_discount,
    items: productTblDataSource,
    receipt_total,
  } = receipt
    ? receipt
    : { receipt_subtotal: 0, receipt_discount: 0, items: [], receipt_total: 0 };

  const { arrPaymentReceipts } = useSelector(
    (state) => state.api.profile.receipt.detail
  );

  const tableCSS = css({
    "& table": {
      borderCollapse: "collapse",
    },
    "& thead > tr > th": {
      fontWeight: "bold",
      color: "black",
      textAlign: "center",
      backgroundColor: "transparent",
    },
    "& .ant-table-placeholder": {
      display: "none",
      backgroundColor: "transparent",
    },
    "& thead > tr > th, & tbody > tr > td, & tfoot > tr > td": {
      borderBottom: "2px dashed #333",
      padding: 8,
    },
    "& tbody > tr:hover > td": {
      backgroundColor: "transparent !important",
    },
    "& .ant-table-footer": {
      padding: 8,
      backgroundColor: "transparent",
    },
  });

  let locale = {
    emptyText: " ",
  };

  return (
    <Table
      locale={locale}
      className={tableCSS}
      dataSource={productTblDataSource}
      columns={columns}
      pagination={false}
      footer={(pageData) => {
        let sumItem = 0;

        pageData.map(({ receiptdt_qty }) => {
          sumItem += receiptdt_qty;
        });

        return (
          <>
            <Row>
              <Col xs={12}>
                <strong>Thành tiền: </strong>
              </Col>
              <Col xs={12} className="text-right">
                <strong>{formatMoney(receipt_subtotal)}</strong>
              </Col>
            </Row>
            <Row>
              <Col xs={12}>
                <strong>Giảm giá: </strong>
              </Col>
              <Col xs={12} className="text-right">
                <strong>{formatMoney(receipt_discount)}</strong>
              </Col>
            </Row>
            <Row>
              <Col xs={12}>
                <strong>Tổng thanh toán:</strong>{" "}
              </Col>
              <Col xs={12} className="text-right">
                <strong>{formatMoney(receipt_total)}</strong>
              </Col>
            </Row>
            {Object.keys(arrPaymentReceipts).length > 0 && arrPaymentReceipts.rows.length > 0 ? (
              _map(
                arrPaymentReceipts.rows,
                ({ payment_method, payment_amount }) => {
                  return (
                    <>
                      {arrPaymentReceipts.payment_methods.map(
                        ({ key, name }) => {
                          if (payment_method == key && payment_method) {
                            return (
                              <Row key={payment_method}>
                                <Col xs={4} className="text-right">
                                  {trans(key)}:
                                </Col>
                                <Col xs={20} className="text-right">
                                  {formatMoney(payment_amount)}
                                </Col>
                              </Row>
                            );
                          }
                        }
                      )}
                    </>
                  );
                }
              )
            ) : (
              <Row>
                <Col xs={4} className="text-right">
                  {trans(1)}:
                </Col>
                <Col xs={20} className="text-right">
                  {formatMoney(receipt_total)}
                </Col>
              </Row>
            )}
            <Row>
              <Col xs={24} className="text-center">
                <span style={{ fontSize: 40, lineHeight: 1.5 }}>{sumItem}</span>
              </Col>
            </Row>
          </>
        );
      }}
    />
  );
}

ProductTable.propTypes = {};

export default ProductTable;
