import { Col, Row } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { createEmptyReceiptRequestAction } from "share/api/Profile/Receipt/Empty/actions";
import { getDetailReceiptSuccessAction } from "share/api/Profile/Receipt/GetDetail/actions";
import formatMoney from "utils/money";
import CustomerInfo from "./components/CustomerInfo";
import FunctionBtns from "./components/FunctionBtns";
import PosBill from "./components/PosBill";
import ReceiptInfo from "./components/ReceiptInfo";
import ReturnInfo from "./components/ReturnInfo";

function PrintReceipt(props) {
  const dispatch = useDispatch();
  const history = useHistory();
  const [isPrint, setIsPrint] = useState(false);

  const { receipt, products, customer, arrReturn } = useSelector(
    (state) => state.api.profile.receipt.detail
  );

  const productTblColumns = [
    {
      title: "STT",
      width: "5%",
      key: "0",
      align: "center",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{index + 1}</>,
        };
      },
    },
    {
      title: "Sản phẩm",
      key: "1",
      render: ({
        receiptdt_price,
        receiptdt_qty,
        receiptdt_sku,
        receiptdt_discount,
      }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {products?.map(({ product_sku, product_name }) => {
                if (product_sku == receiptdt_sku) {
                  return product_name;
                }
              })}{" "}
              - {receiptdt_sku} {receiptdt_discount > 0 && "**"}
              <br />( Giá: {formatMoney(receiptdt_price)}{" "}
              {receiptdt_discount > 0 && (
                <>
                  - Giảm:{" "}
                  {formatMoney(
                    (receiptdt_price - receiptdt_discount) * receiptdt_qty
                  )}
                </>
              )}{" "}
              x SL: {receiptdt_qty} )
            </>
          ),
        };
      },
    },
    {
      title: "Giá",
      key: "2",
      width: "10%",
      align: "right",
      render: ({ receiptdt_price, receiptdt_discount, receiptdt_qty }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {formatMoney(
                (receiptdt_price - receiptdt_discount) * receiptdt_qty
              )}
            </>
          ),
        };
      },
    },
  ];

  const handleClickPrintBtn = async () => {
    await setIsPrint(true);
    await window.print();
    await setIsPrint(false);
  };

  const handleClickNewBtn = () => {
    dispatch(createEmptyReceiptRequestAction(history.push, dispatch));
  };

  useEffect(() => {
    if (!receipt) {
      const receipt_detail = localStorage.getItem("receipt_detail");
      if (receipt_detail) {
        dispatch(
          getDetailReceiptSuccessAction({ detail: JSON.parse(receipt_detail) })
        );
      } else {
        history.push("/");
      }
    }
  }, [receipt]);

  return (
    <section className="content">
      <Row gutter={[32]}>
        <Col xs={isPrint ? 24 : 16}>
          <div className={isPrint ? "" : "box"}>
            <div className="box-body no-padding">
              <PosBill productTblColumns={productTblColumns} />
            </div>
          </div>
        </Col>
        {isPrint ? (
          ""
        ) : (
          <Col xs={8}>
            <FunctionBtns
              handleClickNewBtn={handleClickNewBtn}
              handleClickPrintBtn={handleClickPrintBtn}
            />

            {customer && <CustomerInfo />}

            <ReceiptInfo />

            {arrReturn && arrReturn.length > 0 && <ReturnInfo />}
          </Col>
        )}
      </Row>
    </section>
  );
}

export default PrintReceipt;
