import Styled from "styled-components";

export const Hr = Styled.hr`
  margin-top: 2px !important;
  margin-bottom: 0px !important;
  border-bottom: 2px dashed #333;
`;

export const BarcodeImg = Styled.img`
  @media (max-width: 767px) {
    width: 100%;
  }
`;
