import PrintReceipt from "./Receipt";
import PrintReconcile from "./Reconcile";

export default [
  {
    key: "print_reconcile",
    name: "Print Reconcile",
    component: PrintReconcile,
    path: "/print/reconcile",
    hide: true,
    template: "print",
  },
  {
    key: "print_pos",
    name: "Print POST",
    component: PrintReceipt,
    path: "/print/receipt",
    hide: true,
    template: "print",
  },
];
