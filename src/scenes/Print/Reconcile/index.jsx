import { useBarcode } from "@createnextapp/react-barcode";
import { faFile, faPrint } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, Row, Space } from "antd";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { getDetailReconcileSuccessAction } from "share/api/Profile/Reconcile/GetDetail/actions";
import formatMoney from "utils/money";
import CashPrintTable from "./components/CashPrintTable";
import EsteemGiftPrintTable from "./components/EsteemGiftPrintTable";
import VoucherPrintTable from "./components/VoucherPrintTable";

function PrintReconcile(props) {
  const dispatch = useDispatch();
  const history = useHistory();

  const [isPrint, setIsPrint] = useState(false);

  const { detail } = useSelector((state) => state.api.profile.reconcile);

  const {
    reconcile_begin_balance,
    reconcile_start_time,
    reconcile_total_card_checkout,
    reconcile_total_count,
    reconcile_note,
    reconcile_difference,
    reconcile_total_cash,
    reconcile_total_return,
    reconcile_code,
    total_sodexo_code,
    total_gotit_code,
    reconcile_end_time,
    total_cheque,
    reconcile_user,
    properties,
    total_voucher,
    total_gift_card,
  } = detail;

  const { voucher, esteem_gift } = properties
    ? properties
    : {
        voucher: {
          five_hundreds: 0,
          two_hundreds: 0,
          one_hundreds: 0,
          fifties: 0,
          twenties: 0,
          tens: 0,
          ones: 0,

          total_count: 0,
        },
        esteem_gift: {
          five_hundreds: 0,
          two_hundreds: 0,
          one_hundreds: 0,
          fifties: 0,
          twenties: 0,
          tens: 0,
          ones: 0,

          total_count: 0,
        },
      };

  const { inputRef } = useBarcode({
    value: reconcile_code,
    options: {
      displayValue: false,
    },
  });

  const printBtn_handleClick = async () => {
    await setIsPrint(true);
    await window.print();
    await setIsPrint(false);
  };

  const newBtn_handleClick = () => {
    history.push("/report-profile/reconcile");
  };

  useEffect(() => {
    if (Object.keys(detail).length < 1) {
      const reconcile = localStorage.getItem("reconcile");
      if (reconcile) {
        dispatch(getDetailReconcileSuccessAction(JSON.parse(reconcile)));
      } else {
        history.push("/report-profile/reconcile");
      }
    }
  }, [detail]);

  return (
    <div className="container-fluid">
      <Row>
        <Col xs={isPrint ? 24 : 16}>
          <Row>
            <Col xs={24} className="text-center">
              <h4>
                <strong>Reconciliation</strong>
              </h4>
            </Col>
            <Col xs={24} className="text-center">
              Ngày:{" "}
              {moment(reconcile_start_time * 1000).format("YYYY-MM-DD HH:mm")} -{" "}
              {moment(reconcile_end_time * 1000).format("HH:mm")}
              <br />
              Code: {reconcile_code}
              <br />
              User: {reconcile_user?.name}
            </Col>
          </Row>

          <CashPrintTable />

          <VoucherPrintTable />

          <EsteemGiftPrintTable />

          <Row>
            <p>&nbsp;</p>
          </Row>

          <Row>
            <Col xs={24}>
              <strong>Tổng Reconcile: </strong>
              <span className="pull-right">
                <strong>
                  {formatMoney(
                    reconcile_total_count +
                      reconcile_total_card_checkout +
                      voucher?.total_count +
                      esteem_gift?.total_count
                  )}
                </strong>
              </span>
            </Col>
          </Row>

          <Row>
            <Col xs={24}>
              &nbsp; &nbsp; + Tổng Cash:
              <span className="pull-right">
                {formatMoney(reconcile_total_count)}
              </span>
            </Col>

            <Col xs={24}>
              &nbsp; &nbsp; + Tổng Card:
              <span className="pull-right">
                {formatMoney(reconcile_total_card_checkout)}
              </span>
            </Col>

            <Col xs={24}>
              &nbsp; &nbsp; + Tổng Voucher:
              <span className="pull-right">{formatMoney(total_voucher)}</span>
            </Col>

            <Col xs={24}>
              &nbsp; &nbsp; + Tổng Esteem Gift:
              <span className="pull-right">{formatMoney(total_gift_card)}</span>
            </Col>

            <Col xs={24}>
              &nbsp; &nbsp; + Tổng Sodexo:
              <span className="pull-right">
                {formatMoney(total_sodexo_code ? total_sodexo_code : 0)}
              </span>
            </Col>

            <Col xs={24}>
              &nbsp; &nbsp; + Tổng Goit:
              <span className="pull-right">
                {formatMoney(total_gotit_code ? total_gotit_code : 0)}
              </span>
            </Col>
          </Row>

          <Row>
            <Col xs={24}>
              <strong>Tổng tiền hoàn:</strong>
              <span className="pull-right">
                <strong>{formatMoney(reconcile_total_return)}</strong>
              </span>
            </Col>
          </Row>

          <Row>
            <Col xs={24}>
              <strong>Tổng Cash:</strong>
              <span className="pull-right">
                <strong>{formatMoney(reconcile_total_cash)}</strong>
              </span>
            </Col>
          </Row>

          <Row>
            <Col xs={24}>
              <strong>Số dư bắt đầu:</strong>
              <span className="pull-right">
                <strong>{formatMoney(reconcile_begin_balance)}</strong>
              </span>
            </Col>
          </Row>

          <Row>
            <Col xs={24}>
              <strong>Chênh lệch:</strong>
              <pull className=" pull-right">
                <strong>{formatMoney(reconcile_difference)}</strong>
              </pull>
            </Col>
          </Row>

          <hr />
          <Row>
            <Col xs={24}>
              <div className="form-group">
                <label>Ghi chú:&nbsp;</label>
                {reconcile_note}
              </div>
            </Col>
          </Row>

          <Row className="text-center">
            <Col xs={24}>
              <canvas
                ref={inputRef}
                style={{
                  height: 50,
                  width: 250,
                  verticalAlign: "middle",
                }}
              />
            </Col>
          </Row>

          <p>&nbsp;</p>
          <div className="row text-center">
            <Col xs={24}>Ngày In {moment().format("YYYY-MM-DD HH:mm")}</Col>
          </div>
        </Col>
        {isPrint ? (
          ""
        ) : (
          <Col xs={8}>
            <Row>
              <Col xs={24}>
                <div className="box box-default">
                  <div className="box-header with-border">
                    <h3 className="box-title">#{reconcile_code}</h3>
                  </div>

                  <div className="box-body">
                    <Space size="large">
                      <Button
                        type="primary"
                        size="large"
                        onClick={() => printBtn_handleClick()}
                        icon={<FontAwesomeIcon icon={faPrint} />}
                        loading={isPrint}
                      >
                        &nbsp; Print
                      </Button>
                      <Button
                        size="large"
                        onClick={() => newBtn_handleClick()}
                        icon={<FontAwesomeIcon icon={faFile} />}
                      >
                        &nbsp; New
                      </Button>
                    </Space>
                  </div>
                </div>
              </Col>
            </Row>
          </Col>
        )}
      </Row>
    </div>
  );
}

export default PrintReconcile;
