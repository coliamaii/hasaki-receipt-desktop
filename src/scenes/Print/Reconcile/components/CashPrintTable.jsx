import { css } from "@emotion/css";
import { Col, Row, Table } from "antd";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import formatMoney from "utils/money";

function CashPrintTable(props) {
  const [totalCardPayment, setTotalCardPayment] = useState(0);
  const {
    reconcile_ones,
    reconcile_tens,
    reconcile_fifties,
    reconcile_twenties,
    reconcile_one_hundreds,
    reconcile_two_hundreds,
    reconcile_five_hundreds,
    reconcile_total_count,
    reconcile_status,
    reconcile_total_card_payment,
  } = useSelector((state) => state.api.profile.reconcile.detail);

  const cashTbl_dataSource = [
    {
      key: "0",
      stt: 1,
      value: 500000,
      amount: reconcile_five_hundreds,
    },
    {
      key: "1",
      stt: 2,
      value: 200000,
      amount: reconcile_two_hundreds,
    },
    {
      key: "2",
      stt: 3,
      value: 100000,
      amount: reconcile_one_hundreds,
    },
    {
      key: "3",
      stt: 4,
      value: 50000,
      amount: reconcile_fifties,
    },
    {
      key: "4",
      stt: 5,
      value: 20000,
      amount: reconcile_twenties,
    },
    {
      key: "5",
      stt: 6,
      value: 10000,
      amount: reconcile_tens,
    },
    {
      key: "6",
      stt: 7,
      value: 1000,
      amount: reconcile_ones,
    },
  ];

  const cashTbl_columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "0",
      align: "center",
      render: (stt) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{stt}</>,
        };
      },
    },
    {
      title: "Nội dung",
      dataIndex: "content",
      key: "1",
      render: (text, record) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {formatMoney(record.value)} x {record.amount}
            </>
          ),
        };
      },
    },
    {
      title: "Giá",
      dataIndex: "price",
      key: "2",
      width: "10%",
      align: "right",
      render: (text, record) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{formatMoney(record.value * record.amount)}</>,
        };
      },
    },
  ];

  const tableCSS = css({
    "& table": {
      borderCollapse: "collapse",
    },
    "& thead > tr > th": {
      fontWeight: "bold",
      color: "black",
      textAlign: "center",
    },
    "& .ant-table-placeholder": {
      display: "none",
    },
    "& thead > tr > th, & tbody > tr > td": {
      borderBottom: "2px solid #ccc",
      padding: 8,
    },
    "& tfoot > tr> td": {
      padding: 8,
    },
  });

  let locale = {
    emptyText: " ",
  };

  return (
    <Row>
      <Col xs={24} style={{ color: "#777", fontWeight: 700 }}>
        <strong>Cash</strong>
      </Col>
      <Col xs={24}>
        <Table
          locale={locale}
          className={tableCSS}
          dataSource={cashTbl_dataSource}
          columns={cashTbl_columns}
          pagination={false}
          summary={(pageData) => {
            return (
              <Table.Summary.Row>
                <Table.Summary.Cell />
                <Table.Summary.Cell className="text-center">
                  Tổng:
                </Table.Summary.Cell>
                <Table.Summary.Cell className="text-right">
                  <strong>{formatMoney(reconcile_total_count)}</strong>
                </Table.Summary.Cell>
              </Table.Summary.Row>
            );
          }}
        />
      </Col>
    </Row>
  );
}

CashPrintTable.propTypes = {};

export default CashPrintTable;
