import { css } from "@emotion/css";
import { Col, Row, Table } from "antd";
import React from "react";
import { useSelector } from "react-redux";
import formatMoney from "utils/money";

function VoucherPrintTable(props) {
  const { detail } = useSelector((state) => state.api.profile.reconcile);

  const {
    properties: { voucher },
  } =
    Object.keys(detail).length > 0
      ? detail
      : {
          properties: {
            voucher: {
              five_hundreds: 0,
              two_hundreds: 0,
              one_hundreds: 0,
              fifties: 0,
              twenties: 0,
              tens: 0,
              ones: 0,

              total_count: 0,
            },
          },
        };

  const {
    five_hundreds,
    two_hundreds,
    one_hundreds,
    fifties,
    twenties,
    tens,
    ones,
    total_count,
  } = voucher;

  const voucherTbl_dataSource = [
    {
      key: "0",
      stt: 1,
      value: 500000,
      amount: five_hundreds,
    },
    {
      key: "1",
      stt: 2,
      value: 200000,
      amount: two_hundreds,
    },
    {
      key: "2",
      stt: 3,
      value: 100000,
      amount: one_hundreds,
    },
    {
      key: "3",
      stt: 4,
      value: 50000,
      amount: fifties,
    },
    {
      key: "4",
      stt: 5,
      value: 20000,
      amount: twenties,
    },
    {
      key: "5",
      stt: 6,
      value: 10000,
      amount: tens,
    },
    {
      key: "6",
      stt: 7,
      value: 1000,
      amount: ones,
    },
  ];

  const voucherTbl_columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "0",
      align: "center",
      render: (stt) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{stt}</>,
        };
      },
    },
    {
      title: "Nội dung",
      dataIndex: "content",
      key: "1",
      render: (text, record) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {formatMoney(record.value)} x {record.amount}
            </>
          ),
        };
      },
    },
    {
      title: "Giá",
      dataIndex: "price",
      key: "2",
      width: "10%",
      align: "right",
      render: (text, record) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{formatMoney(record.value * record.amount)}</>,
        };
      },
    },
  ];

  const tableCSS = css({
    "& table": {
      borderCollapse: "collapse",
    },
    "& thead > tr > th": {
      fontWeight: "bold",
      color: "black",
      textAlign: "center",
    },
    "& .ant-table-placeholder": {
      display: "none",
    },
    "& thead > tr > th, & tbody > tr > td": {
      borderBottom: "2px solid #ccc",
      padding: 8,
    },
    "& tfoot > tr> td": {
      padding: 8,
    },
  });

  let locale = {
    emptyText: " ",
  };

  return (
    <Row>
      <Col xs={24} style={{ color: "#777", fontWeight: 700 }}>
        <strong>Voucher</strong>
      </Col>
      <Col xs={24}>
        <Table
          locale={locale}
          className={tableCSS}
          dataSource={voucherTbl_dataSource}
          columns={voucherTbl_columns}
          pagination={false}
          summary={(pageData) => {
            return (
              <Table.Summary.Row>
                <Table.Summary.Cell />
                <Table.Summary.Cell className="text-center">
                  Tổng:
                </Table.Summary.Cell>
                <Table.Summary.Cell
                  className="text-right"
                  style={{ padding: "8px 8px" }}
                >
                  <strong>{formatMoney(total_count)}</strong>
                </Table.Summary.Cell>
              </Table.Summary.Row>
            );
          }}
        />
      </Col>
    </Row>
  );
}

export default VoucherPrintTable;
