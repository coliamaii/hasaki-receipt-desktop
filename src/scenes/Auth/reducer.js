import { combineReducers } from 'redux'
// import Cookies from 'js-cookie';
import login from './scenes/Login/reducer'
import { AUTH } from './actions'
import { LOGIN } from './scenes/Login/actions'
import { API_TOKEN_KEY, USER_INFO } from 'web.config';

const initialState = {
    profile: {},
    staff_info: {},
    permission: [],
    locations: [],
    stores: [],
    departments: [],
    stores_allow: [],
    stores_writable: [],
    stores_read_only: [],
    logged: false,
    verified: false,
}

function info(state = initialState, action) {
    switch (action.type) {
        case AUTH.verify.success:
            return {
                ...state,
                verified: true,
                logged: true,
                profile: action.profile,
                staff_info: action.staff_info || {},
                permission: action.permission,
                locations: action.locations,
                stores: action.stores,
                departments: action.departments,
                stores_allow: action.stores_allow,
                stores_writable: action.stores_writable,
                stores_read_only: action.stores_read_only,
            };
        case AUTH.verify.error:
            localStorage.removeItem(API_TOKEN_KEY)
            localStorage.removeItem(USER_INFO)
            return {
                ...initialState,
                verified: true,
            };
        case LOGIN.success:
            localStorage.setItem(API_TOKEN_KEY, action.token)
            return {
                ...state,
                verified: true,
                logged: true,
                user_id: action.user_id,
                token: action.token,
            }
        case AUTH.logout.request:
            localStorage.removeItem(API_TOKEN_KEY)
            localStorage.removeItem(USER_INFO)
            return {
                ...initialState,
                verified: true,
            }
        default:
            return state
    }
}


const reducer = combineReducers({
    info,
    login,
})

export default reducer