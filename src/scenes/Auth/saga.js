import { takeLatest, call, put, all } from "redux-saga/effects";
// import axios from 'utils/request'
// import Cookies from 'js-cookie';
import { AUTH } from "./actions";
import { LOGIN } from "./scenes/Login/actions";
import LoginSaga from "./scenes/Login/saga";
import { getList as getListStore } from "apis/setting/store";
import { getList as getListLocation } from "apis/setting/location";
import { getList as getListDepartment } from "apis/setting/department";

import {
  ipPublic as getIPPublic,
  profile as userProfile,
} from "apis/setting/user";
import { API_TOKEN_KEY, USER_INFO } from "web.config";
import offlineUser from "utils/offline-user";

function* verifySaga() {
  try {
    const token = localStorage.getItem(API_TOKEN_KEY);
    if (token) {
      const response = yield call(userProfile);
      if (response.status && response.data?.profile) {
        let {
          data: { profile, permission, staff_info },
        } = response;
        profile.ip_allow = true;
        let stores = [];
        let locations = [];
        let departments = [];
        let stores_writable = [];
        let stores_read_only = [];

        for (let index = 0; index < profile.stores.length; index++) {
          if (profile.stores[index].status & 1) {
            stores_writable.push(profile.stores[index]);
          } else if (profile.stores[index].status & 2) {
            stores_read_only.push(profile.stores[index]);
          }
        }
        // }
        if (profile.role_id !== 130) {
          // SUPPER ADMIN
          profile.stores = stores_writable.concat(stores_read_only);
        }

        const responseStores = yield call(getListStore);
        if (responseStores.status) {
          stores = responseStores.data.rows.map((item) => ({
            ...item,
            value: item.store_id,
            label: item.store_name,
            properties:
              item.properties && item.properties != "NULL"
                ? JSON.parse(item.properties)
                : null,
          }));
          profile.store = stores.find(
            (item) => item.store_id === profile.store_id
          );
          profile.stores = profile.stores.map((store) => {
            const store_info = stores.find(
              (item) => store.store_id === item.store_id
            );
            if (store_info) {
              return {
                ...store,
                value: store_info.store_id,
                label: store_info.store_name,
              };
            }
            return {
              ...store,
              label: store.store_id,
              value: store.store_id,
            };
          });
        }

        const responseLocations = yield call(getListLocation, {
          limit: 1000,
        });
        if (responseLocations.status) {
          locations = responseLocations.data.rows;
        }
        const responseDepartments = yield call(getListDepartment);
        if (responseDepartments.status) {
          departments = responseDepartments.data.rows;
        }

        const responseIPPublic = yield call(getIPPublic, {
          limit: 1000,
        });
        if (responseIPPublic.status) {
          profile.ip = responseIPPublic.data.ip;
          const arr_ip_allow = profile.store?.properties?.ip;
          if (
            arr_ip_allow &&
            arr_ip_allow.length > 0 &&
            arr_ip_allow.indexOf(profile.ip) == -1
          ) {
            profile.ip_allow = false;
          }
        }
        profile.ip_allow = true;
        localStorage.setItem(
          USER_INFO,
          JSON.stringify({
            profile,
            permission,
            staff_info,
            locations,
            stores,
            departments,
            stores_writable,
            stores_read_only,
          })
        );

        yield put({
          type: AUTH.verify.success,
          profile,
          permission,
          staff_info,
          locations,
          departments,
          stores,
          stores_writable,
          stores_read_only,
        });
      } else {
        yield put({ type: AUTH.verify.error, error: response });
      }
    } else {
      yield put({ type: AUTH.verify.error });
    }
  } catch (error) {
    yield put({ type: AUTH.verify.error, error });
  }
}

function* verifyOfflineSaga() {
  if (localStorage.getItem(API_TOKEN_KEY) === null) {
    const {
      profile,
      permission,
      staff_info,
      locations,
      stores,
      stores_writable,
      stores_read_only,
    } = offlineUser;
    yield put({
      type: AUTH.verify.success,
      profile,
      permission,
      staff_info,
      locations,
      stores,
      stores_writable,
      stores_read_only,
    });
  } else {
    const {
      profile,
      permission,
      staff_info,
      locations,
      stores,
      stores_writable,
      stores_read_only,
    } = JSON.parse(localStorage.getItem(USER_INFO));
    yield put({
      type: AUTH.verify.success,
      profile,
      permission,
      staff_info,
      locations,
      stores,
      stores_writable,
      stores_read_only,
    });
  }
}

function* watcher() {
  yield all([
    takeLatest(AUTH.verify.request, verifySaga),
    takeLatest(LOGIN.success, verifySaga),
    takeLatest(AUTH.verify_offline.request, verifyOfflineSaga),
  ]);
}
export default function* () {
  yield all([watcher(), LoginSaga()]);
}
