import {
  takeLatest,
  call,
  put,
  all
} from 'redux-saga/effects';
import { replace } from 'connected-react-router';
import {
  LOGIN
} from './actions';
import { login } from 'apis/auth';

function* loginSaga(action) {
  try {
    const { params, redirectCallback } = action
    const response = yield call(login, params)
    if (response.status) {
      const { data: { user_id, token } } = response
      // localStorage.setItem(API_TOKEN_KEY, token)
      yield all([
        put({ type: LOGIN.success, user_id, token }),
      ])
      redirectCallback('/');
    } else {
      yield put({ type: LOGIN.error, error: response })
    }
  } catch (error) {
    console.log('error', error)
    yield all([
      put({ type: LOGIN.error, error })
    ])
  }
}

function* loginWatcher() {
  yield all([
    takeLatest(LOGIN.request, loginSaga)
  ])
}

export default loginWatcher;