import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { RenderInputText, RenderInputPassword } from 'utils/Form';

const validate = (values) => {
  let errors = {}
  return errors
}

function formLogin({ handleSubmit }) {
  return (
    <>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <Field
            name="email"
            placeholder="Email"
            component={RenderInputText}
          />
        </div>
        <div className="form-group">
          <Field
            name="password"
            placeholder="Password"
            component={RenderInputPassword}
          />
        </div>
        <div className="text-center">
          <button type="submit" className="btn btn-info btn-block">Login</button>
        </div>
      </form>
    </>
  )
}

export default reduxForm({
  form: 'formLogin',
  enableReinitialize: true,
  validate
})(formLogin);