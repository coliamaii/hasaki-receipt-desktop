import React, { useEffect } from 'react';
import { Spin } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { logout, verify_offline } from 'scenes/Auth/actions';
import { login } from './actions';
import Form from './components/form'
import { useHistory } from 'react-router-dom';

function LoginPage() {
  const dispatch = useDispatch();
  const loading = useSelector(state => state.auth.login.loading)
  const profile = useSelector(state => state.auth.info.profile);
  const history = useHistory();

  const { isOnline } = useSelector(state => state.mode);

  useEffect(() => {
    dispatch(logout());
  }, [])

  const handleLogin = (values) => {
    if (isOnline) {
      dispatch(login(values, history.push))
    } else {
      // const { email, password } = values;
      // localStorage.setItem('account', JSON.stringify({ email, password }))
      dispatch(verify_offline());
      history.push('/')
    }
  }

  return (
    <>
      <Spin spinning={loading}>
        <div
          style={{ minWidth: 350, backgroundColor: 'white', padding: '2em' }}
        >
          <Form onSubmit={handleLogin} />
        </div>
      </Spin>
    </>
  )
}

export default LoginPage;