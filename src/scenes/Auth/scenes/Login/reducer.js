import {
  LOGIN
} from './actions'

const initialState = {
  loading: false,
  success: false,
}

function PostCreateReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN.request:
      return {
        ...state,
        loading: true,
        success: false,
      }
    case LOGIN.success:
      return {
        ...state,
        loading: false,
        success: true,
      }
    case LOGIN.error:
      return {
        ...state,
        loading: false,
        success: true,
      }
    default:
      return state
  }
}

export default PostCreateReducer