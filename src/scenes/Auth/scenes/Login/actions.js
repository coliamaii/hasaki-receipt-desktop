export const LOGIN = {
  request: 'LOGIN_REQUEST',
  success: 'LOGIN_SUCCESS',
  error: 'LOGIN_ERROR'
};
export function login(params, redirectCallback) {
  return {
    type: LOGIN.request,
    params,
    redirectCallback,
  }
}