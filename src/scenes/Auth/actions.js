export const AUTH = {
  verify: {
    request: 'AUTH_VERIFY_REQUEST',
    success: 'AUTH_VERIFY_SUCCESS',
    error: 'AUTH_VERIFY_ERROR',
  },
  verify_offline: {
    request: 'AUTH_VERIFY_OFFLINE_REQUEST',
    success: 'AUTH_VERIFY_OFFLINE_SUCCESS',
  },
  logout: {
    request: 'AUTH_LOGOUT_REQUEST',
  }
};
export function verify() {
  return {
    type: AUTH.verify.request
  }
}

export function verify_offline() {
  return {
    type: AUTH.verify_offline.request,
  }
}

export function logout() {
  return {
    type: AUTH.logout.request
  }
}