import ReceiptDashboard from "./ReceiptDashboard";
import Profile from "./Profile/routes";
import SupportRequest from "./SupportRequest/routes";
import Accounting from "./Accounting/routes";
import Setting from "./Setting/routes";
import ReturnProduct from "./ReturnProduct/routes";

export default [
  ...Profile,
  {
    key: "dashboard.receipt",
    name: "Receipt Dashboard",
    component: ReceiptDashboard,
    path: "/",
    template: "main",
    exact: true,
    check_store: true,
  },
  ...ReturnProduct,
  ...SupportRequest,
  ...Accounting,
  ...Setting,
];
