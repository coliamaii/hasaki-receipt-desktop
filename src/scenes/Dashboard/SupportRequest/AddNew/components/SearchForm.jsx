import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, Form, Input, Row, Space } from "antd";
import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";

function SearchForm(props) {
  const [form] = Form.useForm();
  const history = useHistory();

  const defaultFormValues = {
    code: "",
  };

  const handleSearch = (values) => {
    history.push(`/support-request?code=${values.code}`);
  };

  useEffect(() => {
    form.setFieldsValue(defaultFormValues);
  }, [form, defaultFormValues]);

  return (
    <div className="box">
      <div className="box-body">
        <Form
          form={form}
          initialValues={defaultFormValues}
          onFinish={handleSearch}
        >
          <Row gutter={[32]}>
            <Col md={4}>
              <Form.Item name="code" style={{ margin: 0 }}>
                <Input placeholder="Code" />
              </Form.Item>
            </Col>

            <Col md={2}>
              <Form.Item style={{ margin: 0 }}>
                <Button
                  type="primary"
                  htmlType="submit"
                  icon={<FontAwesomeIcon icon={faSearch} />}
                >
                  &nbsp; Search
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>
    </div>
  );
}

SearchForm.propTypes = {};

export default SearchForm;
