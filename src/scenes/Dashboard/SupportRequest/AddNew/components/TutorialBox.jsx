import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Space } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTextWidth } from "@fortawesome/free-solid-svg-icons";

const Ul = styled.ul`
  padding-left: 40px;
`;

function TutorialBox() {
  return (
    <div className="box box-solid box-warning">
      <div className="box-header with-border">
        <Space>
          <FontAwesomeIcon icon={faTextWidth} />
          <h3 className="box-title" style={{ color: "white" }}>
            Tips&#40;Hướng dẫn&#41;
          </h3>
        </Space>
      </div>

      <div className="box-body">
        <div>
          <p className="text-red">
            &#42;&#42;&#42; LƯU Ý: Nhân Viên làm phiếu phải thực thực hiện bước
            thứ 2, trước khi trình Ban Giám Đốc ký duyệt.
          </p>
          <p>
            1&#41; Quy trình kiểm tra và theo dõi phiếu.
            <br />
            &#42; Nhân viên khi lập phiếu phải có nghĩa vụ theo dõi và phản hồi
            lại thông tin.
          </p>
          <p></p>
          <Ul>
            <li>
              Bước 1: Lập phiếu yêu cầu và gửi trưởng các bộ phận trực tiếp xác
              nhận.
            </li>
            <li>
              <strong className="text-red">Bước 2:</strong> Trưởng các bộ phận
              xác nhận&#40;click nút verify&#41;.
              <strong className="text-blue">
                {" "}
                Đồng thời trưởng bộ phận phải đề xuất đến bộ phận liên quan
                &#40;Kế toán/ IT&#41; xử lý bằng cách chọn nội dung cần feedback
                là phòng ban &#40; hoặc người tiếp nhận trực tiếp&#41;.
              </strong>
            </li>
            <li>
              Bước 3: Trưởng các bộ phận/phòng ban có liên quan thực hiện xử lý
              và comment.
            </li>
            <li>
              Bước 4: Phòng kế toán kiểm tra xác nhận chi phí &#40;đối với các
              trường hợp liên quan đến reconcile, hoặc theo yêu cầu của ban giám
              đốc&#41;.
            </li>
            <li>Bước 5: Nhân viên gửi mã phiếu trình BGD ký duyệt.</li>
            <li>Bước 6: Nhân viên liên hệ phòng kế toán để nhận tiền.</li>
          </Ul>
          <p>2&#41; Type: cách phân loại các phiếu</p>
          <p></p>
          <Ul>
            <li>
              Adjustment &#40;hiệu chỉnh &#41;: Đối với các trường hợp sai số
              liệu hệ thống do lỗi thao tác nhân viên.
            </li>
            <li>
              Discount Product &#40;giảm giá sản phẩm&#41;: Nhân viên mua sản
              phẩm cty cần giảm giá &#40;thoả các điều kiện quy đinh cty&#41;.
            </li>
            <li>
              Discount Service &#40;giảm giá dịch vụ &#41;: Nhân viên làm liệu
              trình cty cần giảm giá &#40;thoả các điều kiện quy đinh cty&#41;.
            </li>
            <li>
              Feedback: dành cho các trường họp phản hồi góp ý, sai số liệu...
              do hệ thống.
            </li>
            <li>
              Payment &#40;phiếu thanh toán&#41;: Đối với các trường họp phát
              sinh nhu cầu cần cty chi trả.
            </li>
            <li>
              Advance &#40;phiếu tạm ứng&#41;: Đối với các trường họp cần cty
              tạm ứng trước số tiền.
            </li>
          </Ul>

          <p>
            3&#41; Payment Method: hình thức chi trả &#40;Cash: tiền mặt; Bank
            Transfer: chuyển khoản ngân hàng&#41;.
            <br />
            Payment Amount: Số tiền cần chi trả.
            <br />
            Các trường họp cần nhập số tiền và phương thức chi trả:
          </p>
          <p></p>
          <Ul>
            <li>
              Đối với loại Adjustment &#40;hiệu chỉnh &#41; lên quan đến phiếu
              reconcile &#40;chốt ca&#41; cần nhận lại tiền.{" "}
            </li>
            <li>Đối với loại phiếu Payment/Advance.</li>
          </Ul>
        </div>
      </div>
    </div>
  );
}

TutorialBox.propTypes = {};

export default TutorialBox;
