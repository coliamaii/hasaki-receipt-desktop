import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, DatePicker, Form, Input, Row, Select } from "antd";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import { insertSupReqRequestAction } from "share/api/SupportRequest/Insert/actions";
import { options_type } from "share/options/supreq_options";
import _map from "lodash/map";

const { Option } = Select;

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

function RequestForm(props) {
  let query = useQuery();
  const history = useHistory();
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const { isLoading } = useSelector((state) => state.api.supportRequest);

  const initialRequestFormValues = {
    type: 1,
    ref_code: query.get("ref_code"),
    payment_method: "",
    payment_amount: "",
    content: "",
    adjustment_reason: "",
    due_date: "",
  };

  const handleSubmit = (values) => {
    const { content, due_date, ref_code } = values;
    let due_date_format = due_date === "" ? "" : due_date.unix();

    if (content.trim() && ref_code) {
      dispatch(
        insertSupReqRequestAction(
          { ...values, due_date: due_date_format },
          history.push
        )
      );
    } else {
      history.push("/support-request");
    }
  };

  useEffect(() => {
    form.setFieldsValue(initialRequestFormValues);
  }, [form, initialRequestFormValues]);

  return (
    <div className="box box-default">
      <Form
        form={form}
        name="requestForm"
        onFinish={handleSubmit}
        initialValues={initialRequestFormValues}
      >
        <div className="box-header with-border">
          <h3 className="box-title">1&#41; Request</h3>
        </div>

        <div className="box-body">
          <Row gutter={[32, 8]}>
            <Col md={12} xs={24} sm={12}>
              <div className="form-group">
                <label>Type</label>
                <Form.Item name="type" style={{ margin: 0 }}>
                  <Select style={{ width: "100%" }}>
                    <Option value="">All Type</Option>
                    {_map(options_type, ({ value, label }) => (
                      <Option
                        style={{ textTransform: "uppercase" }}
                        key={value}
                        value={value}
                      >
                        {label}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </div>
            </Col>

            <Col md={12} xs={24} sm={12}>
              <div className="form-group">
                <label>Reference Code:</label>
                <Form.Item name="ref_code" style={{ margin: 0 }}>
                  <Input className="form-control" />
                </Form.Item>
              </div>
            </Col>
          </Row>

          <Row gutter={[32, 8]}>
            <Col md={12} xs={24} sm={12}>
              <div className="form-group">
                <label>Payment Method: </label>
                <Form.Item name="payment_method" style={{ margin: 0 }}>
                  <Select className="form-control">
                    <Option value="">-Select Method-</Option>
                    <Option value="1">Cash</Option>
                    <Option value="2">Bank</Option>
                    <Option value="6">Balance&#40;Tranfer&#41;</Option>
                  </Select>
                </Form.Item>
              </div>
            </Col>

            <Col md={12} xs={24} sm={12}>
              <div className="form-group">
                <label>Payment Amount:</label>
                <Form.Item name="payment_amount" style={{ margin: 0 }}>
                  <Input className="form-control" autoComplete="off" />
                </Form.Item>
              </div>
            </Col>
          </Row>
          <Row>
            <Col xs={24}>
              <div className="form-group">
                <label>Content:</label>
                <Form.Item name="content" style={{ margin: 0 }}>
                  <Input.TextArea
                    className="form-control"
                    maxLength={200}
                    rows="5"
                  ></Input.TextArea>
                </Form.Item>
              </div>
            </Col>
          </Row>

          <Row gutter={[32]}>
            <Col lg={6} xs={24} md={12}>
              <div className="form-group">
                <label>Reason</label>
                <Form.Item name="adjustment_reason" style={{ margin: 0 }}>
                  <Select className="form-control">
                    <Option value=""> -Selecct Reason- </Option>
                    <Option value="1">System Error</Option>
                    <Option value="2">Mistakes</Option>
                  </Select>
                </Form.Item>
              </div>
            </Col>

            <Col lg={6} xs={24} md={12}>
              <div className="form-group">
                <label>Due Date</label>
                <Form.Item name="due_date" style={{ margin: 0 }}>
                  <DatePicker suffixIcon={false} className="form-control" />
                </Form.Item>
              </div>
            </Col>
          </Row>
        </div>

        <div className="box-footer">
          <Row>
            <Col xs={24} className="text-right">
              <Form.Item style={{ margin: 0 }}>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={isLoading}
                  icon={<FontAwesomeIcon icon={faPlus} />}
                >
                  &nbsp; Add New
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </div>
      </Form>
    </div>
  );
}

RequestForm.propTypes = {};

export default RequestForm;
