import { Col, Row } from "antd";
import React from "react";
import RequestForm from "./components/RequestForm";
import SearchForm from "./components/SearchForm";
import TutorialBox from "./components/TutorialBox";

function index(props) {
  return (
    <div className="container-fluid">
      <section className="content-header">
        <h1>
          Support Request
          <small>Add new</small>
        </h1>
      </section>
      <section className="content">
        <Row>
          <Col xs={24}>
            <SearchForm />
          </Col>
        </Row>

        <Row gutter={[32, 8]}>
          <Col lg={12} xs={24}>
            <RequestForm />
          </Col>

          <Col lg={12} xs={24}>
            <TutorialBox />
          </Col>
        </Row>
      </section>
    </div>
  );
}

index.propTypes = {};

export default index;
