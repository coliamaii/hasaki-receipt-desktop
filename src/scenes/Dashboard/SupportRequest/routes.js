import SupportRequest from ".";
import SupreqAddNew from "./AddNew";
import SupreqEdit from "./Edit";
import CanvasBarcode from "../Accounting/Payments/components/CanvasBarcode";

export default [
  {
    key: "dashboard.support-request",
    name: "Support Request",
    component: SupportRequest,
    path: "/support-request",
    template: "main",
    exact: true,
    children: [
      {
        key: "support-request.create",
        name: "Support Request Create",
        component: SupreqAddNew,
        path: "/support-request/create",
        hide: true,
        template: "main",
      },
      {
        key: "support-request.edit",
        name: "Support Request Edit",
        component: SupreqEdit,
        path: "/support-request/edit",
        hide: true,
        template: "main",
      },
      {
        key: "support-request.edit.barcode",
        name: "Support Request Edit barcode",
        component: CanvasBarcode,
        path: "/support-request/edit/barcode",
        hide: true,
        template: "main",
      },
    ],
  },
];
