import { Form, Select } from "antd";
import React from "react";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";

FeedbackBySelect.propTypes = {
  onSearch: PropTypes.func,
};

const { Option } = Select;

function FeedbackBySelect({ onSearch }) {
  const { rows } = useSelector((state) => state.api.setting.searchUser);

  return (
    <div className="easy-autocomplete">
      <Form.Item name="feedback_by" style={{ margin: 0 }}>
        <Select
          style={{ width: "100%" }}
          showSearch
          placeholder="Name,Gmail,ID"
          onSearch={onSearch}
          suffixIcon={false}
          filterOption={(inputValue, option) => {
            return true;
          }}
        >
          {rows.map((obj, i) => {
            return (
              <Option key={obj.id} value={obj.id}>
                {obj.id} - {obj.name}
              </Option>
            );
          })}
        </Select>
      </Form.Item>
    </div>
  );
}

export default FeedbackBySelect;
