import {
  faEdit,
  faExclamationTriangle,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, Row, Space } from "antd";
import dayjs from "dayjs";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { getDetailSupReqRequestAction } from "share/api/SupportRequest/GetDetail/actions";
import CheckStatus from "../../components/CheckStatus";
import RefCodeTable from "./RefCodeTable";

function ReferenceCodeForm(props) {
  const dispatch = useDispatch();
  const history = useHistory();
  const { list: dataSource } = useSelector(
    (state) => state.api.supportRequest
  );

  const handleClickDetailBtn = (id) => {
    dispatch(getDetailSupReqRequestAction(id, history.push));
  };

  const columns = [
    {
      title: "Date",
      dataIndex: "supreq_ctime",
      key: "0",
      align: "center",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <span className="text-center">
              {dayjs(record.supreq_ctime * 1000).format("YYYY-MM-DD")}
            </span>
          ),
        };
      },
    },
    {
      title: "Content",
      key: "1",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <span>
              <strong>Ref: {record.supreq_code}</strong>
              <br />
              {record.supreq_content}
              <br />
              Code: {record.code} ( {record.reference_type} )
              <br />
              <small>
                ( User:
                <span data-user-id="1068" className="__user-info-1068">
                  Cashier 176 #{record.supreq_user_id}
                </span>
                - Modified:{" "}
                {dayjs(record.supreq_utime * 1000).format("YYYY-MM-DD HH:ss")} )
              </small>
              <Button
                className="btn btn-default btn-xs text-blue"
                onClick={() => handleClickDetailBtn(record.supreq_id)}
              >
                <FontAwesomeIcon icon={faEdit} />
                Detail
              </Button>
            </span>
          ),
        };
      },
    },
    {
      title: "Status",
      dataIndex: "supreq_status",
      key: "2",
      align: "center",
      render: (text) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <span className="text-center">{CheckStatus(text)}</span>,
        };
      },
    },
  ];

  return (
    <>
      <div className="box box-warning">
        <div className="box-header with-border text-yellow">
          <Space>
            <FontAwesomeIcon icon={faExclamationTriangle} />
            <h3 className="box-title ">Reference Code</h3>
          </Space>
        </div>

        <div className="box-body">
          <Row>
            <Col md={24}>
              <RefCodeTable dataSource={dataSource} columns={columns} />
            </Col>
          </Row>
        </div>
      </div>
    </>
  );
}

ReferenceCodeForm.propTypes = {};

export default ReferenceCodeForm;
