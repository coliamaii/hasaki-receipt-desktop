import { faBarcode, faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Row, Space } from "antd";
import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

function PhotoForm(props) {
  const { code } = useSelector((state) => state.api.supportRequest.detail);
  return (
    <Row>
      <Col md={24} className="text-right">
        <Space size="small">
          <Link
            to={`edit/barcode?code=${code}`}
            target="blank"
            className="btn btn-primary btn-sm"
          >
            <Space>
              <FontAwesomeIcon icon={faBarcode} />
              Scan Barcode &amp; Upload
            </Space>
          </Link>
          <a
            href="http://test.inshasaki.com/company/file?object_type=12&amp;object_id=462"
            className="btn btn-default btn-sm"
          >
            <Space>
              <FontAwesomeIcon icon={faSearch} />
              Photos
            </Space>
          </a>
        </Space>
      </Col>
    </Row>
  );
}

PhotoForm.propTypes = {};

export default PhotoForm;
