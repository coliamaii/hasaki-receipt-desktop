import {
  faClock,
  faComment,
  faPlus,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, Form, Row, Space } from "antd";
import TextArea from "antd/lib/input/TextArea";
import PropTypes from "prop-types";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import CommentTable from "./CommentTable";

CommentForm.propTypes = {
  initialValues: PropTypes.object,
};

CommentForm.defaultProps = {
  initialValues: {},
};

function CommentForm({ initialValues }) {
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const { comments } = useSelector((state) => state.api.supportRequest);

  let locale = {
    emptyText: <div className="text-center text-light-blue">No Comments</div>,
  };

  const columns = [
    {
      title: "Content",
      dataIndex: "content",
      key: "0",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              <strong className="text-blue">
                <Space>
                  <FontAwesomeIcon icon={faUser} />
                  {record.user.name}
                </Space>
              </strong>
              <br />
              {record.content}
            </>
          ),
        };
      },
    },
    {
      title: "Date",
      dataIndex: "date",
      key: "1",
      align: "right",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <span className="pull-right">
              <Space>
                <FontAwesomeIcon icon={faClock} />
                {record.created_at}
              </Space>
            </span>
          ),
        };
      },
    },
  ];

  const footerCommentTable = (
    <Form form={form} name="commentForm" initialValues={initialValues}>
      <Row gutter={[32]}>
        <Col md={20}>
          <Form.Item name="content" style={{ margin: 0 }}>
            <TextArea rows="3"></TextArea>
          </Form.Item>
        </Col>
        <Col md={4}>
          <Form.Item style={{ margin: 0 }}>
            <Button
              htmlType="submit"
              className="text-blue"
              style={{ height: "100%" }}
            >
              <Space style={{ display: "flex", flexDirection: "column" }}>
                <FontAwesomeIcon icon={faPlus} />
                Submit
              </Space>
            </Button>
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );

  return (
    <div className="box box-default">
      <div className="box-header with-border">
        <Space>
          <FontAwesomeIcon icon={faComment} />
          <h3 className="box-title">Comments</h3>
        </Space>
      </div>
      <div className="box-body">
        <Row>
          <Col md={24}>
            <CommentTable
              locale={locale}
              dataSource={comments}
              columns={columns}
              footer={footerCommentTable}
            />
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default CommentForm;
