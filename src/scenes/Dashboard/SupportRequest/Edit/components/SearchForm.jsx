import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, Form, Input, Row } from "antd";
import React from "react";

function SearchForm({ initialValues }) {
  const [form] = Form.useForm();

  return (
    <Form form={form} name="searchForm" initialValues={initialValues}>
      <div className="box-body">
        <Row gutter={[32]}>
          <Col md={4}>
            <Form.Item name="code" style={{ margin: 0 }}>
              <Input placeholder="Code" />
            </Form.Item>
          </Col>

          <Col md={2}>
            <Form.Item style={{ margin: 0 }}>
              <Button
                type="primary"
                htmlType="submit"
                icon={<FontAwesomeIcon icon={faSearch} />}
              >
                &nbsp; Search
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </div>
    </Form>
  );
}

SearchForm.propTypes = {};

export default SearchForm;
