import { faCheckSquare, faReply } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, Form, Row, Select, Space } from "antd";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";

const { Option } = Select;

function VerifyForm({ initialValues, handleSendBack }) {
  const [form] = Form.useForm();

  const { detail } = useSelector((state) => state.api.supportRequest);
  const { departments } = useSelector((state) => state.auth.info);

  const { adjustment_reason, department_id } = detail;

  useEffect(() => {
    form.setFieldsValue({
      adjustment_reason: adjustment_reason + "",
      department_id: department_id,
    });
  }, [detail]);

  return (
    <Form form={form} name="verifyForm" initialValues={initialValues}>
      <div className="box-header with-border">
        <h3 className="box-title">2) Verify</h3>
      </div>

      <div className="box-body">
        <Row gutter={[{ xs: 8 }, { xs: 8 }]}>
          <Col xs={24} md={14}>
            <Row gutter={[{ xs: 8, sm: 16 }]}>
              <Col xs={12}>
                <Form.Item name="adjustment_reason" style={{ margin: 0 }}>
                  <Select style={{ width: "100%" }}>
                    <Option value="0"> -Selecct Reason- </Option>
                    <Option value="1">System Error</Option>
                    <Option value="2">Mistakes</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={12}>
                <Form.Item name="department_id" style={{ margin: 0 }}>
                  <Select style={{ width: "100%" }}>
                    <Option value={0}>All Department</Option>
                    {departments.map(({ id, name }) => {
                      return (
                        <Option key={id} value={id}>
                          {name}
                        </Option>
                      );
                    })}
                  </Select>
                </Form.Item>
              </Col>
            </Row>
          </Col>

          <Col xs={24} md={10} className="text-right">
            <Space>
              <Form.Item style={{ margin: 0 }}>
                <Button
                  type="primary"
                  htmlType="submit"
                  icon={<FontAwesomeIcon icon={faCheckSquare} />}
                >
                  &nbsp; Verify
                </Button>
              </Form.Item>
              <Button
                type="default"
                onClick={() => handleSendBack()}
                icon={<FontAwesomeIcon icon={faReply} />}
              >
                &nbsp; Send Back
              </Button>
            </Space>
          </Col>
        </Row>
      </div>
    </Form>
  );
}

VerifyForm.propTypes = {};

export default VerifyForm;
