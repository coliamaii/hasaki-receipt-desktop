import { Table } from "antd";
import React from "react";
import PropTypes from "prop-types";

RefCodeTable.propTypes = {
  columns: PropTypes.array,
  dataSource: PropTypes.array,
};

function RefCodeTable({ columns, dataSource }) {
  return (
    <div className="box-body table-responsive">
      <Table
        columns={columns}
        dataSource={dataSource}
        pagination={{
          itemRender: (current, type, originalElement) => {
            if (type === "prev") {
              return <a>Prev</a>;
            }
            if (type === "next") {
              return <a>Next</a>;
            }
            return originalElement;
          },
        }}
      />
    </div>
  );
}

export default RefCodeTable;
