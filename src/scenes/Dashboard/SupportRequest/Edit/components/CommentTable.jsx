import { Table } from "antd";
import React from "react";
import PropTypes from "prop-types";

CommentTable.propTypes = {
  dataSource: PropTypes.array.isRequired,
  columns: PropTypes.array.isRequired,
  locale: PropTypes.object,
  footer: PropTypes.element,
};

function CommentTable({ dataSource, columns, footer, locale }) {
  return (
    <Table
      locale={locale}
      dataSource={dataSource}
      columns={columns}
      showHeader={false}
      pagination={false}
      footer={() => footer}
    />
  );
}

export default CommentTable;
