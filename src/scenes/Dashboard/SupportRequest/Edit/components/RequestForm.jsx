import { faCheck, faShare, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, DatePicker, Form, Input, Row, Select } from "antd";
import TextArea from "antd/lib/input/TextArea";
import moment from "moment";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { putUpdateSupReqRequestAction } from "share/api/SupportRequest/Update/actions";
import formatMoney from "utils/money";

const { Option } = Select;

function RequestForm({ initialValues }) {
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  const { accountingCode, detail, users } = useSelector(
    (state) => state.api.supportRequest
  );

  const {
    code,
    supreq_status,
    supreq_user_id,
    supreq_code,
    payment_method,
    payment_amount,
    supreq_content,
    supreq_ctime,
    supreq_id,
    supreq_type,
    adjustment_reason,
    due_date,
    accounting_code,
    department_id,
    payment_code,
  } = detail;

  const handleCancelRequest = () => {
    const requestForm_allValues = form.getFieldsValue(true);
    dispatch(
      putUpdateSupReqRequestAction(supreq_id, {
        ...requestForm_allValues,
        status: 1,
      })
    );
  };

  let statusShow = "";
  let statusBtn = "";
  let cancelBtn = (
    <Button
      type="primary"
      style={{ backgroundColor: "#f39c12", borderColor: "#e08e0b" }}
      onClick={handleCancelRequest}
      icon={<FontAwesomeIcon icon={faTimes} />}
    >
      &nbsp; Cancel Request
    </Button>
  );

  switch (supreq_status) {
    case 1:
      statusShow = "PENDING";
      statusBtn = (
        <Button icon={<FontAwesomeIcon icon={faCheck} />} disabled>
          &nbsp; REQUEST
        </Button>
      );
      break;

    case 2:
      statusShow = "COMPLETED";
      cancelBtn = "";
      statusBtn = (
        <Button icon={<FontAwesomeIcon icon={faCheck} />} disabled>
          &nbsp; DONE
        </Button>
      );
      break;

    case 3:
      statusShow = "send back";
      statusBtn = (
        <Button
          type="primary"
          htmlType="submit"
          icon={<FontAwesomeIcon icon={faShare} />}
        >
          &nbsp; POST REQUEST
        </Button>
      );

      break;

    case 4:
      statusShow = "cancel";
      cancelBtn = (
        <Button
          type="primary"
          disabled
          icon={<FontAwesomeIcon icon={faTimes} />}
        >
          &nbsp; Cancel
        </Button>
      );
      break;

    case 5:
      statusShow = "verified";
      statusBtn = "";
      cancelBtn = "";
      break;

    default:
      statusShow = "";
      break;
  }

  useEffect(() => {
    form.setFieldsValue({
      type: supreq_type === undefined ? "1" : supreq_type + "",
      ref_code: supreq_code,
      payment_method: payment_method === undefined ? "" : payment_method + "",
      payment_amount: formatMoney(payment_amount),
      content: supreq_content,
      adjustment_reason:
        adjustment_reason === undefined ? "0" : adjustment_reason + "",
      due_date:
        due_date === undefined || due_date === 0 ? "" : moment(due_date * 1000),
    });
  }, [detail]);

  return (
    <Form form={form} name="requestForm" initialValues={initialValues}>
      <div className="box-header with-border">
        <h3 className="box-title">1) Request (Code: {code})</h3>
        <div className="pull-right">
          STATUS:&nbsp;<strong className="text-uppercase">{statusShow} </strong>
        </div>
      </div>

      <div className="box-body">
        <Row gutter={[32]}>
          <Col xs={24} sm={12} md={12}>
            <Form.Item
              name="type"
              label="Type"
              labelCol={{ span: 24 }}
              style={{ width: "100%" }}
            >
              <Select>
                <Option value="1">Adjustment</Option>
                <Option value="2">Discount Product</Option>
                <Option value="3">Discount Service</Option>
                <Option value="4">feedback</Option>
                <Option value="5">payment</Option>
                <Option value="6">advance</Option>
                <Option value="7">VAT</Option>
                <Option value="8">Customer Request</Option>
              </Select>
            </Form.Item>
          </Col>

          <Col xs={24} sm={12} md={12}>
            <Form.Item
              name="ref_code"
              label="Reference Code:"
              labelCol={{ span: 24 }}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={[32]}>
          <Col xs={24} sm={12} md={12}>
            <Form.Item
              name="payment_method"
              label="Payment Method: "
              labelCol={{ span: 24 }}
            >
              <Select>
                <Option value="">-Select Method-</Option>
                <Option value="1">Cash</Option>
                <Option value="2">Bank</Option>
                <Option value="6">Balance(Tranfer)</Option>
              </Select>
            </Form.Item>
          </Col>

          <Col xs={24} sm={12} md={12}>
            <Form.Item
              name="payment_amount"
              label="Payment Amount:"
              labelCol={{ span: 24 }}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>

        <Row>
          <Col xs={24}>
            <Form.Item name="content" label="Content:" labelCol={{ span: 24 }}>
              <TextArea rows={5} maxLength={200} />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={[32]}>
          <Col xs={12} md={6}>
            <Form.Item
              name="adjustment_reason"
              label="Reason:"
              labelCol={{ span: 24 }}
            >
              <Select>
                <Option value="0"> -Select Reason- </Option>
                <Option value="1">System Error</Option>

                <Option value="2">Mistakes</Option>
              </Select>
            </Form.Item>
          </Col>

          <Col xs={12} md={6}>
            <Form.Item name="due_date" label="Due Date" labelCol={{ span: 24 }}>
              <DatePicker format="YYYY-MM-DD" style={{ width: "100%" }} />
            </Form.Item>
          </Col>
        </Row>
        {accounting_code !== 0 && (
          <Row>
            <Col md={24}>
              <strong>Accounting Code:</strong>{" "}
              {accountingCode.map((obj, i) => {
                if (obj.code == accounting_code) {
                  return obj.code + " - " + obj.name;
                }
              })}
            </Col>
          </Row>
        )}

        <br />
        <Row>
          <Col md={24}>
            <strong>Feedback:</strong>
            {department_id !== 0 && <>(DeptID: {department_id})</>}
          </Col>
        </Row>
        <br />
        <Row>
          <Col md={24}>
            <p>
              STATUS:&nbsp;
              <strong className="text-uppercase">{statusShow}</strong>
              &nbsp;&nbsp; Created by:{" "}
              {users.map(({ id, name }) => {
                if (supreq_user_id === id) {
                  return (
                    <span key={id}>
                      {name}&nbsp;#&nbsp;{id}
                    </span>
                  );
                }
              })}
              &nbsp;-&nbsp;
              {moment(supreq_ctime * 1000).format("YYYY-MM-DD HH:mm")} -
              SupReqID: {supreq_id}
            </p>
          </Col>
        </Row>
      </div>

      <div className="box-footer">
        <Row>
          <Col md={12}>{cancelBtn}</Col>
          <Col md={12} className="text-right">
            {statusBtn}
          </Col>
        </Row>
      </div>
    </Form>
  );
}

RequestForm.propTypes = {};

export default RequestForm;
