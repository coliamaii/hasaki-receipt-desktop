import {
  faDollarSign,
  faEdit,
  faReply,
  faShare,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, Form, Input, Row, Select, Space } from "antd";
import dayjs from "dayjs";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getSearchListUserRequestAction } from "share/api/Settings/SearchUser/actions.js";
import { putCancelSupReqRequestAction } from "share/api/SupportRequest/Cancel/actions";
import { putUpdateSupReqRequestAction } from "share/api/SupportRequest/Update/actions";
import formatMoney from "utils/money";
import FeedbackBySelect from "./FeedbackBySelect";

const { Option } = Select;

function FeedbackForm({ initialValues }) {
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  const [phanQuyen, setPhanQuyen] = useState(true);
  const { departments } = useSelector((state) => state.auth.info);

  const { detail, accountingCode } = useSelector(
    (state) => state.api.supportRequest
  );

  const {
    supreq_id,
    department_id,
    supreq_status,
    payment_amount,
    payment_code,
    supreq_utime,
  } = detail;

  const handleClickCompletedBtn = () => {
    let status = 2;
    dispatch(putUpdateSupReqRequestAction(supreq_id, { status }));
  };

  const handleClickSendBackBtn = () => {
    let status = 3;
    dispatch(putUpdateSupReqRequestAction(supreq_id, { status }));
  };

  const handleClickCancelBtn = () => {
    let status = 4;
    dispatch(putCancelSupReqRequestAction(supreq_id, { status }));
  };

  const handleFeedbackByChange = (q) => {
    dispatch(getSearchListUserRequestAction({ q }));
  };

  const handleClickRequestPaymentBtn = () => {
    setPhanQuyen(false);
  };

  useEffect(() => {
    form.setFieldsValue({
      department_id:
        department_id === undefined || department_id === 0 ? "" : department_id,
      feedback_by: null,
      accounting_code: "",
      payment_amount: formatMoney(payment_amount),
      payment_code: payment_code,
    });
  }, [detail]);

  return (
    <Form form={form} name="feedbackForm" initialValues={initialValues}>
      <div className="box-header with-border">
        <h3 className="box-title">3) Feedback</h3>
      </div>

      <div className="box-body">
        {supreq_status === 5 ? (
          <Row gutter={[16]}>
            <Col md={6}>
              <Form.Item name="payment_amount" style={{ margin: 0 }}>
                <Input />
              </Form.Item>
            </Col>

            <Col md={6}>
              <Form.Item name="payment_code" style={{ margin: 0 }}>
                {phanQuyen ? (
                  <Button
                    type="primary"
                    onClick={handleClickRequestPaymentBtn}
                    icon={<FontAwesomeIcon icon={faShare} />}
                  >
                    &nbsp; Request Payment
                  </Button>
                ) : (
                  <Button
                    type="primary"
                    disabled
                    onClick={handleClickRequestPaymentBtn}
                    icon={<FontAwesomeIcon icon={faDollarSign} />}
                  >
                    &nbsp; Approve Payment
                  </Button>
                )}
              </Form.Item>
            </Col>
          </Row>
        ) : (
          <>
            <Row
              gutter={[
                { xs: 8, md: 16 },
                { xs: 8, md: 16 },
              ]}
            >
              <Col xs={12} md={6}>
                <Form.Item name="department_id" style={{ margin: 0 }}>
                  <Select style={{ width: "100%" }}>
                    <Option value="">All Department</Option>
                    {departments.map(({ id, name }) => (
                      <Option
                        key={id}
                        style={{ textTransform: "uppercase" }}
                        key={id}
                        value={id}
                      >
                        {name}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>

              <Col xs={12} md={6}>
                <FeedbackBySelect onSearch={handleFeedbackByChange} />
              </Col>

              <Col xs={12} md={6}>
                <Form.Item name="accounting_code" style={{ margin: 0 }}>
                  <Select
                    showSearch
                    style={{ width: "100%" }}
                    placeholder="Select Code"
                    optionFilterProp="children"
                  >
                    <Option style={{ backgroundColor: "#5897fb" }} value="">
                      Select Code
                    </Option>
                    {accountingCode.map((accObj) => {
                      return (
                        <Option key={accObj.id} value={accObj.code}>
                          #{accObj.code} - {accObj.name}
                        </Option>
                      );
                    })}
                  </Select>
                </Form.Item>
              </Col>

              <Col xs={12} md={6}>
                <Form.Item style={{ margin: 0 }}>
                  <Button
                    type="default"
                    htmlType="submit"
                    icon={<FontAwesomeIcon icon={faEdit} />}
                  >
                    &nbsp; Update
                  </Button>
                </Form.Item>
              </Col>
            </Row>
            <br />
          </>
        )}
        <br />
        <Row>
          <Col md={12}>
            <Button
              type="primary"
              style={{ backgroundColor: "#f39c12", borderColor: "#e08e0b" }}
              onClick={handleClickCancelBtn}
              icon={<FontAwesomeIcon icon={faTimes} />}
            >
              &nbsp; Cancel
            </Button>
          </Col>
          <Col md={12} className="text-right">
            <Space>
              <Button
                type="default"
                onClick={handleClickSendBackBtn}
                icon={<FontAwesomeIcon icon={faReply} />}
              >
                &nbsp; Send Back
              </Button>
              <Button
                type="primary"
                onClick={handleClickCompletedBtn}
                icon={<FontAwesomeIcon icon={faEdit} />}
              >
                &nbsp; Completed
              </Button>
            </Space>
          </Col>
        </Row>
        <br />
        Modified: {dayjs(supreq_utime * 1000).format("YYYY-MM-DD HH:ss")}
      </div>

      <div className="box-footer"></div>
    </Form>
  );
}

FeedbackForm.propTypes = {};

export default FeedbackForm;
