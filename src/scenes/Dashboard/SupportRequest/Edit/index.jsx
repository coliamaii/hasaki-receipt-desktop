import { faHome, faPhotoVideo } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Form, Row, Space } from "antd";
import moment from "moment";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { postCommentSupReqRequestAction } from "share/api/SupportRequest/PostComment/actions";
import { putUpdateSupReqRequestAction } from "share/api/SupportRequest/Update/actions";
import CommentForm from "./components/CommentForm";
import FeedbackForm from "./components/FeedbackForm";
import PhotoForm from "./components/PhotoForm";
import ReferenceCodeForm from "./components/ReferenceCodeForm";
import RequestForm from "./components/RequestForm";
import SearchForm from "./components/SearchForm";
import VerifyForm from "./components/VerifyForm";

function SupReqEdit(props) {
  const dispatch = useDispatch();

  const { supreq_id, supreq_status, code: ref_code } = useSelector(
    (state) => state.api.supportRequest.detail
  );

  const { list } = useSelector(
    (state) => state.api.supportRequest
  );

  const initialSearchFormValues = {
    code: "",
  };

  const initialVerifyFormValues = {
    adjustment_reason: "0",
    department_id: "",
  };

  const initialRequestFormValues = {
    type: "1",
    ref_code: "",
    payment_method: "",
    payment_amount: 0,
    content: "",
    adjustment_reason: "",
    due_date: moment(),
  };

  const initialFeedbackFormValues = {
    department_id: "",
    feedback_by: null,
    accounting_code: "",
    payment_amount: 0,
    payment_code: 0,
  };

  const initialCommentFormValues = {
    content: "",
  };

  const handleClickSendBackBtn = () => {
    let status = "3";
    dispatch(putUpdateSupReqRequestAction(supreq_id, { status }));
  };

  const onSupReqEditFormFinish = (formName, { values, forms }) => {
    const {
      verifyForm,
      feedbackForm,
      requestForm,
      searchForm,
      commentForm,
    } = forms;

    if (formName === "verifyForm") {
      let status = "5";
      dispatch(putUpdateSupReqRequestAction(supreq_id, { ...values, status }));
    }
    if (formName === "feedbackForm") {
      let status = 1;
      dispatch(putUpdateSupReqRequestAction(supreq_id, { ...values, status }));
    }
    if (formName === "requestForm") {
      let status = "1";
      dispatch(putUpdateSupReqRequestAction(supreq_id, { ...values, status }));
    }
    if (formName === "searchForm") {
      console.log("searchForm");
    }
    if (formName === "commentForm") {
      const { content } = values;

      if (content && ref_code) {
        commentForm.setFieldsValue(initialCommentFormValues);

        dispatch(postCommentSupReqRequestAction({ ref_code, content }));
      }
    }
  };

  return (
    <Form.Provider onFormFinish={onSupReqEditFormFinish}>
      <section className="content-header">
        <h1>
          Support Request
          <small>Edit</small>
        </h1>
        <ol className="breadcrumb">
          <li>
            <a href="#">
              <Space>
                <FontAwesomeIcon icon={faHome} />
                Dashboard
              </Space>
            </a>
          </li>
          <li>Support Request</li>
        </ol>
      </section>

      <section className="content">
        <Row>
          <Col xs={24}>
            <div className="box">
              <SearchForm initialValues={initialSearchFormValues} />
            </div>
          </Col>
        </Row>

        <Row gutter={[32]}>
          <Col xs={24} lg={12}>
            <div className="box box-default">
              <RequestForm initialValues={initialRequestFormValues} />
            </div>

            {supreq_status === 2 ||
            supreq_status === 3 ||
            supreq_status === 4 ? (
              ""
            ) : (
              <>
                <div className="box box-default">
                  <VerifyForm
                    initialValues={initialVerifyFormValues}
                    handleSendBack={handleClickSendBackBtn}
                  />
                </div>

                <div className="box box-default">
                  <FeedbackForm initialValues={initialFeedbackFormValues} />
                </div>
              </>
            )}
          </Col>

          {list.length > 1 && (
            <Col xs={24} lg={12}>
              <ReferenceCodeForm />
            </Col>
          )}

          <Col xs={24} lg={12}>
            <div className="box box-default">
              <div className="box-header with-border">
                <Space>
                  <FontAwesomeIcon icon={faPhotoVideo} />
                  <h3 className="box-title">Photos</h3>
                </Space>
              </div>

              <div className="box-body">
                <PhotoForm />
              </div>
            </div>

            <CommentForm initialValues={initialCommentFormValues} />
          </Col>
        </Row>
      </section>
    </Form.Provider>
  );
}

export default SupReqEdit;
