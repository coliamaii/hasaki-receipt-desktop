import {
  faCheckSquare,
  faReply,
  faShare,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Space } from "antd";
import React from "react";

function CheckStatus(value) {
  switch (value) {
    case 1:
      return (
        <span className="label label-primary">
          <Space>
            <FontAwesomeIcon icon={faShare} />
            REQUEST
          </Space>
        </span>
      );
    case 2:
      return (
        <span
          className="label label-default text-blue"
          style={{ backgroundColor: "#d2d6de" }}
        >
          COMPLETTED
        </span>
      );
    case 3:
      return (
        <span className="label label-warning">
          <Space>
            <FontAwesomeIcon icon={faReply} />
            SEND_BACK
          </Space>
        </span>
      );
    case 4:
      return (
        <span
          className="label label-default"
          style={{ backgroundColor: "#d2d6de", color: "#444" }}
        >
          CANCEL
        </span>
      );
    case 5:
      return (
        <span className="label label-info">
          <Space>
            <FontAwesomeIcon icon={faCheckSquare} />
            REQ_PAYMENT
          </Space>
        </span>
      );
    default:
      return "";
  }
}

export default CheckStatus;
