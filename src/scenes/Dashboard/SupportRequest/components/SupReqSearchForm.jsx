import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, DatePicker, Form, Input, Row, Select, Space } from "antd";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import ExportReactCSV from "scenes/Dashboard/components/ExportReactCSV";
import {
  options_adjustment_reason,
  options_reference_type,
  options_from_due_date,
  options_status,
  options_type,
} from "share/options/supreq_options";
import _map from "lodash/map";

SupReqSearchForm.propTypes = {
  initialValues: PropTypes.object,
  hanldSearch: PropTypes.func,
};

const { Option } = Select;
const dateFormat = "YYYY-MM-DD";

function SupReqSearchForm({ initialValues }) {
  const [form] = Form.useForm();

  const { departments } = useSelector((state) => state.auth.info);
  const { isLoading, accountingCode } = useSelector(
    (state) => state.api.supportRequest
  );

  const headers = [
    { label: "No.", key: "no" },
    { label: "Year", key: "year" },
    { label: "Month", key: "month" },
    { label: "Day", key: "day" },
    { label: "Hour", key: "hour" },
    { label: "Code", key: "code" },
    { label: "Request", key: "request" },
    { label: "Ref Code", key: "refcode" },
    { label: "Request By", key: "requestby" },
    { label: "PaymentCode", key: "paymentcode" },
    { label: "AccountingCode", key: "accountingcode" },
    { label: "Amount", key: "amount" },
    { label: "Status", key: "status" },
  ];

  const [csvData, setCsvData] = useState([]);

  // useEffect(() => {
  //   rows.map((data, i) => {
  //     csvData.push({
  //       no: i + 1,
  //       year: dayjs(data.supreq_ctime * 1000).format("YYYY"),
  //       month: dayjs(data.supreq_ctime * 1000).format("MM"),
  //       day: dayjs(data.supreq_ctime * 1000).format("DD"),
  //       hour: dayjs(data.supreq_ctime * 1000).format("HH:ss"),
  //       code: data.code,
  //       request: data.supreq_content,
  //       refcode: data.supreq_code,
  //       requestby: data.supreq_user_id,
  //       paymentcode: data.payment_code,
  //       accountingcode: data.accounting_code,
  //       amount: data.payment_amount,
  //       status: data.supreq_status,
  //     });
  //   });
  //   setCsvData(csvData);
  // }, [rows]);

  useEffect(() => {
    form.submit();
  }, []);

  return (
    <Form form={form} name="supReqSearchForm" initialValues={initialValues}>
      <Row>
        <Col md={24}>
          <div className="box">
            <div className="box-body">
              <Row
                gutter={[
                  { sm: 16, md: 32 },
                  { xs: 8, sm: 16, md: 16 },
                ]}
              >
                <Col xs={24} sm={12} md={8} lg={4}>
                  <Form.Item name="from_create" style={{ margin: 0 }}>
                    <DatePicker
                      format={dateFormat}
                      style={{ width: "100%" }}
                      placeholder={dateFormat}
                    />
                  </Form.Item>
                </Col>

                <Col xs={24} sm={12} md={8} lg={4}>
                  <Form.Item name="to_create" style={{ margin: 0 }}>
                    <DatePicker
                      format={dateFormat}
                      style={{ width: "100%" }}
                      placeholder={dateFormat}
                    />
                  </Form.Item>
                </Col>

                <Col xs={24} sm={12} md={8} lg={4}>
                  <Form.Item name="status" style={{ margin: 0 }}>
                    <Select
                      style={{ width: "100%", textTransform: "uppercase" }}
                    >
                      {_map(options_status, ({ value, label }) => (
                        <Option
                          key={value}
                          value={value}
                          style={{ textTransform: "uppercase" }}
                        >
                          {label}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>

                <Col xs={24} sm={12} md={8} lg={4}>
                  <Form.Item name="department_id" style={{ margin: 0 }}>
                    <Select style={{ width: "100%" }}>
                      <Option value="">All Department</Option>
                      {_map(departments, ({ id, name }) => (
                        <Option
                          style={{ textTransform: "uppercase" }}
                          key={id}
                          value={id}
                        >
                          {name}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>

                <Col xs={24} sm={12} md={8} lg={4}>
                  <Form.Item name="type" style={{ margin: 0 }}>
                    <Select
                      style={{ width: "100%", textTransform: "uppercase" }}
                    >
                      <Option value="">All Type</Option>
                      {_map(options_type, ({ value, label }) => (
                        <Option
                          style={{ textTransform: "uppercase" }}
                          key={value}
                          value={value}
                        >
                          {label}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>

                <Col xs={24} sm={12} md={8} lg={4}>
                  <Form.Item name="reference_type" style={{ margin: 0 }}>
                    <Select style={{ width: "100%" }}>
                      {_map(options_reference_type, ({ value, label }) => (
                        <Option key={value} value={value}>
                          {label}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
              <Row
                gutter={[
                  { sm: 16, md: 32 },
                  { xs: 8, sm: 16, md: 16, lg: 0 },
                ]}
              >
                <Col xs={24} sm={12} md={8} lg={4}>
                  <Form.Item name="code" style={{ margin: 0 }}>
                    <Input placeholder="Code" style={{ width: "100%" }} />
                  </Form.Item>
                </Col>

                <Col xs={24} sm={12} md={8} lg={4}>
                  <Form.Item name="user_id" style={{ margin: 0 }}>
                    <Input
                      placeholder="Name,Email,ID"
                      style={{ width: "100%" }}
                    />
                  </Form.Item>
                </Col>

                <Col xs={24} sm={12} md={8} lg={4}>
                  <Form.Item name="adjustment_reason" style={{ margin: 0 }}>
                    <Select style={{ width: "100%" }}>
                      <Option value={0}>-All Reason-</Option>
                      {_map(options_adjustment_reason, ({ value, label }) => (
                        <Option key={value} value={value}>
                          {label}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>

                <Col xs={24} sm={12} md={8} lg={4}>
                  <Form.Item name="from_due_date" style={{ margin: 0 }}>
                    <Select style={{ width: "100%" }}>
                      {_map(options_from_due_date, ({ value, label }) => (
                        <Option key={value} value={value}>
                          {label}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>

                <Col xs={24} sm={12} md={8} lg={4}>
                  <Form.Item name="accounting_code" style={{ margin: 0 }}>
                    <Select
                      showSearch
                      style={{ width: "100%" }}
                      placeholder="Select Accounting Code"
                      optionFilterProp="children"
                    >
                      <Option style={{ backgroundColor: "#5897fb" }} value="">
                        Select Accounting Code
                      </Option>
                      {_map(accountingCode, (accObj) => (
                        <Option key={accObj.id} value={accObj.code}>
                          #{accObj.code} - {accObj.name}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>

                <Col xs={24} sm={12} md={8} lg={4}>
                  <Space>
                    <Form.Item style={{ margin: 0 }}>
                      <Button
                        type="primary"
                        htmlType="submit"
                        loading={isLoading}
                        icon={<FontAwesomeIcon icon={faSearch} />}
                      >
                        &nbsp; Search
                      </Button>
                    </Form.Item>
                    {/* <ExportReactCSV
                      headers={headers}
                      csvData={csvData}
                      fileName="Support-Request.xls"
                    /> */}
                  </Space>
                </Col>
              </Row>
            </div>
          </div>
        </Col>
      </Row>
    </Form>
  );
}

export default SupReqSearchForm;
