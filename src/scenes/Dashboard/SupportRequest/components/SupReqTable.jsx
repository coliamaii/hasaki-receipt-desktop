import { Table } from "antd";
import PropTypes from "prop-types";
import React from "react";
import { useSelector } from "react-redux";

SupReqTable.propTypes = {
  columns: PropTypes.array,
  dataSource: PropTypes.array,
  onPaginationChange: PropTypes.func,
};

SupReqTable.defaultProps = {
  columns: [],
  dataSource: [],
  onPaginationChange: () => {
    return;
  },
};

function SupReqTable({ columns, dataSource, onPaginationChange }) {
  const { isLoading } = useSelector((state) => state.api.supportRequest);

  return (
    <Table
      loading={isLoading}
      columns={columns}
      dataSource={dataSource}
      pagination={{
        pageSize: 10,
        onChange(current) {
          onPaginationChange(current);
        },
        itemRender: (current, type, originalElement) => {
          if (type === "prev") {
            return <a>Prev</a>;
          }
          if (type === "next") {
            return <a>Next</a>;
          }
          return originalElement;
        },
      }}
    />
  );
}

export default SupReqTable;
