import {
  faEdit,
  faExternalLinkAlt,
  faPlus,
  faTags,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, Form, Row, Space } from "antd";
import dayjs from "dayjs";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory, useLocation } from "react-router-dom";
import { getDetailSupReqRequestAction } from "share/api/SupportRequest/GetDetail/actions";
import { getListSupReqRequestAction } from "share/api/SupportRequest/GetList/actions";
import formatMoney from "utils/money";
import CheckStatus from "./components/CheckStatus";
import SupReqSearchForm from "./components/SupReqSearchForm";
import SupReqTable from "./components/SupReqTable";
// import query from 'utils/useQuery'

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

function SupportRequest(props) {
  const dispatch = useDispatch();
  const history = useHistory();
  const query = useQuery();

  const [page, setPage] = useState(1);

  const {
    profile: { id: user_id },
    departments,
  } = useSelector((state) => state.auth.info);

  const { list: subReqTblDataSource, users } = useSelector(
    (state) => state.api.supportRequest
  );

  const initialSupReqSearchFormValues = {
    from_create: moment(),
    to_create: moment(),
    status: 0,
    department_id: "",
    type: "",
    reference_type: 0,
    code: query.get("code"),
    user_id,
    adjustment_reason: 0,
    from_due_date: 0,
    accounting_code: "",
  };

  const subReqTblColumns = [
    {
      title: "No.",
      key: "1",
      align: "center",
      width: "1%",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{(page - 1) * 10 + index + 1}</>,
        };
      },
    },
    {
      title: "Date",
      key: "2",
      align: "center",
      width: "10%",
      render: (text, { supreq_ctime }, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {dayjs(supreq_ctime * 1000).format("YYYY-MM-DD")}
              <br />
              {dayjs(supreq_ctime * 1000).format("HH:mm")}
            </>
          ),
        };
      },
    },
    {
      title: "Code",
      key: "3",
      align: "center",
      width: "9%",
      render: (text, { supreq_id, code }, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {code}
              <br />
              (ID: {supreq_id})
            </>
          ),
        };
      },
    },
    {
      title: "Request",
      key: "4",
      width: "37%",
      render: (
        text,
        {
          due_date,
          supreq_code,
          reference_type,
          supreq_content,
          supreq_user_id,
          supreq_utime,
          adjustment_reason,
        },
        index
      ) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {due_date ? (
                <>
                  <strong
                    className={
                      due_date > dayjs().unix() ? "text-red" : "text-blue"
                    }
                  >
                    Due Date: {dayjs(due_date * 1000).format("YYYY-MM-DD")}
                  </strong>
                  <br />
                </>
              ) : (
                ""
              )}
              Ref Code: {supreq_code} - Type: {reference_type}
              <br />
              {supreq_content}
              <br />
              <small>
                ( User:
                <span>
                  {users.map(({ id, name }) => {
                    if (supreq_user_id === id) {
                      return name;
                    }
                  })}
                  #{supreq_user_id}
                </span>
                - Modified:
                {dayjs(supreq_utime * 1000).format("YYYY-MM-DD HH:mm")} )
              </small>
              {adjustment_reason === 1 ? (
                <span className="label label-info">
                  <Space>
                    <FontAwesomeIcon icon={faTags} />
                    System Error
                  </Space>
                </span>
              ) : adjustment_reason === 2 ? (
                <span className="label label-warning">
                  <Space>
                    <FontAwesomeIcon icon={faTags} />
                    Mistakes
                  </Space>
                </span>
              ) : (
                ""
              )}
            </>
          ),
        };
      },
    },
    {
      title: "Feedback",
      key: "5",
      width: "37%",
      render: ({ payment_code, feedback_by, department_id }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {payment_code > 1 && (
                <>
                  <Space>
                    <strong>Payment:</strong>
                    <Link
                      to={`/accounting/payment?code=${payment_code}`}
                      target="blank"
                    >
                      {payment_code}
                      <FontAwesomeIcon icon={faExternalLinkAlt} />
                    </Link>
                  </Space>
                  <br />
                </>
              )}

              {users.map(({ id, name }) => {
                if (feedback_by === id) {
                  return (
                    <span>
                      ({name} #{id})
                    </span>
                  );
                }
              })}

              {departments.map(({ id, name }) => {
                if (department_id === id) {
                  return <>({name})</>;
                }
              })}
            </>
          ),
        };
      },
    },
    {
      title: "Amount",
      key: "6",
      align: "center",
      width: "7%",
      render: (text, { payment_amount }, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {payment_amount > 0 && (
                <span className="text-right text-blue">
                  <strong>{formatMoney(payment_amount)}</strong>
                </span>
              )}
            </>
          ),
        };
      },
    },
    {
      title: "Status",
      key: "7",
      align: "center",
      width: "7%",
      render: (text, { supreq_status }, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{CheckStatus(supreq_status)}</>,
        };
      },
    },
    {
      title: null,
      key: "8",
      align: "center",
      width: "3%",
      render: (text, { supreq_id }, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <Button
              className="btn btn-default btn-xs"
              onClick={() => handleView(supreq_id)}
            >
              <Space>
                <FontAwesomeIcon icon={faEdit} />
                VIEW
              </Space>
            </Button>
          ),
        };
      },
    },
  ];

  const onSupReqFormFinish = (formName, { values, forms }) => {
    if (formName === "supReqSearchForm") {
      let { from_create, to_create, code } = values;
      let params = {};

      if (code) {
        params = { code };
        dispatch(getListSupReqRequestAction(params));
      } else {
        params = {
          ...values,
          from_create: from_create.format("YYYY-MM-DD"),
          to_create: to_create.format("YYYY-MM-DD"),
        };
        dispatch(getListSupReqRequestAction(params));
      }
    }
  };

  const handleView = (id) => {
    dispatch(getDetailSupReqRequestAction(id, history.push));
  };

  const onPaginationChange = (current) => {
    setPage(current);
  };

  useEffect(() => {
    setPage(1);
  }, [subReqTblDataSource]);

  return (
    <Form.Provider onFormFinish={onSupReqFormFinish}>
      <div className="container-fluid">
        <section className="content-header">
          <h1>
            Support Request &nbsp;
            <Link
              to="/support-request/create"
              className="btn btn-default btn-sm text-light-blue"
              // type="submit"
            >
              <Space>
                <FontAwesomeIcon icon={faPlus} />
                Add new
              </Space>
            </Link>
          </h1>
        </section>

        <section className="content">
          <SupReqSearchForm initialValues={initialSupReqSearchFormValues} />

          <Row>
            <Col xs={24}>
              <div className="box">
                <SupReqTable
                  columns={subReqTblColumns}
                  dataSource={subReqTblDataSource}
                  onPaginationChange={onPaginationChange}
                />
              </div>
            </Col>
          </Row>
        </section>
      </div>
    </Form.Provider>
  );
}

export default SupportRequest;
