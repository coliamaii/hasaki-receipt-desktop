import { faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Row, Space, Table } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getListNotificationRequestAction } from "share/api/Profile/Notification/GetList/actions";
import Trans from "utils/Trans";
import ProfileMenu from "../components/ProfileMenu";
import NotificationList from "./components/NotificationList";

function Notification(props) {
  const dispatch = useDispatch();

  const [listData, setListData] = useState([]);

  const { name } = useSelector((state) => state.auth.info.profile);
  const { isLoading, list } = useSelector(
    (state) => state.api.profile.notification
  );

  const columns = [
    {
      title: "No.",
      key: "no",
      width: "3%",
      align: "center",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{index + 1}</>,
        };
      },
    },
    {
      title: () => <Trans value="Title" />,
      dataIndex: "title",
      key: "title",
      align: "center",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{text}</>,
        };
      },
    },
    {
      title: () => <Trans value="Message" />,
      dataIndex: "message",
      key: "message",
      align: "center",
      render: (text, record) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{text}</>,
        };
      },
    },
  ];

  useEffect(() => {
    dispatch(getListNotificationRequestAction());
  }, []);

  return (
    <div className="container-fluid">
      <section className="content-header">
        <h1>Profile - {name}</h1>
        <ol className="breadcrumb">
          <li>
            <Space>
              <FontAwesomeIcon icon={faUser} />
              Profile
            </Space>
          </li>
        </ol>
      </section>

      <section className="content">
        <Row>
          <Col md={24}>
            <div className="box">
              <ProfileMenu currentKey="notification" />
              <div className="box-body">
                <Row>
                  <Col md={14}>
                    {/* <Table
                      columns={columns}
                      dataSource={list}
                      loading={isLoading}
                      pagination={false}
                    /> */}
                    <NotificationList listData={list} />
                  </Col>
                </Row>
              </div>
            </div>
          </Col>
        </Row>
      </section>
    </div>
  );
}

Notification.propTypes = {};

export default Notification;
