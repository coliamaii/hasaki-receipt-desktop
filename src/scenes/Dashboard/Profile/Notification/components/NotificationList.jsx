import { faClock } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Avatar, List, Space } from "antd";
import React from "react";

function NotificationList({ listData }) {
  return (
    <List
      itemLayout="horizontal"
      dataSource={listData}
      renderItem={(item) => (
        <List.Item
          style={{
            backgroundColor: "#40a9ff",
            paddingLeft: 20,
            paddingRight: 20,
            borderRadius: 5,
          }}
        >
          <List.Item.Meta
            avatar={
              <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
            }
            title={item.title}
            description={item.message}
          />
          <Space>
            <FontAwesomeIcon icon={faClock} />
            {item.created_at}
          </Space>
        </List.Item>
      )}
    />
  );
}

NotificationList.propTypes = {};

export default NotificationList;
