import { faCheck, faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Form, Row, Space } from "antd";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import ProfileMenu from "../components/ProfileMenu";
import ProfileUpdateForm from "./component/ProfileUpdateForm";

function Profile() {
  const dispatch = useDispatch();
  const history = useHistory();
  const [isAlert, setIsAlert] = useState(false);

  const { name, email, locale } = useSelector(
    (state) => state.auth.info.profile
  );

  const initialProfileUpdateFormValues = {
    nameUser: name,
    emailUser: email,
    locale: locale,
  };

  const onProfileFormFinish = (formName, { values, forms }) => {};

  return (
    <div className="container-fluid">
      <section className="content-header">
        <h1>Profile - {name}</h1>
        <ol className="breadcrumb">
          <li>
            <Space>
              <FontAwesomeIcon icon={faUser} />
              Profile
            </Space>
          </li>
        </ol>
      </section>

      <section className="content">
        <Form.Provider onFormFinish={onProfileFormFinish}>
          <Row>
            <Col md={24}>
              {isAlert && (
                <Row>
                  <Col md={24}>
                    <div class="alert alert-info alert-dismissible">
                      <h4>
                        <Space>
                          <FontAwesomeIcon icon={faCheck} />
                          Info!
                        </Space>
                      </h4>
                      <p>
                        Update profile success! Please logout for changed affect
                      </p>
                    </div>
                  </Col>
                </Row>
              )}
              <div className="box">
                <ProfileMenu currentKey="profile" />

                <ProfileUpdateForm
                  initialValues={initialProfileUpdateFormValues}
                />
              </div>
            </Col>
          </Row>
        </Form.Provider>
      </section>
    </div>
  );
}

export default Profile;
