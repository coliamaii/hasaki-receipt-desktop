import { faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, Form, Input, Row, Select, Space } from "antd";
import PropTypes from "prop-types";
import React from "react";
import Trans from "utils/Trans";

const { Option } = Select;

ProfileUpdateForm.propTypes = {
  initialValues: PropTypes.object,
};

ProfileUpdateForm.defaultProps = {
  initialValues: {},
};

function ProfileUpdateForm({ initialValues }) {
  const [form] = Form.useForm();

  return (
    <Form
      form={form}
      name="profileUpdateForm"
      className="form-horizontal"
      initialValues={initialValues}
    >
      <div className="box-body">
        <Row>
          <Col lg={6} xs={24} sm={16}>
            <Row gutter={[0, 16]}>
              <Col xs={6} className="text-center">
                <label className="control-label" htmlFor="product_name">
                  <Trans value="Name" />
                </label>
              </Col>
              <Col xs={18}>
                <Form.Item name="nameUser" style={{ margin: 0 }}>
                  <Input type="text" readOnly />
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={[0, 16]}>
              <Col xs={6} className="text-center">
                <label className="control-label" htmlFor="product_name">
                  <Trans value="Email" />
                </label>
              </Col>
              <Col xs={18}>
                <Form.Item name="emailUser" style={{ margin: 0 }}>
                  <Input type="text" readOnly />
                </Form.Item>
              </Col>
            </Row>

            <Row>
              <Col xs={6} className="text-center">
                <label className="control-label">
                  <Trans value="Locale" />
                </label>
              </Col>
              <Col xs={18}>
                <Form.Item name="locale" style={{ margin: 0 }}>
                  <Select>
                    <Option value="vi">Tiếng Việt</Option>
                    <Option value="en">English</Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
      <div className="box-footer">
        <Row>
          <Col offset={8}>
            <Button
              style={{
                backgroundColor: "#3c8dbc",
                borderColor: "#367fa9",
              }}
              htmlType="submit"
              type="primary"
            >
              <Space>
                <FontAwesomeIcon icon={faPencilAlt} />
                <Trans value="Update" />
              </Space>
            </Button>
          </Col>
        </Row>
      </div>
    </Form>
  );
}

export default ProfileUpdateForm;
