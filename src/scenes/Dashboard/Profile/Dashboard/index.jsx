import { faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Row, Space } from "antd";
import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import ProfileMenu from "../components/ProfileMenu";

function Dashboard(props) {
  const { id: user_id, name: user_name } = useSelector((state) => state.auth.info.profile);

  const { code } = useSelector((state) => state.auth.info.staff_info);

  return (
    <div className="container-fluid">
      <section className="content-header">
        <h1>
          Profile #{code} - {user_name} (ID: {user_id})
        </h1>
        <ol className="breadcrumb">
          <li>
            <Space>
              <FontAwesomeIcon icon={faUser} />
              Profile
            </Space>
          </li>
          <input type="hidden" id="hd_user_id" value="1013" />
        </ol>
      </section>

      <section className="content">
        <Row>
          <Col md={24}>
            <div className="box">
              <ProfileMenu currentKey="dashboard" />
            </div>
          </Col>
        </Row>

        <Row>
          <Col xs={24}>
            <div className="box">
              <div className="box-header with-border">
                <h3 className="box-title">Dashboard</h3>
              </div>

              <div className="box-body">
                <Row gutter={[32]}>
                  <Col md={6} id="box_tasks">
                    <div className="box box-primary box-solid">
                      <div className="box-header">
                        <h3 className="box-title" style={{ color: "white" }}>
                          Tasks
                        </h3>
                      </div>
                      <div className="box-body">
                        <h3 id="tasks_project">
                          0
                          <small>
                            {" "}
                            <Link to="/support-request">Tasks (Projects)</Link>
                          </small>
                        </h3>

                        <h3 id="tasks_daily">
                          0
                          <small>
                            {" "}
                            <Link to="/support-request">Tasks (Daily)</Link>
                          </small>
                        </h3>
                      </div>
                    </div>
                  </Col>

                  <Col md={6} id="box_support_request">
                    <div className="box box-primary box-solid">
                      <div className="box-header">
                        <h3 className="box-title" style={{ color: "white" }}>
                          Support Request
                        </h3>
                      </div>
                      <div className="box-body">
                        <h3 id="support_request_total">
                          0
                          <small>
                            {" "}
                            <Link to="/support-request">Requests</Link>
                          </small>
                        </h3>
                      </div>
                    </div>
                  </Col>

                  <Col md={6} id="box_Telesales">
                    <div className="box box-primary box-solid">
                      <div className="box-header">
                        <h3 className="box-title" style={{ color: "white" }}>
                          Reminders
                        </h3>
                      </div>
                      <div className="box-body">
                        <h3 id="telesales_total">
                          3{" "}
                          <small>
                            <a
                              target="_blank"
                              rel="noreferrer"
                              href="http://test.inshasaki.com/spa/reminder?date=&amp;feedback_status=22&amp;feedback_by=1013"
                            >
                              Telesales
                            </a>
                          </small>
                        </h3>
                        <h3 id="telesales_callbacks">
                          0<small> Callbacks</small>
                        </h3>
                      </div>
                    </div>
                  </Col>
                </Row>

                <Row gutter={[32]}>
                  <Col md={6} id="box_feeback">
                    <div className="box box-primary box-solid">
                      <div className="box-header">
                        <h3 className="box-title" style={{ color: "white" }}>
                          Feedbacks
                        </h3>
                      </div>
                      <div className="box-body">
                        <h3 id="feedback_total">
                          3{" "}
                          <small>
                            <a
                              target="_blank"
                              rel="noreferrer"
                              href="https://spa.inshasaki.com/cs/feedback?"
                            >
                              Feedbacks
                            </a>
                          </small>
                        </h3>
                      </div>
                    </div>
                  </Col>

                  <Col md={6} id="box_debit">
                    <div className="box box-default box-solid">
                      <div className="box-header">
                        <h3 className="box-title">Debit</h3>
                      </div>
                      <div className="box-body">
                        <h3 id="debit" staff-id="367">
                          499,900,000{" "}
                          <small>
                            <a
                              target="_blank"
                              rel="noreferrer"
                              href="http://test.inshasaki.com/company/staff/367/edit"
                            >
                              Debit
                            </a>
                          </small>
                        </h3>
                      </div>
                    </div>
                  </Col>

                  <Col md={6} id="box_schedule">
                    <div className="box box-default box-solid">
                      <div className="box-header">
                        <h3 className="box-title">Schedule</h3>
                      </div>
                      <div className="box-body">
                        <p>Check In/Out:</p>
                        <p></p>
                        <p>Schedule:</p>
                        <p></p>
                        <p>Location:</p>
                        <p></p>
                      </div>
                    </div>
                  </Col>
                </Row>
              </div>
            </div>
          </Col>
        </Row>
      </section>
    </div>
  );
}

Dashboard.propTypes = {};

export default Dashboard;
