import { faSearch, faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, Form, Row, Space } from "antd";
import _map from "lodash/map";
import moment from "moment";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { getDetailReconcileRequestAction } from "share/api/Profile/Reconcile/GetDetail/actions";
import { getListReconcileRequestAction } from "share/api/Profile/Reconcile/GetList/actions";
import { updateReconcileRequestAction } from "share/api/Profile/Reconcile/Update/actions";
import { getDetailSupReqRequestAction } from "share/api/SupportRequest/GetDetail/actions";
import formatMoney from "utils/money";
import ProfileMenu from "../components/ProfileMenu";
import HistoryTable from "./components/HistoryTable";
import ReconcileForm from "./components/ReconcileForm";
import SupReqBox from "./components/SupReqBox";
import TransactionBox from "./components/TransactionBox";

const formatTime = "YYYY-MM-DD";

function Reconciles() {
  const history = useHistory();
  const dispatch = useDispatch();

  const { profile, stores } = useSelector((state) => state.auth.info);

  const { name: user_name, id: user_id } = profile;

  const {
    detail: reconcileDetail,
    isLoading,
    showViewHistory,
    list: historyTblDataSource,
    sup_req_tbl: supReqTblDataSource,
    balance_trans_tbl: transactionTblDataSource,
    accountList,
  } = useSelector((state) => state.api.profile.reconcile);

  const initialreconcileFormValues = {
    reconcile_begin_balance: 0,
    reconcile_start_time: moment(),

    reconcile_total_cash: 0,
    reconcile_total_card_checkout: 0,
    reconcile_total_return: 0,
    reconcile_difference: 0,
    reconcile_five_hundreds: 0,
    reconcile_two_hundreds: 0,
    reconcile_one_hundreds: 0,
    reconcile_fifties: 0,
    reconcile_twenties: 0,
    reconcile_tens: 0,
    reconcile_fives: 0,
    reconcile_twos: 0,
    reconcile_ones: 0,
    reconcile_note: "",
    reconcile_status: 1,

    properties: {
      voucher: {
        five_hundreds: 0,
        two_hundreds: 0,
        one_hundreds: 0,
        fifties: 0,
        twenties: 0,
        tens: 0,
        ones: 0,

        total_count: 0,
      },
      esteem_gift: {
        five_hundreds: 0,
        two_hundreds: 0,
        one_hundreds: 0,
        fifties: 0,
        twenties: 0,
        tens: 0,
        ones: 0,

        total_count: 0,
      },
    },
  };

  const supReqTblColumns = [
    {
      title: "Date",
      key: "0",
      width: "78%",
      render: ({ supreq_content, supreq_ctime }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              <strong>{supreq_content}</strong>({" "}
              {moment(supreq_ctime * 1000).format(formatTime)} )
            </>
          ),
        };
      },
    },
    {
      title: "Action",
      key: "1",
      align: "center",
      width: "22%",
      render: ({ supreq_id }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <Button
              type="default"
              size="small"
              onClick={() => handleViewClick(supreq_id)}
            >
              <Space>
                <FontAwesomeIcon icon={faSearch} />
                VIEW
              </Space>
            </Button>
          ),
        };
      },
    },
  ];

  const transactionTblColumns = [
    {
      title: "Date",
      key: "0",
      align: "center",
      render: ({ balance_ctime }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <span className="text-center">
              {moment(balance_ctime * 1000).format(formatTime)}
            </span>
          ),
        };
      },
    },
    {
      title: "Content",
      key: "1",
      render: ({ transaction_code, balance_store_id, user, account_id }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <span>
              Payment ( Code: {transaction_code} )
              <br />
              Account:&nbsp;
              {_map(stores, ({ store_id, store_name }) => {
                if (store_id === balance_store_id) {
                  return store_name;
                }
              })}
              &nbsp; (&nbsp;
              {_map(accountList, (obj) => {
                if (obj.id === account_id) {
                  return obj.account_code;
                }
              })}
              &nbsp; )
              <br />
              User: {user.name} ( UID: {user.id} )
            </span>
          ),
        };
      },
    },
    {
      title: "Blance Out",
      key: "2",
      align: "center",
      render: ({ balance_out }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {balance_out > 0 ? (
                <span className="text-center" style={{ color: "red" }}>
                  -{balance_out}
                </span>
              ) : (
                balance_out
              )}
            </>
          ),
        };
      },
    },
  ];

  const historyTblColumns = [
    {
      title: "No.",
      key: "0",
      align: "center",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{index + 1}</>,
        };
      },
    },
    {
      title: "Date",
      key: "1",
      align: "center",
      render: ({ reconcile_start_time }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <span className="text-center">
              {moment(reconcile_start_time * 1000).format(formatTime)}
            </span>
          ),
        };
      },
    },
    {
      title: "Code",
      key: "2",
      align: "center",
      render: ({ reconcile_code, reconcile_id }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {reconcile_code}
              <br />
              (ID: {reconcile_id})
            </>
          ),
        };
      },
    },
    {
      title: "Content",
      key: "3",
      render: ({
        reconcile_note,
        transaction_code,
        reconcile_start_time,
        reconcile_end_time,
      }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              Note: {reconcile_note}
              <br />
              Trans: {transaction_code}
              <br />
              From:{" "}
              {moment(reconcile_start_time * 1000).format(
                "YYYY-MM-DD HH:mm"
              )}{" "}
              To: {moment(reconcile_end_time * 1000).format("YYYY-MM-DD HH:mm")}
            </>
          ),
        };
      },
    },
    {
      title: "Photos",
      key: "4",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <></>,
        };
      },
    },
    {
      title: "Status",
      key: "5",
      align: "center",
      render: ({ reconcile_status }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{reconcile_status === 1 ? "Pending" : "Commited"}</>,
        };
      },
    },
    {
      title: "Diff",
      key: "6",
      align: "center",
      render: ({ reconcile_difference, reconcile_status }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <strong
              className={reconcile_difference < 0 ? "text-red" : "text-blue"}
            >
              {reconcile_status === 2
                ? formatMoney(reconcile_difference)
                : null}
            </strong>
          ),
        };
      },
    },
    {
      title: null,
      key: "7",
      align: "center",
      render: ({ reconcile_id }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <Button
              className="btn btn-default"
              onClick={() => handleViewInHistory(reconcile_id)}
              icon={<FontAwesomeIcon icon={faSearch} />}
            >
              &nbsp; View
            </Button>
          ),
        };
      },
    },
  ];

  const equar = (a, b) => {
    if (a.length !== b.length) {
      return false;
    } else {
      for (let i = 0; i < a.length; i++) {
        if (a[i] !== b[i]) {
          return false;
        }
      }
      return true;
    }
  };

  const onReconcileFormFinish = (formName, { values, forms }) => {
    const { reconcileForm } = forms;

    if (formName === "reconcileForm") {
      dispatch(
        updateReconcileRequestAction(reconcileDetail["reconcile_id"], values)
      );
    }
  };

  const onReconcileFormChange = (formName, { changedFields, forms }) => {
    const { reconcileForm } = forms;

    if (formName === "reconcileForm") {
      console.log("changedFields");
      console.log(changedFields);
      _map(changedFields, ({ name, value }) => {
        if (!equar(name, ["reconcile_note"])) {
          reconcileForm.setFieldsValue({
            [name]: formatMoney(value),
          });
        }
      });
    }
  };

  const handleViewInHistory = (id) => {
    dispatch(getDetailReconcileRequestAction(id));
  };

  const handleViewInReconcile = () => {
    const params = {
      user_id,
      join_images: 1,
      limit: 40,
      sort: "reconcile_id",
      data_accounts: 1,
      data_users: 1,
      total: 1,
    };

    dispatch(getListReconcileRequestAction(params));
  };
  const handleViewClick = (id) => {
    dispatch(getDetailSupReqRequestAction(id, history.push));
  };

  return (
    <div className="container-fluid">
      <section className="content-header">
        <h1>Profile - {user_name}</h1>
        <ol className="breadcrumb">
          <li>
            <Space>
              <FontAwesomeIcon icon={faShoppingCart} />
              Profile
            </Space>
          </li>
        </ol>
      </section>
      <section className="content">
        <Form.Provider
          onFormFinish={onReconcileFormFinish}
          onFormChange={onReconcileFormChange}
        >
          <Row>
            <Col md={24}>
              <div className="box">
                <ProfileMenu currentKey="reconcile" />
              </div>
            </Col>
          </Row>
          {showViewHistory ? (
            <HistoryTable
              dataSource={historyTblDataSource}
              columns={historyTblColumns}
            />
          ) : (
            <Row gutter={[32]}>
              <Col md={12} xs={24}>
                <ReconcileForm
                  defaultValues={initialreconcileFormValues}
                  handleViewInReconcile={handleViewInReconcile}
                />
              </Col>

              <Col md={12} xs={24}>
                <SupReqBox
                  columns={supReqTblColumns}
                  dataSource={supReqTblDataSource}
                />

                <TransactionBox
                  columns={transactionTblColumns}
                  dataSource={transactionTblDataSource}
                />
              </Col>
            </Row>
          )}
        </Form.Provider>
      </section>
    </div>
  );
}

export default Reconciles;
