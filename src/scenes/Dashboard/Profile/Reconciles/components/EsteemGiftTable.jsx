import { Col, Form, Input, Row, Table } from "antd";
import PropTypes from "prop-types";
import React from "react";
import { useSelector } from "react-redux";
import CustomTableAntd from "scenes/Dashboard/components/CustomTableAntd";
import formatMoney from "utils/money";

EsteemGiftTable.propTypes = {
  defaultValues: PropTypes.object,
};

EsteemGiftTable.defaultProps = {
  defaultValues: {},
};

function EsteemGiftTable({ defaultValues }) {
  const {
    properties,
    reconcile_status,
    total_cheque,
    reconcile_total_card_payment,
  } = useSelector((state) => state.api.profile.reconcile.detail);

  const { esteem_gift } = properties ? properties : defaultValues;

  const esteemGiftDataSource = [
    {
      key: "0",
      value: 500000,
      amount: esteem_gift["five_hundreds"],
      formName: "five_hundreds",
    },
    {
      key: "1",
      value: 200000,
      amount: esteem_gift["two_hundreds"],
      formName: "two_hundreds",
    },
    {
      key: "2",
      value: 100000,
      amount: esteem_gift["one_hundreds"],
      formName: "one_hundreds",
    },
    {
      key: "3",
      value: 50000,
      amount: esteem_gift["fifties"],
      formName: "fifties",
    },
    {
      key: "4",
      value: 20000,
      amount: esteem_gift["twenties"],
      formName: "twenties",
    },
    {
      key: "5",
      value: 10000,
      amount: esteem_gift["tens"],
      formName: "tens",
    },
    {
      key: "6",
      value: 1000,
      amount: esteem_gift["ones"],
      formName: "ones",
    },
  ];

  const esteemGiftColumns = [
    {
      title: "4. ESTEEM GIFT",
      dataIndex: "value",
      key: "0",
      align: "left",
      render: (value, record) => {
        return {
          props: {
            style: { padding: "8px 8px" },
          },
          children: (
            <Row style={{ padding: 0 }}>
              <Col xs={8}>
                <strong> {formatMoney(value)}</strong>
              </Col>
              <Col xs={8}>
                <Form.Item
                  style={{ margin: 0 }}
                  name={["esteem_gift", record.formName]}
                >
                  <Input
                    className={
                      reconcile_status === 2
                        ? "form-control text-right"
                        : "text-right"
                    }
                    readOnly={reconcile_status === 2 && true}
                  />
                </Form.Item>
              </Col>
              <Col xs={8} className="text-right">
                {formatMoney(record.amount * value)}
              </Col>
            </Row>
          ),
        };
      },
    },
  ];

  return (
    <Row>
      <Col md={24} xs={24}>
        <CustomTableAntd
          pagination={false}
          columns={esteemGiftColumns}
          dataSource={esteemGiftDataSource}
          summary={(pageData) => {
            let total = 0;

            pageData.forEach((date) => {
              total += date.amount * date.value;
            });

            return (
              <>
                <Table.Summary.Row>
                  <Table.Summary.Cell
                    className="text-right"
                    style={{ padding: "8px 8px" }}
                  >
                    <Row>
                      <Col xs={8}></Col>
                      <Col xs={8}>Total Count:</Col>
                      <Col xs={8}>
                        <strong>
                          {esteem_gift["total_count"]
                            ? formatMoney(esteem_gift["total_count"])
                            : formatMoney(+total)}
                        </strong>
                      </Col>
                    </Row>
                  </Table.Summary.Cell>
                </Table.Summary.Row>
                <Table.Summary.Row>
                  <Table.Summary.Cell
                    style={{ padding: "8px 8px" }}
                    className="text-right text-blue"
                  >
                    <Row>
                      <Col xs={8}></Col>
                      <Col xs={8}>Estimate:</Col>
                      <Col xs={8}>{total_cheque}</Col>
                    </Row>
                  </Table.Summary.Cell>
                </Table.Summary.Row>
              </>
            );
          }}
        />
      </Col>
    </Row>
  );
}

export default EsteemGiftTable;
