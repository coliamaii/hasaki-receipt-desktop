import {
  faPencilAlt,
  faPlus,
  faPrint,
  faUpload,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, DatePicker, Form, Input, Row, Space } from "antd";
import moment from "moment";
import PropTypes from "prop-types";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { commitReconcileRequestAction } from "share/api/Profile/Reconcile/Commit/actions";
import { insertReconcileRequestAction } from "share/api/Profile/Reconcile/Insert/actions";
import formatMoney from "utils/money";
import AlertBar from "./AlertBar";
import CashTable from "./CashTable";
import EsteemGiftTable from "./EsteemGiftTable";
import NoteCard from "./NoteCard";
import ReturnCashDifferenceBar from "./ReturnCashDifferenceBar";
import TotalCardTable from "./TotalCardTable";
import TotalVNPayTable from "./TotalVNPayTable";
import TotalVoucherTable from "./TotalVoucherTable";

ReconcileForm.propTypes = {
  defaultValues: PropTypes.object,
  handleViewInReconcile: PropTypes.func,
};

ReconcileForm.defaultProps = {
  defaultValues: {},
  handleViewInReconcile: [],
};

function ReconcileForm({ defaultValues, handleViewInReconcile }) {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const history = useHistory();

  const { detail: reconcileDetail, isLoading, isCommit } = useSelector(
    (state) => state.api.profile.reconcile
  );

  let formButton;

  const handleClickCommitBtn = () => {
    dispatch(
      commitReconcileRequestAction(reconcileDetail["reconcile_id"], {
        reconcile_status: 2,
      })
    );
  };

  const handleClickPrintBtn = () => {
    history.push("/print/reconcile");
  };

  const handleClickInsertBtn = () => {
    const reconcileForm_allValues = {
      reconcile_begin_balance: form.getFieldValue("reconcile_begin_balance"),
      reconcile_start_time: form
        .getFieldValue("reconcile_start_time")
        .format("YYYY-MM-DD HH:mm"),
    };

    dispatch(insertReconcileRequestAction(reconcileForm_allValues));
  };

  switch (reconcileDetail["reconcile_status"]) {
    case 1:
      formButton = (
        <Form.Item style={{ margin: 0 }} name="reconcile_status">
          <Row>
            <Col xs={12} className="text-left">
              <Button
                type="primary"
                htmlType="submit"
                loading={isLoading}
                icon={<FontAwesomeIcon icon={faPencilAlt} />}
              >
                &nbsp; Update
              </Button>
            </Col>
            <Col xs={12} className="text-right">
              <Button
                type="primary"
                loading={isCommit}
                onClick={handleClickCommitBtn}
                icon={<FontAwesomeIcon icon={faUpload} />}
                style={{ backgroundColor: "#f39c12", borderColor: "#e08e0b" }}
              >
                &nbsp; Commit
              </Button>
            </Col>
          </Row>
        </Form.Item>
      );

      break;
    case 2:
      formButton = (
        <Button onClick={handleClickPrintBtn}>
          <Space>
            <FontAwesomeIcon icon={faPrint} />
            Print
          </Space>
        </Button>
      );
      break;

    default:
      formButton = (
        <Button
          type="primary"
          onClick={handleClickInsertBtn}
          loading={isLoading}
          icon={<FontAwesomeIcon icon={faPlus} />}
        >
          &nbsp; Insert
        </Button>
      );
      break;
  }

  useEffect(() => {
    if (reconcileDetail && Object.keys(reconcileDetail).length !== 0) {
      const { voucher, esteem_gift } = reconcileDetail["properties"];

      form.setFieldsValue({
        reconcile_begin_balance: formatMoney(
          reconcileDetail["reconcile_begin_balance"]
        ),
        reconcile_start_time: moment(
          reconcileDetail["reconcile_start_time"] * 1000
        ),
        reconcile_note: reconcileDetail["reconcile_note"],
        reconcile_total_cash: formatMoney(
          reconcileDetail["reconcile_total_cash"]
        ),
        reconcile_difference: formatMoney(
          reconcileDetail["reconcile_difference"]
        ),
        reconcile_total_return: formatMoney(
          reconcileDetail["reconcile_total_return"]
        ),

        reconcile_total_card_checkout: formatMoney(
          reconcileDetail["reconcile_total_card_checkout"]
        ),

        reconcile_five_hundreds: reconcileDetail["reconcile_five_hundreds"],
        reconcile_two_hundreds: reconcileDetail["reconcile_two_hundreds"],
        reconcile_one_hundreds: reconcileDetail["reconcile_one_hundreds"],
        reconcile_fifties: reconcileDetail["reconcile_fifties"],
        reconcile_twenties: reconcileDetail["reconcile_twenties"],
        reconcile_tens: reconcileDetail["reconcile_tens"],
        reconcile_ones: reconcileDetail["reconcile_ones"],

        reconcile_total_count: reconcileDetail["reconcile_total_count"],

        voucher,
        esteem_gift,
      });
    } else {
      form.setFieldsValue(defaultValues);
    }
  }, [reconcileDetail]);

  return (
    <div className="box box-default">
      <Form
        form={form}
        scrollToFirstError
        name="reconcileForm"
        initialValues={defaultValues}
      >
        {reconcileDetail["reconcile_code"] && (
          <div className="box-header with-border">
            <h3 className="box-title">
              CODE: {reconcileDetail["reconcile_code"]}{" "}
            </h3>
          </div>
        )}

        <div className="box-body">
          {reconcileDetail["reconcile_id"] &&
          reconcileDetail["reconcile_status"] === 1 &&
          moment(reconcileDetail["reconcile_start_time"] * 1000).format(
            "YYYY-MM-DD"
          ) !== moment().format("YYYY-MM-DD") ? (
            <AlertBar />
          ) : (
            ""
          )}
          <Row gutter={[32]}>
            <Col md={12} xs={24}>
              <Form.Item
                label="Begin balance"
                labelCol={{ span: 24 }}
                name="reconcile_begin_balance"
              >
                <Input
                  readOnly={reconcileDetail["reconcile_status"] && true}
                  className={
                    reconcileDetail["reconcile_status"] && "form-control"
                  }
                />
              </Form.Item>
            </Col>
            <Col md={12} xs={24}>
              <Form.Item
                label="Start date:"
                labelCol={{ span: 24 }}
                name="reconcile_start_time"
              >
                <DatePicker
                  suffixIcon={false}
                  format="YYYY-MM-DD HH:mm"
                  open={reconcileDetail["reconcile_status"] && false}
                  inputReadOnly={reconcileDetail["reconcile_status"] && true}
                  allowClear={
                    reconcileDetail["reconcile_status"] ? false : true
                  }
                  showTime={{ defaultValue: moment("00:00", "HH:mm") }}
                  style={{
                    width: "100%",
                    backgroundColor:
                      reconcileDetail["reconcile_status"] && "#eee",
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
          {reconcileDetail["reconcile_status"] !== undefined && (
            <>
              <TotalCardTable />

              <CashTable />

              <TotalVoucherTable defaultValues={defaultValues} />

              <EsteemGiftTable defaultValues={defaultValues} />

              <TotalVNPayTable />

              <ReturnCashDifferenceBar />

              <NoteCard />
            </>
          )}
        </div>

        <div className="box-footer ">
          <Row>
            <Col md={24} xs={24} className="text-right">
              {formButton}
            </Col>
            <Col md={24} xs={24}>
              <p>&nbsp;</p>
            </Col>

            <Col md={24} xs={24}>
              <Button onClick={handleViewInReconcile}>View History</Button>
            </Col>
          </Row>
        </div>
      </Form>
    </div>
  );
}

export default ReconcileForm;
