import { Col, Form, Input, Row, Table } from "antd";
import React from "react";
import { useSelector } from "react-redux";
import CustomTableAntd from "scenes/Dashboard/components/CustomTableAntd";
import formatMoney from "utils/money";

function TotalVoucherTable({ defaultValues }) {
  const { properties, reconcile_status, total_voucher } = useSelector(
    (state) => state.api.profile.reconcile.detail
  );

  const { voucher } = properties ? properties : defaultValues;

  const voucherTblDataSource = [
    {
      key: "0",
      value: 500000,
      formName: "five_hundreds",
      amount: voucher["five_hundreds"],
    },
    {
      key: "1",
      value: 200000,
      formName: "two_hundreds",
      amount: voucher["two_hundreds"],
    },
    {
      key: "2",
      value: 100000,
      formName: "one_hundreds",
      amount: voucher["one_hundreds"],
    },
    {
      key: "3",
      value: 50000,
      formName: "fifties",
      amount: voucher["fifties"],
    },
    {
      key: "4",
      value: 20000,
      formName: "twenties",
      amount: voucher["twenties"],
    },
    {
      key: "5",
      value: 10000,
      formName: "tens",
      amount: voucher["tens"],
    },
    {
      key: "6",
      value: 1000,
      formName: "ones",
      amount: voucher["ones"],
    },
  ];

  const voucherTblColumns = [
    {
      title: "3. VOUCHER",
      dataIndex: "value",
      key: "0",
      align: "left",
      render: (value, record) => {
        return {
          props: {
            style: { padding: "8px 8px" },
          },
          children: (
            <Row style={{ padding: 0 }}>
              <Col xs={8}>
                <strong> {formatMoney(value)}</strong>
              </Col>
              <Col xs={8}>
                <Form.Item
                  name={["voucher", record.formName]}
                  style={{ margin: 0 }}
                >
                  <Input
                    className={
                      reconcile_status === 2
                        ? "form-control text-right"
                        : "text-right"
                    }
                    readOnly={reconcile_status === 2 && true}
                  />
                </Form.Item>
              </Col>
              <Col xs={8} className="text-right">
                {formatMoney(record.amount * value)}
              </Col>
            </Row>
          ),
        };
      },
    },
  ];

  return (
    <Row>
      <Col md={24} xs={24}>
        <CustomTableAntd
          columns={voucherTblColumns}
          dataSource={voucherTblDataSource}
          pagination={false}
          summary={(pageData) => {
            let total = 0;

            pageData.forEach((date) => {
              total += date.amount * date.value;
            });

            return (
              <>
                <Table.Summary.Row>
                  <Table.Summary.Cell
                    className="text-right"
                    style={{ padding: "8px 8px" }}
                  >
                    <Row>
                      <Col xs={8}></Col>
                      <Col xs={8}>Total Count:</Col>
                      <Col xs={8}>
                        <strong>
                          {formatMoney(
                            voucher["total_count"]
                              ? voucher["total_count"]
                              : +total
                          )}
                        </strong>
                      </Col>
                    </Row>
                  </Table.Summary.Cell>
                </Table.Summary.Row>
                <Table.Summary.Row>
                  <Table.Summary.Cell
                    className="text-right text-blue"
                    style={{ padding: "8px 8px" }}
                  >
                    <Row>
                      <Col xs={8}></Col>
                      <Col xs={8}>Estimate:</Col>
                      <Col xs={8}>{total_voucher}</Col>
                    </Row>
                  </Table.Summary.Cell>
                </Table.Summary.Row>
              </>
            );
          }}
        />
      </Col>
    </Row>
  );
}

TotalVoucherTable.propTypes = {};

export default TotalVoucherTable;
