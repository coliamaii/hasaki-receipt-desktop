import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Form, Input, Row } from "antd";
import dayjs from "dayjs";
import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import CustomTableAntd from "scenes/Dashboard/components/CustomTableAntd";
import formatMoney from "utils/money";

function TotalCardTable(props) {
  const { detail: reconcileDetail } = useSelector(
    (state) => state.api.profile.reconcile
  );

  const totalCardTblColumns = [
    {
      title: (
        <span className="bg-gray disabled text-blue text-left">
          1. TOTAL CARD
        </span>
      ),
      key: "0",
      align: "left",
      render: () => {
        return {
          props: {
            style: { padding: "8px 8px" },
          },
          children: (
            <Row>
              <Col xs={16}>
                Estimate: {formatMoney(reconcileDetail["reconcile_total_cash"])}
                <Link
                  to={`/report-profile/payment-receipt?payment_method=3&
                    date=${dayjs(
                      reconcileDetail["reconcile_start_time"] * 1000
                    ).format("YYYY-MM-DD")}`}
                  className="btn btn-default btn-sm"
                >
                  <FontAwesomeIcon icon={faSearch} />
                </Link>
              </Col>
              <Col xs={8}>
                <Form.Item
                  style={{ margin: 0 }}
                  className=""
                  name="reconcile_total_card_checkout"
                >
                  <Input
                    readOnly={reconcileDetail["reconcile_status"] === 2 && true}
                    className={
                      reconcileDetail["reconcile_status"] === 2
                        ? "form-control text-right"
                        : "text-right"
                    }
                  />
                </Form.Item>
              </Col>
            </Row>
          ),
        };
      },
    },
  ];

  const totalCardTblDataSource = [{ key: "1", col: 1 }];

  return (
    <Row>
      <Col md={24} xs={24}>
        <CustomTableAntd
          pagination={false}
          columns={totalCardTblColumns}
          dataSource={totalCardTblDataSource}
        />
      </Col>
    </Row>
  );
}

export default TotalCardTable;
