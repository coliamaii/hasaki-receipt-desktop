import { faExclamationTriangle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Space } from "antd";
import React from "react";

function AlertBar(props) {
  return (
    <div className="alert alert-warning alert-dismissible">
      <h4>
        <Space>
          <FontAwesomeIcon icon={faExclamationTriangle} /> WARNING!
        </Space>
      </h4>
      Check Reconcile start date.
    </div>
  );
}

export default AlertBar;
