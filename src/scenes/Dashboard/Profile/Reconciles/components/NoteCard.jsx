import { Col, Form, Input, Row } from "antd";
import React from "react";
import { useSelector } from "react-redux";

function NoteCard(props) {
  const { detail: reconcileDetail } = useSelector(
    (state) => state.api.profile.reconcile
  );
  return (
    <Row>
      <Col md={24} xs={24}>
        <Form.Item label="Note" labelCol={{ span: 24 }} name="reconcile_note">
          <Input.TextArea
            readOnly={reconcileDetail["reconcile_status"] === 2 && true}
            className={
              reconcileDetail["reconcile_status"] === 2 && "form-control"
            }
          />
        </Form.Item>
      </Col>
    </Row>
  );
}

NoteCard.propTypes = {};

export default NoteCard;
