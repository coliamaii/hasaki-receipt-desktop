import { faLifeRing, faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Row, Space } from "antd";
import PropTypes from "prop-types";
import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import CustomTableAntd from "scenes/Dashboard/components/CustomTableAntd";

SupReqBox.propTypes = {
  columns: PropTypes.array,
  dataSource: PropTypes.array,
};

SupReqBox.defaultProps = {
  columns: [],
  dataSource: [],
};

function SupReqBox({ columns, dataSource }) {
  const { detail: reconcileDetail } = useSelector(
    (state) => state.api.profile.reconcile
  );

  return (
    <div className="box box-primary">
      <div className="box-header with-border">
        <Space>
          <FontAwesomeIcon icon={faLifeRing} />
          SUPPORT REQUEST
        </Space>
      </div>

      <div className="box-body" style={{ paddingBottom: 0 }}>
        {reconcileDetail["reconcile_status"] === 2 && (
          <Col md={24}>
            <CustomTableAntd
              dataSource={dataSource}
              columns={columns}
              showHeader={false}
              pagination={false}
              footer={() => (
                <Row>
                  <Col xs={24} className="text-right">
                    <Link
                      className="btn btn-default btn-sm"
                      to={`/support-request/create?ref_code=${reconcileDetail["reconcile_code"]}`}
                    >
                      <Space>
                        <FontAwesomeIcon icon={faPlus} />
                        Add New
                      </Space>
                    </Link>
                  </Col>
                </Row>
              )}
            />
          </Col>
        )}
      </div>
    </div>
  );
}

export default SupReqBox;
