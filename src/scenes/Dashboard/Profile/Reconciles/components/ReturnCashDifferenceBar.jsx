import { Col, Form, Input, Row } from "antd";
import React from "react";
import { useSelector } from "react-redux";

function ReturnCashDifferenceBar() {
  const { detail: reconcileDetail } = useSelector(
    (state) => state.api.profile.reconcile
  );

  return (
    <Row gutter={[32]}>
      <Col md={8} xs={24}>
        <Form.Item
          name="reconcile_total_return"
          label="Total Return"
          labelCol={{ span: 24 }}
        >
          <Input className="form-control text-right" readOnly />
        </Form.Item>
      </Col>
      {reconcileDetail["reconcile_status"] === 2 ? (
        <>
          <Col md={8} xs={24}>
            <Form.Item
              name="reconcile_total_cash"
              label="Total Cash"
              labelCol={{ span: 24 }}
            >
              <Input className="form-control text-right" readOnly />
            </Form.Item>
          </Col>

          <Col md={8} xs={24} className="text-right">
            <Form.Item
              name="reconcile_difference"
              label="Difference"
              labelCol={{ span: 24 }}
            >
              <Input className="form-control text-right" readOnly />
            </Form.Item>
          </Col>
        </>
      ) : (
        ""
      )}
    </Row>
  );
}

export default ReturnCashDifferenceBar;
