import { faList } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Row, Space } from "antd";
import PropTypes from "prop-types";
import React from "react";
import { useSelector } from "react-redux";
import CustomTableAntd from "scenes/Dashboard/components/CustomTableAntd";

TransactionBox.propTypes = {
  columns: PropTypes.array,
  dataSource: PropTypes.array,
};

TransactionBox.defaultProps = {
  columns: [],
  dataSource: [],
};

function TransactionBox({ columns, dataSource }) {
  const { reconcile_status } = useSelector(
    (state) => state.api.profile.reconcile.detail
  );

  return (
    <div className="box box-primary">
      <div className="box-header with-border">
        <Space>
          <FontAwesomeIcon icon={faList} />
          TRANSACTIONS
        </Space>
      </div>

      <div className="box-body">
        <Row>
          <Col md={24}>
            {reconcile_status === 2 && (
              <CustomTableAntd
                dataSource={dataSource}
                columns={columns}
                showHeader={false}
                pagination={false}
              />
            )}
          </Col>
        </Row>
      </div>
    </div>
  );
}

TransactionBox.propTypes = {};

export default TransactionBox;
