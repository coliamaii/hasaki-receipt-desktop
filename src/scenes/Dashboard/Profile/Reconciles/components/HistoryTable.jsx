import PropTypes from "prop-types";
import React from "react";
import { useSelector } from "react-redux";
import CustomTableAntd from "scenes/Dashboard/components/CustomTableAntd";

HistoryTable.propTypes = {
  columns: PropTypes.array,
  dataSource: PropTypes.array,
};

HistoryTable.defaultProps = {
  columns: [],
  dataSource: [],
};

function HistoryTable({ columns, dataSource }) {
  const { isLoading } = useSelector((state) => state.api.profile.reconcile);

  return (
    <CustomTableAntd
      columns={columns}
      pagination={false}
      loading={isLoading}
      dataSource={dataSource}
    />
  );
}

export default HistoryTable;
