import { Col, Form, Input, Row, Table } from "antd";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import CustomTableAntd from "scenes/Dashboard/components/CustomTableAntd";
import formatMoney from "utils/money";

function CashCard(props) {
  const [totalCardPayment, setTotalCardPayment] = useState(0);
  
  const { detail: reconcileDetail } = useSelector(
    (state) => state.api.profile.reconcile
  );

  const cashTblDataSource = [
    {
      key: "0",
      value: 500000,
      formName: "five_hundreds",
      amount: reconcileDetail["reconcile_five_hundreds"],
    },
    {
      key: "1",
      value: 200000,
      formName: "two_hundreds",
      amount: reconcileDetail["reconcile_two_hundreds"],
    },
    {
      key: "2",
      value: 100000,
      formName: "one_hundreds",
      amount: reconcileDetail["reconcile_one_hundreds"],
    },
    {
      key: "3",
      value: 50000,
      formName: "fifties",
      amount: reconcileDetail["reconcile_fifties"],
    },
    {
      key: "4",
      value: 20000,
      formName: "twenties",
      amount: reconcileDetail["reconcile_twenties"],
    },
    {
      key: "5",
      value: 10000,
      formName: "tens",
      amount: reconcileDetail["reconcile_tens"],
    },
    {
      key: "6",
      value: 1000,
      formName: "ones",
      amount: reconcileDetail["reconcile_ones"],
    },
  ];

  const cashTblColumns = [
    {
      title: "2. CASH",
      dataIndex: "value",
      key: "0",
      align: "left",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top", padding: "8px 8px" },
          },
          children: (
            <Row style={{ padding: 0 }}>
              <Col xs={8}>
                <strong> {formatMoney(text)}</strong>
              </Col>
              <Col xs={8}>
                <Form.Item
                  style={{ margin: 0 }}
                  name={`reconcile_${record.formName}`}
                  // hasFeedback
                  // validateStatus="validating"
                  // help="The information is being validated..."
                  // rules={[{ type: "number", message: "Chỉ nhập số" }]}
                >
                  <Input
                    className={
                      reconcileDetail["reconcile_status"] === 2
                        ? "form-control text-right"
                        : "text-right"
                    }
                    readOnly={reconcileDetail["reconcile_status"] === 2 && true}
                  />
                </Form.Item>
              </Col>
              <Col xs={8} className="text-right">
                {formatMoney(record.amount * text)}
              </Col>
            </Row>
          ),
        };
      },
    },
  ];

  useEffect(() => {
    setTotalCardPayment(reconcileDetail["reconcile_total_card_payment"]);
  }, [reconcileDetail["reconcile_total_card_payment"]]);

  return (
    <Row>
      <Col md={24} xs={24}>
        <CustomTableAntd
          pagination={false}
          columns={cashTblColumns}
          dataSource={cashTblDataSource}
          summary={(pageData) => {
            let total = 0;

            pageData.forEach((date) => {
              total += date.amount * date.value;
            });

            return (
              <>
                <Table.Summary.Row>
                  <Table.Summary.Cell
                    className="text-right"
                    style={{ padding: "8px 8px" }}
                  >
                    <Row>
                      <Col xs={8}></Col>
                      <Col xs={8}>Total Count:</Col>
                      <Col xs={8}>
                        <strong>{formatMoney(+total)}</strong>
                      </Col>
                    </Row>
                  </Table.Summary.Cell>
                </Table.Summary.Row>
                <Table.Summary.Row>
                  <Table.Summary.Cell
                    className="text-right text-blue"
                    style={{ padding: "8px 8px" }}
                  >
                    <Row>
                      <Col xs={8}></Col>
                      <Col xs={8}>Estimate:</Col>
                      <Col xs={8}>{totalCardPayment}</Col>
                    </Row>
                  </Table.Summary.Cell>
                </Table.Summary.Row>
              </>
            );
          }}
        />
      </Col>
    </Row>
  );
}

export default CashCard;
