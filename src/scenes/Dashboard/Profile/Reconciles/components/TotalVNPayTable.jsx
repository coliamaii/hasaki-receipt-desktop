import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Input, Row } from "antd";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import CustomTableAntd from "scenes/Dashboard/components/CustomTableAntd";

function TotalVNPayTable(props) {
  const [totalVNPay, setTotalVNPay] = useState(0);
  const [totalCardPayment, setTotalCardPayment] = useState(0);

  const { detail: reconcileDetail } = useSelector(
    (state) => state.api.profile.reconcile
  );

  const totalVNPayTblColumns = [
    {
      title: "5. TOTAL VNPay",
      key: "totalVNPay",
      align: "left",
      render: () => {
        return {
          props: {
            style: { padding: "8px 8px" },
          },
          children: (
            <Row>
              <Col xs={16}>
                Estimate:
                <span>
                  <span>{totalCardPayment}</span>&nbsp;
                  <a href="#" className="btn btn-default btn-xs">
                    <FontAwesomeIcon icon={faSearch} />
                  </a>
                </span>
              </Col>
              <Col xs={8}>
                <Input
                  className="form-control text-right"
                  placeholder="0"
                  defaultValue={totalVNPay}
                  readOnly
                />
              </Col>
            </Row>
          ),
        };
      },
    },
  ];

  const totalVNPayTblDataSource = [{ key: "1", col: 1 }];

  useEffect(() => {
    setTotalVNPay(reconcileDetail["total_bank_tranf"]);
  }, [reconcileDetail["total_bank_tranf"]]);

  useEffect(() => {
    setTotalCardPayment(reconcileDetail["reconcile_total_card_payment"]);
  }, [reconcileDetail["reconcile_total_card_payment"]]);

  return (
    <Row>
      <Col md={24}>
        <CustomTableAntd
          columns={totalVNPayTblColumns}
          dataSource={totalVNPayTblDataSource}
          pagination={false}
        />
      </Col>
    </Row>
  );
}

export default TotalVNPayTable;
