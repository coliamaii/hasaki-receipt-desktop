import { RightOutlined } from "@ant-design/icons";
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Menu, Space } from "antd";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

function ProfileMenu({ currentKey }) {
  const { t } = useTranslation();

  return (
    <Menu mode="horizontal" selectedKeys={[currentKey]}>
      <Menu.Item key="dashboard">
        <Link to="/report-profile/dashboard" icon={<RightOutlined />}>
          <Space>
            <FontAwesomeIcon icon={faChevronRight} />
            {t('Dashboard')}
          </Space>
        </Link>
      </Menu.Item>
      <Menu.Item key="reconcile">
        <Link to="/report-profile/reconcile">{t('Reconciles')}</Link>
      </Menu.Item>
      <Menu.Item key="receipt">
        <Link to="/report-profile/receipt">{t('Receipts')}</Link>
      </Menu.Item>
      <Menu.Item key="consultant">
        <Link to="/report-profile/consultant">{t('Consultant')}</Link>
      </Menu.Item>
      <Menu.Item key="timesheet">
        <Link to="/report-profile/time-sheet">{t('TimeSheet')}</Link>
      </Menu.Item>
      <Menu.Item key="profile">
        <Link to="/report-profile">{t('Profile')}</Link>
      </Menu.Item>
      <Menu.Item key="notification">
        <Link to="/report-profile/notification">{t('Notification')}</Link>
      </Menu.Item>
    </Menu>
  );
}

ProfileMenu.propTypes = {};

export default ProfileMenu;
