import { faList, faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, DatePicker, Form, Input, Row, Select, Space } from "antd";
import _map from "lodash/map";
import PropTypes from "prop-types";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { options_payment_method } from "share/options/payrec_options";

PayRecSearchForm.propTypes = {
  initialValues: PropTypes.object,
};

PayRecSearchForm.defaultProps = {
  initialValues: {},
};

const { Option } = Select;

function PayRecSearchForm({ initialValues }) {
  const [form] = Form.useForm();
  const { isLoading } = useSelector(
    (state) => state.api.profile.paymentReceipt
  );

  useEffect(() => {
    form.submit();
  }, []);

  return (
    <Form form={form} name="payRecSearchForm" initialValues={initialValues}>
      <Row gutter={[32]}>
        <Col md={4}>
          <div className="input-group input-group">
            <span className="input-group-addon">Date</span>
            <Form.Item name="date" style={{ margin: 0 }}>
              <DatePicker format="YYYY-MM-DD" placeholder="YYYY-MM-DD" />
            </Form.Item>
          </div>
        </Col>

        <Col md={4}>
          <Form.Item name="payment_method" style={{ margin: 0 }}>
            <Select>
              <Option value={0}>-All Payment-</Option>
              {_map(options_payment_method, ({ value, label }) => (
                <Option value={value}>{label}</Option>
              ))}
            </Select>
          </Form.Item>
        </Col>

        <Col md={4}>
          <Form.Item name="code" style={{ margin: 0 }}>
            <Input placeholder="Payment Code,Receipt Code" />
          </Form.Item>
        </Col>

        <Col md={4}>
          <Form.Item style={{ margin: 0 }}>
            <Button
              type="primary"
              htmlType="submit"
              loading={isLoading}
              icon={<FontAwesomeIcon icon={faSearch} />}
            >
              &nbsp; Search
            </Button>
          </Form.Item>
        </Col>

        <Col md={6}>
          <Link
            className="btn btn-default btn-block"
            to="/report-profile/receipt"
          >
            <Space>
              <FontAwesomeIcon icon={faList} />
              Receipts
            </Space>
          </Link>
        </Col>
      </Row>
    </Form>
  );
}

export default PayRecSearchForm;
