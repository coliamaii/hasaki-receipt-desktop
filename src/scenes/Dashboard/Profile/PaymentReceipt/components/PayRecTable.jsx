import { Table } from "antd";
import Text from "antd/lib/typography/Text";
import PropTypes from "prop-types";
import React from "react";
import { useSelector } from "react-redux";
import CustomTableAntd from "scenes/Dashboard/components/CustomTableAntd";
import formatMoney from "utils/money";

PayRecTable.propTypes = {
  dataSource: PropTypes.array,
  columns: PropTypes.array,
};

PayRecTable.defaultProps = {
  dataSource: [],
  columns: [],
};

function PayRecTable({ dataSource, columns }) {
  const { isLoading } = useSelector(
    (state) => state.api.profile.paymentReceipt
  );

  return (
    <CustomTableAntd
      columns={columns}
      dataSource={dataSource}
      loading={isLoading}
      pagination={false}
      summary={(pageData) => {
        let sum = 0;

        pageData.map(({ total }) => {
          sum += total;
        });
        return (
          <>
            <Table.Summary.Row>
              <Table.Summary.Cell />
              <Table.Summary.Cell />
              <Table.Summary.Cell />
              <Table.Summary.Cell />
              <Table.Summary.Cell />

              <Table.Summary.Cell className="text-center">
                <Text style={{ fontWeight: "bold" }} className="text-right lead">
                  <strong>{formatMoney(sum)}</strong>
                </Text>
              </Table.Summary.Cell>
            </Table.Summary.Row>
          </>
        );
      }}
    />
  );
}

export default PayRecTable;
