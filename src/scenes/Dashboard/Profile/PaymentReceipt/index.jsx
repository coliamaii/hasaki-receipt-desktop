import { faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Form, Row, Space } from "antd";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import { getListPaymentReceiptRequestAction } from "share/api/Profile/Payment-Receipt/GetList/actions";
import { getSearchRCPaymentReceiptRequestAction } from "share/api/Profile/Payment-Receipt/Search/actions";
import formatMoney from "utils/money";
import ProfileMenu from "../components/ProfileMenu";
import PayRecSearchForm from "./components/PayRecSearchForm";
import PayRecTable from "./components/PayRecTable";
import _map from "lodash/map";
import { options_payment_method } from "share/options/payrec_options";

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

function PaymentReceipt(props) {
  const dispatch = useDispatch();

  const query = useQuery();
  const payment_method_param = query.get("payment_method");

  const {
    stores,
    profile: { id: user_id, name: user_name },
  } = useSelector((state) => state.auth.info);

  const { list: payRecTblDataSource } = useSelector(
    (state) => state.api.profile.paymentReceipt
  );

  const initialPayRecSearchFormValues = {
    date: moment(),
    payment_method: payment_method_param === null ? 0 : payment_method_param,
    code: "",
  };

  const payRecTblColumns = [
    {
      title: "No.",
      key: "0",
      width: "3%",
      align: "center",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{index + 1}</>,
        };
      },
    },
    {
      title: "ReceiptCode",
      key: "1",
      align: "center",
      width: "9%",
      render: ({ reference_code }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{reference_code}</>,
        };
      },
    },
    {
      title: "PaymentCode",
      key: "2",
      width: "9%",
      align: "center",
      render: ({ payment_receipt_code }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{payment_receipt_code}</>,
        };
      },
    },
    {
      title: "Content",
      key: "3",
      render: ({ customer_id, customer, store_id, created_at }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {customer !== null && (
                <>
                  {customer.customer_phone} - {customer.customer_name}
                </>
              )}
              (ID: {customer_id})
              <br />
              Store:{" "}
              {_map(stores, (store) => {
                if (store_id === store.store_id) {
                  return store.store_name;
                }
              })}{" "}
              - Created At: {created_at}
            </>
          ),
        };
      },
    },
    {
      title: "Payment",
      key: "4",
      width: "16%",
      align: "center",
      render: ({ payment_method }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {_map(options_payment_method, ({ value, label }) => {
                if (value === payment_method) {
                  return label;
                }
              })}
            </>
          ),
        };
      },
    },
    {
      title: "Total",
      key: "5",
      width: "8%",
      align: "center",
      render: ({ payment_amount }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{formatMoney(payment_amount)}</>,
        };
      },
    },
  ];

  const onPayRecFormFinish = (formName, { values, forms }) => {
    if (formName === "payRecSearchForm") {
      let { code, payment_method, date } = values;
      let reference_code = code;

      if (reference_code.trim() === "") {
        date = date.format("YYYY-MM-DD");

        let from_date = date;
        let to_date = date;
        let total = 0;

        dispatch(
          getListPaymentReceiptRequestAction({
            from_date,
            to_date,
            date,
            user_id,
            total,
            payment_method,
          })
        );
      } else {
        dispatch(getSearchRCPaymentReceiptRequestAction({ reference_code }));
      }
    }
  };

  return (
    <div className="container-fluid">
      <section className="content-header">
        <h1>Profile - {user_name}</h1>
        <ol className="breadcrumb">
          <li>
            <Space>
              <FontAwesomeIcon icon={faUser} />
              Profile
            </Space>
          </li>
        </ol>
      </section>
      <section className="content">
        <Form.Provider onFormFinish={onPayRecFormFinish}>
          <Row>
            <Col md={24}>
              <div className="box">
                <ProfileMenu currentKey="receipt" />
                <div className="box-body">
                  <PayRecSearchForm
                    initialValues={initialPayRecSearchFormValues}
                  />
                </div>
              </div>
            </Col>
          </Row>

          <Row>
            <Col xs={24}>
              <div className="box">
                <div className="box-header">
                  <h3 className="box-title">Receipt</h3>
                </div>

                <div className="box-body">
                  <PayRecTable
                    dataSource={payRecTblDataSource}
                    columns={payRecTblColumns}
                  />
                </div>
              </div>
            </Col>
          </Row>
        </Form.Provider>
      </section>
    </div>
  );
}

export default PaymentReceipt;
