import { faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, Form, Row, Space } from "antd";
import _findLast from "lodash/findLast";
import _map from "lodash/map";
import moment from "moment";
import React, { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import {
  getDetailOfflineReceiptRequestAction,
  getDetailOnlineReceiptRequestAction,
} from "share/api/Profile/Receipt/GetDetail/actions";
import {
  getListOfflineReceiptRequestAction,
  getListReceiptRequestAction,
} from "share/api/Profile/Receipt/GetList/actions";
import formatMoney from "utils/money";
import Trans from "utils/Trans";
import ProfileMenu from "../components/ProfileMenu";
import ReceiptSearchForm from "./components/ReceiptSearchForm";
import ReceiptTable from "./components/ReceiptTable";

function Receipt(props) {
  const dispatch = useDispatch();
  const history = useHistory();

  const [page, setPage] = useState(1);
  const [receiptTblColumns, setReceiptTblColumns] = useState([]);

  let initialReceiptSearchFormValues = {
    status: 0,
    date: moment(),
  };

  const { isOnline } = useSelector((state) => state.mode);
  const { profile, stores } = useSelector((state) => state.auth.info);
  const { name: user_name, id: user_id } = profile;
  const { list: receiptTblDataSource, customers } = useSelector(
    (state) => state.api.profile.receipt
  );

  const onlineReceiptTblColumns = [
    {
      title: "No.",
      key: "0",
      align: "center",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{(page - 1) * 10 + index + 1}</>,
        };
      },
    },
    {
      title: <Trans value="Code" />,
      key: "1",
      align: "center",
      render: ({ receipt_code, receipt_id }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {receipt_code}
              <br />
              (ID: {receipt_id})
            </>
          ),
        };
      },
    },
    {
      title: <Trans value="Customer" />,
      key: "2",
      render: ({ receipt_customer_id }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {receipt_customer_id > 0 ? (
                _map(
                  customers,
                  ({ customer_id, customer_name, customer_phone }) => {
                    if (receipt_customer_id === customer_id) {
                      return (
                        <Fragment key={customer_id}>
                          {customer_name}
                          <br />
                          Phone:{customer_phone}
                          <br />
                          (ID: {customer_id})
                        </Fragment>
                      );
                    }
                  }
                )
              ) : (
                <>(ID: {receipt_customer_id})</>
              )}
            </>
          ),
        };
      },
    },
    {
      title: <Trans value="Info" />,
      key: "3",
      render: ({
        user,
        receipt_type,
        receipt_store_id,
        receipt_cdate,
        receipt_udate,
        receipt_desc,
      }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              <strong>Note:&nbsp;</strong>
              {receipt_desc}
              <br />
              <strong>User:&nbsp;</strong>
              {user.name} (ID:{user.id} )
              <br />
              Type: {receipt_type === 3 ? "SERVICE" : "PAYMENT"}
              <br />
              Store:&nbsp;
              {_map(stores, ({ store_id, store_name }) => {
                if (receipt_store_id === store_id) {
                  return store_name;
                }
              })}
              <br />
              Create:&nbsp;
              {moment(receipt_cdate * 1000).format("YY-MM-DD HH:mm")}
              <br />
              Updated:&nbsp;
              {moment(receipt_udate * 1000).format("YY-MM-DD HH:mm")}
            </>
          ),
        };
      },
    },
    {
      title: <Trans value="Payment" />,
      key: "4",
      align: "center",
      render: ({ receipt_balance }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {receipt_balance > 0 && (
                <strong className="text-red">
                  Balance: {formatMoney(receipt_balance)}
                </strong>
              )}
            </>
          ),
        };
      },
    },
    {
      title: <Trans value="Status" />,
      key: "5",
      align: "center",
      render: ({ receipt_status }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{receipt_status === 2 ? "COMPLETED" : "PENDING"}</>,
        };
      },
    },
    {
      title: <Trans value="Total" />,
      key: "6",
      align: "center",
      render: ({ receipt_total, receipt_total_item }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              <strong>{formatMoney(receipt_total)}</strong>
              <br />
              {receipt_total_item}&nbsp;(items)
            </>
          ),
        };
      },
    },
    {
      title: <Trans value="Action" />,
      key: "7",
      align: "center",
      render: ({ receipt_code }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <Button
              type="default"
              onClick={() => handleClickViewButton({ code: receipt_code })}
            >
              <Trans value="View" />
            </Button>
          ),
        };
      },
    },
  ];

  const onPaginationChange = (current) => {
    setPage(current);
  };

  const handleClickViewButton = (code) => {
    if (isOnline) {
      dispatch(getDetailOnlineReceiptRequestAction(code, history.push));
    } else {
      dispatch(getDetailOfflineReceiptRequestAction(code, history.push));
    }
  };

  const handleClickReceiptTblRow = (record, rowIndex) => {
    return {
      onClick: (event) => {
        handleClickViewButton({ code: record.receipt_code }, history.push);
      },
    };
  };

  const onReceiptFormFinish = (formName, { values, forms }) => {
    if (formName === "receiptSearchForm") {
      let { date, status } = values;
      date = date.format("YYYY-MM-DD");

      const params = {
        user_id,
        status,
        date,
        from_date: date,
        to_date: date,
      };

      if (isOnline) {
        dispatch(getListReceiptRequestAction(params));
      } else {
        dispatch(getListOfflineReceiptRequestAction(params));
      }
    }
  };

  // useEffect(() => {
  // setPage(1);
  // if (isOnline) {
  //   setReceiptTblColumns(onlineReceiptTblColumns);
  // } else {
  //   setReceiptTblColumns(offlineReceiptTblColumns);
  // }
  // }, []);

  useEffect(() => {
    setPage(1);
  }, [receiptTblDataSource]);

  return (
    <div className="container-fluid">
      <section className="content-header">
        <h1>Profile - {user_name}</h1>
        <ol className="breadcrumb">
          <li>
            <Space>
              <FontAwesomeIcon icon={faUser} />
              Profile
            </Space>
          </li>
        </ol>
      </section>
      <section className="content">
        <Form.Provider onFormFinish={onReceiptFormFinish}>
          <Row>
            <Col md={24}>
              <div className="box">
                <ProfileMenu currentKey="receipt" />

                <div className="box-body">
                  <ReceiptSearchForm
                    initialValues={initialReceiptSearchFormValues}
                  />
                </div>
              </div>
            </Col>
          </Row>

          <Row>
            <Col xs={24}>
              <div className="box">
                <div className="box-header">
                  <h3 className="box-title">
                    <Trans value="Receipt" />
                  </h3>
                </div>

                <ReceiptTable
                  dataSource={receiptTblDataSource}
                  columns={onlineReceiptTblColumns}
                  onRow={handleClickReceiptTblRow}
                  onPaginationChange={onPaginationChange}
                />
              </div>
            </Col>
          </Row>
        </Form.Provider>
      </section>
    </div>
  );
}

export default Receipt;
