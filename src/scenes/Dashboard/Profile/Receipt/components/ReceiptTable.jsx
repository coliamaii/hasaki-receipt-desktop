import { css } from "@emotion/css";
import { Table } from "antd";
import React from "react";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";
import formatMoney from "utils/money";
import Text from "antd/lib/typography/Text";

ReceiptTable.propTypes = {
  dataSource: PropTypes.array,
  columns: PropTypes.array,
  onRow: PropTypes.func,
  onPaginationChange: PropTypes.func,
};

ReceiptTable.defaultProps = {
  dataSource: [],
  columns: [],
  onRow: () => {
    return;
  },
  onPaginationChange: () => {
    return;
  },
};

function ReceiptTable({ dataSource, columns, onRow, onPaginationChange }) {
  const { isLoading } = useSelector((state) => state.api.profile.receipt);

  const tableCSS = css({
    "& table": {
      borderCollapse: "collapse",
    },
    "& thead > tr > th": {
      backgroundColor: "#E2E4EA !important",
      fontWeight: "bold",
      color: "gray",
      textAlign: "center",
    },

    "& .ant-table-placeholder": {
      display: "none",
    },

    "& thead > tr > th, & tbody > tr > td, & tfoot > tr > td": {
      padding: 8,
    },

    "& .ant-table-tbody > tr.ant-table-row:hover > td": {
      background: "#1890ff",
    },
  });

  let locale = {
    emptyText: " ",
  };

  return (
    <Table
      loading={isLoading}
      locale={locale}
      className={tableCSS}
      columns={columns}
      dataSource={dataSource}
      onRow={onRow}
      // pagination={{
      //   pageSize: 10,
      //   onChange(current) {
      //     onPaginationChange(current);
      //   },
      //   itemRender: (current, type, originalElement) => {
      //     if (type === "prev") {
      //       return <a>Prev</a>;
      //     }
      //     if (type === "next") {
      //       return <a>Next</a>;
      //     }
      //     return originalElement;
      //   },
      // }}
      pagination={false}
      summary={(pageData) => {
        let sumTotal = 0;
        let sumItem = 0;

        pageData.map(({ receipt_total, receipt_total_item }) => {
          sumTotal += receipt_total;
          sumItem += receipt_total_item;
        });
        return (
          <>
            <Table.Summary.Row>
              <Table.Summary.Cell />
              <Table.Summary.Cell />
              <Table.Summary.Cell />
              <Table.Summary.Cell />
              <Table.Summary.Cell />
              <Table.Summary.Cell />

              <Table.Summary.Cell className="text-center">
                <Text
                  style={{ fontWeight: "bold" }}
                  className="text-right lead"
                >
                  <strong>{formatMoney(sumTotal)}</strong>
                </Text>
                <br />
                {formatMoney(sumItem)}&nbsp;(items)
              </Table.Summary.Cell>
            </Table.Summary.Row>
          </>
        );
      }}
    />
  );
}

export default ReceiptTable;
