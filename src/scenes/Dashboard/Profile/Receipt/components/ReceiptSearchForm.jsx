import { faList, faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, DatePicker, Form, Row, Select, Space } from "antd";
import PropTypes from "prop-types";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import Trans from "utils/Trans";
import { options_status } from "share/options/receipt_options";
import _map from "lodash/map";

ReceiptSearchForm.propTypes = {
  initialValues: PropTypes.object,
};

ReceiptSearchForm.defaultProps = {
  initialValues: {},
};

const { Option } = Select;

function ReceiptSearchForm({ initialValues }) {
  const [form] = Form.useForm();

  const { isLoading } = useSelector((state) => state.api.profile.receipt);
  const { isOnline } = useSelector((state) => state.mode);

  useEffect(() => {
    form.submit();
  }, [isOnline]);

  useEffect(() => {
    form.submit();
  }, []);

  return (
    <Form form={form} name="receiptSearchForm" initialValues={initialValues}>
      <Row gutter={[32]}>
        <Col md={4}>
          <div className="input-group input-group">
            <span className="input-group-addon">
              <Trans value="Date" />
            </span>
            <Form.Item name="date" style={{ margin: 0 }}>
              <DatePicker format="YYYY-MM-DD" />
            </Form.Item>
          </div>
        </Col>

        <Col md={4}>
          <Form.Item name="status" style={{ margin: 0 }}>
            <Select>
              {_map(options_status, ({ value, label }) => (
                <Option key={value} value={value}>
                  {label}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col>

        <Col md={4}>
          <Button
            type="primary"
            htmlType="submit"
            loading={isLoading}
            icon={<FontAwesomeIcon icon={faSearch} />}
          >
            &nbsp; <Trans value="Search" />
          </Button>
        </Col>

        <Col md={6}>
          <Link
            className="btn btn-default btn-block"
            to="/report-profile/payment-receipt"
          >
            <Space>
              <FontAwesomeIcon icon={faList} />
              <Trans value="Payments" />
            </Space>
          </Link>
        </Col>
      </Row>
    </Form>
  );
}

export default ReceiptSearchForm;
