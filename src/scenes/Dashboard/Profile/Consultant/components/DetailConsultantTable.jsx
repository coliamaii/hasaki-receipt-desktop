import { Table } from "antd";
import PropTypes from "prop-types";
import React from "react";
import { useSelector } from "react-redux";
import formatMoney from "utils/money";
import Trans from "utils/Trans";

DetailConsultantTable.propTypes = {
  dataSource: PropTypes.array,
  columns: PropTypes.array,
};

DetailConsultantTable.defaultProps = {
  dataSource: [],
  columns: [],
};

function DetailConsultantTable({ dataSource, columns }) {
  const { isLoading, summary } = useSelector(
    (state) => state.api.profile.consultant
  );

  return (
    <Table
      loading={isLoading}
      columns={columns}
      dataSource={dataSource}
      pagination={false}
      summary={(pageData) => {
        return (
          <>
            <Table.Summary.Row>
              <Table.Summary.Cell />
              <Table.Summary.Cell />
              <Table.Summary.Cell />
              <Table.Summary.Cell />
              <Table.Summary.Cell />

              <Table.Summary.Cell className="text-right">
                <Trans value="Sum" />
              </Table.Summary.Cell>
              <Table.Summary.Cell className="text-center">
                <span
                  className="text-right lead"
                  style={{ fontWeight: "bold" }}
                >
                  <strong>{formatMoney(summary)}</strong>
                </span>
              </Table.Summary.Cell>
            </Table.Summary.Row>
          </>
        );
      }}
    />
  );
}

export default DetailConsultantTable;
