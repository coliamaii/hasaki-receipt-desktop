import { Table } from "antd";
import Text from "antd/lib/typography/Text";
import PropTypes from "prop-types";
import React from "react";
import { useSelector } from "react-redux";
import CustomTableAntd from "scenes/Dashboard/components/CustomTableAntd";
import formatMoney from "utils/money";
import Trans from "utils/Trans";

DetailConsultantTable.propTypes = {
  dataSource: PropTypes.array,
  columns: PropTypes.array,
};

DetailConsultantTable.defaultProps = {
  dataSource: [],
  columns: [],
};

function DetailConsultantTable({ dataSource, columns }) {
  const { isLoading } = useSelector((state) => state.api.profile.consultant);

  return (
    <CustomTableAntd
      columns={columns}
      dataSource={dataSource}
      loading={isLoading}
      pagination={false}
      summary={(pageData) => {
        let total = 0;

        pageData.forEach(({ amount }) => {
          total += amount;
        });

        return (
          <Table.Summary.Row>
            <Table.Summary.Cell />
            <Table.Summary.Cell />
            <Table.Summary.Cell />

            <Table.Summary.Cell className="text-right">
              <Trans value="Sum" />:
            </Table.Summary.Cell>
            <Table.Summary.Cell className="text-center">
              <Text style={{ fontWeight: "bold" }}>{formatMoney(+total)}</Text>
            </Table.Summary.Cell>
          </Table.Summary.Row>
        );
      }}
    />
  );
}

export default DetailConsultantTable;
