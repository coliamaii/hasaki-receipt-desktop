import { LoadingOutlined } from "@ant-design/icons";
import { faCalendarAlt, faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, DatePicker, Form, Row, Space } from "antd";
import PropTypes from "prop-types";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import Trans from "utils/Trans";

ConsultantSearchForm.propTypes = {
  initialValues: PropTypes.object,
  datePickerFormat: PropTypes.string,
  picker: PropTypes.string,
  isDetail: PropTypes.bool,
  handleDaily: PropTypes.func,
};

ConsultantSearchForm.defaultProps = {
  initialValues: {},
  datePickerFormat: "YYYY-MM",
  picker: "month",
  isDetail: false,
  handleDaily: () => {},
};

function ConsultantSearchForm({
  initialValues,
  datePickerFormat,
  picker,
  isDetail,
  handleDaily,
  isDay,
}) {
  const [form] = Form.useForm();
  const { isLoading } = useSelector((state) => state.api.profile.consultant);

  useEffect(() => {
    form.submit();
  }, []);

  useEffect(() => {
    form.setFieldsValue({ month: isDay });
  }, [isDay]);

  return (
    <Form form={form} name="consultantSearchForm" initialValues={initialValues}>
      <Row gutter={[32]}>
        <Col md={4}>
          <div className="input-group input-group">
            <span className="input-group-addon">
              <Trans value={picker[0].toUpperCase() + picker.slice(1)} />
            </span>
            <Form.Item name="month" style={{ margin: 0 }}>
              <DatePicker
                picker={picker}
                format={datePickerFormat}
                placeholder={datePickerFormat}
              />
            </Form.Item>
          </div>
        </Col>

        <Col md={2}>
          <Form.Item style={{ margin: 0 }}>
            <Button type="primary" htmlType="submit">
              <Space>
                {isLoading ? (
                  <LoadingOutlined />
                ) : (
                  <FontAwesomeIcon icon={faSearch} />
                )}
                <Trans value="Search" />
              </Space>
            </Button>
          </Form.Item>
        </Col>
        {isDetail && (
          <Col md={2}>
            <Button onClick={() => handleDaily(form)}>
              <Space>
                <FontAwesomeIcon icon={faCalendarAlt} />
                <Trans value="Daily" />
              </Space>
            </Button>
          </Col>
        )}
      </Row>
    </Form>
  );
}

export default ConsultantSearchForm;
