import { faSearch, faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, Form, Row, Space } from "antd";
import dayjs from "dayjs";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getListDailyConsultantRequestAction } from "share/api/Profile/Consultant/Daily/actions";
import { getListDetailConsultantRequestAction } from "share/api/Profile/Consultant/Detail/actions";
import { getSummaryConsultantRequestAction } from "share/api/Profile/Consultant/Summary/actions";
import { getListSupReqRequestAction } from "share/api/SupportRequest/GetList/actions";
import formatMoney from "utils/money";
import Trans from "utils/Trans";
import ProfileMenu from "../components/ProfileMenu";
import ConsultantSearchForm from "./components/ConsultantSearchForm";
import ConsultantTable from "./components/ConsultantTable";
import DetailConsultantTable from "./components/DetailConsultantTable";

function Consultant(props) {
  const dispatch = useDispatch();

  const [picker, setPicker] = useState("month");
  const [isDetail, setIsDetail] = useState(false);
  const [isDay, setIsDay] = useState(moment());
  const [consultantTblDataSource, setConsultantTblDataSource] = useState([]);
  const [datePickerFormat, setDatePickerFormat] = useState("YYYY-MM");
  const [
    detailConsultantTblDataSource,
    setDetailConsultantTblDataSource,
  ] = useState([]);

  const { id: user_id, name: user_name } = useSelector(
    (state) => state.auth.info.profile
  );

  const consultantTbl_dataAPI = useSelector(
    (state) => state.api.profile.consultant.rows
  );
  const detailConsultantTbl_dataAPI = useSelector(
    (state) => state.api.profile.consultant.rows
  );

  const { support_status } = useSelector((state) => state.api.supportRequest);

  const initialConsultantSearchFormValues = {
    month: moment().subtract(7, "d"),
  };

  const consultantTblColumns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "0",
      width: "3%",
      align: "center",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{text + 1}</>,
        };
      },
    },
    {
      title: <Trans value="Date" />,
      dataIndex: "date",
      key: "1",
      align: "center",
      render: (text, record) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{text}</>,
        };
      },
    },
    {
      title: <Trans value="Total" />,
      dataIndex: "total",
      key: "2",
      width: "15%",
      align: "center",
      render: (text, record) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{text}</>,
        };
      },
    },
    {
      title: <Trans value="Receipts" />,
      dataIndex: "receipts",
      key: "3",
      width: "15%",
      align: "center",
      render: (text, record) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{text}</>,
        };
      },
    },
    {
      title: <Trans value="Amount" />,
      dataIndex: "amount",
      key: "4",
      width: "16%",
      align: "center",
      render: (text, record) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{formatMoney(text)}</>,
        };
      },
    },
    {
      title: <Trans value="Action" />,
      dataIndex: "action",
      key: "5",
      width: "6%",
      align: "center",
      render: (text, record) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <Button
              className="btn btn-default btn-xs text-blue "
              onClick={() => handleClickDetailBtn(text)}
            >
              <Space>
                <FontAwesomeIcon icon={faSearch} />
                <Trans value="Detail" />
              </Space>
            </Button>
          ),
        };
      },
    },
  ];

  const detailConsultantTblColumns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "0",
      align: "center",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{text + 1}</>,
        };
      },
    },
    {
      title: <Trans value="Date" />,
      dataIndex: "date",
      key: "1",
      align: "center",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{text}</>,
        };
      },
    },
    {
      title: <Trans value="Code" />,
      dataIndex: "code",
      key: "2",
      align: "center",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{text}</>,
        };
      },
    },
    {
      title: <Trans value="Content" />,
      dataIndex: "content",
      key: "3",
      align: "center",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>(UTP: {text})</>,
        };
      },
    },
    {
      title: <Trans value="ReceiptCode" />,
      dataIndex: "receiptcode",
      key: "4",
      align: "center",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{text}</>,
        };
      },
    },
    {
      title: <Trans value="Amount" />,
      dataIndex: "amount",
      key: "5",
      align: "center",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <span className="text-uppercase">{support_status[text].name}</span>
          ),
        };
      },
    },
    {
      title: <Trans value="Status" />,
      dataIndex: "status",
      key: "6",
      align: "center",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{formatMoney(text)}</>,
        };
      },
    },
  ];

  const handleClickDailyBtn = (consultantSearchForm) => {
    consultantSearchForm.submit();
    setIsDetail(false);
  };

  const onConsultantFormFinish = (formName, { values, forms }) => {
    console.log("askdfj");
    if (formName === "consultantSearchForm") {
      const { month } = values;

      let currentMonth = month;
      let status = 2;
      let from_date;
      let to_date;

      if (isDetail) {
        currentMonth = currentMonth.format("YYYY-MM-DD");
        from_date = currentMonth;
        to_date = currentMonth;
      } else {
        from_date = currentMonth.format("YYYY-MM-01");
        to_date = currentMonth.format("YYYY-MM-") + moment().daysInMonth();
      }
      dispatch(
        getListDailyConsultantRequestAction({
          from_date,
          to_date,
          status,
          user_id,
        })
      );
    }
  };

  const handleClickDetailBtn = (date) => {
    setIsDay(moment(date));
    let from_date = date;
    let to_date = date;

    dispatch(
      getListDetailConsultantRequestAction({
        from_date,
        to_date,
        user_id,
      })
    );
    dispatch(
      getSummaryConsultantRequestAction({
        from_date,
        to_date,
        user_id,
      })
    );
    setIsDetail(true);
  };

  useEffect(() => {
    setDatePickerFormat(isDetail ? "YYYY-MM-DD" : "YYYY-MM");
    setPicker(isDetail ? "date" : "month");
  }, [isDetail]);

  useEffect(() => {
    let dataSource_temp = [];
    let consultantTbl_dataKey = Object.keys(consultantTbl_dataAPI);

    consultantTbl_dataKey.map((obj, i) => {
      dataSource_temp.push({
        key: obj.id,
        id: i,
        date: consultantTbl_dataAPI[obj].date,
        total: consultantTbl_dataAPI[obj].count_consultant,
        receipts: obj.created_at,
        amount: consultantTbl_dataAPI[obj].sum_payment,
        action: consultantTbl_dataAPI[obj].date,
      });
    });

    setConsultantTblDataSource([...dataSource_temp]);
  }, [consultantTbl_dataAPI]);

  useEffect(() => {
    if (Array.isArray(detailConsultantTbl_dataAPI)) {
      let dataSource_temp = [];

      detailConsultantTbl_dataAPI.map((obj, i) => {
        dataSource_temp.push({
          key: i,
          id: i,
          date: obj.created_at,
          code: obj.code,
          content: obj.updated_at,
          receiptcode: obj.receipt_code,
          amount: obj.status,
          status: obj.sum_payment_receipt.total,
        });
      });

      setDetailConsultantTblDataSource([...dataSource_temp]);
    }
  }, [detailConsultantTbl_dataAPI]);

  useEffect(() => {
    let from_create = dayjs().format("YYYY-MM-DD");
    let to_create = from_create;
    dispatch(getListSupReqRequestAction({ from_create, to_create, user_id }));
  }, []);

  return (
    <div className="container-fluid">
      <section className="content-header">
        <h1>
          Profile # - {user_name} (ID: {user_id})
        </h1>
        <ol className="breadcrumb">
          <li>
            <Space>
              <FontAwesomeIcon icon={faUser} />
              Profile
            </Space>
          </li>
        </ol>
      </section>

      <section className="content">
        <Form.Provider onFormFinish={onConsultantFormFinish}>
          <Row>
            <Col md={24}>
              <div className="box">
                <ProfileMenu currentKey="consultant" />

                <div className="box-body">
                  <ConsultantSearchForm
                    initialValues={initialConsultantSearchFormValues}
                    isDetail={isDetail}
                    handleDaily={handleClickDailyBtn}
                    datePickerFormat={datePickerFormat}
                    picker={picker}
                    isDay={isDay}
                  />
                </div>
              </div>
            </Col>
          </Row>

          <Row>
            <Col xs={24}>
              <div className="box">
                <div className="box-header with-border">
                  <h3 className="box-title ">
                    <Trans value="Consultants" />
                  </h3>
                </div>

                <div className="box-body">
                  {isDetail ? (
                    <DetailConsultantTable
                      dataSource={detailConsultantTblDataSource}
                      columns={detailConsultantTblColumns}
                    />
                  ) : (
                    <ConsultantTable
                      dataSource={consultantTblDataSource}
                      columns={consultantTblColumns}
                    />
                  )}
                </div>
              </div>
            </Col>
          </Row>
        </Form.Provider>
      </section>
    </div>
  );
}

export default Consultant;
