import Consultant from "./Consultant";
import ReportProfileDashboard from "./Dashboard";
import Notification from "./Notification";
import PaymentReceipt from "./PaymentReceipt";
import Profile from "./Profile";
import Receipt from "./Receipt";
import Reconciles from "./Reconciles";
import HistoryTable from "./Reconciles/components/HistoryTable";
import TimeSheet from "./TimeSheet";

export default [
  {
    key: "dashboard.report-profile",
    name: "Report Profile Dashboard",
    component: ReportProfileDashboard,
    path: "/report-profile/dashboard",
    template: "main",
    exact: true,
    children: [
      {
        key: "report-profile.reconcile",
        name: "Reconciles",
        component: Reconciles,
        path: "/report-profile/reconcile",
        hide: true,
        template: "main",
      },
      {
        key: "reconcile.list",
        name: "History List",
        component: HistoryTable,
        path: "/report-profile/reconcile/list",
        hide: true,
        template: "main",
      },
      {
        key: "report-profile.receipt",
        name: "Receipt",
        component: Receipt,
        path: "/report-profile/receipt",
        hide: true,
        template: "main",
      },
      {
        key: "report-profile.payment-receipt",
        name: "Payment Receipt",
        component: PaymentReceipt,
        path: "/report-profile/payment-receipt",
        hide: true,
        template: "main",
      },
      {
        key: "report-profile.consultant",
        name: "Consultant",
        component: Consultant,
        path: "/report-profile/consultant",
        hide: true,
        template: "main",
      },
      {
        key: "report-profile.time-sheet",
        name: "Time Sheet",
        component: TimeSheet,
        path: "/report-profile/time-sheet",
        hide: true,
        template: "main",
      },
      {
        key: "report-profile.profile",
        name: "Profile",
        component: Profile,
        path: "/report-profile",
        hide: true,
        template: "main",
      },
      {
        key: "report-profile.notification",
        name: "Notification",
        component: Notification,
        path: "/report-profile/notification",
        hide: true,
        template: "main",
      },
    ],
  },
];
