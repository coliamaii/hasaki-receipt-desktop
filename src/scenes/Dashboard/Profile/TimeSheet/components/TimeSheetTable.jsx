import { css } from "@emotion/css";
import { Table } from "antd";
import PropTypes from "prop-types";
import React from "react";
import { useSelector } from "react-redux";

TimeSheetTable.propTypes = {
  dataSource: PropTypes.array,
  columns: PropTypes.array,
};

TimeSheetTable.defaultProps = {
  dataSource: [],
  columns: [],
};

function TimeSheetTable({ dataSource, columns }) {
  const { isLoading } = useSelector((state) => state.api.profile.timesheet);

  const tableCSS = css({
    marginBottom: 20,

    "& table": {
      borderCollapse: "collapse",
    },
    "& thead > tr > th": {
      fontWeight: "bold",
    },

    "& tbody > tr > td": {
      backgroundColor: "#f39c12 !important",
      color: "white",
      padding: 8,
    },
    
    "& .ant-table-placeholder": {
      display: "none",
    },
  });

  let locale = {
    emptyText: " ",
  };

  return (
    <Table
      columns={columns}
      dataSource={dataSource}
      locale={locale}
      className={tableCSS}
      loading={isLoading}
      pagination={false}
      footer={false}
    />
  );
}

export default TimeSheetTable;
