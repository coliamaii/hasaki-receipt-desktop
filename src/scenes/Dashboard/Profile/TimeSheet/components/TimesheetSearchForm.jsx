import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, DatePicker, Form, Row } from "antd";
import PropTypes from "prop-types";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import Trans from "utils/Trans";

TimeSheetSearchForm.propTypes = {
  initialValues: PropTypes.object,
};

TimeSheetSearchForm.defaultProps = {
  initialValues: {},
};

const datePickerFormat = "YYYY-MM-DD";

function TimeSheetSearchForm({ initialValues }) {
  const [form] = Form.useForm();

  const { isLoading } = useSelector((state) => state.api.profile.timesheet);

  useEffect(() => {
    form.submit();
  }, []);

  return (
    <Form form={form} name="TimeSheetSearchForm" initialValues={initialValues}>
      <Row gutter={[32]}>
        <Col md={6}>
          <Form.Item name="from_date" style={{ margin: 0 }}>
            <DatePicker
              placeholder="From Date"
              format={datePickerFormat}
              style={{ width: "100%" }}
            />
          </Form.Item>
        </Col>
        <Col md={6}>
          <Form.Item name="to_date" style={{ margin: 0 }}>
            <DatePicker
              placeholder="To Date"
              format={datePickerFormat}
              style={{ width: "100%" }}
            />
          </Form.Item>
        </Col>
        <Col md={6}>
          <Form.Item style={{ margin: 0 }}>
            <Button
              type="primary"
              htmlType="submit"
              loading={isLoading}
              icon={<FontAwesomeIcon icon={faSearch} />}
            >
              &nbsp; <Trans value="Search" />
            </Button>
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
}

export default TimeSheetSearchForm;
