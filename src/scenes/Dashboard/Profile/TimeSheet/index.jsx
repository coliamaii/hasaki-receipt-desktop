import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Form, Row, Space } from "antd";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getListTimeSheetRequestAction } from "share/api/Profile/TimeSheet/GetList/actions";
import Trans from "utils/Trans";
import ProfileMenu from "../components/ProfileMenu";
import TimeSheetSearchForm from "./components/TimesheetSearchForm";
import TimeSheetTable from "./components/TimeSheetTable";

function TimeSheet(props) {
  const dispatch = useDispatch();

  const [dataSourceTimesheetTbl, setDataSourceTimesheetTbl] = useState([]);

  const {
    locations,
    profile: { id: user_id },
  } = useSelector((state) => state.auth.info);

  const { list: timesheetTbl_dataAPI } = useSelector(
    (state) => state.api.profile.timesheet
  );

  const timeSheetSearchFormInitialValues = {
    from_date: moment().subtract(7, "d"),
    to_date: moment(),
  };

  const timesheetTbl_columns = [
    {
      title: "#",
      dataIndex: "no",
      key: "1",
      width: "3%",
      align: "center",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{text + 1}</>,
        };
      },
    },
    {
      title: <Trans value="Date" />,
      dataIndex: "date",
      key: "date",
      align: "center",
      render: (text, record) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{text}</>,
        };
      },
    },
    {
      title: <Trans value="Location" />,
      dataIndex: "location",
      key: "location",
      width: "15%",
      align: "center",
      render: (text, record) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {locations.map((loc, id) => {
                if (loc.id === text) return loc.name;
              })}
            </>
          ),
        };
      },
    },
    {
      title: <Trans value="Staff Code" />,
      dataIndex: "staffCode",
      key: "staffCode",
      width: "15%",
      align: "center",
      render: (text, record) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{text}</>,
        };
      },
    },
    {
      title: <Trans value="Time In/Out" />,
      dataIndex: "time",
      key: "time",
      width: "16%",
      align: "center",
      render: (text, record) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {text.check_in === null
                ? "--"
                : moment(text.check_in * 1000).format("HH:mm:ss")}
              /
              {text.check_out === null
                ? "--"
                : moment(text.check_out * 1000).format("HH:mm:ss")}
            </>
          ),
        };
      },
    },
    {
      title: <Trans value="Schedule" />,
      dataIndex: "schedule",
      key: "schedule",
      width: "16%",
      align: "center",
      render: (text, record) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{text === null ? "--/--" : "chưa rõ api"}</>,
        };
      },
    },
    {
      title: <Trans value="Status" />,
      dataIndex: "status",
      key: "Status",
      width: "6%",
      align: "center",
      render: (text, record) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>N/A</>,
        };
      },
    },
  ];

  const onTimeSheetFormFinish = (formName, { values, forms }) => {
    console.log(formName);
    console.log(values);

    if (formName === "TimeSheetSearchForm") {
      let { from_date, to_date } = values;
      from_date = from_date.format("YYYY-MM-DD");
      to_date = to_date.format("YYYY-MM-DD");
      dispatch(getListTimeSheetRequestAction({ user_id, from_date, to_date }));
    }
  };

  useEffect(() => {
    let dataSource_temp = [];

    timesheetTbl_dataAPI.map((obj, i) => {
      dataSource_temp.push({
        key: obj.id,
        no: i,
        date: obj.date,
        location: obj.location_id,
        staffCode: obj.staff_code,
        time: {
          check_in: obj.check_in,
          check_out: obj.check_out,
        },
        schedule: obj.staff_schedule,
        status: obj,
      });
    });

    setDataSourceTimesheetTbl([...dataSource_temp]);
  }, [timesheetTbl_dataAPI]);

  return (
    <div className="container-fluid">
      <section className="content-header">
        <h1>Timesheet</h1>
        <ol className="breadcrumb">
          <li>
            <Space>
              <FontAwesomeIcon icon={faShoppingCart} />
              Profile
            </Space>
          </li>
        </ol>
      </section>

      <section className="content">
        <Form.Provider onFormFinish={onTimeSheetFormFinish}>
          <Row>
            <Col md={24}>
              <div className="box">
                <ProfileMenu currentKey="timesheet" />
              </div>
            </Col>
          </Row>

          <Row>
            <Col md={18}>
              <div className="box box-default">
                <div className="box-header">
                  <TimeSheetSearchForm
                    initialValues={timeSheetSearchFormInitialValues}
                  />
                </div>
                <div className="box-body">
                  <TimeSheetTable
                    dataSource={dataSourceTimesheetTbl}
                    columns={timesheetTbl_columns}
                  />
                </div>
              </div>
            </Col>
          </Row>
        </Form.Provider>
      </section>
    </div>
  );
}

export default TimeSheet;
