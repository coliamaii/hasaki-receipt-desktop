import React, { useEffect, useState } from "react";
import Modal from "antd/lib/modal/Modal";
import PropTypes from "prop-types";

CalculatorModal.propTypes = {
  isVisible: PropTypes.bool.isRequired,
  caculatorNum: PropTypes.string.isRequired,
  caculatorId: PropTypes.string.isRequired,
  setNoneDisplay: PropTypes.func.isRequired,
  handleDoneCaculator: PropTypes.func.isRequired,
}

function CalculatorModal({
  isVisible,
  setNoneDisplay,
  handleDoneCaculator,
  caculatorNum,
  caculatorId
}) {
  const [money, setMoney] = useState('0');
  const [firstTime, setFirstTime] = useState(true);

  useEffect(() => {
    setMoney(caculatorNum);
  }, [caculatorNum])

  const handleNumClick = (num) => {
    const getMoney = (firstTime || money === '0') ? '' : money;
    setFirstTime(false);
    setMoney(getMoney.toString().concat(num));
  }

  const handleNumDel = () => {
    let newMoney = money.toString().slice(0, -1);
    if (newMoney === '')
      newMoney = '0';
    setMoney(newMoney);
  }

  return (
    <Modal
      visible={isVisible}
      okButtonProps={{ hidden: true }}
      cancelButtonProps={{ hidden: true }}
      footer={false}
      centered
      onCancel={() => {
        setMoney(money);
        setFirstTime(true);
        setNoneDisplay();
      }}
      closable={false}
      bodyStyle={{ padding: 0 }}
      width={278}
    >
      <table aria-label="CalculatorModal" className="table modal-content nmpd-grid">
        <tbody>
          <tr>
            <td colSpan="4">
              <input
                className="form-control input-lg nmpd-display text-right"
                type="text"
                value={money}
                onChange={(e) => { if (!isNaN(e.target.value)) { setMoney(e.target.value) } }}
              />
            </td>
          </tr>
          <tr>
            <td>
              <button onClick={() => { handleNumClick('7') }} type="button" className="btn btn-default btn-lg numero">
                7
              </button>
            </td>
            <td>
              <button onClick={() => { handleNumClick('8') }} type="button" className="btn btn-default btn-lg numero">
                8
              </button>
            </td>
            <td>
              <button onClick={() => { handleNumClick('9') }} type="button" className="btn btn-default btn-lg numero">
                9
              </button>
            </td>
            <td>
              <button
                type="button"
                className="btn btn-lg del"
                style={{ width: "100%" }}
                onClick={() => { handleNumDel() }}
              >
                Del
              </button>
            </td>
          </tr>
          <tr>
            <td>
              <button onClick={() => { handleNumClick('4') }} type="button" className="btn btn-default btn-lg numero">
                4
              </button>
            </td>
            <td>
              <button aria-label="5-Button" onClick={() => { handleNumClick('5') }} type="button" className="btn btn-default btn-lg numero">
                5
              </button>
            </td>
            <td>
              <button onClick={() => { handleNumClick('6') }} type="button" className="btn btn-default btn-lg numero">
                6
              </button>
            </td>
            <td>
              <button
                type="button"
                className="btn btn-lg clear"
                style={{ width: "100%" }}
                onClick={() => { setMoney('0') }}
              >
                Clear
              </button>
            </td>
          </tr>
          <tr>
            <td>
              <button onClick={() => { handleNumClick('1') }} type="button" className="btn btn-default btn-lg numero">
                1
              </button>
            </td>
            <td>
              <button onClick={() => { handleNumClick('2') }} type="button" className="btn btn-default btn-lg numero">
                2
              </button>
            </td>
            <td>
              <button onClick={() => { handleNumClick('3') }} type="button" className="btn btn-default btn-lg numero">
                3
              </button>
            </td>
            <td>
              <button
                type="button"
                className="btn btn-lg cancel w-100"
                onClick={() => {
                  setMoney('0');
                  setNoneDisplay();
                }}
              >
                Cancel
              </button>
            </td>
          </tr>
          <tr>
            <td>
              <button
                type="button"
                className="btn btn-lg neg w-100"
                style={{ display: "none" }}
              >
                ±
              </button>
            </td>
            <td>
              <button onClick={() => { handleNumClick('0') }} type="button" className="btn btn-default btn-lg numero">
                0
              </button>
            </td>
            <td>
              <button
                type="button"
                className="btn btn-lg sep w-100"
                style={{ display: "none" }}
              >
                ,
              </button>
            </td>
            <td>
              <button
                type="button"
                className="btn btn-lg done btn-primary"
                style={{ width: "100%" }}
                onClick={() => {
                  setMoney('0');
                  setNoneDisplay();
                  setFirstTime(true);
                  handleDoneCaculator(money, caculatorId);
                }}
              >
                Done
              </button>
            </td>
          </tr>
        </tbody>
      </table>
    </Modal>
  );
}

export default CalculatorModal;
