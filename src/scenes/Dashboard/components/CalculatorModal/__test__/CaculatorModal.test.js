const { screen, render, cleanup, waitFor } = require("@testing-library/react");
const { default: userEvent } = require("@testing-library/user-event");
const { default: CalculatorModal } = require("..");

afterEach(cleanup);
beforeEach(() => {
  jest.spyOn(console, 'error').mockImplementation(() => { });
});

describe('Test CalculatorModal when display on screen', () => {
  const renderingComponent = ({ isVisible, value }) => {
    const handleDoneCaculator = jest.fn();
    const setNoneDisplay = jest.fn();

    const { container } = render(
      <CalculatorModal
        handleDoneCaculator={handleDoneCaculator}
        setNoneDisplay={setNoneDisplay}
        isVisible={isVisible}
        caculatorNum={value}
      />
    )

    return { container: container, handleDoneCaculator, setNoneDisplay };
  };

  test('Calculator render correctly', () => {
    const { container } = renderingComponent({ isVisible: true, value: 0 });
    const inputElement = screen.getByRole('textbox');

    expect(inputElement).toBeInTheDocument();
    expect(inputElement.value).toEqual('0');
  });

  test('Clear number on input correctly', () => {
    const { container } = renderingComponent({ isVisible: true, value: 10 });
    const clearBtnElement = screen.queryByText('Clear');
    const inputElement = screen.queryByRole('textbox');

    expect(inputElement.value).toEqual('10');
    expect(clearBtnElement).toBeInTheDocument();
    expect(clearBtnElement.disabled).toEqual(false);

    userEvent.click(clearBtnElement);
    expect(inputElement.value).toEqual('0');
  });

  test('Delete number on input correctly', () => {
    const { container } = renderingComponent({ isVisible: true, value: 100 });
    const inputElement = screen.queryByRole('textbox');
    const deleteBtnComponent = screen.queryByText('Del');

    expect(deleteBtnComponent).toBeInTheDocument();
    expect(deleteBtnComponent.disabled).toEqual(false);

    userEvent.click(deleteBtnComponent);
    expect(inputElement.value).toEqual('10');

    userEvent.click(deleteBtnComponent);
    expect(inputElement.value).toEqual('1');

    userEvent.click(deleteBtnComponent);
    expect(inputElement.value).toEqual('0');

    userEvent.click(deleteBtnComponent);
    expect(inputElement.value).toEqual('0');
  });

  test('Fire event when click done', async () => {
    const { container, handleDoneCaculator } = renderingComponent({ isVisible: true, value: 100 });
    const doneBtnElement = screen.queryByText('Done');

    expect(doneBtnElement).toBeInTheDocument();
    expect(doneBtnElement.disabled).toEqual(false);

    userEvent.click(doneBtnElement);
    expect(handleDoneCaculator).toBeCalledTimes(1);
  });

  test('Click numpad element', () => {
    const { container } = renderingComponent({ isVisible: true, value: 10 });
    const inputElement = screen.queryByRole('textbox');
    const numpad5 = screen.queryByText(5);

    expect(numpad5).toBeInTheDocument();
    expect(numpad5.disabled).toEqual(false);

    userEvent.click(numpad5);
    expect(inputElement.value).toEqual('5');
    userEvent.click(numpad5);
    expect(inputElement.value).toEqual('55');
  });

  test('Change input number directly', () => {
    const { container } = renderingComponent({ isVisible: true, value: 10 });
    const inputElement = screen.queryByRole('textbox');

    userEvent.type(inputElement, '0');
    expect(inputElement.value).toEqual('100');
    userEvent.type(inputElement, 'A');
    expect(inputElement.value).toEqual('100');
    userEvent.type(inputElement, '*');
    expect(inputElement.value).toEqual('100');
  })
});

