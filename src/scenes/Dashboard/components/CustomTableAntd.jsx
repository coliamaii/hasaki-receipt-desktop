import { css } from "@emotion/css";
import { Table } from "antd";
import PropTypes from "prop-types";
import React from "react";

CustomTableAntd.propTypes = {
  loading: PropTypes.bool,
  columns: PropTypes.array,
  dataSource: PropTypes.array,
  pagination: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
  summary: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
  showHeader: PropTypes.bool,
  footer: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.func,
    PropTypes.element,
  ]),
  onRow: PropTypes.func,
};

CustomTableAntd.defaultProps = {
  loading: false,
  columns: [],
  dataSource: [],
  pagination: true,
  summary: false,
  showHeader: true,
  footer: false,
  onRow: (record, index) => {
    return;
  },
};

function CustomTableAntd({
  loading,
  columns,
  dataSource,
  pagination,
  summary,
  showHeader,
  footer,
  onRow,
}) {
  const tableCSS = css({
    marginBottom: 20,

    "& table": {
      borderCollapse: "collapse",
    },
    "& thead > tr > th": {
      backgroundColor: "#E2E4EA !important",
      fontWeight: "bold",
      color: "gray",
      textAlign: "center",
    },

    "& .ant-table-placeholder": {
      display: "none",
    },

    "& thead > tr > th, & tbody > tr > td, & tfoot > tr > td": {
      padding: 8,
    },

    "& .ant-table-tbody > tr.ant-table-row:hover > td": {
      background: "#1890ff",
    },
  });

  let locale = {
    emptyText: " ",
  };

  return (
    <Table
      loading={loading}
      locale={locale}
      showHeader={showHeader}
      className={tableCSS}
      columns={columns}
      dataSource={dataSource}
      pagination={pagination}
      summary={summary}
      onRow={onRow}
      footer={footer}
    />
  );
}

export default CustomTableAntd;
