import React from "react";
import PropTypes from "prop-types";
import { CSVLink } from "react-csv";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileExcel } from "@fortawesome/free-solid-svg-icons";
import { Button, Space } from "antd";

function ExportReactCSV({ csvData, fileName, headers }) {
  return (
    <Button className="text-blue">
      <Space>
        <FontAwesomeIcon icon={faFileExcel} />
        <CSVLink data={csvData} filename={fileName} headers={headers}>
          Excel
        </CSVLink>
      </Space>
    </Button>
  );
}

ExportReactCSV.propTypes = {};

export default ExportReactCSV;
