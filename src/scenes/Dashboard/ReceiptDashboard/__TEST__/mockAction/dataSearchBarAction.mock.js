const { act, screen } = require("@testing-library/react");
const { default: userEvent } = require("@testing-library/user-event");

const enterAndShowSearchResult = (searchInputElement, value) => {
  expect(searchInputElement).toBeInTheDocument();
  userEvent.type(searchInputElement, value);
  act(() => {
    jest.runOnlyPendingTimers(); // after 400ms, if click event do not fired, then call mock api
  });
}

const clickOnASearchingResult = (index) => {
  const searchResultList = screen.queryAllByLabelText('searchBarResultContainerItem');
  userEvent.click(searchResultList[index]);
}

module.exports = {
  enterAndShowSearchResult,
  clickOnASearchingResult,
}