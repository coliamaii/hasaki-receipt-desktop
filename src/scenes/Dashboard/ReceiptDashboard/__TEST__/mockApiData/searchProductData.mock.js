const mockSearchProductData = {
  100390065: {
    "message": "Success",
    "status": 1,
    "code": 200,
    "data": {
      "rows": [
        {
          "product_sku": 100390065,
          "product_barcode": "3614221234969",
          "product_name": "Nước hoa MARC JACOBS DECADENCE EDP 100ML",
          "product_price": 2450000,
          "product_config": 6,
          "product_category_id": 209,
          "product_brand_id": 57
        },
      ],
    },
  },
  223700009: {
    "message": "Success",
    "status": 1,
    "code": 200,
    "data": {
      "rows": [
        {
          "product_sku": 223700009,
          "product_barcode": "8809491941234",
          "product_name": "ANPOY Tattoo Tint  #1 Jan",
          "product_price": 130000,
          "product_config": 4,
          "product_category_id": 209,
          "product_brand_id": 237
        }
      ]
    }
  },
  1234: {
    "message": "Success",
    "status": 1,
    "code": 200,
    "data": {
      "rows": [
        {
          "product_sku": 100390065,
          "product_barcode": "3614221234969",
          "product_name": "Nước hoa MARC JACOBS DECADENCE EDP 100ML",
          "product_price": 2450000,
          "product_config": 6,
          "product_category_id": 209,
          "product_brand_id": 57
        },
        {
          "product_sku": 100190123,
          "product_barcode": "4901234247222",
          "product_name": "SRM Everish utena tẩy tbc trà xanh 135g",
          "product_price": 140000,
          "product_config": 4,
          "product_category_id": 209,
          "product_brand_id": 185
        },
        {
          "product_sku": 100190124,
          "product_barcode": "4901234248021",
          "product_name": "SRM everish utena than hoạt tính 135g",
          "product_price": 140000,
          "product_config": 4,
          "product_category_id": 209,
          "product_brand_id": 185
        },
        {
          "product_sku": 223700009,
          "product_barcode": "8809491941234",
          "product_name": "ANPOY Tattoo Tint  #1 Jan",
          "product_price": 130000,
          "product_config": 4,
          "product_category_id": 209,
          "product_brand_id": 237
        },
        {
          "product_sku": 258700037,
          "product_barcode": "9556006012345",
          "product_name": "Sữa Tắm Gội Tòan Thân Combo Johnson Baby 900ml (Ag)",
          "product_price": 129000,
          "product_config": 4,
          "product_category_id": 3,
          "product_brand_id": 155
        },
        {
          "product_sku": 265500008,
          "product_barcode": "8935238241234",
          "product_name": "Siêu Xe Nhấn Để Chạy Cars 3",
          "product_price": 273873,
          "product_config": 4,
          "product_category_id": 3,
          "product_brand_id": 655
        },
        {
          "product_sku": 301500004,
          "product_barcode": "8936123410131",
          "product_name": "Dung Dịch Vệ Sinh Phụ Nữ Lactacid Ngày Dài Năng Động Tinh Chất Lá Trầu Không  150ml",
          "product_price": 0,
          "product_config": 4,
          "product_category_id": 0,
          "product_brand_id": 1015
        },
        {
          "product_sku": 201800013,
          "product_barcode": "12345678911",
          "product_name": "test tạo sp",
          "product_price": 100000,
          "product_config": 4,
          "product_category_id": 0,
          "product_brand_id": 18
        },
        {
          "product_sku": 303600003,
          "product_barcode": "123456789",
          "product_name": "new product",
          "product_price": 200000,
          "product_config": 4,
          "product_category_id": 0,
          "product_brand_id": 1036
        },
        {
          "product_sku": 203200050,
          "product_barcode": "8934839123437",
          "product_name": "P/S BAN CHAI MUOI TRE 48X1PC",
          "product_price": 0,
          "product_config": 4,
          "product_category_id": 175,
          "product_brand_id": 32
        },
        {
          "product_sku": 203200051,
          "product_barcode": "8934839123444",
          "product_name": "P/S BAN CHAI MUOI HIMALAYA 48X1PC",
          "product_price": 0,
          "product_config": 4,
          "product_category_id": 175,
          "product_brand_id": 32
        }
      ]
    }
  }
}

module.exports = mockSearchProductData;