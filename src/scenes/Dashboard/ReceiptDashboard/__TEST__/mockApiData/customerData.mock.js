const mockCustomerData = {
  '037123456': {
    "message": "Success",
    "status": 1,
    "code": 200,
    "data": {
      "info": {
        "customer_id": 123456,
        "customer_name": "test test test",
        "customer_email": "test@hasaki.vn",
        "customer_phone": "037123456",
        "customer_facebook": "",
        "customer_point": 228124,
        "customer_used_point": 3001,
        "customer_profile": "http://test.inshasaki.com/sales/customer/123456/order"
      },
      "orders": [
        {
          "order_id": 260892,
          "order_code": "20112400001",
          "order_address": "37/9 Nguyen Minh Hoang,Phường Phường 13, Quận Tân Bình, Hồ Chí Minh",
          "order_cdate": "2020-11-24 11:50:33",
          "order_status": "Picking",
          "order_total": "10,000",
          "order_note": "",
          "bg_class": "",
          "pickup_store": 6,
          "order_url": "http://test.inshasaki.com/sales/order/260892/edit"
        },
        {
          "order_id": 253803,
          "order_code": "20011510104",
          "order_address": "555 3/2, Phường 8, Quận 10, Hồ Chí Minh",
          "order_cdate": "2020-01-15 16:17:48",
          "order_status": "Pending",
          "order_total": "27,000",
          "order_note": "",
          "bg_class": "bg-yellow",
          "pickup_store": 6,
          "order_url": "http://test.inshasaki.com/sales/order/253803/edit"
        },
        {
          "order_id": 249827,
          "order_code": "19071910015",
          "order_address": "160/1 Phan Huy Ich, Phường Tân Định, Quận 1, Hồ Chí Minh",
          "order_cdate": "2019-07-19 15:48:18",
          "order_status": "Packed",
          "order_total": "69,000",
          "order_note": "",
          "bg_class": "bg-yellow",
          "pickup_store": 8,
          "order_url": "http://test.inshasaki.com/sales/order/249827/edit"
        },
        {
          "order_id": 248774,
          "order_code": "19042210003",
          "order_address": "160/1 Phan Huy Ich, Phường Tân Định, Quận 1, Hồ Chí Minh",
          "order_cdate": "2019-04-22 13:43:40",
          "order_status": "Completed",
          "order_total": "88,000",
          "order_note": "",
          "bg_class": "",
          "pickup_store": 6,
          "order_url": "http://test.inshasaki.com/sales/order/248774/edit"
        },
        {
          "order_id": 248772,
          "order_code": "19042210002",
          "order_address": "555 duong 3 tháng 2, Phường 8, Quận 10, Hồ Chí Minh",
          "order_cdate": "2019-04-22 13:41:07",
          "order_status": "Shipped",
          "order_total": "69,000",
          "order_note": "",
          "bg_class": "",
          "pickup_store": 6,
          "order_url": "http://test.inshasaki.com/sales/order/248772/edit"
        }
      ]
    }
  },
}

module.exports = mockCustomerData;