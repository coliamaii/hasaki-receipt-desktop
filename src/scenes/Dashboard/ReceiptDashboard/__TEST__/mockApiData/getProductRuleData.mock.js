const mockProductRuleData = [
  //index 0
  {
    "message": "Success",
    "status": 1,
    "code": 200,
    "data": {
      "rows": [
        {
          "sku": 100190123,
          "qty": 1,
          "base_price": 140000,
          "price": 140000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        }
      ],
      "gifts": [],
      "products": {
        "100190123": {
          "product_sku": 100190123,
          "product_name": "Product A",
          "product_barcode": "4901234247222",
          "product_config": 4,
          "product_brand_id": 185
        }
      },
    }
  },
  //index 1
  {
    "message": "Success",
    "status": 1,
    "code": 200,
    "data": {
      "rows": [
        {
          "sku": 100190123,
          "qty": 2,
          "base_price": 140000,
          "price": 140000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        }
      ],
      "gifts": [],
      "products": {
        "100190123": {
          "product_sku": 100190123,
          "product_name": "Product A",
          "product_barcode": "4901234247222",
          "product_config": 4,
          "product_brand_id": 185
        }
      },
    }
  },
  //index 2
  {
    "message": "Success",
    "status": 1,
    "code": 200,
    "data": {
      "rows": [
        {
          "sku": 100190123,
          "qty": 5,
          "base_price": 140000,
          "price": 140000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        }
      ],
      "gifts": [],
      "products": {
        "100190123": {
          "product_sku": 100190123,
          "product_name": "Product A",
          "product_barcode": "4901234247222",
          "product_config": 4,
          "product_brand_id": 185
        }
      },
    }
  },
  //index 3
  {
    "message": "Success",
    "status": 1,
    "code": 200,
    "data": {
      "rows": [
        {
          "sku": 100190123,
          "qty": 5,
          "base_price": 140000,
          "price": 140000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 223700009,
          "qty": 1,
          "base_price": 130000,
          "price": 130000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        }
      ],
      "gifts": [],
      "products": {
        "100190123": {
          "product_sku": 100190123,
          "product_name": "Product A",
          "product_barcode": "4901234247222",
          "product_config": 4,
          "product_brand_id": 185
        },
        "223700009": {
          "product_sku": 223700009,
          "product_name": "Product B",
          "product_barcode": "8809491941234",
          "product_config": 4,
          "product_brand_id": 237
        }
      },
    },
  },
  //index 4
  {
    "message": "Success",
    "status": 1,
    "code": 200,
    "data": {
      "rows": [
        {
          "sku": 100190123,
          "qty": 5,
          "base_price": 140000,
          "price": 140000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 223700009,
          "qty": 2,
          "base_price": 130000,
          "price": 130000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        }
      ],
      "gifts": [],
      "products": {
        "100190123": {
          "product_sku": 100190123,
          "product_name": "Product A",
          "product_barcode": "4901234247222",
          "product_config": 4,
          "product_brand_id": 185
        },
        "223700009": {
          "product_sku": 223700009,
          "product_name": "Product B",
          "product_barcode": "8809491941234",
          "product_config": 4,
          "product_brand_id": 237
        }
      },
    },
  },
  //index 5
  {
    "message": "Success",
    "status": 1,
    "code": 200,
    "data": {
      "rows": [
        {
          "sku": 223700009,
          "qty": 1,
          "base_price": 130000,
          "price": 130000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        }
      ],
      "gifts": [],
      "products": {
        "223700009": {
          "product_sku": 223700009,
          "product_name": "Product B",
          "product_barcode": "8809491941234",
          "product_config": 4,
          "product_brand_id": 237
        }
      },
    },
  },
  //index 6
  {
    "message": "Success",
    "status": 1,
    "code": 200,
    "data": {
      "rows": [
        {
          "sku": 223700009,
          "qty": 1,
          "base_price": 130000,
          "price": 130000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 278700019,
          "qty": 1,
          "base_price": 171000,
          "price": 171000,
          "rule_id": 1254,
          "is_baby_mom_spa_cate": 0,
          "discount": 19000,
          "is_deal": 1,
          "hasaki_rule_id": "0"
        },
      ],
      "gifts": [],
      "products": {
        "223700009": {
          "product_sku": 223700009,
          "product_name": "Product B",
          "product_barcode": "8809491941234",
          "product_config": 4,
          "product_brand_id": 237
        },
        "278700019": {
          "product_sku": 278700019,
          "product_name": "Product C",
          "product_barcode": "8851989061221",
          "product_config": 4,
          "product_brand_id": 787
        }
      },
    }
  },
  //index 7
  {
    "message": "Success",
    "status": 1,
    "code": 200,
    "data": {
      "rows": [
        {
          "sku": 223700009,
          "qty": 1,
          "base_price": 130000,
          "price": 130000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 278700019,
          "qty": 2,
          "base_price": 171000,
          "price": 171000,
          "rule_id": 1254,
          "is_baby_mom_spa_cate": 0,
          "discount": 19000,
          "is_deal": 1,
          "hasaki_rule_id": "0"
        },
      ],
      "gifts": [],
      "products": {
        "223700009": {
          "product_sku": 223700009,
          "product_name": "Product B",
          "product_barcode": "8809491941234",
          "product_config": 4,
          "product_brand_id": 237
        },
        "278700019": {
          "product_sku": 278700019,
          "product_name": "Product C",
          "product_barcode": "8851989061221",
          "product_config": 4,
          "product_brand_id": 787
        }
      },
    }
  },
  //index 8
  {
    "message": "Success",
    "status": 1,
    "code": 200,
    "data": {
      "rows": [
        {
          "sku": 223700009,
          "qty": 1,
          "base_price": 130000,
          "price": 130000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 278700019,
          "qty": 2,
          "base_price": 171000,
          "price": 171000,
          "rule_id": 1254,
          "is_baby_mom_spa_cate": 0,
          "discount": 19000,
          "is_deal": 1,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 100390065,
          "qty": 1,
          "base_price": 2450000,
          "price": 2450000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        }
      ],
      "gifts": [],
      "products": {
        "100390065": {
          "product_sku": 100390065,
          "product_name": "Product D",
          "product_barcode": "3614221234969",
          "product_config": 6,
          "product_brand_id": 57
        },
        "223700009": {
          "product_sku": 223700009,
          "product_name": "Product B",
          "product_barcode": "8809491941234",
          "product_config": 4,
          "product_brand_id": 237
        },
        "278700019": {
          "product_sku": 278700019,
          "product_name": "Product C",
          "product_barcode": "8851989061221",
          "product_config": 4,
          "product_brand_id": 787
        }
      },
    }
  },
  //index 9
  {
    "message": "Success",
    "status": 1,
    "code": 200,
    "data": {
      "rows": [
        {
          "sku": 223700009,
          "qty": 1,
          "base_price": 130000,
          "price": 130000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 278700019,
          "qty": 2,
          "base_price": 171000,
          "price": 171000,
          "rule_id": 1254,
          "is_baby_mom_spa_cate": 0,
          "discount": 19000,
          "is_deal": 1,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 100390065,
          "qty": 1,
          "base_price": 2450000,
          "price": 2450000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 328500002,
          "qty": 1,
          "base_price": 200000,
          "price": 200000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 1,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        }
      ],
      "gifts": [],
      "products": {
        "100390065": {
          "product_sku": 100390065,
          "product_name": "Product D",
          "product_barcode": "3614221234969",
          "product_config": 6,
          "product_brand_id": 57
        },
        "223700009": {
          "product_sku": 223700009,
          "product_name": "Product B",
          "product_barcode": "8809491941234",
          "product_config": 4,
          "product_brand_id": 237
        },
        "278700019": {
          "product_sku": 278700019,
          "product_name": "Product C",
          "product_barcode": "8851989061221",
          "product_config": 4,
          "product_brand_id": 787
        },
        "328500002": {
          "product_sku": 328500002,
          "product_name": "Phiếu mua hàng",
          "product_barcode": "",
          "product_config": 4,
          "product_brand_id": 1285
        }
      },
    }
  },
  //index 10
  {
    "message": "Success",
    "status": 1,
    "code": 200,
    "data": {
      "rows": [
        {
          "sku": 223700009,
          "qty": 1,
          "base_price": 130000,
          "price": 130000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 278700019,
          "qty": 2,
          "base_price": 171000,
          "price": 171000,
          "rule_id": 1254,
          "is_baby_mom_spa_cate": 0,
          "discount": 19000,
          "is_deal": 1,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 100390065,
          "qty": 1,
          "base_price": 2450000,
          "price": 2450000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 328500002,
          "qty": 1,
          "base_price": 200000,
          "price": 200000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 1,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 100240053,
          "qty": 1,
          "base_price": 1078000,
          "price": 1078000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        },
      ],
      "gifts": [],
      "products": {
        "100390065": {
          "product_sku": 100390065,
          "product_name": "Product D",
          "product_barcode": "3614221234969",
          "product_config": 6,
          "product_brand_id": 57
        },
        "223700009": {
          "product_sku": 223700009,
          "product_name": "Product B",
          "product_barcode": "8809491941234",
          "product_config": 4,
          "product_brand_id": 237
        },
        "278700019": {
          "product_sku": 278700019,
          "product_name": "Product C",
          "product_barcode": "8851989061221",
          "product_config": 4,
          "product_brand_id": 787
        },
        "328500002": {
          "product_sku": 328500002,
          "product_name": "Phiếu mua hàng",
          "product_barcode": "",
          "product_config": 4,
          "product_brand_id": 1285
        },
        "100240053": {
          "product_sku": 100240053,
          "product_name": "Product have Gift Code Discount",
          "product_barcode": "3337871328634",
          "product_config": 4,
          "product_brand_id": 16
        },
      },
    }
  },
  //index 11
  {
    "message": "Success",
    "status": 1,
    "code": 200,
    "data": {
      "rows": [
        {
          "sku": 223700009,
          "qty": 1,
          "base_price": 130000,
          "price": 130000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 278700019,
          "qty": 2,
          "base_price": 171000,
          "price": 171000,
          "rule_id": 1254,
          "is_baby_mom_spa_cate": 0,
          "discount": 19000,
          "is_deal": 1,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 100390065,
          "qty": 1,
          "base_price": 2450000,
          "price": 2450000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 328500002,
          "qty": 1,
          "base_price": 200000,
          "price": 200000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 1,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 100240053,
          "qty": 1,
          "base_price": 1078000,
          "price": 1078000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 107800,
          "is_deal": 0,
          "hasaki_rule_id": "0,5"
        },
      ],
      "gifts": [],
      "products": {
        "100390065": {
          "product_sku": 100390065,
          "product_name": "Product D",
          "product_barcode": "3614221234969",
          "product_config": 6,
          "product_brand_id": 57
        },
        "223700009": {
          "product_sku": 223700009,
          "product_name": "Product B",
          "product_barcode": "8809491941234",
          "product_config": 4,
          "product_brand_id": 237
        },
        "278700019": {
          "product_sku": 278700019,
          "product_name": "Product C",
          "product_barcode": "8851989061221",
          "product_config": 4,
          "product_brand_id": 787
        },
        "328500002": {
          "product_sku": 328500002,
          "product_name": "Phiếu mua hàng",
          "product_barcode": "",
          "product_config": 4,
          "product_brand_id": 1285
        },
        "100240053": {
          "product_sku": 100240053,
          "product_name": "Product have Gift Code Discount",
          "product_barcode": "3337871328634",
          "product_config": 4,
          "product_brand_id": 16
        },
      },
    }
  },
  //index 12
  {
    "message": "Success",
    "status": 1,
    "code": 200,
    "data": {
      "rows": [
        {
          "sku": 223700009,
          "qty": 1,
          "base_price": 130000,
          "price": 130000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 278700019,
          "qty": 2,
          "base_price": 171000,
          "price": 171000,
          "rule_id": 1254,
          "is_baby_mom_spa_cate": 0,
          "discount": 19000,
          "is_deal": 1,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 100390065,
          "qty": 1,
          "base_price": 2450000,
          "price": 2450000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 328500002,
          "qty": 1,
          "base_price": 200000,
          "price": 200000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 1,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0"
        },
        {
          "sku": 100240053,
          "qty": 1,
          "base_price": 1078000,
          "price": 1078000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 107800,
          "is_deal": 0,
          "hasaki_rule_id": "0,5"
        },
        {
          "sku": 100750038,
          "qty": 1,
          "base_price": 104000,
          "price": 104000,
          "rule_id": 0,
          "is_baby_mom_spa_cate": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": "0,5,3070"
        },
      ],
      "gifts": {
        "422200043": {
          "sku": 422200043,
          "qty": 1,
          "price": 0,
          "discount": 0,
          "is_deal": 0,
          "hasaki_rule_id": 3070
        }
      },
      "products": {
        "100390065": {
          "product_sku": 100390065,
          "product_name": "Product D",
          "product_barcode": "3614221234969",
          "product_config": 6,
          "product_brand_id": 57
        },
        "223700009": {
          "product_sku": 223700009,
          "product_name": "Product B",
          "product_barcode": "8809491941234",
          "product_config": 4,
          "product_brand_id": 237
        },
        "278700019": {
          "product_sku": 278700019,
          "product_name": "Product C",
          "product_barcode": "8851989061221",
          "product_config": 4,
          "product_brand_id": 787
        },
        "328500002": {
          "product_sku": 328500002,
          "product_name": "Phiếu mua hàng",
          "product_barcode": "",
          "product_config": 4,
          "product_brand_id": 1285
        },
        "100240053": {
          "product_sku": 100240053,
          "product_name": "Product have Gift Code Discount",
          "product_barcode": "3337871328634",
          "product_config": 4,
          "product_brand_id": 16
        },
        "100750038": {
          "product_sku": 100750038,
          "product_name": "Product have gift",
          "product_barcode": "8436097092079",
          "product_config": 4,
          "product_brand_id": 26
        },
        "422200043": {
          "product_sku": 422200043,
          "product_name": "Voucher service",
          "product_barcode": "",
          "product_config": 4,
          "product_brand_id": 1285
        },
      },
    }
  }
];

module.exports = mockProductRuleData;