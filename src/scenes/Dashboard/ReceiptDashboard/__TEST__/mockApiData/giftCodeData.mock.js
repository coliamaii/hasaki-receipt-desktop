const mockGiftCodeData = {
  'HASAKIGIFTCODE': {
    "message": "Success",
    "status": 1,
    "code": 200,
    "data": {
      "error": 0,
      "discount": 109900,
      "total": 1099000,
      "apply": {
        "100240053": {
          "discount": 109900,
          "price": 1099000
        }
      },
      "msg": "Discount 109,900. Applied code HASAKIGIFTCODE. Use discount",
      "coupon_id": 17
    }
  }
}

module.exports = mockGiftCodeData;