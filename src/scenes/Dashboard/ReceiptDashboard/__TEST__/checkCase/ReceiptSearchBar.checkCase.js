const { waitFor, screen } = require("@testing-library/react");
const mockSearchProductData = require("../mockApiData/searchProductData.mock");

const checkSearchResultExist = async (value) => {
  await waitFor(async () => expect(await screen.findAllByLabelText('searchBarResultContainerItem'))
    .toHaveLength(mockSearchProductData[value].data.rows.length));
};

const checkSearchResultLoading = async () => {
  const productSearchBarElement = screen.queryByLabelText('SearchBar-search_data');
  expect(productSearchBarElement).toBeDisabled();
  await waitFor(() => expect(productSearchBarElement).not.toBeDisabled());
  expect(productSearchBarElement.value).toEqual('');
}

module.exports = {
  checkSearchResultExist,
  checkSearchResultLoading,
}