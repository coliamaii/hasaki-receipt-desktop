const { screen } = require('@testing-library/react');
const { default: formatMoney } = require('utils/money');

const checkNormalReceiptItemRenderCorrectly = ({
  receiptItemComponnent,
  sku,
  name,
  base_price,
  discount,
  qty,
  lineTotal
}) => {
  expect(receiptItemComponnent).toHaveTextContent(`${sku}${name}${formatMoney(base_price)}Giam: ${discount}${formatMoney(lineTotal)}`);
  expect(receiptItemComponnent.querySelector('.btn_remove_item')).not.toBeDisabled();
  expect(receiptItemComponnent.querySelector('.table-qty-input')).not.toBeDisabled();
  expect(receiptItemComponnent.querySelector('.table-qty-input').value).toEqual(`${qty}`);
  expect(receiptItemComponnent.querySelector('.btn_increase_qty')).not.toBeDisabled();
}

const checkDiscountItemRenderCorrectly = ({
  receiptItemComponnent,
  sku,
  name,
  base_price,
  discount_price,
  discount,
  qty,
  lineTotal
}) => {
  expect(receiptItemComponnent).toHaveTextContent(`${sku}${name}${formatMoney(base_price)}Giam: ${formatMoney(discount_price)}${formatMoney(lineTotal)}`);
  expect(receiptItemComponnent.querySelector('.btn_remove_item')).not.toBeDisabled();
  expect(receiptItemComponnent.querySelector('.table-qty-input')).not.toBeDisabled();
  expect(receiptItemComponnent.querySelector('.table-qty-input').value).toEqual(`${qty}`);
  expect(receiptItemComponnent.querySelector('.btn_increase_qty')).not.toBeDisabled();
  expect(screen.queryByText(`Giam: ${formatMoney(discount_price)}`)).toBeVisible();
}

const checkSetExpiredItemRenderCorrrectly = ({
  receiptItemComponnent,
  sku,
  name,
  base_price,
  discount,
  qty,
  lineTotal
}) => {
  expect(receiptItemComponnent).toHaveTextContent(`${sku}${name}${formatMoney(base_price)}Giam: ${discount}${formatMoney(lineTotal)}`);
  expect(receiptItemComponnent.querySelector('.btn_remove_item')).not.toBeDisabled();
  expect(receiptItemComponnent.querySelector('.table-qty-input')).toBeDisabled();
  expect(receiptItemComponnent.querySelector('.table-qty-input').value).toEqual(`${qty}`);
  expect(receiptItemComponnent.querySelector('.btn_increase_qty').classList.contains('disabled')).toEqual(true);
}

const checkVoucherItemRenderCorrrectly = ({
  receiptItemComponnent,
  sku,
  name,
  base_price,
  discount,
  qty,
  lineTotal
}) => {
  expect(receiptItemComponnent).toHaveTextContent(`${sku}${name}${formatMoney(base_price)}Giam: ${discount}${formatMoney(lineTotal)}`);
  expect(receiptItemComponnent.querySelector('.btn_remove_item')).not.toBeDisabled();
  expect(receiptItemComponnent.querySelector('.table-qty-input')).toBeDisabled();
  expect(receiptItemComponnent.querySelector('.table-qty-input').value).toEqual(`${qty}`);
  expect(receiptItemComponnent.querySelector('.btn_increase_qty').classList.contains('disabled')).toEqual(true);
  expect(receiptItemComponnent.querySelector('.fa-lock-open')).toBeVisible();
  expect(receiptItemComponnent.querySelector('.fa-lock-open')).not.toBeDisabled();
}

const checkGiftItemRenderCorrectly = ({
  receiptItemComponnent,
  sku,
  name,
  base_price,
  discount,
  qty,
  lineTotal
}) => {
  expect(receiptItemComponnent).toHaveTextContent(`${sku}${name}${formatMoney(base_price)}${formatMoney(lineTotal)}`);
  expect(receiptItemComponnent.querySelector('.btn_remove_item')).not.toBeInTheDocument();
  expect(receiptItemComponnent.querySelector('.table-qty-input')).toBeDisabled();
  expect(receiptItemComponnent.querySelector('.table-qty-input').value).toEqual(`${qty}`);
  expect(receiptItemComponnent.querySelector('.btn_increase_qty')).not.toBeInTheDocument();
}

module.exports = {
  checkNormalReceiptItemRenderCorrectly,
  checkDiscountItemRenderCorrectly,
  checkSetExpiredItemRenderCorrrectly,
  checkVoucherItemRenderCorrrectly,
  checkGiftItemRenderCorrectly,
}