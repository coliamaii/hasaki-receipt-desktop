const { screen } = require('@testing-library/react');
const { default: formatNumber } = require('utils/money');

const checkTotalAndItemRendering = (total, item) => {
  expect(screen.getByLabelText('Total').innerHTML).toEqual(`${formatNumber(total)}`)
  expect(screen.getByLabelText('Item').innerHTML).toEqual(`${item}`)
};

module.exports = {
  checkTotalAndItemRendering,
}