const { render } = require("@testing-library/react");
const { Provider } = require("react-redux");
const { default: rootReducer } = require("reducer");
const { createStore, applyMiddleware } = require("redux");
const { default: createSagaMiddleware } = require('redux-saga');
const { default: rootSaga } = require("saga");
const { default: ReceiptDashboard } = require("..");

const firstTimeRenderingComponent = () => {
  const sagaMiddleware = createSagaMiddleware();
  const mockStore = createStore(
    rootReducer(),
    applyMiddleware(sagaMiddleware),
  )
  sagaMiddleware.run(rootSaga);

  const { container } = render(
    <Provider store={mockStore}>
      <ReceiptDashboard />
    </Provider>
  )

  return { container };
}

module.exports = {
  firstTimeRenderingComponent,
}