const { cleanup, screen, act, waitFor } = require('@testing-library/react');
const { default: userEvent } = require('@testing-library/user-event');
const { firstTimeRenderingComponent } = require('./ReceiptDashboard.mock');
const searchProductApi = require('../../../../apis/sales/product');
const getProductRuleApi = require('../../../../apis/sales/rule');
const searchByPhoneNumberApi = require('../../../../apis/sales/customer');
const giftCodeApi = require('../../../../apis/sales/gift');
const mockSearchProductData = require('./mockApiData/searchProductData.mock');
const mockProductRuleData = require('./mockApiData/getProductRuleData.mock');
const {
  checkNormalReceiptItemRenderCorrectly,
  checkDiscountItemRenderCorrectly,
  checkSetExpiredItemRenderCorrrectly,
  checkVoucherItemRenderCorrrectly,
  checkGiftItemRenderCorrectly,
} = require('./checkCase/ReceiptItem.checkCase');
const { enterAndShowSearchResult, clickOnASearchingResult } = require('./mockAction/dataSearchBarAction.mock');
const { checkSearchResultExist, checkSearchResultLoading } = require('./checkCase/ReceiptSearchBar.checkCase');
const { checkTotalAndItemRendering } = require('./checkCase/TotalItem.checkCase');
const mockCustomerData = require('./mockApiData/customerData.mock');
const mockGiftCodeData = require('./mockApiData/giftCodeData.mock');

afterEach(cleanup);
beforeEach(() => {
  jest.spyOn(console, 'error').mockImplementation(() => { });
});

describe('Test POS', () => {
  test('Component render without crash', () => {
    const { container } = firstTimeRenderingComponent();
  });

  test('On normal scenario without error', async () => {
    //Prepair
    jest.useFakeTimers();
    global.alert = jest.fn();
    const mockSearchProductApi = jest.spyOn(searchProductApi, 'search');
    const mockGetProductRuleApi = jest.spyOn(getProductRuleApi, 'product_rule');
    const mockSearchByPhoneNumberApi = jest.spyOn(searchByPhoneNumberApi, 'searchByPhoneNumber');
    const mockValidateGiftCodeApi = jest.spyOn(giftCodeApi, 'validateGiftCode');
    const { container } = firstTimeRenderingComponent();
    const productSearchBarElement = screen.queryByLabelText('SearchBar-search_data');

    //
    global.window.api = jest.fn().mockResolvedValue(Promise.resolve('1.0.0'));

    //Enter 1234 and show search result
    mockSearchProductApi.mockResolvedValue(Promise.resolve(mockSearchProductData['1234']));
    enterAndShowSearchResult(productSearchBarElement, '1234');
    await checkSearchResultExist('1234');

    //Click one search result normal item
    mockGetProductRuleApi.mockReturnValue(Promise.resolve(mockProductRuleData[0]));
    clickOnASearchingResult(1);
    await checkSearchResultLoading();
    let listReceiptItem = screen.queryAllByLabelText('ReceiptItem');
    expect(listReceiptItem).toHaveLength(1);
    checkNormalReceiptItemRenderCorrectly({
      receiptItemComponnent: listReceiptItem[0],
      base_price: 140000,
      discount: '0',
      qty: 1,
      name: "Product A",
      sku: "100190123",
      lineTotal: 140000
    })
    checkTotalAndItemRendering(140000, 1);

    //Click plus btn to increase qty by 1
    mockGetProductRuleApi.mockClear().mockReturnValue(Promise.resolve(mockProductRuleData[1]));
    userEvent.click(listReceiptItem[0].querySelector('.btn_increase_qty'));
    await checkSearchResultLoading();
    listReceiptItem = screen.queryAllByLabelText('ReceiptItem');
    checkNormalReceiptItemRenderCorrectly({
      receiptItemComponnent: listReceiptItem[0],
      base_price: 140000,
      discount: '0',
      qty: 2,
      name: "Product A",
      sku: "100190123",
      lineTotal: 280000
    })
    checkTotalAndItemRendering(280000, 2);

    //Change qty by click to caculator and change qty
    mockGetProductRuleApi.mockClear().mockReturnValue(Promise.resolve(mockProductRuleData[2]));
    userEvent.click(listReceiptItem[0].querySelector('.table-qty-input'));
    expect(screen.queryByLabelText('CalculatorModal')).toBeVisible();
    expect(screen.queryByLabelText('CalculatorModal').querySelector('input').value).toEqual('2');
    userEvent.click(screen.queryByLabelText('5-Button'));
    expect(screen.queryByLabelText('CalculatorModal').querySelector('input').value).toEqual('5');
    userEvent.click(screen.queryByText('Done'));

    await checkSearchResultLoading();
    listReceiptItem = screen.queryAllByLabelText('ReceiptItem');
    checkNormalReceiptItemRenderCorrectly({
      receiptItemComponnent: listReceiptItem[0],
      base_price: 140000,
      discount: '0',
      qty: 5,
      name: "Product A",
      sku: "100190123",
      lineTotal: 700000
    })
    checkTotalAndItemRendering(700000, 5);

    //Add another product (SKU:223700009), newest item will be top of table
    mockGetProductRuleApi.mockClear().mockReturnValue(Promise.resolve(mockProductRuleData[3]));
    mockSearchProductApi.mockClear().mockReturnValue(Promise.resolve(mockSearchProductData['223700009']));
    userEvent.type(productSearchBarElement, '223700009');
    userEvent.type(productSearchBarElement, '{enter}');
    await checkSearchResultLoading();
    listReceiptItem = screen.queryAllByLabelText('ReceiptItem');
    checkNormalReceiptItemRenderCorrectly({
      receiptItemComponnent: listReceiptItem[0],
      base_price: 130000,
      discount: '0',
      qty: 1,
      name: "Product B",
      sku: "223700009",
      lineTotal: 130000,
    })
    checkNormalReceiptItemRenderCorrectly({
      receiptItemComponnent: listReceiptItem[1],
      base_price: 140000,
      discount: '0',
      qty: 5,
      name: "Product A",
      sku: "100190123",
      lineTotal: 700000
    })
    checkTotalAndItemRendering(830000, 6);

    //Increase item on recent receipt item
    mockGetProductRuleApi.mockClear().mockReturnValue(Promise.resolve(mockProductRuleData[4]));
    userEvent.click(listReceiptItem[0].querySelector('.btn_increase_qty'));
    await checkSearchResultLoading();
    listReceiptItem = screen.queryAllByLabelText('ReceiptItem');
    checkNormalReceiptItemRenderCorrectly({
      receiptItemComponnent: listReceiptItem[0],
      base_price: 130000,
      discount: '0',
      qty: 2,
      name: "Product B",
      sku: "223700009",
      lineTotal: 260000,
    })
    checkTotalAndItemRendering(960000, 7);

    //Delete a item
    mockGetProductRuleApi.mockClear().mockReturnValue(Promise.resolve(mockProductRuleData[2]));
    userEvent.click(listReceiptItem[0].querySelector('.btn_remove_item'));
    await checkSearchResultLoading();
    listReceiptItem = screen.queryAllByLabelText('ReceiptItem');
    checkNormalReceiptItemRenderCorrectly({
      receiptItemComponnent: listReceiptItem[0],
      base_price: 140000,
      discount: '0',
      qty: 5,
      name: "Product A",
      sku: "100190123",
      lineTotal: 700000
    })
    checkTotalAndItemRendering(700000, 5);

    //Delete remaining item to emtpy receipt
    userEvent.click(listReceiptItem[0].querySelector('.btn_remove_item'));
    listReceiptItem = screen.queryAllByLabelText('ReceiptItem');
    expect(listReceiptItem).toHaveLength(0);

    //Re add new normal item
    mockGetProductRuleApi.mockReturnValue(Promise.resolve(mockProductRuleData[5]));
    mockSearchProductApi.mockResolvedValue(Promise.resolve(mockSearchProductData['223700009']));
    userEvent.type(productSearchBarElement, '223700009');
    userEvent.type(productSearchBarElement, '{enter}');
    await checkSearchResultLoading();
    listReceiptItem = screen.queryAllByLabelText('ReceiptItem');
    checkNormalReceiptItemRenderCorrectly({
      receiptItemComponnent: listReceiptItem[0],
      base_price: 130000,
      discount: '0',
      qty: 1,
      name: "Product B",
      sku: "223700009",
      lineTotal: 130000
    })
    checkTotalAndItemRendering(130000, 1);

    //Add new discount item
    mockGetProductRuleApi.mockClear().mockReturnValue(Promise.resolve(mockProductRuleData[6]));
    userEvent.type(productSearchBarElement, '278700019');
    userEvent.type(productSearchBarElement, '{enter}');
    await checkSearchResultLoading();
    listReceiptItem = screen.queryAllByLabelText('ReceiptItem');
    checkDiscountItemRenderCorrectly({
      receiptItemComponnent: listReceiptItem[0],
      base_price: 171000,
      discount: '0',
      qty: 1,
      name: "Product C",
      sku: "278700019",
      lineTotal: 152000,
      discount_price: 19000,
    })
    checkNormalReceiptItemRenderCorrectly({
      receiptItemComponnent: listReceiptItem[1],
      base_price: 130000,
      discount: '0',
      qty: 1,
      name: "Product B",
      sku: "223700009",
      lineTotal: 130000
    })
    checkTotalAndItemRendering(282000, 2);

    //Increase discount item by 1
    mockGetProductRuleApi.mockClear().mockReturnValue(Promise.resolve(mockProductRuleData[7]));
    userEvent.click(listReceiptItem[0].querySelector('.btn_increase_qty'));
    await checkSearchResultLoading();
    listReceiptItem = screen.queryAllByLabelText('ReceiptItem');
    checkDiscountItemRenderCorrectly({
      receiptItemComponnent: listReceiptItem[0],
      base_price: 171000,
      discount: '0',
      qty: 2,
      name: "Product C",
      sku: "278700019",
      lineTotal: 304000,
      discount_price: 19000,
    })
    checkNormalReceiptItemRenderCorrectly({
      receiptItemComponnent: listReceiptItem[1],
      base_price: 130000,
      discount: '0',
      qty: 1,
      name: "Product B",
      sku: "223700009",
      lineTotal: 130000
    })
    checkTotalAndItemRendering(434000, 3);

    //Add new product need to set expired
    mockGetProductRuleApi.mockClear().mockReturnValue(Promise.resolve(mockProductRuleData[8]));
    expect(screen.queryByText('Set Expire Date')).not.toBeInTheDocument()
    userEvent.type(productSearchBarElement, '100390065');
    userEvent.type(productSearchBarElement, '{enter}');
    await checkSearchResultLoading();
    expect(screen.queryByText('Set Expire Date')).toBeVisible();
    userEvent.click(screen.queryByLabelText('OpenSearch'));
    userEvent.type(screen.queryByLabelText('InputExpiredDay'), '2021-12');
    expect(screen.queryByLabelText('InputExpiredDay').value).toEqual('2021-12');
    userEvent.click(screen.queryByLabelText('AddExpiredDay'));
    userEvent.click(screen.queryByText('Save'));
    expect(screen.queryByText('Set Expire Date')).not.toBeVisible();
    listReceiptItem = screen.queryAllByLabelText('ReceiptItem');
    checkSetExpiredItemRenderCorrrectly({
      receiptItemComponnent: listReceiptItem[0],
      base_price: 2450000,
      discount: '0',
      qty: 1,
      name: "Product D",
      sku: "100390065",
      lineTotal: 2450000
    })
    checkDiscountItemRenderCorrectly({
      receiptItemComponnent: listReceiptItem[1],
      base_price: 171000,
      discount: '0',
      qty: 2,
      name: "Product C",
      sku: "278700019",
      lineTotal: 304000,
      discount_price: 19000,
    })
    checkNormalReceiptItemRenderCorrectly({
      receiptItemComponnent: listReceiptItem[2],
      base_price: 130000,
      discount: '0',
      qty: 1,
      name: "Product B",
      sku: "223700009",
      lineTotal: 130000
    })
    checkTotalAndItemRendering(2884000, 4);

    //Add and active new voucher
    mockGetProductRuleApi.mockClear().mockReturnValue(Promise.resolve(mockProductRuleData[9]));
    expect(screen.queryByText('Active: Phiếu mua hàng #328500002')).not.toBeInTheDocument();
    userEvent.type(productSearchBarElement, '328500002');
    userEvent.type(productSearchBarElement, '{enter}');
    await checkSearchResultLoading();
    listReceiptItem = screen.queryAllByLabelText('ReceiptItem');
    checkVoucherItemRenderCorrrectly({
      receiptItemComponnent: listReceiptItem[0],
      base_price: 200000,
      discount: '0',
      qty: 1,
      name: "Phiếu mua hàng",
      sku: "328500002",
      lineTotal: 200000
    });
    checkTotalAndItemRendering(3084000, 5);
    userEvent.click(listReceiptItem[0].querySelector('.fa-lock-open'));
    expect(screen.queryByText('Active: Phiếu mua hàng #328500002')).toBeInTheDocument();
    userEvent.type(screen.queryByPlaceholderText('Active Code'), '123');
    userEvent.click(screen.queryByText('Scan'));
    expect(global.alert).toBeCalled();//Active code length must be greater than 7!
    userEvent.type(screen.queryByPlaceholderText('Active Code'), '12345678');
    userEvent.click(screen.queryByText('Scan'));
    expect(listReceiptItem[0].querySelector('.fa-lock-open')).not.toBeInTheDocument();

    //Add customer info
    mockSearchByPhoneNumberApi.mockClear().mockReturnValue(Promise.resolve(mockCustomerData['037123456']));
    userEvent.type(screen.queryByPlaceholderText('Search Phone (F4)'), '037123456');
    userEvent.click(screen.queryByText('Check'));
    await screen.findByText('test test test');
    expect(screen.queryByText('228,124')).toBeVisible();

    //Add new Gift Code
    mockGetProductRuleApi.mockClear().mockReturnValue(Promise.resolve(mockProductRuleData[10]));
    mockValidateGiftCodeApi.mockClear().mockReturnValue(Promise.resolve(mockGiftCodeData['HASAKIGIFTCODE']));
    userEvent.type(productSearchBarElement, '100240053');
    userEvent.type(productSearchBarElement, '{enter}');
    await checkSearchResultLoading();
    listReceiptItem = screen.queryAllByLabelText('ReceiptItem');
    checkNormalReceiptItemRenderCorrectly({
      receiptItemComponnent: listReceiptItem[0],
      base_price: 1078000,
      discount: '0',
      qty: 1,
      name: "Product have Gift Code Discount",
      sku: "100240053",
      lineTotal: 1078000
    });
    checkTotalAndItemRendering(4162000, 6);
    mockGetProductRuleApi.mockClear().mockReturnValue(Promise.resolve(mockProductRuleData[11]));
    userEvent.type(screen.queryByPlaceholderText('Gift Code'), 'HASAKIGIFTCODE');
    userEvent.click(screen.queryByText('Use'));
    await waitFor((() => screen.findByText('Loading!!!')));
    listReceiptItem = screen.queryAllByLabelText('ReceiptItem');
    expect(screen.queryByText('Use')).not.toBeInTheDocument();
    expect(screen.queryByLabelText('RemoveGiftCodeButton'));
    checkDiscountItemRenderCorrectly({
      receiptItemComponnent: listReceiptItem[0],
      base_price: 1078000,
      discount: '0',
      qty: 1,
      name: "Product have Gift Code Discount",
      sku: "100240053",
      lineTotal: 970200,
      discount_price: 107800,
    });
    checkTotalAndItemRendering(4054200, 6);

    //Add new product have gift
    mockGetProductRuleApi.mockClear().mockReturnValue(Promise.resolve(mockProductRuleData[12]));
    userEvent.type(productSearchBarElement, '100750038');
    userEvent.type(productSearchBarElement, '{enter}');
    await checkSearchResultLoading();
    listReceiptItem = screen.queryAllByLabelText('ReceiptItem');
    checkNormalReceiptItemRenderCorrectly({
      receiptItemComponnent: listReceiptItem[0],
      base_price: 104000,
      discount: '0',
      qty: 1,
      name: "Product have gift",
      sku: "100750038",
      lineTotal: 104000
    });
    checkTotalAndItemRendering(4158200, 7);
    expect(screen.queryByLabelText('GiftsAppButton')).not.toBeDisabled();
    userEvent.click(screen.queryByLabelText('GiftsAppButton'));
    expect(screen.queryByText('Add Gift Into Receipt')).toBeInTheDocument();
    userEvent.type(screen.queryByPlaceholderText('Enter Gift, SKU, Barcode, Name (Ctrl+F3)'), '422200043');
    userEvent.type(screen.queryByPlaceholderText('Enter Gift, SKU, Barcode, Name (Ctrl+F3)'), '{enter}');
    expect(screen.queryByText('Voucher service (SKU: 422200043) was added successfully!')).toBeInTheDocument();
    listReceiptItem = screen.queryAllByLabelText('ReceiptItem');
    checkGiftItemRenderCorrectly({
      receiptItemComponnent: listReceiptItem[6],
      base_price: 0,
      discount: '0',
      qty: 1,
      name: "Voucher service",
      sku: "422200043",
      lineTotal: 0
    });
    checkTotalAndItemRendering(4158200, 8);

    //
  });
});