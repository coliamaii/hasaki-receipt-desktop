import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

function InfoBox(props) {
  const [version, setVersion] = useState('');

  useEffect(async () => {
    const ver = await window.api?.getVersion();
    setVersion(ver);
  }, [])

  return (
    <div className="box box-default">
      <div className="box-body">
        <div className="row">
          <div className="col-md-9">
            Store: <strong>SHOP-555BTH</strong>, IP: 14.161.19.94{" "}, Version:<b>{ version }</b>
          </div>
          <div className="col-md-3">
            <a
              href="#"
              className="dropdown-toggle btn btn-default btn-sm"
              data-toggle="dropdown"
            >
              Screen <span className="caret"></span>
            </a>
            <ul className="dropdown-menu">
              <li className="active">
                <a href="http://test.inshasaki.com/pointos/receipt/shared-screen/register?screen_id=0">
                  Turn off
                </a>
              </li>
              <li className="">
                <a
                  href="http://test.inshasaki.com/pointos/receipt/shared-screen/register?screen_id=store_555_1"
                  target="_blank"
                >
                  Screen 1
                </a>
              </li>
              <li className="">
                <a
                  href="http://test.inshasaki.com/pointos/receipt/shared-screen/register?screen_id=store_555_2"
                  target="_blank"
                >
                  Screen 2
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}

InfoBox.propTypes = {};

export default InfoBox;
