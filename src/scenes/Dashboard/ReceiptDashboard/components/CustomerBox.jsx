import {
  faBan,
  faCheck,
  faEdit,
  faSearch,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Row } from "antd";
import PropTypes from "prop-types";
import React, { useEffect, useRef, useState } from "react";
import { useHotkeys } from "react-hotkeys-hook";
import { useDispatch, useSelector } from "react-redux";
import HOTKEY from "utils/hotkey";
import formatNumber from "utils/money";
import { searchCustomerStoreNewAction, searchCustomerSuccessAction } from 'share/api/Customer/SearchCustomer/actions';
import EditCustomerModal from "./EditCustomerModal";

CustomerBox.propTypes = {
  isPayment: PropTypes.bool.isRequired,
  handleCheck: PropTypes.func.isRequired,
};

function CustomerBox({
  isPayment,
  handleCheck,
  removeCustomer,
}) {
  const { data: customer, loading, isNewCustomer, isSearching } = useSelector(
    (state) => state.api.customer.searchCustomer
  );
  const { paymentState } = useSelector(state => state.mode);
  const { client_receipt_id } = useSelector(state => state.api.posReceipt);

  const [isEditCustomerModalOpen, setIsEditCustomerModalOpen] = useState(false);
  const [customerPhoneInput, setCustomerPhoneInput] = useState("");
  const [customerNameInput, setCustomerNameInput] = useState("");
  const [customerEmailInput, setCustomerEmailInput] = useState('');

  const dispatch = useDispatch();

  const phoneInputRef = useRef(null);
  const nameInputRef = useRef(null);

  //Track customer change on redux and update UI
  useEffect(() => {
    if (customer != null) {
      setCustomerPhoneInput(customer?.customer_phone ?? "");
      setCustomerNameInput(customer?.customer_name ?? "");
      setCustomerEmailInput(customer?.customer_email ?? "");
      return;
    }

    setCustomerNameInput('');
    setCustomerPhoneInput('');
    setCustomerEmailInput('');

    if (isSearching) {
      phoneInputRef.current?.focus();
      return;
    }
  }, [customer]);

  //If new customer, focus to enter name input
  useEffect(() => {
    if (isNewCustomer && isSearching) {
      nameInputRef.current.focus();
    }
  }, [isNewCustomer]);

  //Update to redux when have any change on customer name, email
  useEffect(() => {
    if (customerPhoneInput !== '') {
      handleUpdateNewCustomer();
    }
  }, [customerEmailInput, customerNameInput]);

  //Clean customer phone if on payment state and do not have customer data
  useEffect(() => {
    if (!customer && paymentState) {
      setCustomerPhoneInput('');
    }
  }, [paymentState, customer]);

  useHotkeys(
    HOTKEY.POS_SEARCH_CUSTOMER_BY_PHONE_FOCUS.key,
    () => {
      phoneInputRef.current?.focus();
    },
    {
      enableOnTags: ["INPUT", "TEXTAREA"],
    }
  );

  const handleCreateGuest = () => {
    handleCheck("1900636900");
  };

  const checkPhone = () => {
    const phone = phoneInputRef.current.value;
    phoneInputRef.current.value = "";
    const phoneNumber = phone;
    if (phoneNumber.length <= 6) {
      phoneInputRef.current?.focus();
      return;
    }
    if (phoneNumber !== "" && !isNaN(phoneNumber)) {
      handleCheck(phoneNumber);
    }
  };

  const trackSubmitPhone = (e) => {
    (e.code === "Enter" || e.code === "NumpadEnter") && checkPhone();
  };

  const handleUpdateNewCustomer = () => {
    dispatch(searchCustomerStoreNewAction(customerNameInput, customerEmailInput));
  }

  const handleRemovePhone = () => {
    setCustomerNameInput('');
    setCustomerPhoneInput('');
    setCustomerEmailInput('');
    removeCustomer();
  }

  return (
    <>
      {
        !isPayment && (
          <div className="box-header with-border">
            <h3 className="box-title">Customer</h3>
            <div className="box-tools pull-right" id="lnk_cus_edit"></div>
          </div>
        )
      }
      <Row className="customer-box" style={{ marginBottom: "0.5em" }}>
        <Col md={24}>
          <div className="input-group input-group-lg">
            {
              paymentState && (
                <span class="input-group-addon bg-blue">Phone</span>
              )
            }
            <input
              className="form-control"
              type="text"
              value={customerPhoneInput}
              onChange={(e) => {
                setCustomerPhoneInput(e.target.value);
              }}
              placeholder={`${paymentState ? 'Customer phone' : `Search Phone (${HOTKEY.POS_SEARCH_CUSTOMER_BY_PHONE_FOCUS.key})`}`}
              onKeyDown={trackSubmitPhone}
              ref={phoneInputRef}
              disabled={isPayment || customer != null || loading}
            />
            {
              !paymentState && (
                <span className="input-group-btn">
                  {(customer == null && !isPayment) && (
                    <button
                      className="btn btn-default"
                      type="button"
                      onClick={checkPhone}
                      disabled={loading}
                    >
                      <FontAwesomeIcon icon={faSearch} /> Check
                    </button>
                  )}
                  {customer != null && !isNewCustomer && (
                    <button
                      className="btn btn-default btn-success"
                      type="button"
                      onClick={() => { setIsEditCustomerModalOpen(true) }}
                    >
                      <FontAwesomeIcon icon={faEdit} /> Edit
                    </button>
                  )}
                </span>
              )
            }
            <span className="input-group-btn" style={{ paddingLeft: 3 }}>
              {
                !isPayment && (
                  <>
                    {customer == null ? (
                      <button
                        className="btn btn-default"
                        type="button"
                        onClick={handleCreateGuest}
                        disabled={loading}
                      >
                        <FontAwesomeIcon icon={faCheck} /> Guest
                      </button>
                    ) : (
                      <button
                        className="btn btn-default btn-danger"
                        type="button"
                        onClick={handleRemovePhone}
                      >
                        <FontAwesomeIcon icon={faBan} /> Remove
                      </button>
                    )}
                  </>
                )
              }
            </span>
          </div>
        </Col>
        <Col className="customer_box__info" md={24}>
          <div className="input-group input-group-lg">
            {/* Name:{" "}
            <span className="text-green">{customer?.customer_name}</span> */}
            <span class="input-group-addon bg-blue">Name</span>
            <input
              className="form-control"
              type="text"
              value={customerNameInput}
              placeholder={`Customer name`}
              disabled={!isNewCustomer || paymentState}
              ref={nameInputRef}
              onChange={(e) => {
                const customer_name = e.target.value;
                setCustomerNameInput(customer_name)
                dispatch(searchCustomerSuccessAction({ ...customer, customer_name, }, true));
                window.api.updateReceiptById(client_receipt_id, 'customer', JSON.stringify({ customer: { ...customer, customer_name, }, isNewCustomer }));
              }}
            />
          </div>
          <div className="input-group input-group-lg">
            <span class="input-group-addon bg-blue">Email</span>
            <input
              className="form-control"
              type="text"
              value={customerEmailInput}
              placeholder={`Customer email`}
              disabled={!isNewCustomer || paymentState}
              onChange={(e) => {
                const customer_email = e.target.value;
                setCustomerEmailInput(customer_email)
                dispatch(searchCustomerSuccessAction({ ...customer, customer_email, }, true));
                window.api.updateReceiptById(client_receipt_id, 'customer', JSON.stringify({ customer: { ...customer, customer_email, }, isNewCustomer }));
              }}
            />
          </div>
          <div className="input-group input-group-lg">
            <span class="input-group-addon bg-blue">Point</span>
            <input
              className="form-control"
              type="text"
              defaultValue={formatNumber(customer?.customer_point)}
              placeholder={`Customer Point`}
              disabled={true}
            />
          </div>
        </Col>
      </Row>
      <p className="margin"></p>

      {/* Begin Modal Area */}

      <EditCustomerModal
        isOpen={isEditCustomerModalOpen}
        handleCancel={() => {
          setIsEditCustomerModalOpen(false);
        }}
        customerName={customer?.customer_name}
        customerPoint={customer?.customer_point}
        customerPhone={customer?.customer_phone}
        customerEmail={customer?.customer_email}
        customerFacebook={customer?.customer_facebook}
      />

      {/* End Modal Area */}
    </>
  );
}

export default CustomerBox;
