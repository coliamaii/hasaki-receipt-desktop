import React from "react";
import PropTypes from "prop-types";

NoteBox.propTypes = {
  handleChangeNote: PropTypes.func.isRequired,
  note: PropTypes.string.isRequired,
};

function NoteBox({
  handleChangeNote,
  note
}) {
  return (
    <div className="row">
      <div className="col-xs-12">
        <div className="form-group">
          <label htmlFor="receipt_change">Note</label>
          <textarea value={note} onChange={handleChangeNote} className="form-control" name="receipt_desc"></textarea>
        </div>
      </div>
    </div>
  );
}

export default NoteBox;
