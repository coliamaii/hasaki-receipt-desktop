const { screen, render, cleanup } = require("@testing-library/react");
const { default: TableCaculateInfo } = require("..");

afterEach(cleanup);
beforeEach(() => {
  jest.spyOn(console, 'error').mockImplementation(() => { });
});

describe('Test TableCaculateInfo component', () => {
  const renderingComponent = ({ gift_items, productData }) => {
    const { container } = render(
      <TableCaculateInfo
        gift_items={gift_items}
        productData={productData}
      />
    )

    return { container };
  }

  test('When both gift and product are empty', () => {
    const { container } = renderingComponent({
      gift_items: [],
      productData: [],
    });

    const totalPriceElement = screen.queryByLabelText('Total');
    const totalItemElement = screen.queryByLabelText('Item');

    expect(totalPriceElement).not.toBeInTheDocument();
    expect(totalItemElement).not.toBeInTheDocument();
  });

  test('When product have data and gift is empty', () => {
    const { container } = renderingComponent({
      gift_items: [],
      productData: [
        { lineTotal: 200000, qty: 2 },
        { lineTotal: 140000, qty: 1 },
      ],
    });

    const totalPriceElement = screen.queryByText('340,000');
    const totalItemElement = screen.queryByText('3');

    expect(totalPriceElement).toBeVisible();
    expect(totalItemElement).toBeVisible();
  });

  test('When both gift and product have data', () => {
    const { container } = renderingComponent({
      gift_items: [
        { lineTotal: 0, qty: 2 },
        { lineTotal: 0, qty: 1 },
      ],
      productData: [
        { lineTotal: 200000, qty: 2 },
        { lineTotal: 140000, qty: 1 },
      ],
    });

    const totalPriceElement = screen.queryByText('340,000');
    const totalItemElement = screen.queryByText('6');

    expect(totalPriceElement).toBeVisible();
    expect(totalItemElement).toBeVisible();
  });
});