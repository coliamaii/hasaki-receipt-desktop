import React from 'react';
import PropTypes from 'prop-types';
import formatMoney from 'utils/money';

TableCaculateInfo.propTypes = {
  productData: PropTypes.array.isRequired,
  gift_items: PropTypes.array.isRequired,
}

function TableCaculateInfo({ productData, gift_items }) {
  const countItem = () => {
    return [
      ...productData, ...gift_items
    ].reduce((total, item) => {
      return total + item.qty;
    }, 0)
  }
  return (
    <div>
      {
        productData.length !== 0 && (
          <div>
            <div className="row">
              <div className="col-xs-5">
                <strong className="text-green">Total:</strong>
              </div>
              <div aria-label="Total" className="col-xs-7">
                {
                  formatMoney(productData.reduce((total, item) => {
                    return total + item.lineTotal;
                  }, 0))
                }
              </div>
            </div>
            <div className="row">
              <div className="col-xs-5">
                <strong className="text-green">Items:</strong>
              </div>
              <div aria-label="Item" className="col-xs-7">
                {
                  countItem()
                }
              </div>
            </div>
          </div>
        )
      }
    </div>
  )
}

export default TableCaculateInfo;