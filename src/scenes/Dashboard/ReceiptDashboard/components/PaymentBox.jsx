import { faDollarSign } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import HOTKEY from 'utils/hotkey';
import { Col, Row } from 'antd';
import AppButton from 'share/components/AppButton';
import NoteBox from './NoteBox';

PaymentBox.propTypes = {
  isPayment: PropTypes.bool.isRequired,
  handlePaymentClick: PropTypes.func.isRequired,
  handleReopenModal: PropTypes.func.isRequired,
  productTable: PropTypes.array.isRequired,
  note: PropTypes.string.isRequired,
  handleChangeNote: PropTypes.func.isRequired,
}

function PaymentBox({
  isPayment,
  handlePaymentClick,
  handleReopenModal,
  productTable,
  note,
  handleChangeNote,
}) {
  const btnRef = useRef(null);

  return (
    <div className="box box-default">
      <div className="box-body">
        <Row gutter={4} justify="end">
          <Col md={18}>
            <NoteBox
              handleChangeNote={handleChangeNote}
              note={note}
            />
          </Col>
          {
            !isPayment
              ? (
                <Col md={6}>
                  <AppButton
                    color="#2827CC"
                    content={`Payment`}
                    icon={<FontAwesomeIcon icon={faDollarSign} size="2x" />}
                    innerRef={btnRef}
                    handleClick={handlePaymentClick}
                    isDisabled={productTable.length === 0}
                    hotKey={HOTKEY.POS_PAYMENT_CLICK.key}
                  />
                </Col>
              )
              : (
                <Col md={6}>
                  <AppButton
                    color="#FF0000"
                    content={`Open Payment`}
                    icon={null}
                    innerRef={btnRef}
                    handleClick={handleReopenModal}
                    isDisabled={productTable.length === 0}
                    hotKey={HOTKEY.POS_PAYMENT_CLICK.key}
                  />
                </Col>
              )
          }
        </Row>
      </div>
    </div>
  )
}

export default PaymentBox;
