import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretDown, faCaretUp, faPlus } from '@fortawesome/free-solid-svg-icons';
import { useDispatch, useSelector } from 'react-redux';
import { getProductByRuleAddExpiredItem, getProductByRuleCheckExpiredAction } from 'share/api/Product/GetProductsByRule/actions';

CheckExpiredModal.propTypes = {

}

function CheckExpiredModal({

}) {
  const [isOpenSearch, setIsOpenSeach] = useState(false);
  const [date, setDate] = useState('');
  const [searchText, setSearchText] = useState('');

  const check_expired_items = useSelector(state => state.api.product.productTable.check_expired_items);
  const { client_receipt_id } = useSelector(state => state.api.posReceipt);

  const dispatch = useDispatch();

  const searchRef = useRef(null);

  const handleSave = () => {
    if (date !== '') {
      dispatch(getProductByRuleCheckExpiredAction(null));
      dispatch(getProductByRuleAddExpiredItem(check_expired_items, date, client_receipt_id));
      setDate('');
    }
  }

  const handleAddExpiredDay = () => {
    if (searchText !== '') {
      setDate(searchText);
      setSearchText('');
      setIsOpenSeach(false);
      searchRef.current.value = '';
    }
  }

  return (
    <>
      <Modal
        className="check-expired-modal"
        title="Set Expire Date"
        style={{ top: 20 }}
        visible={check_expired_items !== null}
        okButtonProps={{ hidden: true }}
        cancelButtonProps={{ hidden: true }}
        footer={false}
        closable={false}
        width={800}
        bodyStyle={{ height: "20vh", overflow: "auto" }}
      >
        <div className="row">
          <div className="col-xs-10 check-expired-input">
            <button aria-label="OpenSearch" type="button" className="btn" onClick={() => { setIsOpenSeach(!isOpenSearch) }}>
              <span>
                {
                  date === '' ? 'Please choose expire date or scan from product and press enter button' : date
                }
              </span>
              <span>
                <FontAwesomeIcon icon={isOpenSearch ? faCaretUp : faCaretDown} />
              </span>
            </button>
            <div className={`check-expired-enter ${!isOpenSearch && 'hidden'}`}>
              <input
                aria-label="InputExpiredDay"
                ref={searchRef}
                className="select2-search__field" type="search"
                tabIndex="0" autoComplete="off" autoCorrect="off" autoCapitalize="off" spellCheck="false" role="textbox"
                onChange={(e) => { setSearchText(e.target.value) }}
              />
              <div
                aria-label="AddExpiredDay"
                style={{ marginTop: '0.5em', padding: '0.5em', cursor: 'pointer', backgroundColor: `${searchText !== '' ? '#5897fb' : '#FFFFFF'}` }}
                onClick={handleAddExpiredDay}
              >
                {
                  searchText === '' ? 'No result found' : searchText
                }
              </div>
            </div>
          </div>
          <div className="col-xs-2">
            <span id="modelLoading" className="hidden"><i className="fa fa-spin fa-spinner fa-2x"></i></span>
            <div className="pull-right">
              <button onClick={handleSave} type="button" className="btn btn-primary btn-lg">
                <FontAwesomeIcon icon={faPlus} /> Save
							</button>
            </div>
          </div>
        </div>
      </Modal>
    </>
  )
}

export default CheckExpiredModal;