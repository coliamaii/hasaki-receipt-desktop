const { screen, render, cleanup, queryByAltText, queryByText } = require("@testing-library/react");
const { default: userEvent } = require("@testing-library/user-event");
const { Provider } = require("react-redux");
const { default: configureStore } = require('redux-mock-store');
const { default: CheckExpiredModal } = require("..");
const redux = require('react-redux');
const {
  getProductByRuleCheckExpiredAction,
  getProductByRuleAddExpiredItem,
} = require("share/api/Product/GetProductsByRule/actions");

afterEach(cleanup);
beforeEach(() => {
  jest.spyOn(console, 'error').mockImplementation(() => { });
});

const renderingComponent = ({ check_expired_items }) => {
  const mockStore = configureStore([]);

  const initalState = {
    api: {
      product: {
        productTable: {
          check_expired_items,
        }
      }
    }
  }
  const store = mockStore(initalState);

  const { container } = render(
    <Provider store={store}>
      <CheckExpiredModal
      />
    </Provider>
  );

  return { container };
}

describe('Test CheckExpired Modal', () => {
  test('Component render correctly', () => {
    const { container } = renderingComponent({ check_expired_items: '12345678' });

    const titleElement = screen.queryByText('Set Expire Date');
    const openSearchBtnElement = screen.queryByLabelText('OpenSearch');
    const addExpiredDateArea = document.querySelector('.check-expired-enter');

    expect(titleElement).toBeInTheDocument();
    expect(titleElement).toBeVisible();
    expect(openSearchBtnElement).toBeVisible();
    expect(addExpiredDateArea.classList.contains('hidden')).toEqual(true);
  });

  test('Save new expired day', () => {
    const { container } = renderingComponent({ check_expired_items: '12345678' });
    const openSearchBtnElement = screen.queryByLabelText('OpenSearch');
    const addExpiredDateArea = document.querySelector('.check-expired-enter');
    const inputElement = screen.queryByRole('textbox');
    const addExpiredDateClickableElement = screen.queryByLabelText('AddExpiredDay');
    const saveBtnElement = screen.queryByText('Save');

    const useDispatchSpy = jest.spyOn(redux, 'useDispatch');
    const mockDispatchFn = jest.fn()
    useDispatchSpy.mockReturnValue(mockDispatchFn);

    expect(saveBtnElement).toBeVisible();
    expect(saveBtnElement.disabled).toEqual(false);

    userEvent.click(openSearchBtnElement);
    expect(addExpiredDateArea.classList.contains('hidden')).toEqual(false);
    expect(screen.queryByText('No result found')).toBeInTheDocument();

    userEvent.type(inputElement, '1234');
    expect(inputElement.value).toEqual('1234');
    expect(screen.queryByText('No result found')).not.toBeInTheDocument();
    expect(addExpiredDateArea).toBeVisible();

    userEvent.click(addExpiredDateClickableElement);
    expect(addExpiredDateArea.classList.contains('hidden')).toEqual(true);
    expect(screen.queryByText('1234')).toBeInTheDocument();

    userEvent.click(saveBtnElement);
    expect(mockDispatchFn.mock.calls).toEqual([
      [getProductByRuleCheckExpiredAction(null)],
      [getProductByRuleAddExpiredItem('12345678', '1234')],
    ]);
  });
})