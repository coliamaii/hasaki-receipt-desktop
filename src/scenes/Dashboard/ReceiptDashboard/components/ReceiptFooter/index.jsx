import React from "react";
import PropTypes from "prop-types";
import formatMoney from 'utils/money';
import { useSelector } from "react-redux";

ReceiptFooter.propTypes = {
  showModal: PropTypes.func.isRequired,
  distNum: PropTypes.string.isRequired,
  distPercent: PropTypes.string.isRequired,
  totalRemainder: PropTypes.string.isRequired,
  isPaymentState: PropTypes.bool.isRequired,
}

function ReceiptFooter({ distNum, distPercent, showModal, totalRemainder, isPaymentState }) {
  const { remainder } = useSelector(state => state.api.payment.summary);

  return (
    <>
      <div className="row">
        <div className="col-xs-4 text-right">
          <div className="input-group input-group-lg">
            <span className="input-group-addon">Dist(%)</span>
            <input
              aria-label="DistPercent"
              style={{ cursor: `${isPaymentState ? 'not-allowed' : 'pointer'}` }}
              readOnly={true}
              type="text"
              className="form-control text-right number-format nmpd-target"
              value={distPercent}
              onClick={() => { showModal(distPercent, 'distPercent') }}
              disabled={isPaymentState}
            />
          </div>
        </div>
        <div className="col-xs-4 text-right">
          <div className="input-group input-group-lg">
            <span className="input-group-addon">Dist(VND)</span>
            <input
              aria-label="DistNum"
              style={{ cursor: `${isPaymentState ? 'not-allowed' : 'pointer'}` }}
              readOnly={true}
              type="text"
              className="form-control text-right number-format nmpd-target"
              value={formatMoney(distNum)}
              onClick={() => { showModal(distNum, 'distNum') }}
              disabled={isPaymentState}
            />
          </div>
        </div>

        <div className="col-xs-4 text-right">
          <div className="input-group input-group-lg">
            <span className="input-group-addon text-green">
              <strong>Total:</strong>
            </span>
            <input
              style={{ cursor: `not-allowed` }}
              type="text"
              className="form-control text-right"
              readOnly
              value={formatMoney(totalRemainder)}
            />
          </div>
        </div>
      </div>
      <div className="row" id="remainder-block">
        <br />
        <div className="col-xs-4 text-right pull-right">
          <div className="input-group input-group-lg">
            <span className="input-group-addon text-red">
              <strong>Remainder:</strong>
            </span>
            <input
              style={{ cursor: `not-allowed` }}
              type="text"
              className="form-control text-right"
              readOnly={false}
              value={isPaymentState ? formatMoney(remainder) : formatMoney(totalRemainder)}
              disabled
            />
          </div>
        </div>
      </div>
    </>
  );
}

export default ReceiptFooter;
