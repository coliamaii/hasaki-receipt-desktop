const { screen, render, cleanup } = require("@testing-library/react");
const { default: userEvent } = require("@testing-library/user-event");
const { default: ReceiptFooter } = require("..");

afterEach(cleanup);
beforeEach(() => {
  jest.spyOn(console, 'error').mockImplementation(() => { });
});

const renderingComponent = () => {
  const showModal = jest.fn();

  const { container } = render(
    <ReceiptFooter
      distNum="0"
      distPercent="0"
      totalRemainder="0"
      isPaymentState={false}
      showModal={showModal}
    />
  )

  return { container, showModal };
}

describe('Test ReceiptFooter component', () => {
  test('Component render correctly', () => {
    const { container, showModal } = renderingComponent();

    const distPercentLabelElement = screen.queryByText('Dist(%)');
    const distNumLabelElement = screen.queryByText('Dist(VND)');
    const distPercentClickableInputElement = screen.queryByLabelText('DistPercent');
    const distNumClickableInputElement = screen.queryByLabelText('DistNum');

    expect(distPercentLabelElement).toBeVisible();
    expect(distNumLabelElement).toBeVisible();
    expect(distPercentClickableInputElement).toBeVisible();
    expect(distNumClickableInputElement).toBeVisible();

    expect(distPercentClickableInputElement.disabled).toEqual(false);
    expect(distNumClickableInputElement.disabled).toEqual(false);

    userEvent.click(distPercentClickableInputElement);
    expect(showModal).toBeCalledTimes(1);

    userEvent.click(distNumClickableInputElement);
    expect(showModal).toBeCalledTimes(2);
  });
});

