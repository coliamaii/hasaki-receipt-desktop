import React, { useRef } from 'react';
import { Modal } from 'antd';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

ActiveCodeModal.propTypes = {
  isVisible: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  handleCancel: PropTypes.func.isRequired,
  handleScan: PropTypes.func.isRequired,
}

function ActiveCodeModal({ isVisible, title, handleCancel, handleScan }) {
  const codeRef = useRef(null);

  return (
    <>
      <Modal
        className="check-expired-modal"
        title={title}
        style={{ top: 20 }}
        visible={isVisible}
        okButtonProps={{ hidden: true }}
        cancelButtonProps={{ hidden: true }}
        width={600}
        bodyStyle={{ height: "14vh", overflow: "auto" }}
        onCancel={handleCancel}
      >
        <div className="row">
          <div className="col-xs-9">
            <input ref={codeRef} className="form-control input-lg" type="text" placeholder="Active Code" />
          </div>
          <div className="col-xs-3">
            <button
              onClick={() => {
                const code = codeRef.current.value;
                if (code.length <= 7) {
                  alert(`Active code length should be greater than 7`);
                  return;
                }
                codeRef.current.value = '';
                handleScan(code);
              }}
              className="btn btn-primary btn-lg"
            >
              <FontAwesomeIcon icon={faPlus} />
              Scan
            </button>
          </div>
        </div>
      </Modal>
    </>
  )
}

export default ActiveCodeModal;