const { screen, render, cleanup, fireEvent } = require("@testing-library/react");
const { default: userEvent } = require("@testing-library/user-event");
const { default: ActiveCodeModal } = require("..");

afterEach(cleanup);
beforeEach(() => {
  jest.spyOn(console, 'error').mockImplementation(() => { });
});

describe('Test ActiveCodeModal component', () => {
  const renderingComponent = ({ isVisible }) => {
    const handleCancel = jest.fn();
    const handleScan = jest.fn();
    const { container } = render(
      <ActiveCodeModal
        handleCancel={handleCancel}
        handleScan={handleScan}
        isVisible={isVisible}
        title="Sản phẩm A (SKU: 123456789)"
      />
    );
    return { container, handleCancel, handleScan }
  }

  test('Component render correctly when visible to user', () => {
    const { } = renderingComponent({ isVisible: true });
    const titleElement = screen.queryByText('Sản phẩm A (SKU: 123456789)');
    const inputElement = screen.queryByRole('textbox');

    expect(titleElement).toBeInTheDocument();
    expect(titleElement).toBeVisible();
    expect(inputElement).toBeInTheDocument();
    expect(inputElement.disabled).toEqual(false);
  });

  test('Fire event when enter to input element and click scan button', () => {
    const { handleScan } = renderingComponent({ isVisible: true });
    const scanBtnElement = screen.queryByText('Scan');
    const inputElement = screen.queryByRole('textbox');

    expect(scanBtnElement).toBeInTheDocument();
    expect(scanBtnElement.disabled).toEqual(false);

    userEvent.type(inputElement, '123456789');
    expect(inputElement.value).toEqual('123456789');

    userEvent.click(scanBtnElement);
    expect(handleScan).toBeCalledTimes(1);
  });

  test('Not fire scan event and show alert when input element have no text', () => {
    const { handleScan } = renderingComponent({ isVisible: true });
    const scanBtnElement = screen.queryByText('Scan');

    global.alert = jest.fn();
    userEvent.click(scanBtnElement);
    expect(global.alert).toBeCalledTimes(1);
    expect(handleScan).toBeCalledTimes(0);
  });

  test('Fire event when click cancel button', () => {
    const { handleCancel } = renderingComponent({ isVisible: true });

    const cancelBtnElement = screen.getByLabelText('Close');
    expect(cancelBtnElement).toBeInTheDocument();

    userEvent.click(cancelBtnElement);
    expect(handleCancel).toBeCalledTimes(1);
  });
});