const mockSearchResult = [
  {
    product_barcode: "3614221234969",
    product_brand_id: 57,
    product_category_id: 209,
    product_config: 6,
    product_name: "Sản phẩm A",
    product_price: 2450000,
    product_sku: 100390065,
  },
  {
    product_barcode: "4901234247222",
    product_brand_id: 185,
    product_category_id: 209,
    product_config: 4,
    product_name: "San pham B",
    product_price: 140000,
    product_sku: 100190123,
  },
  {
    product_barcode: "4901234248021",
    product_brand_id: 185,
    product_category_id: 209,
    product_config: 4,
    product_name: "SảN PHẩm C",
    product_price: 140000,
    product_sku: 100190124,
  }
]

export default mockSearchResult;