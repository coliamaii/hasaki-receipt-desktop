const { screen, render, getByRole, fireEvent, cleanup } = require("@testing-library/react");
const { SEARCH_TYPE } = require("share/api/Product/SearchProduct/search-type");
const { default: ReceiptSearchBar } = require("..");
const { default: mockSearchResult } = require("./searchResult.mock");

afterEach(cleanup);

describe('Test ReceiptSearchBar component rendering when search input empty', () => {
  const renderingComponent = () => {
    const placeholderText = 'Enter SKU, Barcode, Name (F3)';
    const handleSearchProduct = jest.fn();
    const handleGetProduct = jest.fn();

    return render(
      <ReceiptSearchBar
        searchData={[]}
        type={SEARCH_TYPE.SEARCH_DATA}
        placeholder={placeholderText}
        handleSearchProduct={handleSearchProduct}
        isLoading={false}
        hotkeyFocus="F3"
        isPaymentState={false}
        handleGetProducts={handleGetProduct}
      />
    )
  }

  test('Text in search input element is empty', () => {
    const { container } = renderingComponent();
    const searchInputElement = getByRole(container, 'textbox');

    expect(searchInputElement.value).toEqual('');
  });

  test('Focus to receipt searchbar and not disabled', () => {
    const { container } = renderingComponent();
    const searchInputElement = getByRole(container, 'textbox');

    expect(searchInputElement).toHaveFocus();
    expect(searchInputElement.disabled).toEqual(false);
  });

  test('No search result exist', () => {
    const { container } = renderingComponent();
    const searchResultElements = [...container.querySelectorAll('.searchBarResultContainerItem')];

    expect(searchResultElements.length).toEqual(0);
  });
});

describe('Test ReceiptSearchBar component when enter to search input element', () => {
  const renderingComponent = (handleSearchProduct, handleGetProduct) => {
    const placeholderText = 'Enter SKU, Barcode, Name (F3)';
    return render(
      <ReceiptSearchBar
        searchData={[]}
        type={SEARCH_TYPE.SEARCH_DATA}
        placeholder={placeholderText}
        handleSearchProduct={handleSearchProduct}
        isLoading={false}
        hotkeyFocus="F3"
        isPaymentState={false}
        handleGetProducts={handleGetProduct}
      />
    )
  };

  test('When search element input length < 4, do not getting search results and vice versa', () => {
    const handleSearchProduct = jest.fn();
    const handleGetProduct = jest.fn();
    const { container } = renderingComponent(handleSearchProduct, handleGetProduct);
    const searchInputElement = getByRole(container, 'textbox');

    fireEvent.change(searchInputElement, {
      target: { value: '123' },
    });
    expect(handleSearchProduct).not.toBeCalled();

    fireEvent.change(searchInputElement, {
      target: { value: '1234' },
    });
    expect(handleSearchProduct).toBeCalled();
  });
});

describe('Test ReceiptSearchBar when searching data', () => {
  const renderingComponent = () => {
    const placeholderText = 'Enter SKU, Barcode, Name (F3)';
    const handleSearchProduct = jest.fn();
    const handleGetProduct = jest.fn();
    return render(
      <ReceiptSearchBar
        searchData={[]}
        type={SEARCH_TYPE.SEARCH_DATA}
        placeholder={placeholderText}
        handleSearchProduct={handleSearchProduct}
        isLoading={true}
        hotkeyFocus="F3"
        isPaymentState={false}
        handleGetProducts={handleGetProduct}
      />
    )
  };

  test('Search input element must be disabled', () => {
    const { container } = renderingComponent();
    const searchInputElement = getByRole(container, 'textbox');

    expect(searchInputElement.disabled).toEqual(true);
  })
});

describe('Test ReceiptSearchBar when on Payment state', () => {
  const renderingComponent = () => {
    const placeholderText = 'Enter SKU, Barcode, Name (F3)';
    const handleSearchProduct = jest.fn();
    const handleGetProduct = jest.fn();
    return render(
      <ReceiptSearchBar
        searchData={[]}
        type={SEARCH_TYPE.SEARCH_DATA}
        placeholder={placeholderText}
        handleSearchProduct={handleSearchProduct}
        isLoading={false}
        hotkeyFocus="F3"
        isPaymentState={true}
        handleGetProducts={handleGetProduct}
      />
    )
  };

  test('Search input element must be disabled', () => {
    const { container } = renderingComponent();
    const searchInputElement = getByRole(container, 'textbox');

    expect(searchInputElement.disabled).toEqual(true);
  });

  test('Search results must be empty', () => {
    const { container } = renderingComponent();
    const searchResultElements = [...container.querySelectorAll('.searchBarResultContainerItem')];

    expect(searchResultElements.length).toEqual(0);
  });
});

describe('Test ReceiptSearchBar when have search results', () => {
  const containerComponent = (sku) => {
    const placeholderText = 'Enter SKU, Barcode, Name (F3)';
    const handleSearchProduct = jest.fn();
    const handleGetProduct = jest.fn();
    const { container, rerender } = render(
      <ReceiptSearchBar
        searchData={[]}
        type={SEARCH_TYPE.SEARCH_DATA}
        placeholder={placeholderText}
        handleSearchProduct={handleSearchProduct}
        isLoading={false}
        hotkeyFocus="F3"
        isPaymentState={false}
        handleGetProducts={handleGetProduct}
      />
    )

    const searchInputElement = getByRole(container, 'textbox');
    fireEvent.change(searchInputElement, {
      target: { value: sku },
    });

    rerender(
      <ReceiptSearchBar
        searchData={[...mockSearchResult]}
        type={SEARCH_TYPE.SEARCH_DATA}
        placeholder={placeholderText}
        handleSearchProduct={handleSearchProduct}
        isLoading={false}
        hotkeyFocus="F3"
        isPaymentState={false}
        handleGetProducts={handleGetProduct}
      />
    )

    return { container, handleGetProduct, handleSearchProduct };
  }

  test('Check amount of result elements', () => {
    const { container } = containerComponent();
    const searchResultElements = [...container.querySelectorAll('.searchBarResultContainerItem')];

    expect(searchResultElements.length).toEqual([...mockSearchResult].length);
  });

  test('Search result elements render correctly when find by SKU', () => {
    const { container } = containerComponent(1234);
    const searchResultElements = container.querySelectorAll('.searchBarResultContainerItem');
    expect(searchResultElements[0]).toContainHTML('<span>100390065*Sản phẩm A*361422<strong>1234</strong>969**2450000</span>');
    expect(searchResultElements[1]).toContainHTML('<span>100190123*San pham B*490<strong>1234</strong>247222**140000</span>');
    expect(searchResultElements[2]).toContainHTML('<span>100190124*SảN PHẩm C*490<strong>1234</strong>248021**140000</span>');
  });

  test('Search result elements render correctly when find by name', () => {
    const { container } = containerComponent('san pham');
    const searchResultElements = container.querySelectorAll('.searchBarResultContainerItem');
    expect(searchResultElements[0]).toContainHTML('<span>100390065*<strong>Sản phẩm</strong> A*3614221234969**2450000</span>');
    expect(searchResultElements[1]).toContainHTML('<span>100190123*<strong>San pham</strong> B*4901234247222**140000</span>');
    expect(searchResultElements[2]).toContainHTML('<span>100190124*<strong>SảN PHẩm</strong> C*4901234248021**140000</span>');
  });

  test('Check product role when click on a search result element', () => {
    const { container, handleGetProduct } = containerComponent('1234');
    const searchResultElements = container.querySelector('.searchBarResultContainerItem');
    fireEvent.mouseDown(searchResultElements);
    expect(handleGetProduct).toBeCalledTimes(1);
  });
});

