import React, { useEffect, useRef, useState } from "react";
import PropTypes from 'prop-types';
import { removeVietnameseAccent } from 'utils/lang';
import { useHotkeys } from "react-hotkeys-hook";
import { useDispatch, useSelector } from "react-redux";
import { isSearchingCustomerAction } from "share/api/Customer/SearchCustomer/actions";

ReceiptSearchBar.propTypes = {
  handleSearchProduct: PropTypes.func.isRequired,
  handleGetProducts: PropTypes.func.isRequired,
  searchData: PropTypes.array.isRequired,
  type: PropTypes.string.isRequired,
  isLoading: PropTypes.bool.isRequired,
  placeholder: PropTypes.string.isRequired,
  hotkeyFocus: PropTypes.string.isRequired,
  isPaymentState: PropTypes.bool.isRequired,
}

function ReceiptSearchBar({
  handleSearchProduct,
  handleGetProducts,
  searchData,
  type,
  isLoading,
  placeholder,
  hotkeyFocus,
  isPaymentState,
}) {
  const [isSuggestHidden, setIsSuggestHidden] = useState(true);
  const [cursor, setCursor] = useState(-1);
  const [requestProductName, setRequestProductName] = useState('');

  const { isSearching: isSearchingCustomer } = useSelector(state => state.api.customer.searchCustomer);

  const searchBarRef = useRef(null);

  const dispatch = useDispatch();

  useEffect(() => {
    setCursor(-1);
    if (!isSearchingCustomer) {
      searchBarRef.current?.focus();
    }
  }, [searchData]);

  useEffect(() => {
    if (isLoading) {
      searchBarRef.current.value = requestProductName;
      return;
    }
    else {
      searchBarRef.current.value = '';
      setRequestProductName('');
    }
    if (!isSearchingCustomer) {
      searchBarRef.current?.focus();
    }
  }, [isLoading]);

  useHotkeys(hotkeyFocus, () => {
    if (!isPaymentState) {
      searchBarRef.current?.focus();
      window.scrollTo(0, 0);
      dispatch(isSearchingCustomerAction(false));
    }
  }, {
    enableOnTags: ['INPUT', 'TEXTAREA']
  });

  const prepairCheckRule = () => {
    let sku;
    if (cursor === -1) {
      sku = searchBarRef.current.value;
      setRequestProductName(sku);
    } else {
      sku = searchData[cursor].product_sku;
      setRequestProductName(searchData[cursor].product_name);
    }
    setIsSuggestHidden(true);
    handleGetProducts(sku);
    searchBarRef.current.value = '';
  }

  const handleKeyDown = (e) => {
    if (e.keyCode === 40 && cursor < searchData.length - 1) {
      setCursor(cursor + 1);
    }
    if (e.keyCode === 38 && cursor > 0) {
      setCursor(cursor - 1);
    }
    if (e.keyCode === 13) {
      prepairCheckRule();
    }
  }

  const boldSuggestText = (str, substring) => {
    if (str.length !== 0 && substring.length !== 0) {
      let cloneStr = str;
      const noToneStr = removeVietnameseAccent(str.toLowerCase());
      const noToneSubstring = removeVietnameseAccent(substring.toLowerCase());
      const regex = new RegExp(noToneSubstring, 'g');
      let match;

      while ((match = regex.exec(noToneStr)) !== null) {
        const text = str.slice(match.index, match.index + match[0].length);
        cloneStr = cloneStr.replace(text, `<strong>${text}</strong>`);
      }

      return cloneStr;
    }
  }

  return (
    <>
      <div
        className="autocomplete"
        style={{ width: '100%', flexDirection: 'column', display: 'flex', justifyContent: 'space-between' }}
        onBlur={() => { setIsSuggestHidden(true) }}
      >
        <input
          aria-label={`SearchBar-${type}`}
          name="txt_item_sku"
          id="txt_item_sku"
          className="form-control input-lg"
          type="text"
          placeholder={placeholder}
          autoFocus={true}
          autoComplete="off"
          onChange={(e) => {
            setIsSuggestHidden(false);
            e.target.value.length >= 4 ? handleSearchProduct(e.target.value, type) : setIsSuggestHidden(true);
          }}
          onFocus={() => {
            dispatch(isSearchingCustomerAction(false));
            setIsSuggestHidden(false)
          }}
          onKeyDown={handleKeyDown}
          ref={searchBarRef}
          disabled={isLoading || isPaymentState}
        />
        {
          !isPaymentState && (
            <div className={`autocomplete-items ${isSuggestHidden && 'hidden'}`}>
              {
                searchData.length !== 0 && (
                  <div className="autocomplete-items">
                    {
                      searchData.map((item, index) => {
                        let printedProduct = `${item.product_sku}*${item.product_name}*${item.product_barcode}**${item.product_price ?? 100000}`;
                        printedProduct = boldSuggestText(printedProduct, searchBarRef.current?.value);
                        return (
                          <div
                            aria-label="searchBarResultContainerItem"
                            onMouseDown={() => { prepairCheckRule() }}
                            key={item.product_sku}
                            className={`searchBarResultContainerItem ${index === cursor && 'active'}`}
                            onMouseOver={() => { setCursor(index) }}
                          >
                            {
                              <span dangerouslySetInnerHTML={{ __html: printedProduct }}></span>
                            }
                          </div>
                        )
                      })
                    }
                  </div>
                )
              }
            </div>
          )
        }
      </div>
    </>
  );
}

export default ReceiptSearchBar;
