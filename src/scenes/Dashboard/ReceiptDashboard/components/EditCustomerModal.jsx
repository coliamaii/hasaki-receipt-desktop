import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Modal from 'antd/lib/modal/Modal';
import { Col, Image, Row, Tabs } from 'antd';
import formatNumber from 'utils/money';
import fallbackProfileImage from 'assets/images/fallback_profile.png';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

const formSchema = yup.object().shape({
  customerName: yup
    .string()
    .required('Customer name is required!'),
  customerEmail: yup
    .string()
    .email('Email is not valid!')
    .optional(),
});

EditCustomerModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleCancel: PropTypes.func.isRequired,
  customerPoint: PropTypes.number.isRequired,
  customerName: PropTypes.string.isRequired,
  customerPhone: PropTypes.string.isRequired,
  customerEmail: PropTypes.string.isRequired,
  customerFacebook: PropTypes.string.isRequired,
}

function EditCustomerModal({
  isOpen,
  handleCancel,
  customerPoint,
  customerName,
  customerPhone,
  customerEmail,
  customerFacebook,
}) {
  const [nameInput, setNameInput] = useState('');
  const [emailInput, setEmailInput] = useState('');
  const [facebookInput, setFacebookInput] = useState('');

  const { register, handleSubmit, errors, clearErrors } = useForm({
    resolver: yupResolver(formSchema),
    reValidateMode: 'onSubmit'
  });

  useEffect(() => {
    setNameInput(customerName);
    setEmailInput(customerEmail);
    setFacebookInput(customerFacebook);
  }, [customerName, customerEmail, customerFacebook]);

  const handleEditCustomer = ({ customerName, customerEmail }) => {
    console.log({ customerName, customerEmail })
    alert('dispatch!');
    handleCancel();
  }

  const handleCancelClick = () => {
    setNameInput(customerName);
    setEmailInput(customerEmail);
    setFacebookInput(customerFacebook);
    clearErrors();
    handleCancel();
  }

  return (
    <Modal
      visible={isOpen}
      title={`Customer: ${customerName}`}
      width="100%"
      onCancel={handleCancelClick}
      okButtonProps={{
        type: 'primary',
        form: 'editCustomerForm',
        htmlType: 'submit',
      }}
      className="edit-customer-modal"
    >
      <Tabs defaultActiveKey="profile">
        <Tabs.TabPane tab="Profile" key="profile">
          <Row
          >
            <Col
              md={4}
              className="edit-customer-modal-summary"
            >
              <div>
                <Image
                  src="errr"
                  width={100}
                  height={100}
                  fallback={fallbackProfileImage}
                />
              </div>
              <div className="edit-customer-modal-summary-title">Profile</div>
            </Col>
            <Col
              md={5}
              className="edit-customer-modal-summary"
            >
              <div className="edit-customer-modal-summary-title">Total Orders
              ()
              </div>
            </Col>
            <Col
              md={5}
              className="edit-customer-modal-summary"
            >
              <div className="edit-customer-modal-summary-title">Total Receipts
              ()
              </div>
            </Col>
            <Col
              md={5}
              className="edit-customer-modal-summary"
            >
              <div className="edit-customer-modal-summary-title">Total Point
              </div>
              <div
                className="edit-customer-modal-summary-title text-red text-bold">{formatNumber(customerPoint)}</div>
            </Col>
            <Col
              md={5}
              className="edit-customer-modal-summary"
            >
              <div className="edit-customer-modal-summary-title">Total Voucher
              </div>
            </Col>
          </Row>
          <hr />
          <form id="editCustomerForm" onSubmit={handleSubmit(handleEditCustomer)}>
            <Row gutter={3}>
              <Col md={6}>
                <div className="input-group input-group-lg">
                  <label htmlFor="customerPhone">Phone</label>
                  <input
                    id="customerPhone"
                    className="form-control"
                    type="text"
                    disabled={true}
                    value={customerPhone}
                  />
                </div>
              </Col>
              <Col md={6}>
                <div className="input-group input-group-lg">
                  <label htmlFor="customerName">Name <span className="text-red">{errors.customerName?.message}</span></label>
                  <input
                    id="customerName"
                    className="form-control"
                    type="text"
                    value={nameInput}
                    placeholder="Customer Name"
                    onChange={(e) => { setNameInput(e.target.value) }}
                    ref={register}
                    name="customerName"
                  />
                </div>
              </Col>
              <Col md={6}>
                <div className="input-group input-group-lg">
                  <label htmlFor="customerEmail">Email: <span className="text-red">{errors.customerEmail?.message}</span></label>
                  <input
                    id="customerEmail"
                    className="form-control"
                    type="text"
                    value={emailInput}
                    placeholder="Customer Email"
                    onChange={(e) => { setEmailInput(e.target.value) }}
                    ref={register}
                    name="customerEmail"
                  />
                </div>
              </Col>
              <Col md={6}>
                <div className="input-group input-group-lg">
                  <label htmlFor="customerFacebook">Facebook: </label>
                  <input
                    id="customerFacebook"
                    className="form-control"
                    type="text"
                    value={facebookInput}
                    placeholder="Customer Facebook"
                    onChange={(e) => { setFacebookInput(e.target.value) }}
                  />
                </div>
              </Col>
            </Row>
          </form>
        </Tabs.TabPane>
      </Tabs>
    </Modal >
  )
}

export default EditCustomerModal;
