import React from "react";
import PropTypes from "prop-types";
import ReceiptSearchBar from "../ReceiptSearchBar";
import { useSelector } from "react-redux";
import HOTKEY from "utils/hotkey";
import { SEARCH_TYPE } from "share/api/Product/SearchProduct/search-type";

GiftBlock.propTypes = {
  handleSearchProduct: PropTypes.func.isRequired,
  handleGetProducts: PropTypes.func.isRequired,
  searchData: PropTypes.array.isRequired,
  message: PropTypes.string.isRequired,
}

function GiftBlock({ handleSearchProduct, searchData, handleGetProducts, message }) {
  const gifts = useSelector(state => state.api.product.searchProduct.gifts);

  return (
    <div className="box" id="gift-block">
      <div className="box-header">
        <h3 className="box-title">Add Gift Into Receipt</h3>
      </div>
      <div className="box-body">
        <div className="row">
          <div className="col-md-6">
            <ReceiptSearchBar
              handleSearchProduct={handleSearchProduct}
              searchData={searchData}
              handleGetProducts={handleGetProducts}
              type={SEARCH_TYPE.SEARCH_GIFT}
              placeholder={`Enter Gift, SKU, Barcode, Name (${HOTKEY.POS_SEARCH_GIFT_FOCUS.key})`}
              hotkeyFocus={HOTKEY.POS_SEARCH_GIFT_FOCUS.key}
            />
            <div
              id="gift-msg"
              className="text-red text-bold"
              style={{ marginTop: 10 }}
            >
              {message}
            </div>
          </div>
          <div className="col-md-6">
            {/* <!-- list items --> */}
            <table
              className="table table-hover table-bordered"
              id="tbl_receipt_gift_item"
            >
              <thead>
                <tr data-total-item="0">
                  <th className="text-center">No.</th>
                  <th className="text-center">SKU</th>
                  <th className="col-xs-6 text-center">Item</th>
                  <th className="col-xs-2 text-center">Unit Cost</th>
                  <th className="col-xs-2 text-center">Qty</th>
                </tr>
              </thead>
              <tbody>
                {
                  gifts.map((item, index) => (
                    <tr key={item.sku}>
                      <td>{index + 1}</td>
                      <td>{item.sku}</td>
                      <td>{item.name}</td>
                      <td>{item.unit_cost}</td>
                      <td>{item.qty}</td>
                    </tr>
                  ))
                }
              </tbody>
            </table>
            {/* <!-- end list items --> */}
          </div>
        </div>
        <hr />
      </div>
    </div>
  );
}

export default GiftBlock;
