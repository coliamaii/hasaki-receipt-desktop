export const mock_gifts = [
  {
    sku: 1900123456,
    name: 'Product A',
    unit_cost: 20000,
    qty: 4,
  },
  {
    sku: 1900456789,
    name: 'Product B',
    unit_cost: 10000,
    qty: 2,
  },
]