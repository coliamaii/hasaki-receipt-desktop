const { screen, render, cleanup } = require("@testing-library/react");
const { Provider } = require("react-redux");
const { default: configureStore } = require('redux-mock-store');
const { default: GiftBlock } = require("..");
const { mock_gifts } = require("./gifts.mock");

afterEach(cleanup);
beforeEach(() => {
  jest.spyOn(console, 'error').mockImplementation(() => { });
});

const renderingComponent = ({ gifts, message }) => {
  const mockStore = configureStore([]);
  const initalState = {
    api: {
      product: {
        searchProduct: {
          gifts,
        }
      }
    }
  }
  const store = mockStore(initalState);

  const handleGetProducts = jest.fn();
  const handleSearchProduct = jest.fn();

  const { container } = render(
    <Provider store={store}>
      <GiftBlock
        searchData={[]}
        message={message}
        handleGetProducts={handleGetProducts}
        handleSearchProduct={handleSearchProduct}
      />
    </Provider>
  )

  return { container };
}

describe('Test GiftBlock component', () => {
  test('Component render correctly', () => {
    const { container } = renderingComponent({
      gifts: mock_gifts,
      message: '',
    });

    const giftElements = [...document.querySelectorAll('tbody tr')];

    expect(giftElements.length).toEqual(2);
    expect(screen.queryByText('Product A')).toBeVisible();
    expect(screen.queryByText('Product B')).toBeVisible();
    expect(screen.queryByText('10000')).toBeVisible();
    expect(screen.queryByText('20000')).toBeVisible();
  });
});