import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Col, Modal, Row, Space, Tabs } from "antd";
import CardPaymentIcon from "share/components/CardPaymentIcon";
import CashPaymentIcon from "share/components/CashPaymentIcon";
import GiftCardPaymentIcon from "share/components/GiftCardPaymentIcon";
import QrPaymentIcon from "share/components/QrPaymentIcon";
import GiftCardPane from "./components/GiftCardPane";
import { useDispatch, useSelector } from "react-redux";
import formatMoney from "utils/money";
import CashPane from "./components/CashPane";
import CardPane from "./components/CardPane";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCheckSquare,
  faGifts,
  faPrint,
  faSquare,
} from "@fortawesome/free-solid-svg-icons";
import CouponModal from "./components/CouponModal";
import VATModal from "./components/VATModal";
import QrPayPane from "./components/QrPayPane";
import BalancePaymentIcon from "share/components/BalancePaymentIcon";
import BalancePane from "./components/BalancePane";
import { useHotkeys } from "react-hotkeys-hook";
import {
  paymentSummaryAddConsultantAction,
  paymentSummaryInitAction,
  paymentSummaryInsertCashAction,
  paymentSummaryUpdateConsultantAction,
  showConfirmModalAction,
} from "share/api/Payment/Summary/action";
import { useHistory } from "react-router";
import DiscountModal from "./components/DiscountModal";


import './payment-modal.scss';
import { paymentPosReceiptAction } from 'share/api/POSReceipt/action';
import GIFT_CARD_TYPE from 'share/api/Payment/Type/GiftCard/type';
import { LoadingOutlined } from "@ant-design/icons";


PaymentModal.propTypes = {
  isModalPaymentOpen: PropTypes.bool.isRequired,
  handleCancel: PropTypes.func.isRequired,
  handleShowCaculator: PropTypes.func.isRequired,
  receipt_discount_percent: PropTypes.number.isRequired,
  receipt_discount: PropTypes.number.isRequired,
  note: PropTypes.string.isRequired,
};

function PaymentModal({
  isModalPaymentOpen,
  handleCancel,
  handleShowCaculator,
  receipt_discount_percent,
  receipt_discount,
  note,
}) {
  const {
    items,
    subTotalPrice,
    totalPrices,
    discount,
    charge,
    remainder,
    cash,
    isConfirmModalOpen,
    consultant,
  } = useSelector(state => state.api.payment.summary);
  const { data: products } = useSelector((state) => state.api.product.productTable);
  const { amount: cardAmount, transaction_id: card_transaction_id } = useSelector(state => state.api.payment.type.card)
  const { amount: qrPayAmount, transaction_id: qrPay_transaction_id } = useSelector(state => state.api.payment.type.qrPay);
  const { current: currentBalance, transaction_id: balance_transaction_id } = useSelector(state => state.api.payment.type.balance);
  const { current: currentGiftCardPrice, listCode } = useSelector(state => state.api.payment.type.giftCard);
  const { isDone, companyName, address, regCode, amount: vatAmount, email } = useSelector(state => state.api.payment.vat);
  const customer = useSelector(state => state.api.customer.searchCustomer.data);
  const { remote_receipt_code, remote_receipt_id, client_receipt_id, isPrint } = useSelector(state => state.api.posReceipt);
  const { store_id } = useSelector((state) => state.auth.info.profile);
  const stock_id = useSelector(
    (state) => state.auth.info.profile.store.store_stock_id
  );
  const coupon = useSelector((state) => state.api.product.gift.gift_code.code);
  const { isOnline } = useSelector((state) => state.mode);

  const [isCouponModalOpen, setIsCouponModalOpen] = useState(false);
  const [isVATModalOpen, setIsVATModalOpen] = useState(false);
  const [isDiscountModalOpen, setIsDiscountModalOpen] = useState(false);
  const [currentPane, setCurrentPane] = useState("CashPayment");
  const [isCaculating, setIsCaculating] = useState(true);

  useEffect(() => {
    if (discount > 0) {
      setIsDiscountModalOpen(true);
    }
  }, [discount]);

  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    if (!isCaculating) {
      handlePaymentPrint();
    }
  }, [cash]);

  useHotkeys('Enter', () => {
    console.log('Enter!')
    console.log({ isModalPaymentOpen, isConfirmModalOpen, isVATModalOpen, isCouponModalOpen });
    if (isModalPaymentOpen && !isConfirmModalOpen && !isVATModalOpen && !isCouponModalOpen) {
      handleAutoAddCash();
    }
  });

  const handleAutoAddCash = () => {
    if (currentPane === 'CashPayment') {
      setIsCaculating(false);
      dispatch(paymentSummaryInsertCashAction(totalPrices, client_receipt_id));
    }
  }

  const handlePaymentPrint = () => {
    if (remainder !== 0 && currentPane !== 'CashPayment') {
      dispatch(showConfirmModalAction(true, client_receipt_id));
      Modal.warning({
        title: 'Thông báo',
        content: 'Vui lòng nhập đủ số tiền cần thanh toán',
        onOk: () => { dispatch(showConfirmModalAction(false, client_receipt_id)); }
      })
      return;
    }

    const formatedProducts = products
      .map((item, index) => ({
        [`receipt_item_sku[${index}]`]: item.sku,
        [`receipt_item_sku_type[${item.sku}]`]: 2,
        [`receipt_item_sku_price[${item.sku}]`]: item.base_price,
        [`receipt_item_sku_display[${item.sku}]`]: 0,
        [`receipt_item_sku_discount[${item.sku}]`]: item.discount_price,
        [`receipt_item_sku_discount_rule[${item.sku}]`]: item.rule_id,
        [`receipt_item_sku_discount_group[${item.sku}]`]: 0,
        [`receipt_item_sku_apply_rule[${item.sku}]`]: 0,
        [`receipt_item_sku_pick_price[${item.sku}]`]: 0,
        [`receipt_item_sku_qty[${item.sku}]`]: item.qty,
      }))
      .reduce((init, item) => {
        return { ...init, ...item };
      }, {});

    const hasakiListCode = listCode.filter(item => item.type === GIFT_CARD_TYPE.HASAKI_GIFT)
    const hasaki_transaction = hasakiListCode.reduce((initVal, item) => {
      initVal[item.code] = item.value;
      return initVal;
    }, {})
    const hasaki_total_amount = hasakiListCode.reduce((initVal, item) => {
      return initVal + item.value;
    }, 0)

    const otherListGiftCode = listCode.filter(item => item.type !== GIFT_CARD_TYPE.HASAKI_GIFT);
    const other_transaction = otherListGiftCode.reduce((initVal, item) => {
      initVal[item.transaction_id] = item.value;
      return initVal;
    }, {})
    const other_total_value = otherListGiftCode.reduce((initVal, item) => {
      if (item.type === GIFT_CARD_TYPE.GOT_IT) {
        initVal["3"] = item.value;
      } else if (item.type === GIFT_CARD_TYPE.SODEXO) {
        initVal["1"] = item.value;
      }
      return initVal;
    }, { "1": 0, "3": 0 })

    const completePaymentData = {
      receipt_code: remote_receipt_code,
      receipt_id: remote_receipt_id,
      receipt_store_id: store_id,
      stock_id,
      use_coupon: coupon === "" ? 0 : 1,
      voucher_5: 0,
      use_discount_code: coupon === "" ? 0 : 1,
      txt_item_sku: "",
      receipt_discount_percent,
      receipt_discount,
      receipt_total: totalPrices,
      remainder,
      gift_item_sku: "",
      receipt_customer_id: customer?.customer_id ?? 0,
      customer_phone: customer?.customer_phone ?? '',
      gift_code: '',
      "transaction_id[3]": card_transaction_id ?? null,
      "transaction_id[9]": qrPay_transaction_id ?? null,
      "transaction_id[6]": balance_transaction_id ?? null,
      receipt_gift: currentGiftCardPrice,
      "transaction_id[7]": '',
      "receipt_gift_data[7]": 0,
      "transaction_id[5]": hasaki_transaction,
      "receipt_gift_data[5]": hasaki_total_amount,
      "transaction_id[8]": other_transaction,
      "receipt_gift_data[8]": other_total_value,
      receipt_desc: note,
      receipt_cash: cash,
      receipt_card: cardAmount,
      receipt_qr: qrPayAmount,
      partner_type: 2,
      "gift-code": coupon,
      consultant_id: consultant?.id ?? null,
      require_vat: isDone ? 1 : 0,
      "vat[name]": companyName,
      "vat[address]": address,
      "vat[registration_number]": regCode ?? "",
      "vat[amount]": vatAmount,
      "vat[email]": email ?? "",
      ...formatedProducts,
    }
    window.api.updateReceiptById(client_receipt_id, 'status', 2);
    dispatch(paymentPosReceiptAction(completePaymentData, history.push));
  };

  const checkChangePayment = (pane_key) => {
    setCurrentPane(pane_key);
  };

  const handleCancelDiscount = () => {
    dispatch(paymentSummaryInitAction(items, subTotalPrice, subTotalPrice, 0, client_receipt_id));
    setIsDiscountModalOpen(false);
  };

  const checkConsultant = (e) => {
    const code = e.target.value;
    if (code.length === 5) {
      dispatch(paymentSummaryAddConsultantAction(code, client_receipt_id));
    } else {
      dispatch(paymentSummaryUpdateConsultantAction(null, client_receipt_id));
    }
  };

  return (
    <>
      <Modal
        title={
          remote_receipt_code && <b>{`Payment for #${remote_receipt_code}`}</b>
        }
        visible={isModalPaymentOpen}
        width="100%"
        onCancel={handleCancel}
        footer={null}
        className="payment-modal"
        centered
      >
        <Row>
          <Col className="payment-modal__row" md={7}>
            <div>
              <span>Summary</span>
              <span>Items: {items}</span>
            </div>
            <hr />
            <div>
              <span>Subtotal</span>
              <span>{formatMoney(subTotalPrice)}</span>
            </div>
            <div>
              <span>Discount</span>
              <span>{formatMoney(discount)}</span>
            </div>
            <div>
              <span>
                <b>Total</b>
              </span>
              <span>
                <b>{formatMoney(totalPrices)}</b>
              </span>
            </div>
            <br />
            <div>
              <span>Cash</span>
              <span>{formatMoney(cash)}</span>
            </div>
            <div>
              <span>Card</span>
              <span>{formatMoney(cardAmount)}</span>
            </div>
            <div>
              <span>QR Pay</span>
              <span>{formatMoney(qrPayAmount)}</span>
            </div>
            <div>
              <span>Gift Card</span>
              <span>{formatMoney(currentGiftCardPrice)}</span>
            </div>
            <div>
              <span>Balance</span>
              <span>{formatMoney(currentBalance)}</span>
            </div>
            <br />
            <div>
              <span>Charge</span>
              <span className={`${charge !== 0 && "text-green"}`}>
                <b>{formatMoney(charge)}</b>
              </span>
            </div>
            <div>
              <span className="text-red">Remainder</span>
              <span className={`${remainder !== 0 && "text-red"}`}>
                <b>{formatMoney(remainder)}</b>
              </span>
            </div>
          </Col>
          <Col md={1}></Col>
          <Col md={16}>
            <Tabs
              size="large"
              type="card"
              defaultActiveKey="CashPayment"
              animated={{ tabPane: false, inkBar: false }}
              centered={true}
              onChange={checkChangePayment}
            >
              <Tabs.TabPane
                tab={
                  <div style={{ width: "5em" }}>
                    <CashPaymentIcon />
                  </div>
                }
                key="CashPayment"
              >
                <CashPane handleShowCaculator={handleShowCaculator} />
              </Tabs.TabPane>
              {
                isOnline && (
                  <>
                    <Tabs.TabPane
                      tab={
                        <div style={{ width: "5em" }}>
                          <CardPaymentIcon />
                        </div>
                      }
                      key="CardPayment"
                    >
                      <CardPane handleShowCaculator={handleShowCaculator} />
                    </Tabs.TabPane>
                    <Tabs.TabPane
                      tab={
                        <div style={{ width: "5em" }}>
                          <QrPaymentIcon />
                        </div>
                      }
                      key="QrPayment"
                    >
                      <QrPayPane handleShowCaculator={handleShowCaculator} />
                    </Tabs.TabPane>
                    <Tabs.TabPane
                      tab={
                        <div style={{ width: "5em" }}>
                          <GiftCardPaymentIcon />
                        </div>
                      }
                      key="GiftCardPayment"
                    >
                      <GiftCardPane
                        currentPane={currentPane}
                      />
                    </Tabs.TabPane>
                  </>
                )
              }
              {(customer?.customer_name && isOnline) && (
                <Tabs.TabPane
                  tab={
                    <div style={{ width: "5em" }}>
                      <BalancePaymentIcon />
                    </div>
                  }
                  key="BalancePayment"
                >
                  <BalancePane handleShowCaculator={handleShowCaculator} />
                </Tabs.TabPane>
              )}
            </Tabs>
          </Col>
        </Row>
        <hr />
        <Row>
          <Col
            md={7}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "flex-end",
              flexDirection: "column",
            }}
          >
            <div
              className="input-group input-group-lg"
              style={{ width: "100%" }}
            >
              <span className="input-group-addon consultant-label">
                Consultant
              </span>
              <input
                className="form-control"
                type="text"
                minLength="5"
                maxLength="5"
                placeholder="Consultant"
                onChange={checkConsultant}
              />
            </div>
            <div>
              {consultant === null && <span></span>}
              {consultant && consultant.isDone && (
                <div className="text-success">{consultant.name}</div>
              )}
              {consultant && !consultant.isDone && (
                <div className="text-danger">Consultant not found!</div>
              )}
            </div>
          </Col>
          <Col md={1}></Col>
          <Col
            md={16}
            style={{ display: "flex", justifyContent: "space-between" }}
          >
            <div style={{ display: "flex", alignItems: "center" }}>
              <button
                onClick={() => {
                  setIsCouponModalOpen(true);
                }}
                className="btn btn-default btn-cash"
                style={{
                  marginLeft: "2em",
                  backgroundColor: "#FF0000",
                  color: "white",
                  width: "10em",
                }}
              >
                <Space>
                  <FontAwesomeIcon icon={faGifts} />
                  <b>Coupon</b>
                </Space>
              </button>
              <button
                onClick={() => {
                  setIsVATModalOpen(true);
                }}
                className="btn btn-cash"
                style={{
                  marginLeft: "2em",
                  backgroundColor: `${isDone ? "#383CC1" : "#CAD5E2"}`,
                  color: "white",
                  width: "10em",
                }}
              >
                <Space>
                  <FontAwesomeIcon icon={isDone ? faCheckSquare : faSquare} />
                  <b>VAT</b>
                </Space>
              </button>
            </div>
            <button
              style={{
                width: "15em",
                height: "7em",
                backgroundColor: "#3DBE29",
                color: "#FFFFFF",
              }}
              onClick={() => {
                handleAutoAddCash();
                handlePaymentPrint();
              }}
              className={`btn btn-cash`}
            >
              <Space>
                {isPrint ? (
                  <LoadingOutlined />
                ) : (
                  <FontAwesomeIcon size="lg" icon={faPrint} />
                )}

                <div style={{ display: "flex", flexDirection: "column" }}>
                  <b>PRINT</b>
                  <span>(Enter)</span>
                </div>
              </Space>
            </button>
          </Col>
        </Row>

        <CouponModal
          isCouponModalOpen={isCouponModalOpen}
          handleClose={() => {
            setIsCouponModalOpen(false);
          }}
        />
        <VATModal
          isVATModalOpen={isVATModalOpen}
          handleClose={() => {
            setIsVATModalOpen(false);
          }}
          amount={totalPrices}
        />
        <DiscountModal
          isOpen={isDiscountModalOpen}
          handleCancelDiscount={handleCancelDiscount}
        />
      </Modal>
    </>
  );
}

export default PaymentModal;
