import { faMailBulk, faMoneyBill, faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import formatMoney from 'utils/money';
import { useDispatch, useSelector } from 'react-redux';
import { Modal, Space } from 'antd';
import { balancePaymentDone, sendOptBalanceAction } from 'share/api/Payment/Type/Balance/action';

BalancePane.propTypes = {
  handleShowCaculator: PropTypes.func.isRequired,
}

function BalancePane({
  handleShowCaculator
}) {
  const OPT_LENGTH = 6;
  const COUNT_LENGTH = 30;

  const { balance, amount, isSend, isDone, have_account_balance } = useSelector(state => state.api.payment.type.balance);
  const { client_receipt_id } = useSelector(state => state.api.posReceipt);

  const [isBalanceModalOpen, setIsBalanceModalOpen] = useState(false);
  const [isPayByOtpBtnDisabled, setIsPayByOtpBtnDisabled] = useState(true);
  const [countdown, setCountdown] = useState(0);
  const [isCountRuning, setIsCountRunning] = useState(false);

  //Countdown for resend OTP
  useEffect(() => {
    const timer = countdown > 0 && setInterval(() => {
      setCountdown(countdown - 1);
    }, 1000);
    if (countdown === 0) {
      setIsCountRunning(false);
    }
    return () => clearInterval(timer);
  }, [countdown]);

  //When finish, close send otp modal
  useEffect(() => {
    if (isDone) {
      setIsBalanceModalOpen(false);
    }
  }, [isDone]);

  const optInputRef = useRef(null);

  const dispatch = useDispatch();

  const handleChangeOtp = (e) => {
    const otp = e.target.value;
    if (otp.length === OPT_LENGTH) {
      setIsPayByOtpBtnDisabled(false);
      return;
    }
    setIsPayByOtpBtnDisabled(true);
  }

  const handleSendOtp = () => {
    const otp = optInputRef.current.value;
    setIsCountRunning(true);
    setCountdown(COUNT_LENGTH);
    dispatch(sendOptBalanceAction(otp, client_receipt_id));
  }

  const handlePayWithBalance = () => {
    dispatch(balancePaymentDone('1234', client_receipt_id))
  }

  return (
    <div>
      <div style={{ margin: '1em 0 2em 0' }} className="input-group input-group-lg">
        <span className="input-group-addon">
          <FontAwesomeIcon size="lg" icon={faMoneyBill} />
        </span>
        <input
          className="form-control text-right"
          type="text"
          readonly="readonly"
          style={{ cursor: 'pointer' }}
          onClick={() => { handleShowCaculator(amount, 'balancePayment') }}
          value={formatMoney(amount)}
          disabled={!have_account_balance}
        />
      </div>
      <div>
        {
          have_account_balance
            ? (
              <div>
                <h3 className="text-red">Customer balance: {formatMoney(balance)}</h3>
              </div>
            )
            : (
              <h3 className="text-red">Account balance not found! </h3>
            )
        }
      </div>
      <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
        {
          !isDone && (
            <button
              onClick={() => { setIsBalanceModalOpen(true) }}
              className="btn btn-cash"
              style={{ color: 'white', backgroundColor: '#242B2E' }}
            >
              <b>PAY</b>
            </button>
          )
        }
      </div>
      <Modal
        visible={isBalanceModalOpen}
        title={<span style={{ fontWeight: 'bolder', fontSize: '1.5em' }}>Payment with balance</span>}
        footer={null}
        onCancel={() => { setIsBalanceModalOpen(false) }}
        width="30%"
        centered
      >
        <span className="payment-with-balance">
          <span className="payment-with-balance__send">
            <input
              ref={optInputRef}
              className="form-control input-lg"
              type="text"
              placeholder="OTP Code"
              onChange={handleChangeOtp}
              maxLength={OPT_LENGTH}
            />
          </span>
          {
            countdown > 0 && (
              <p className="text-red">Resend code after {countdown}s</p>
            )
          }
          <div className="payment-with-balance__pay">
            <Space>
              <button
                disabled={isCountRuning}
                type="button"
                className="btn btn-default btn-lg btn-cash"
                onClick={handleSendOtp}
              >
                <Space>
                  {
                    !isCountRuning && (
                      <FontAwesomeIcon icon={faMailBulk} />
                    )
                  }
                  <span>Send OTP</span>
                </Space>
              </button>
              <button
                type="button"
                className={`btn ${(isPayByOtpBtnDisabled || !isSend) ? 'btn-secondary' : 'btn-primary'} btn-lg btn-cash`}
                disabled={(isPayByOtpBtnDisabled || !isSend)}
                onClick={handlePayWithBalance}
              >
                <Space>
                  <span><FontAwesomeIcon icon={faPlus} /></span>
                  <span style={{ fontWeight: 'bolder' }}>PAY</span>
                </Space>
              </button>
            </Space>
          </div>
        </span>
      </Modal>
    </div>
  )
}

export default BalancePane;