import { CheckCircleOutlined, CloseCircleOutlined, LoadingOutlined } from '@ant-design/icons';
import { faMoneyBill } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Modal, Space } from 'antd';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { qrPayCancelAction, qrPaymentRequestAction } from 'share/api/Payment/Type/QrPay/action';
import { sendQrCodeToScreen } from 'share/socket';
import formatMoney from 'utils/money';

function QrPayPane({
  handleShowCaculator,
}) {
  const { remainder, totalPrices } = useSelector(state => state.api.payment.summary);
  const { amount, isDone, isPaymentRequest, message, isLoading, isError, code } = useSelector(state => state.api.payment.type.qrPay);
  const { client_receipt_id } = useSelector(state => state.api.posReceipt);
  const { store_id } = useSelector(state => state.auth.info.profile);
  const terminal_id = useSelector(state => state.websocket.screen.id);
  const source_code = 1;

  const dispatch = useDispatch();

  useEffect(() => {
    if (code !== null) {
      sendQrCodeToScreen(code, amount, terminal_id, source_code);
    }
  }, [code])

  const handleRequestPayment = () => {
    if (!terminal_id) {
      Modal.warning({
        title: 'Please select POS screen in setting!',
      });
      return;
    }
    dispatch(qrPaymentRequestAction(store_id, source_code, amount, terminal_id, client_receipt_id));
  }

  const handleCancelPayment = () => {
    dispatch(qrPayCancelAction(client_receipt_id));
  }

  const handleSuccessPayment = () => {

  }

  return (
    <div>
      <div style={{ margin: '1em 0 2em 0' }} className="input-group input-group-lg">
        <span className="input-group-addon">
          <FontAwesomeIcon size="lg" icon={faMoneyBill} />
        </span>
        <input
          className="form-control text-right"
          type="text"
          readonly="readonly"
          style={{ cursor: `${!isDone && 'pointer'}`, color: `${isDone && 'green'}`, fontWeight: `${isDone && 'bold'}` }}
          value={formatMoney(amount)}
          onClick={() => { handleShowCaculator(amount, 'qrPayment') }}
          disabled={isDone}
        />
      </div>
      <div>
        {
          !isDone && (
            <>
              <div style={{ margin: '4em 0 2em 0', display: 'flex', justifyContent: 'flex-end' }}>
                <button
                  onClick={handleRequestPayment}
                  className="btn btn-cash"
                  style={{ color: 'white', backgroundColor: '#242B2E' }}
                >
                  <b>PAY</b>
                </button>
              </div>
            </>
          )
        }
      </div>
      <Modal
        title="Payment with QR"
        className="payment-check-status-modal"
        visible={isPaymentRequest}
        closeIcon={null}
        closable={false}
        cancelButtonProps={{ style: { display: 'none' } }}
        okButtonProps={{ style: { display: 'none' } }}
        footer={null}
      >
        <div className="payment-check-status-modal__container">
          {
            isLoading && <LoadingOutlined style={{ color: 'blue' }} />
          }
          {
            isError && <CloseCircleOutlined style={{ color: 'red' }} />
          }
          {
            isDone && <CheckCircleOutlined style={{ color: 'green' }} />
          }
          <span>{message}</span>
        </div>
        <h4>Request payment for amount: <span style={{ fontSize: '2em', color: 'green' }}>{formatMoney(amount)}đ</span></h4>
        <div className="payment-check-status-modal__btn-group">
          {
            (!isLoading && isError) && (
              <Space>
                <button onClick={handleRequestPayment} className="btn btn-info btn-cash"><b>Retry</b></button>
                <button onClick={handleCancelPayment} className="btn btn-danger btn-cash"><b>Cancel</b></button>
              </Space>
            )
          }
          {
            (!isLoading && isDone) && (
              <button onClick={handleSuccessPayment} className="btn btn-success btn-cash"><b>Confirm</b></button>
            )
          }
        </div>
      </Modal>
    </div>
  )
}

export default QrPayPane;