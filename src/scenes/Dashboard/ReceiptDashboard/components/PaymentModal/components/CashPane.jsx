import React from 'react';
import PropTypes from 'prop-types';
import { faMoneyBill } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useDispatch, useSelector } from 'react-redux';
import formatMoney from 'utils/money';
import {
  paymentSummaryCashRequestAction,
  paymentSummaryInsertCashAction
} from 'share/api/Payment/Summary/action';
import QuickAmountArea from './QuickAmountArea';

CashPane.propTypes = {
  handleShowCaculator: PropTypes.func.isRequired,
}

function CashPane({
  handleShowCaculator,
}) {
  const { cash, remainder, totalPrices } = useSelector(state => state.api.payment.summary);
  const { client_receipt_id } = useSelector(state => state.api.posReceipt);

  const dispatch = useDispatch();

  const handleAddCash = (money) => {
    if (money === 0) {
      dispatch(paymentSummaryInsertCashAction(totalPrices, client_receipt_id));
      return;
    }
    dispatch(paymentSummaryInsertCashAction(money + cash, client_receipt_id));
  }

  return (
    <div>
      <div style={{ margin: '1em 0 2em 0' }} className="input-group input-group-lg">
        <span className="input-group-addon">
          <FontAwesomeIcon size="lg" icon={faMoneyBill} />
        </span>
        <input
          className="form-control text-right"
          type="text"
          readonly="readonly"
          style={{ cursor: 'pointer' }}
          onClick={() => { handleShowCaculator(cash, 'cashPayment') }}
          value={formatMoney(cash)}
        />
      </div>
      <QuickAmountArea
        handleAddAmount={handleAddCash}
        remainder={remainder}
      />
    </div>
  )
}

export default CashPane;