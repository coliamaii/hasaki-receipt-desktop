import Modal from 'antd/lib/modal/Modal';
import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMagic, faPlus } from '@fortawesome/free-solid-svg-icons';
import { Space } from 'antd';

CouponModal.propTypes = {
  isCouponModalOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
}

function CouponModal({
  isCouponModalOpen,
  handleClose,
}) {
  const couponInputRef = useRef(null);

  const handleCheckCoupon = () => {
    const coupon = couponInputRef.current.value;
    if (coupon !== '') {
      alert(coupon)
    }
  }

  return (
    <Modal
      visible={isCouponModalOpen}
      title="Voucher"
      footer={null}
      onCancel={handleClose}
      style={{ width: 'fit-content' }}
    >
      <Space style={{ display: 'flex', justifyContent: 'center' }}>
        <input
          class="form-control input-lg"
          type="text"
          placeholder="HASAKI Coupon Code"
          ref={couponInputRef}
        />
        <button onClick={handleCheckCoupon} type="button" class="btn btn-primary btn-lg">
          <FontAwesomeIcon icon={faMagic} /> Check
        </button>
        <button type="button" class="btn btn-success btn-lg">
          <FontAwesomeIcon icon={faPlus} /> Apply
        </button>
      </Space>
    </Modal>
  )
}

export default CouponModal;