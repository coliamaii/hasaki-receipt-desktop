import React from 'react';
import formatMoney from 'utils/money';
import PropTypes from 'prop-types';
import { Space } from 'antd';

QuickAmountArea.propTypes = {
  handleAddAmount: PropTypes.func.isRequired,
  remainder: PropTypes.number.isRequired,
}

function QuickAmountArea({ handleAddAmount, remainder }) {
  return (
    <Space>
      <button onClick={() => { handleAddAmount(500000) }} className="btn btn-info btn-cash"><b>500K</b></button>
      <button onClick={() => { handleAddAmount(200000) }} className="btn btn-info btn-cash"><b>200K</b></button>
      <button onClick={() => { handleAddAmount(100000) }} className="btn btn-info btn-cash"><b>100K</b></button>
      <button onClick={() => { handleAddAmount(50000) }} className="btn btn-info btn-cash"><b>50K</b></button>
      <button onClick={() => { handleAddAmount(20000) }} className="btn btn-info btn-cash"><b>20K</b></button>
      <button onClick={() => { handleAddAmount(10000) }} className="btn btn-info btn-cash"><b>10K</b></button>
      <button onClick={() => { handleAddAmount(remainder) }} className="btn btn-warning btn-cash" >
        <b>{`${formatMoney(remainder)}đ`}</b>
      </button>
    </Space>
  )
}

export default QuickAmountArea;