import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGift } from '@fortawesome/free-solid-svg-icons';
import { useDispatch, useSelector } from 'react-redux';
import GIFT_CARD_TYPE from 'share/api/Payment/Type/GiftCard/type';
import { giftCardCheckRequestAction, beginRedeemGiftsAction } from 'share/api/Payment/Type/GiftCard/action';
import { SyncOutlined } from '@ant-design/icons';
import formatMoney from 'utils/money';

GiftCardPane.propTypes = {
  currentPane: PropTypes.string.isRequired,
}

function GiftCardPane({ currentPane }) {
  const [currentGiftCardType, setCurrentGiftCardType] = useState(GIFT_CARD_TYPE.SODEXO);

  const { listCode, totalPrice } = useSelector(state => state.api.payment.type.giftCard);
  const { store_id } = useSelector(state => state.auth.info.profile);
  const { remote_receipt_code, remote_receipt_id, client_receipt_id } = useSelector(state => state.api.posReceipt);

  useEffect(() => {
    codeInputRef.current.focus();
  }, [currentGiftCardType]);

  useEffect(() => {
    if (currentPane === 'GiftCardPayment') {
      codeInputRef.current.focus();
    }
  }, [currentPane]);

  const codeInputRef = useRef(null);
  const dispatch = useDispatch();

  const handleCheckGift = () => {
    const serial_number = codeInputRef.current.value;
    codeInputRef.current.value = '';
    if (serial_number === '') return;
    const index = listCode.findIndex(item => item.serial_number === serial_number);
    if (index !== -1) return;
    dispatch(giftCardCheckRequestAction(serial_number, store_id, currentGiftCardType, client_receipt_id));
  }

  const handleRedeemGift = () => {
    if (listCode.length !== 0) {
      const listValidCode = listCode.filter(item => item.status === 1 && !item.isRedeem);
      dispatch(beginRedeemGiftsAction(listValidCode, remote_receipt_code, remote_receipt_id, store_id, client_receipt_id));
    }
  }

  const detectPressingEnter = (e) => {
    (e.code === "Enter" || e.code === "NumpadEnter") && handleCheckGift();
  }

  return (
    <div>
      <div className="btn-group-choose-partner">
        <button
          type="button"
          className={`btn btn-app btn-choose-partner ${currentGiftCardType === GIFT_CARD_TYPE.SODEXO && 'active'}`}
          onClick={() => { setCurrentGiftCardType(GIFT_CARD_TYPE.SODEXO) }}
        >
          <FontAwesomeIcon size="lg" icon={faGift} />
          <b>SODEXO</b>
        </button>
        <button
          type="button"
          className={`btn btn-app btn-choose-partner ${currentGiftCardType === GIFT_CARD_TYPE.HASAKI_GIFT && 'active'}`}
          onClick={() => { setCurrentGiftCardType(GIFT_CARD_TYPE.HASAKI_GIFT) }}
        >
          <FontAwesomeIcon size="lg" icon={faGift} />
          <b>HASAKI GIFT</b>
        </button>
        <button
          type="button"
          className={`btn btn-app btn-choose-partner ${currentGiftCardType === GIFT_CARD_TYPE.GOT_IT && 'active'}`}
          onClick={() => { setCurrentGiftCardType(GIFT_CARD_TYPE.GOT_IT) }}
        >
          <FontAwesomeIcon size="lg" icon={faGift} />
          <b>GOT IT</b>
        </button>
      </div>
      <div>
        <div className="input-group" style={{ margin: '1em 0 1em 0' }}>
          <input
            autoFocus
            ref={codeInputRef}
            className="form-control input-lg"
            defaultValue=""
            placeholder={`Enter ${currentGiftCardType} code`}
            onKeyDown={detectPressingEnter}
          />
          <span className="input-group-btn">
            <button onClick={handleCheckGift} type="button" className="btn btn-success btn-lg" name="btnAddCode">
              CHECK
            </button>
          </span>
        </div>
        <div>
          <table className="table table-bordered gift-list">
            <thead>
              <tr>
                <th className="text-center">#</th>
                <th>Code</th>
                <th className="text-center">Value</th>
                <th className="text-center">Status</th>
                <th className="text-center">Type</th>
              </tr>
            </thead>
            <tbody>
              {
                listCode.length !== 0 && listCode.filter(item => !item.isRedeem).map((item, index) => (
                  <tr key={item.code}>
                    <td className="text-center">{index + 1}</td>
                    <td >{item.serial_number}</td>
                    {
                      item.status === null
                        ? (
                          <td colSpan="2" className="text-center"><SyncOutlined spin /></td>
                        )
                        : (
                          <>
                            <td className="text-center">{item.status === 0 ? 'N/A' : formatMoney(item.value)}</td>
                            <td className="text-center">
                              {
                                item.status === 0
                                  ? <span className="text-red">In-Valid</span>
                                  : <span className="text-green">Valid</span>
                              }
                            </td>
                          </>
                        )
                    }
                    <td className="text-center">{item.type}</td>
                  </tr>
                ))
              }
            </tbody>
            <tfoot>
              <tr>
                <td colSpan="2" className="text-center"><strong>Total</strong>
                </td>
                <td className="text-center">
                  <span id="totalAmount" className="text-bold" >{formatMoney(totalPrice)}</span>
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
        <div>
          <div className="pull-right">
            <button
              type="button"
              className="btn btn-primary btn-lg"
              name="btnModelRedeem"
              onClick={handleRedeemGift}
            >
              <FontAwesomeIcon className="text-white" icon={faGift} />Redeem
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default GiftCardPane;