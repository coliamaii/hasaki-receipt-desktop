import React, { useState } from 'react';

import PropTypes from 'prop-types';
import { Form, Input, Modal, Select, Space } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck, faTimes } from '@fortawesome/free-solid-svg-icons';

DiscountModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleCancelDiscount: PropTypes.func.isRequired,
}

function DiscountModal({
  isOpen,
  handleCancelDiscount,
}) {

  const [isOtherReason, setIsOtherReason] = useState(false);

  const handleSubmit = ({ code, reason, other_reason }) => {
    console.log({ code, reason, other_reason });
  }

  const handleChange = (value) => {
    if (value === 'other') {
      setIsOtherReason(true);
      return;
    }
    setIsOtherReason(false);
  }

  const handleCancel = () => {
    Modal.confirm({
      title: 'Are you sure you want to cancel discount?',
      okText: 'OK',
      cancelText: 'Cancel',
      onOk: () => { handleCancelDiscount() }
    })
  }

  return (
    <Modal
      title="Verify code to apply discount"
      visible={isOpen}
      footer={null}
      closable={false}
      closeIcon={null}
    >
      <Form
        id="applyDiscountForm"
        onFinish={handleSubmit}
      >
        <Form.Item
          name="code"
        >
          <Input
            placeholder="Input verify code with 6 digits"
            size="large"
          />
        </Form.Item>
        <Form.Item
          name="reason"
        >
          <Select
            defaultValue="Please choose reason"
            placeholder="Please choose reason"
            size="large"
            onChange={handleChange}
          >
            <Select.Option value="point">Use Customer Point</Select.Option>
            <Select.Option value="member">Hasaki member</Select.Option>
            <Select.Option value="other">Other reason</Select.Option>
          </Select>
        </Form.Item>
        {
          isOtherReason && (
            <Form.Item
              name="other_reason"
            >
              <Input
                placeholder="Reason"
                size="large"
              />
            </Form.Item>
          )
        }
      </Form>
      <div style={{ display: 'flex', justifyContent: 'flex-end', width: '100%' }}>
        <Space>
          <button
            type="button"
            onClick={handleCancel}
            style={{ width: '14em' }}
            className="btn btn-danger btn-cash"
          >
            <Space>
              <FontAwesomeIcon size="2x" icon={faTimes} />
              <b>Cancel Discount</b>
            </Space>
          </button>
          <button
            type="submit"
            form="applyDiscountForm"
            style={{ width: '14em' }}
            className="btn btn-success btn-cash"
          >
            <FontAwesomeIcon size="2x" icon={faCheck} />
            <b>Verify</b>
          </button>
        </Space>
      </div>
    </Modal>
  )
}

export default DiscountModal;