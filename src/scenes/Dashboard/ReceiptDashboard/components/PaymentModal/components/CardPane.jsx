import React, { useEffect, useRef } from 'react';
import QuickAmountArea from './QuickAmountArea';
import PropTypes from 'prop-types';
import { faMoneyBill } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import formatMoney from 'utils/money';
import { useDispatch, useSelector } from 'react-redux';
import { addAmountCardPaymentAction, bidvCardPaymentRequestAction, cancelCardPaymentAction, cardPaymentSuccessAction, handleBidvCardPaymentAction, vcbCardPaymentRequestAction } from 'share/api/Payment/Type/Card/action';
import Modal from 'antd/lib/modal/Modal';
import { CheckCircleOutlined, CloseCircleOutlined, LoadingOutlined } from '@ant-design/icons';
import { Space } from 'antd';
import { useHotkeys } from 'react-hotkeys-hook';

CardPane.propTypes = {
  handleShowCaculator: PropTypes.func.isRequired,
}

function CardPane({ handleShowCaculator }) {
  const {
    amount,
    isPaymentRequest,
    message,
    partner,
    isLoading,
    isError,
    isDone
  } = useSelector(state => state.api.payment.type.card);
  const { remainder, totalPrices } = useSelector(state => state.api.payment.summary);
  const { client_receipt_id } = useSelector(state => state.api.posReceipt);
  const { store_id } = useSelector(state => state.auth.info.profile);

  const dispatch = useDispatch();

  useEffect(async () => {
    await window.api.listenBidvResponse((e, bidvResponse) => {
      dispatch(handleBidvCardPaymentAction(JSON.parse(bidvResponse), client_receipt_id));
    })
  }, [])

  useHotkeys('Enter', () => {
    if (isPaymentRequest && isDone) {
      handleSuccessPayment();
    }
  }, {
    enableOnTags: ['INPUT', 'SELECT', 'TEXTAREA',],
  });

  const handleAddCash = (money) => {
    if (money === 0) {
      dispatch(addAmountCardPaymentAction(totalPrices, client_receipt_id));
      return;
    }
    dispatch(addAmountCardPaymentAction(money + amount, client_receipt_id))
  }

  const handleRequestPayment = () => {
    const source_code = 21101900000001;
    const ip_pos = '172.16.10.32';

    if (ip_pos.indexOf('.') !== -1) {
      dispatch(bidvCardPaymentRequestAction(source_code, store_id, amount, ip_pos, client_receipt_id));
    } else {
      dispatch(vcbCardPaymentRequestAction(source_code, store_id, amount, ip_pos, client_receipt_id));
    }
  }

  const handleCancelPayment = () => {
    dispatch(cancelCardPaymentAction(client_receipt_id));
  }

  const handleSuccessPayment = () => {
    dispatch(cardPaymentSuccessAction(client_receipt_id));
  }

  return (
    <>
      <div>
        <div style={{ margin: '1em 0 2em 0' }} className="input-group input-group-lg">
          <span className="input-group-addon">
            <FontAwesomeIcon size="lg" icon={faMoneyBill} />
          </span>
          <input
            className="form-control text-right"
            type="text"
            readonly="readonly"
            style={{ cursor: `${!isDone && 'pointer'}`, color: `${isDone && 'green'}`, fontWeight: `${isDone && 'bold'}` }}
            value={formatMoney(amount)}
            onClick={() => { handleShowCaculator(amount, 'cardPayment') }}
            disabled={isDone}
          />
        </div>
        {
          !isDone && (
            <>
              <QuickAmountArea
                handleAddAmount={handleAddCash}
                remainder={remainder}
              />
              <div style={{ margin: '4em 0 2em 0', display: 'flex', justifyContent: 'flex-end' }}>
                <button
                  onClick={handleRequestPayment}
                  className="btn btn-cash"
                  style={{ color: 'white', backgroundColor: '#242B2E' }}
                >
                  <b>PAY</b>
                </button>
              </div>
            </>
          )
        }
      </div>
      <Modal
        className="payment-check-status-modal"
        title={`${partner} Payment Request`}
        visible={isPaymentRequest}
        closeIcon={null}
        closable={false}
        cancelButtonProps={{ style: { display: 'none' } }}
        okButtonProps={{ style: { display: 'none' } }}
        footer={null}
      >
        <div className="payment-check-status-modal__container">
          {
            isLoading && <LoadingOutlined style={{ color: 'blue' }} />
          }
          {
            isError && <CloseCircleOutlined style={{ color: 'red' }} />
          }
          {
            isDone && <CheckCircleOutlined style={{ color: 'green' }} />
          }
          <span>{message}</span>
        </div>
        <h4>Request payment for amount: <span style={{ fontSize: '2em', color: 'green' }}>{formatMoney(amount)}đ</span></h4>
        <div className="payment-check-status-modal__btn-group">
          {
            (!isLoading && isError) && (
              <Space>
                <button onClick={handleRequestPayment} className="btn btn-info btn-cash"><b>Retry</b></button>
                <button onClick={handleCancelPayment} className="btn btn-danger btn-cash"><b>Cancel</b></button>
              </Space>
            )
          }
          {
            (!isLoading && isDone) && (
              <button onClick={handleSuccessPayment} className="btn btn-success btn-cash"><b>Confirm</b></button>
            )
          }
        </div>
      </Modal>
    </>
  )
}

export default CardPane;