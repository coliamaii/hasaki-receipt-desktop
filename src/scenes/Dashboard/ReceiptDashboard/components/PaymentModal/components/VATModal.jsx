import Modal from 'antd/lib/modal/Modal';
import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Input, Space, Spin } from 'antd';
import { faMagic, faSave } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import formatMoney from 'utils/money';
import { useDispatch, useSelector } from 'react-redux';
import { checkVATPaymentAction, vatPaymentDoneAction } from 'share/api/Payment/VAT/action';

VATModal.propTypes = {
  isVATModalOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  amount: PropTypes.number.isRequired,
}

function VATModal({
  isVATModalOpen,
  handleClose,
  amount,
}) {
  const [taxInput, setTaxInput] = useState('');

  const { companyName, address, isLoading } = useSelector(state => state.api.payment.vat);
  const { client_receipt_id } = useSelector(state => state.api.posReceipt);

  const formRef = useRef(null);

  const dispatch = useDispatch();

  useEffect(() => {
    formRef.current?.setFieldsValue({
      company_name: companyName,
      address
    });
  }, [companyName, address]);

  const handleSubmit = ({ address, company_name, email, tax }) => {
    dispatch(vatPaymentDoneAction(tax, company_name, address, email,client_receipt_id));
    handleClose();
  }

  const handleCheckRegNumber = () => {
    dispatch(checkVATPaymentAction(taxInput, amount, client_receipt_id));
  }

  return (
    <Modal
      title="VAT Information"
      footer={
        <span className="text-red">
          ** Không xuất hóa đơn đối với các hình thức thanh toán sử dụng gift card (sodexo, got it)
        </span>
      }
      visible={isVATModalOpen}
      onCancel={handleClose}
      width="40%"
    >
      <Spin size="large" spinning={isLoading} tip="Loading...">
        <Form
          id="VATForm"
          ref={formRef}
          onFinish={handleSubmit}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{
            company_name: companyName,
            address,
            amount: formatMoney(amount),
          }}
        >
          <Form.Item
            label="Tax Identification Number"
            name="tax"
          >
            <Input
              placeholder="Tax Identification Number"
              value={taxInput}
              onChange={(e) => { setTaxInput(e.target.value) }}
            />
          </Form.Item>
          <Form.Item
            label="Name"
            name="company_name"
            rules={[{ required: true, message: 'Please input company name!' }]}
          >
            <Input
              placeholder="Company name"
            />
          </Form.Item>
          <Form.Item
            label="Address"
            name="address"
            rules={[{ required: true, message: 'Please input address!' }]}
          >
            <Input.TextArea
            />
          </Form.Item>
          <Form.Item
            label="Amount"
            name="amount"
          >
            <Input
              placeholder="Amount"
              disabled={true}
            />
          </Form.Item>
          <Form.Item
            label="Email"
            name="email"
            rules={[{ type: 'email', message: 'Email không đúng định dạng!' }]}
          >
            <Input
              placeholder="Email"
            />
          </Form.Item>
        </Form>
        <Space style={{ display: 'flex', justifyContent: 'flex-end', marginTop: '1em' }}>
          <button
            type="button"
            className={`btn btn-secondary btn-cash`}
            onClick={handleCheckRegNumber}
          >
            <Space>
              <FontAwesomeIcon size="lg" icon={faMagic} />
              <b>Check</b>
            </Space>
          </button>
          <button
            type="submit"
            className={`btn btn-primary btn-cash`}
            form="VATForm"
          >
            <Space>
              <FontAwesomeIcon size="lg" icon={faSave} />
              <b>Save</b>
            </Space>
          </button>
        </Space>
      </Spin>
    </Modal>
  )
}

export default VATModal;