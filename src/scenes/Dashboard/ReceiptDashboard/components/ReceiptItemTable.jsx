import React from "react";
import ReceiptItem from "./ReceiptItem";
import PropTypes from 'prop-types';
import formatMoney from 'utils/money';

ReceiptItemTable.propTypes = {
  data: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired,
  handleIncreaseQty: PropTypes.func.isRequired,
  handleRemoveProduct: PropTypes.func.isRequired,
  showModal: PropTypes.func.isRequired,
  handleActiveCode: PropTypes.func.isRequired,
  gift_items: PropTypes.array.isRequired,
}

function ReceiptItemTable({
  data,
  isLoading,
  handleIncreaseQty,
  handleRemoveProduct,
  showModal,
  handleActiveCode,
  gift_items,
}) {
  const countItem = () => {
    return [
      ...data, ...gift_items
    ].reduce((total, item) => {
      return total + item.qty;
    }, 0)
  }

  return (
    <table className="table table-hover table-bordered" id="tbl_receipt_item">
      <thead>
        <tr data-total-item="0">
          <th className="text-center">No.</th>
          <th className="text-center">SKU</th>
          <th className="col-xs-4 text-center">Item</th>
          <th className="col-xs-2 text-center">Unit Cost</th>
          <th className="col-xs-2 text-center">Discount</th>
          <th className="col-xs-2 text-center">Qty</th>
          <th className="col-xs-2 text-center">Line Total</th>
        </tr>
      </thead>
      <tbody>
        {
          isLoading && (
            <tr className="table-loading">
              <td colSpan="7">
                <span>Loading!!!</span>
              </td>
            </tr>
          )
        }
        {
          !isLoading && data.length !== 0 && [...data].reverse().map((item, index) => {
            return (
              <ReceiptItem
                key={item.sku}
                sku={item.sku}
                name={item.name}
                base_price={item.base_price}
                discount_price={item.discount_price}
                discount={item.discount}
                qty={item.qty}
                lineTotal={item.lineTotal}
                handleIncreaseQty={handleIncreaseQty}
                handleRemoveProduct={handleRemoveProduct}
                showModal={showModal}
                product_config={item.product_config}
                product_brand_id={item.product_brand_id}
                handleActiveCode={handleActiveCode}
                isGift={false}
                index={index + 1}
              />
            )
          })
        }
        {
          !isLoading && gift_items.length !== 0 && gift_items.map(item => {
            return (
              <ReceiptItem
                key={item.sku}
                sku={item.sku}
                name={item.name}
                base_price={item.base_price}
                discount_price={item.discount_price}
                discount={item.discount}
                qty={item.qty}
                lineTotal={item.lineTotal}
                isGift={true}
              />
            )
          })
        }
        <tr className="bg-gray active">
          <td colSpan="5">&nbsp;</td>
          <td>
            <input
              className="form-control text-right input-lg"
              type="text"
              readOnly={true}
              value={countItem()}
            />
          </td>
          <td>
            <input
              className="form-control text-right input-lg"
              type="text"
              readOnly={true}
              value={
                formatMoney(data.reduce((total, item) => {
                  return total + item.lineTotal
                }, 0))
              }
            />
          </td>
        </tr>
      </tbody>
    </table>
  );
}

export default ReceiptItemTable;
