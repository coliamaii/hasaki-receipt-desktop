const { screen, render, cleanup } = require("@testing-library/react");
const { default: ReceiptItem } = require("..");
const { default: configureStore } = require('redux-mock-store');
const { Provider } = require("react-redux");
const { default: userEvent } = require("@testing-library/user-event");

afterEach(cleanup);
beforeEach(() => {
  jest.spyOn(console, 'error').mockImplementation(() => { });
});

const mockStore = configureStore([]);

const renderingComponent = ({ index, discount, product_config, brand_id = 209, activeCode, price, isGift }) => {
  const initalState = {
    mode: {
      paymentState: false,
    },
    api: {
      product: {
        productTable: {
          codes: activeCode !== undefined ? { 100390065: activeCode } : {},
        }
      }
    }
  }
  const store = mockStore(initalState);
  const handleIncreaseQty = jest.fn();
  const showCaculatorModal = jest.fn();
  const handleRemoveProduct = jest.fn();
  const handleActiveCode = jest.fn();

  const { container } = render(
    <Provider store={store}>
      <ReceiptItem
        sku="100390065"
        name="Sản phẩm A"
        base_price={price ?? 100000}
        discount_price={0}
        discount={discount}
        qty={2}
        lineTotal={200000 - (discount * 2)}
        isGift={isGift ?? false}
        index={index}
        handleIncreaseQty={handleIncreaseQty}
        showModal={showCaculatorModal}
        handleRemoveProduct={handleRemoveProduct}
        product_config={product_config}
        product_brand_id={brand_id}
        handleActiveCode={handleActiveCode}
      />
    </Provider>
  )

  return { container, handleIncreaseQty, showCaculatorModal, handleRemoveProduct, handleActiveCode };
}

describe('Test normal ReceiptItem rendering', () => {
  test('Component render visual correctly', () => {
    const { container } = renderingComponent({ index: 5, discount: 0, product_config: 4 });
    const productNameElement = screen.queryByText('Sản phẩm A');
    const productSkuElement = screen.queryByText('100390065');
    const productBasePriceElement = screen.queryByText('100,000');
    const productLineTotalElement = screen.queryByText('200,000');
    const showCaculatorElement = screen.getByDisplayValue('2');
    const increaseQtyBtnElement = container.querySelector('.fa-plus').parentElement;
    const deleteProductBtnElement = container.querySelector('.fa-trash').parentElement;
    const activeCodeBtnElement = container.querySelector('.fa-lock-open');

    expect(productNameElement).toBeInTheDocument();
    expect(productSkuElement).toBeInTheDocument();
    expect(productBasePriceElement).toBeInTheDocument();
    expect(productLineTotalElement).toBeInTheDocument();
    expect(deleteProductBtnElement).toBeInTheDocument();
    expect(activeCodeBtnElement).not.toBeInTheDocument();

    expect(productNameElement.classList.contains('text-bold')).toEqual(false);
    expect(productSkuElement.classList.contains('text-bold')).toEqual(false);

    expect(increaseQtyBtnElement.disabled).toEqual(false);
    expect(showCaculatorElement.disabled).toEqual(false);
  });

  test('Bold text if component is newest', () => {
    const { container } = renderingComponent({ index: 1, discount: 0, product_config: 4 });
    const productNameElement = screen.queryByText('Sản phẩm A');
    const productSkuElement = screen.queryByText('100390065').parentNode;

    expect(productNameElement.classList.contains('text-bold')).toEqual(true);
    expect(productSkuElement.classList.contains('text-bold')).toEqual(true);
  })

  test('Fire event when click increase qty', () => {
    const { container, handleIncreaseQty } = renderingComponent({ index: 5, discount: 0, product_config: 4 });
    const increaseQtyBtnElement = container.querySelector('.fa-plus').parentElement;

    userEvent.click(increaseQtyBtnElement);
    expect(handleIncreaseQty).toBeCalledTimes(1);
  });

  test('Can click to show caculator modal', () => {
    const { showCaculatorModal, container } = renderingComponent({ index: 1, discount: 0, product_config: 4 });
    const showCaculatorElement = container.querySelector('.table-qty-input');
    userEvent.click(showCaculatorElement);
    expect(showCaculatorModal).toBeCalledTimes(1);
  });

  test('Fire event when click remove button', () => {
    const { handleRemoveProduct, container } = renderingComponent({ index: 1, discount: 0, product_config: 4 });
    const removeProductBtnElement = container.querySelector('.fa-trash').parentElement;
    userEvent.click(removeProductBtnElement);
    expect(handleRemoveProduct).toBeCalledTimes(1);
  });
});

describe('Test ReceiptItem when discount', () => {
  test('Component render infomation correctly', () => {
    const { container } = renderingComponent({ index: 1, discount: 20000, product_config: 4 });

    const discountPriceElement = screen.queryByText('Giam: 20,000');
    const lineTotalElement = screen.queryByText('160,000');

    expect(lineTotalElement).toBeInTheDocument();
    expect(discountPriceElement).toBeInTheDocument();
  });

  test('Component render with different color', () => {
    const { container } = renderingComponent({ index: 1, discount: 20000, product_config: 4 });

    expect(container.firstChild.classList.contains('table-discount')).toEqual(true);
  });
});

describe('Test ReceiptItem when set expired day', () => {
  test('Can not change qty and disabled', () => {
    const { container, handleIncreaseQty, showCaculatorModal } = renderingComponent({ index: 1, discount: 20000, product_config: 6 });
    const showCaculatorElement = screen.getByDisplayValue('2');
    const increaseQtyBtnElement = container.querySelector('.fa-plus');
    const blockQtyBtnElement = container.querySelector('.fa-ban');

    expect(increaseQtyBtnElement).not.toBeInTheDocument();
    expect(blockQtyBtnElement).toBeInTheDocument();
    expect(blockQtyBtnElement.parentElement.classList.contains('disabled')).toEqual(true);
    expect(showCaculatorElement.disabled).toEqual(true);

    userEvent.click(blockQtyBtnElement);
    expect(handleIncreaseQty).not.toBeCalled();

    userEvent.click(showCaculatorElement);
    expect(showCaculatorModal).not.toBeCalled();
  });
});

describe('Test ReceiptItem which have active code', () => {
  test('When amount of active code smaller than qty, can coutinue active', () => {
    const { container, handleActiveCode } = renderingComponent({
      index: 1,
      discount: 20000,
      product_config: 4,
      brand_id: 1285,
      activeCode: [12345678910],
    });

    const activeCodeBtnElement = document.querySelector('.fa-lock-open');
    expect(activeCodeBtnElement).toBeInTheDocument();
    expect(activeCodeBtnElement.parentElement.disabled).not.toEqual(true);

    userEvent.click(activeCodeBtnElement);
    expect(handleActiveCode).toBeCalledTimes(1);
  });

  test('When amount of active code equal qty, can not active code', () => {
    const { container } = renderingComponent({
      index: 1,
      discount: 20000,
      product_config: 4,
      brand_id: 1285,
      activeCode: [12345678910, 12345678910],
    });

    const activeCodeBtnElement = document.querySelector('.fa-lock-open');
    expect(activeCodeBtnElement).not.toBeInTheDocument();
  });
});

describe('Test ReceiptItem when it is a gift', () => {
  test('Component render correctly', () => {
    const { container, showCaculatorModal } = renderingComponent({
      price: 0,
      discount: 0,
      isGift: true,
    });

    const deleteProductBtnElement = container.querySelector('.fa-trash');
    const showCaculatorElement = screen.getByDisplayValue('2');
    const increaseQtyBtnElement = container.querySelector('.fa-plus');

    expect(deleteProductBtnElement).not.toBeInTheDocument();
    expect(increaseQtyBtnElement).not.toBeInTheDocument();
    expect(showCaculatorElement).toBeInTheDocument();
    expect(showCaculatorElement.disabled).toEqual(true);

    userEvent.click(showCaculatorElement);
    expect(showCaculatorModal).not.toBeCalled();
  });
});

