import React from 'react';
import { faBan, faLockOpen, faPlus, faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PropTypes from 'prop-types';
import formatMoney from 'utils/money';
import { useSelector } from 'react-redux';

ReceiptItem.propTypes = {
  sku: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  base_price: PropTypes.number.isRequired,
  discount_price: PropTypes.number.isRequired,
  discount: PropTypes.number.isRequired,
  qty: PropTypes.number.isRequired,
  lineTotal: PropTypes.number.isRequired,
  isGift: PropTypes.bool.isRequired,
  index: PropTypes.number,
  handleIncreaseQty: PropTypes.func,
  handleRemoveProduct: PropTypes.func,
  showModal: PropTypes.func,
  product_config: PropTypes.number,
  product_brand_id: PropTypes.number,
  handleActiveCode: PropTypes.func,
}

function ReceiptItem({
  sku,
  name,
  base_price,
  discount_price,
  discount,
  qty,
  lineTotal,
  handleIncreaseQty,
  handleRemoveProduct,
  showModal,
  product_config,
  product_brand_id,
  handleActiveCode,
  isGift,
  index,
}) {
  const { paymentState } = useSelector(state => state.mode);
  const codes = useSelector(state => state.api.product.productTable.codes);

  const canNotChangeQty = () => {
    return (product_config & 2) === 2 || product_brand_id === 1285 || product_brand_id === 'Hasaki' || isGift || paymentState;
  }

  const canActiveCode = () => {
    return (product_brand_id === 1285 || product_brand_id === 'Hasaki') && ((codes[sku]?.length ?? 0) < qty);
  }

  return (
    <tr
      aria-label="ReceiptItem"
      className={`${discount !== 0 && 'table-discount'}`}
    >
      <td>
        {
          (!isGift && !paymentState) && (
            <button
              className="btn btn-default btn-xs btn_remove_item btn-danger"
              onClick={() => { handleRemoveProduct(sku) }}
            >
              <FontAwesomeIcon icon={faTrash} />
            </button>
          )
        }
        {
          paymentState && index
        }
      </td>
      <td className={`${index === 1 && 'text-bold'}`}>
        <div>{sku}</div>
        {
          canActiveCode() && (
            <button
              aria-label="ActiveCodeButton"
              onClick={() => { handleActiveCode(sku, name) }}
              type="button"
              className="btn btn-default btn-xs"
            >
              <FontAwesomeIcon icon={faLockOpen} size="xs" />
            </button>
          )
        }
      </td>
      <td className={`${index === 1 && 'text-bold'}`}>
        {name}
      </td>
      <td className="text-right">
        {formatMoney(base_price)}
        <br />
        {
          !isGift && (
            <small>Giam: {formatMoney(discount)}</small>
          )
        }
      </td>
      <td className="text-right">
        <input
          type="text"
          className="form-control text-right"
          readOnly={false}
          defaultValue="0"
          disabled
        />
      </td>
      <td>
        <div className="input-group">
          <input
            type="text"
            className="table-qty-input form-control nmpd-display nmpd-target"
            value={qty}
            data-type="qty"
            readOnly={true}
            onClick={() => { showModal(qty, sku) }}
            disabled={canNotChangeQty()}
          />
          {
            !isGift && (
              <span className="input-group-btn">
                <button
                  className={`btn btn-default btn_increase_qty ${canNotChangeQty() ? 'disabled btn-danger' : 'btn-success'}`}
                  style={{ outline: 'none' }}
                  type="button"
                  onClick={() => {
                    if (!canNotChangeQty()) {
                      handleIncreaseQty(sku)
                    }
                  }}
                >
                  {
                    !canNotChangeQty() ? <FontAwesomeIcon icon={faPlus} /> : <FontAwesomeIcon icon={faBan} />
                  }
                </button>
              </span>
            )
          }
        </div>
      </td>
      <td className="text-right ">{formatMoney(lineTotal)}</td>
    </tr>
  )
}

export default ReceiptItem;