import React from 'react';

function SummaryBox() {
  return (
    <>
      <div className="box">
        <div className="box-header with-border">
          <h3 className="box-title">Summary</h3>
        </div>
        <div className="box-body">
          <div className="row" style={{ fontSize: '16px' }}>
            <div className="col-xs-12">
              <div className="col-xs-5">Cash</div>
              <div className="col-xs-7 text-right">
                <span className="receipt_cash">0</span>
              </div>
            </div>
            <div className="col-xs-12">
              <div className="col-xs-5">Card Payment</div>
              <div className="col-xs-7 text-right">
                <span className="receipt_card">0</span>
              </div>
            </div>
            <div className="col-xs-12">
              <div className="col-xs-5">QR Payment</div>
              <div className="col-xs-7 text-right">
                <span className="receipt_qr">0</span>
              </div>
            </div>
            <div className="col-xs-12">
              <div className="col-xs-5">Gift Card</div>
              <div className="col-xs-7 text-right">
                <span className="receipt_gift_card">0</span>
              </div>
            </div>
            <div className="col-xs-12">
              <div className="col-xs-5">Balance</div>
              <div className="col-xs-7 text-right">
                <span className="receipt_gift_card">0</span>
              </div>
            </div>
            <div className="col-xs-12">
              <div className="col-xs-5">Charge</div>
              <div className="col-xs-7 text-right">
                <span className="receipt_charge">0</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default SummaryBox;