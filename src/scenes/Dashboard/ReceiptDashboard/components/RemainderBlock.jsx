import React from "react";
import PropTypes from "prop-types";

function RemainderBlock(props) {
  return (
    <div className="row" id="remainder-block">
      <br />
      <div className="col-xs-4 text-right pull-right">
        <div className="input-group input-group-lg">
          <span className="input-group-addon text-red">
            <strong>Remainder:</strong>
          </span>
          <input
            type="text"
            name="remainder"
            id="remainder"
            className="form-control text-right"
            readOnly={false}
            defaultValue=""
            disabled
          />
        </div>
      </div>
    </div>
  );
}

RemainderBlock.propTypes = {};

export default RemainderBlock;
