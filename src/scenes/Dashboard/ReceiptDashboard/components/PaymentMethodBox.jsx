import React from 'react';
import CardPaymentIcon from 'share/components/CardPaymentIcon';
import CashPaymentIcon from 'share/components/CashPaymentIcon';
import GiftCardPaymentIcon from 'share/components/GiftCardPaymentIcon';
import QrPaymentIcon from 'share/components/QrPaymentIcon';

function PaymentMethodBox() {
  return (
    <>
      <div id="payment-method-group">
        <div className="box box-default">
          <div className="box-header with-border">
            <h3 className="box-title">Payment Method</h3>
          </div>
          <div className="box-body">
            <div className="row">
              <div className="col-xs-12 no-padding text-center">
                <button type="button" className="btn btn-default btn-app btn-payment bg-gray" name="cash_payment">
                  <CashPaymentIcon />
                </button>

                <button type="button" className="btn btn-default btn-app btn-payment bg-gray" name="card_payment">
                  <CardPaymentIcon />
                </button>

                <button type="button" className="btn btn-default btn-app btn-payment bg-gray" name="qr_payment">
                  <QrPaymentIcon />
                </button>

                <button type="button" className="btn btn-default btn-app btn-payment bg-gray" name="gift_payment">
                  <GiftCardPaymentIcon />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default PaymentMethodBox;