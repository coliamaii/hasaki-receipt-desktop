import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import appButtonColor from 'share/components/AppButton/app-button-color';
import ComboBlock from '../ComboBlock';
import areaState from './area-state';

import './left-bot-area.scss';
import { setTopBotAreaStateModeAction } from 'share/mode/action';
import GiftBlock from '../GiftBlock';

LeftBotArea.propTypes = {
  handleAddCombo: PropTypes.func.isRequired,
  handleTypingSearch: PropTypes.func.isRequired,
  searchGift: PropTypes.array.isRequired,
  handleSearchGift: PropTypes.func.isRequired,
  giftBlockMessage: PropTypes.string.isRequired,
}

function LeftBotArea({
  handleAddCombo,
  handleTypingSearch,
  searchGift,
  handleSearchGift,
  giftBlockMessage,
}) {
  const { topBotAreaState, paymentState } = useSelector(state => state.mode);
  const { combo_suggestion } = useSelector(state => state.api.product.productTable);

  const dispatch = useDispatch();

  useEffect(() => {
    if (combo_suggestion.length === 0) {
      dispatch(setTopBotAreaStateModeAction(null));
    }
  }, [combo_suggestion]);

  return (
    <>
      {
        (topBotAreaState === areaState.COMBO && !paymentState) && (
          <div
            className="left-bot-area"
            style={{ borderColor: `${appButtonColor.COMBOS}` }}
          >
            <ComboBlock
              handleAddCombo={handleAddCombo}
            />
          </div>
        )
      }
      {
        (topBotAreaState === areaState.GIFTS && !paymentState) && (
          <div
            className="left-bot-area"
            style={{ borderTop: `0.5em solid ${appButtonColor.GIFTS}` }}
          >
            <GiftBlock
              handleSearchProduct={handleTypingSearch}
              searchData={searchGift}
              handleGetProducts={handleSearchGift}
              message={giftBlockMessage}
            />
          </div>
        )
      }
    </>
  )
}

export default LeftBotArea;