import React from "react";
import PropTypes from "prop-types";
import {
  faCalculator,
  faCheckSquare,
  faFile,
  faGifts,
  faList,
  faTabletAlt,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Row } from "antd";
import AppButton from "share/components/AppButton";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import areaState from "./LeftBotArea/area-state";
import { setTopBotAreaStateModeAction } from "share/mode/action";
import appButtonColor from "share/components/AppButton/app-button-color";
import HOTKEY from "utils/hotkey";

TopBtnBox.propTypes = {
  isPayment: PropTypes.bool,
  handleResetTable: PropTypes.func.isRequired,
  handleClickBtnFillInfo: PropTypes.func.isRequired
}

function TopBtnBox({ isPayment, handleResetTable, handleClickBtnFillInfo }) {
  const { combo_suggestion } = useSelector(state => state.api.product.productTable);
  const { gifts } = useSelector(state => state.api.product.searchProduct);
  const { topBotAreaState } = useSelector(state => state.mode);

  const history = useHistory();
  const dispatch = useDispatch();

  const handleClickComboButton = () => {
    if (topBotAreaState !== areaState.COMBO && combo_suggestion.length !== 0) {
      dispatch(setTopBotAreaStateModeAction(areaState.COMBO));
      return;
    }
    dispatch(setTopBotAreaStateModeAction(null));
  }

  const handleClickGiftsButton = () => {
    if (topBotAreaState !== areaState.GIFTS && gifts.length !== 0) {
      dispatch(setTopBotAreaStateModeAction(areaState.GIFTS));
      return;
    }
    dispatch(setTopBotAreaStateModeAction(null));
  }

  return (
    <div className="box box-default">
      <div className="box-body">
        <Row gutter={6}>
          <Col md={6}>
            <AppButton
              color={appButtonColor.NEW}
              content={`New`}
              icon={<FontAwesomeIcon icon={faFile} size="2x" />}
              handleClick={handleResetTable}
              hotKey={HOTKEY.POS_NEW_CLICK.key}
            />
          </Col>
          <Col md={6}>
            <AppButton
              color={appButtonColor.RECONCILE}
              content="Reconcile"
              icon={<FontAwesomeIcon icon={faCalculator} size="2x" />}
              handleClick={() => { history.push('/report-profile/reconcile') }}
            />
          </Col>
          <Col md={6}>
            <AppButton
              color={appButtonColor.RECEIPTS}
              icon={<FontAwesomeIcon icon={faList} size="2x" />}
              content="Receipts"
              handleClick={() => { history.push('/report-profile/receipt') }}
            />
          </Col>
          <Col md={6}>
            <AppButton
              color="#fa8c16"
              content="Test"
            />
          </Col>
          <Col md={6}>
            <AppButton
              color={appButtonColor.GIFTS}
              content={`Gifts`}
              icon={<FontAwesomeIcon icon={faGifts} size="2x" />}
              counter={gifts.length}
              isDisabled={gifts.length === 0}
              handleClick={handleClickGiftsButton}
              hotKey={HOTKEY.POS_SHOW_GIFTS_CLICK.key}
              ariaLabel="GiftsAppButton"
            />
          </Col>
          <Col md={6}>
            <AppButton
              color={appButtonColor.COMBOS}
              content={`Combos`}
              icon={<FontAwesomeIcon icon={faCheckSquare} size="2x" />}
              counter={combo_suggestion.length}
              handleClick={handleClickComboButton}
              isDisabled={combo_suggestion.length === 0}
              hotKey={HOTKEY.POS_SHOW_COMBOS_CLICK.key}
            />
          </Col>
          <Col md={6}>
            <AppButton
              color="#00FF00"
              content="Test"
            />
          </Col>
          <Col md={6}>
            <AppButton
              color="#50DBB4"
              content="Fill"
              icon={<FontAwesomeIcon icon={faTabletAlt} size="2x" />}
              isDisabled={false}
              handleClick={handleClickBtnFillInfo}
            />
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default TopBtnBox;
