export const mock_combo_suggestion = [
  {
    combo_sku: 190012345,
    combo_name: 'Combo A',
    combo_details: [
      { sku: 200000000, qty: 1 },
    ],
  },
  {
    combo_sku: 190054321,
    combo_name: 'Combo B',
    combo_details: [
      { sku: 200000000, qty: 1 },
      { sku: 200000001, qty: 1 },
      { sku: 200000002, qty: 1 },
    ],
  },
  {
    combo_sku: 190034567,
    combo_name: 'Combo C',
    combo_details: [
      { sku: 200000000, qty: 1 },
      { sku: 200000001, qty: 1 },
      { sku: 200000002, qty: 4 },
      { sku: 200000003, qty: 1 },
    ],
  },
]