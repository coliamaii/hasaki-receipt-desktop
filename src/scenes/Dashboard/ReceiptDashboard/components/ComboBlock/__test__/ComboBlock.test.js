const { screen, render, cleanup } = require("@testing-library/react");
const { default: userEvent } = require("@testing-library/user-event");
const { Provider } = require("react-redux");
const { default: configureStore } = require('redux-mock-store');
const { default: ComboBlock } = require("..");
const { mock_combo_suggestion } = require("./comboSuggestion.mock");

afterEach(cleanup);
beforeEach(() => {
  jest.spyOn(console, 'error').mockImplementation(() => { });
});

const renderingComponent = ({ combo_suggestion }) => {
  const mockStore = configureStore([]);
  const initalState = {
    api: {
      product: {
        productTable: {
          combo_suggestion,
        }
      }
    }
  }

  const handleAddCombo = jest.fn();

  const store = mockStore(initalState);
  const { container } = render(
    <Provider store={store}>
      <ComboBlock
        handleAddCombo={handleAddCombo}
      />
    </Provider>
  )

  return { container, handleAddCombo };
}

describe('Test ComboBlock component', () => {
  test('When combo items is empty', () => {
    const { container } = renderingComponent({
      combo_suggestion: [],
    });

    const comboSuggestionItemsElement = [...document.querySelectorAll('tbody tr')];
    expect(comboSuggestionItemsElement.length).toEqual(0);
  });

  test('When combo items have data', () => {
    const { container } = renderingComponent({
      combo_suggestion: mock_combo_suggestion,
    });

    const comboSuggestionItemsElement = [...document.querySelectorAll('tbody tr')];
    expect(comboSuggestionItemsElement.length).toEqual(3);

    expect(screen.queryByText('Combo A')).toBeVisible();
    expect(screen.queryByText('Combo B')).toBeVisible();
    expect(screen.queryByText('Combo C')).toBeVisible();

    //expect productPrice

    expect(screen.queryByText('200000000 (SL: 1)')).toBeVisible();
    expect(screen.queryByText('200000000 (SL: 1) + 200000001 (SL: 1) + 200000002 (SL: 1)')).toBeVisible();
    expect(screen.queryByText('200000000 (SL: 1) + 200000001 (SL: 1) + 200000002 (SL: 4) + 200000003 (SL: 1)')).toBeVisible();
  });

  test('Fire event when click add combo', () => {
    const { container, handleAddCombo } = renderingComponent({
      combo_suggestion: mock_combo_suggestion,
    });

    const comboSuggestionAddButtonElement = [...document.querySelectorAll('tbody tr button')];
    expect(comboSuggestionAddButtonElement.length).toEqual(3);

    comboSuggestionAddButtonElement.forEach(item => {
      userEvent.click(item);
    })
    expect(handleAddCombo).toBeCalledTimes(mock_combo_suggestion.length);
  });
});
