import React from "react";
import { useSelector } from "react-redux";
import { Col, Row } from "antd";
import formatMoney from 'utils/money';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import appButtonColor from "share/components/AppButton/app-button-color";

ComboBlock.propTypes = {
  handleAddCombo: PropTypes.func.isRequired,
}

function ComboBlock({
  handleAddCombo,
}) {
  const { combo_suggestion } = useSelector(state => state.api.product.productTable);

  return (
    <div className={`combo_suggestion ${combo_suggestion.length === 0 && 'hidden'}`} id="combo-block">
      <div className="box-header">
        <h3
          className="box-title"
          style={{ color: appButtonColor.COMBOS }}
        >
          Combo Suggestion
        </h3>
      </div>
      <div className="box-body">
        <Row>
          <Col md={24}>
            {/* <!-- list items --> */}
            <table
              className="table table-hover table-bordered"
              id="tbl_receipt_combo_item"
            >
              <thead>
                <tr>
                  <th className="text-center">No.</th>
                  <th className="text-center">SKU</th>
                  <th className="col-xs-3 text-center">Name</th>
                  <th className="col-xs-1 text-center">Price</th>
                  <th className="col-xs-6 text-center">Combo Detail</th>
                </tr>
              </thead>
              <tbody>
                {
                  combo_suggestion.map((item) => (
                    <tr key={item.combo_sku}>
                      <td className="text-center">
                        <button
                          className="btn btn-default btn-xs btn_remove_item btn-success"
                          onClick={() => { handleAddCombo(item.combo_sku, item.combo_details) }}
                        >
                          <FontAwesomeIcon icon={faPlus} />
                        </button>
                      </td>
                      <td className="text-center">{item.combo_sku}</td>
                      <td className="col-xs-3 text-center">{item.combo_name}</td>
                      <td className="col-xs-1 text-center">{formatMoney(item.combo_price ?? 50000)}</td>
                      <td className="col-xs-6 text-center">
                        {
                          item.combo_details.map(item => `${item.sku} (SL: ${item.qty})`).join(' + ')
                        }
                      </td>
                    </tr>
                  ))
                }
              </tbody>
            </table>
            {/* <!-- end list items --> */}
          </Col>
        </Row>
        <hr />
      </div>
    </div>
  );
}

export default ComboBlock;
