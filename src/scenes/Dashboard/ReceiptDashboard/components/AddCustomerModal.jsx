import Modal from 'antd/lib/modal/Modal';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Col, Row } from 'antd';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useDispatch } from 'react-redux';
import { searchCustomerStoreNewAction } from 'share/api/Customer/SearchCustomer/actions';

AddCustomerModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleCancel: PropTypes.func.isRequired,
  customerPhone: PropTypes.string.isRequired,
  customerName: PropTypes.string,
  customerEmail: PropTypes.string,
}

const formSchema = yup.object().shape({
  customerName: yup
    .string()
    .required('Customer name is required!'),
  customerEmail: yup
    .string()
    .optional()
    .email('Email is not valid!')
});

function AddCustomerModal({
  isOpen,
  handleCancel,
  customerPhone,
  customerName,
  customerEmail,
}) {
  const [nameInput, setNameInput] = useState('');
  const [emailInput, setEmailInput] = useState('');

  useEffect(() => {
    setNameInput(customerName);
    setEmailInput(customerEmail);
  }, [customerName, customerEmail]);

  useEffect(() => {
    setNameInput('');
    setEmailInput('');
  }, [customerPhone])

  const { register, handleSubmit, errors, clearErrors } = useForm({
    resolver: yupResolver(formSchema),
    reValidateMode: 'onSubmit',
  });

  const dispatch = useDispatch();

  const handleAddCustomer = ({ customerName, customerEmail }) => {
    handleCancel()
    dispatch(searchCustomerStoreNewAction(customerName, customerEmail));
  }

  const handleCancelClick = () => {
    setNameInput(customerName ?? '');
    setEmailInput(customerEmail ?? '');
    clearErrors();
    handleCancel();
  }

  return (
    <Modal
      visible={isOpen}
      title="New customer"
      onCancel={handleCancelClick}
      okButtonProps={{
        type: 'primary',
        form: 'addCustomerForm',
        htmlType: 'submit',
      }}
      className="add-customer-modal"
    >
      <Row style={{ marginTop: '0.5em' }}>
        <form id="addCustomerForm" onSubmit={handleSubmit(handleAddCustomer)}>
          <Col md={24}>
            <div className="input-group input-group-lg blocked">
              <label htmlFor="customerPhone">Phone: </label>
              <input
                id="customerPhone"
                className="form-control"
                type="text"
                disabled={true}
                value={customerPhone}
              />
            </div>
          </Col>
          <Col md={24}>
            <div className="input-group input-group-lg">
              <label htmlFor="customerPhone">Name: </label>
              <div className="text-red">{errors.customerName?.message}</div>
              <input
                id="customerName"
                className="form-control"
                type="text"
                placeholder="Customer Name"
                ref={register}
                name="customerName"
                value={nameInput}
                onChange={(e) => { setNameInput(e.target.value) }}
              />
            </div>
          </Col>
          <Col md={24}>
            <div className="input-group input-group-lg">
              <label htmlFor="customerEmail">Email: </label>
              <div className="text-red">{errors.customerEmail?.message}</div>
              <input
                id="customerEmail"
                className="form-control"
                type="text"
                placeholder="Customer Email"
                ref={register}
                name="customerEmail"
                onChange={(e) => { setEmailInput(e.target.value) }}
                value={emailInput}
              />
            </div>
          </Col>
          <Col md={24}>
            <div className="input-group input-group-lg">
              <label htmlFor="customerPoint">Point: </label>
              <input
                id="customerPoint"
                className="form-control"
                type="text"
                value="0"
                disabled={true}
              />
            </div>
          </Col>
        </form>
      </Row>
    </Modal>
  )
}

export default AddCustomerModal;