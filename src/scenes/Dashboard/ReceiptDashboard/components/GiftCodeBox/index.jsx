import React, { useEffect, useRef } from "react";
import { faGift } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import {
  giftCodeRequestAction,
  giftCodeSuccessAction,
  giftCodeRemoveAction,
} from "share/api/Product/Gift/action";

GiftCodeBox.propTypes = {
  isPayment: PropTypes.bool.isRequired,
  items: PropTypes.array.isRequired,
}

function GiftCodeBox({ isPayment, items }) {
  const store_id = useSelector(state => state.auth.info.profile.store_id);
  const customer = useSelector(state => state.api.customer.searchCustomer.data);
  const { error, code, loading, waiting } = useSelector(state => state.api.product.gift.gift_code);

  const dispatch = useDispatch();
  const codeRef = useRef(null);

  useEffect(() => {
    codeRef.current.value = code
  }, [code])

  const readyUpdate = () => {
    return (code !== '' && waiting === false);
  }

  const handleUseCode = () => {
    if (readyUpdate()) {
      codeRef.current.value = '';
      dispatch(giftCodeRemoveAction());
      return;
    }

    const code = codeRef.current.value;

    if (code === '') {
      alert('Please input gift code!');
      return;
    }
    if (customer == null) {
      alert('Please input your phone!');
      return;
    }

    if (items.length === 0) {
      dispatch(giftCodeSuccessAction(code, true));
      return;
    }

    const data = items.map(item => ({ sku: item.sku, qty: item.qty, price: item.base_price, discount: item.discount }));

    dispatch(giftCodeRequestAction(code, customer.customer_id, data, store_id));
  }

  return (
    <>
      <p className="margin"></p>
      <div className="row">
        <div className="col-xs-12">
          <label htmlFor="receipt_change">Gift Code</label>
          <span style={{ marginLeft: '0.5em' }} className="text-red gift-msg">{error}</span>
          <div className="input-group input-group-lg">
            <input
              className="form-control"
              type="text"
              defaultValue={code}
              name="gift_code"
              placeholder="Gift Code"
              id="gift_code"
              {...((loading || isPayment || code !== '') && { disabled: true })}
              ref={codeRef}
            />
            <span className="input-group-btn">
              {
                !isPayment && (
                  <button
                    className={`btn btn-default ${readyUpdate() ? 'btn-danger' : 'btn-success'}`}
                    type="button"
                    id="btn_use_gift_code"
                    onClick={handleUseCode}
                    {...((loading) && { disabled: true })}
                  >
                    <FontAwesomeIcon icon={faGift} />
                    {readyUpdate() ? 'Remove' : 'Use'}
                  </button>
                )
              }
              <button
                aria-label="RemoveGiftCodeButton"
                className="btn btn-default hidden"
                type="button"
                id="btn_remove_gift_code"
              >
                <FontAwesomeIcon icon={faGift} /> Remove
                </button>
            </span>
          </div>
        </div>
      </div>
      <p className="margin"></p>
    </>
  );
}

export default GiftCodeBox;
