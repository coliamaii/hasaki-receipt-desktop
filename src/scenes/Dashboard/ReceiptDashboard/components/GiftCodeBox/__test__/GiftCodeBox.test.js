const { screen, render, cleanup } = require("@testing-library/react");
const { default: userEvent } = require("@testing-library/user-event");
const { Provider } = require("react-redux");
const { default: configureStore } = require('redux-mock-store');
const { default: GiftCodeBox } = require("..");
const redux = require('react-redux');

afterEach(cleanup);
beforeEach(() => {
  jest.spyOn(console, 'error').mockImplementation(() => { });
});

const renderingComponent = ({ data, error, code, loading, waiting, isPayment, items }) => {
  const mockStore = configureStore([]);
  const initalState = {
    api: {
      customer: {
        searchCustomer: {
          data,
        }
      },
      product: {
        gift: {
          gift_code: {
            error, code, loading, waiting
          },
        }
      }
    },
    auth: {
      info: {
        profile: {
          store_id: 6
        }
      }
    }
  };
  const store = mockStore(initalState);

  const { container } = render(
    <Provider store={store}>
      <GiftCodeBox
        isPayment={isPayment}
        items={items}
      />
    </Provider>
  )

  return { container };
}

describe('Test GiftCodeBox component', () => {
  test('Component first time render correctly', () => {
    const { container } = renderingComponent({
      data: undefined,
      error: "",
      code: "",
      loading: false,
      waiting: false,
      isPayment: false,
      items: [],
    });

    const inputElement = screen.queryByRole('textbox');
    const useCodeBtnElement = screen.queryByText('Use');

    expect(inputElement).toBeVisible();
    expect(inputElement.disabled).toEqual(false);
    expect(inputElement).not.toHaveFocus();

    expect(useCodeBtnElement).toBeInTheDocument();
    expect(useCodeBtnElement.disabled).toEqual(false);
  });

  test('When loading, disable component input', () => {
    const { container } = renderingComponent({
      data: undefined,
      error: "",
      code: "",
      loading: true,
      waiting: false,
      isPayment: false,
      items: [],
    });

    const inputElement = screen.queryByRole('textbox');
    expect(inputElement.disabled).toEqual(true);
  });

  test('Notify when input empty', () => {
    const { container } = renderingComponent({
      data: undefined,
      error: "",
      code: "",
      loading: false,
      waiting: false,
      isPayment: false,
      items: [],
      //items: [{ sku: 190012345, qty: 1, price: 10000, discount: 0 }],
    });

    const inputElement = screen.queryByRole('textbox');
    expect(inputElement.value).toEqual('');
    const useCodeBtnElement = screen.queryByText('Use');
    global.alert = jest.fn();

    userEvent.click(useCodeBtnElement);
    expect(global.alert).toHaveBeenCalledTimes(1);
  });

  test('Notify when customer info is empty', () => {
    const { container } = renderingComponent({
      data: null,
      error: "",
      code: "",
      loading: false,
      waiting: false,
      isPayment: false,
      items: [{ sku: 190012345, qty: 1, price: 10000, discount: 0 }],
    });

    const inputElement = screen.queryByRole('textbox');
    const useCodeBtnElement = screen.queryByText('Use');
    global.alert = jest.fn();

    userEvent.type(inputElement, 'CODE');
    userEvent.click(useCodeBtnElement);
    expect(global.alert).toHaveBeenCalledTimes(1);
  });
});