import React, { useEffect, useRef, useState } from "react";
import { Col, Modal, Row, Spin } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  searchProductClearAction,
  searchProductClearGiftsAction,
  searchProductRequestAction,
  searchProductRequestOfflineAction,
} from "share/api/Product/SearchProduct/actions";
import CustomerBox from "./components/CustomerBox";
import GiftCodeBox from "./components/GiftCodeBox";

import TopBtnBox from "./components/TopBtnBox";
import ReceiptFooter from "./components/ReceiptFooter";
import ReceiptItemTable from "./components/ReceiptItemTable";
import ReceiptSearchBar from "./components/ReceiptSearchBar";
import {
  searchCustomerCleanAction,
  searchCustomerOfflineRequestAction,
  searchCustomerRequestAction,
} from "share/api/Customer/SearchCustomer/actions";
import {
  getProductByRuleAddCodeAction,
  getProductByRuleAddGiftAction,
  getProductByRuleOfflineRequestAction,
  getProductByRuleRefreshGiftAction,
  getProductByRuleRemoveExpiredItem,
  getProductPricesByRuleClearAction,
  getProductPricesByRuleRequestAction,
} from "share/api/Product/GetProductsByRule/actions";
import { giftCodeRemoveAction } from "share/api/Product/Gift/action";
import TableCaculateInfo from "./components/TableCaculateInfo";
import CalculatorModal from "../components/CalculatorModal";
import CheckExpiredModal from "./components/CheckExpiredModal";
import ActiveCodeModal from "./components/ActiveCodeModal";
import HOTKEY from "utils/hotkey";
import PaymentModal from "./components/PaymentModal";
import PaymentBox from "./components/PaymentBox";
import LeftBotArea from "./components/LeftBotArea";
import {
  setPaymentStateModeAction,
  setTopBotAreaStateModeAction,
  updateReceiptTabAction,
} from "share/mode/action";
import { SEARCH_TYPE } from "share/api/Product/SearchProduct/search-type";
import _findLast from "lodash/findLast";
import _map from "lodash/map";

import "./style.scss";
import { createNewLocalReceiptRequestAction } from "share/api/Profile/Receipt/CreateNewLocal/actions";
import {
  caculatePaymentSummaryAction,
  paymentSummaryInitAction,
  paymentSummaryInsertCashAction,
  showConfirmModalAction,
} from "share/api/Payment/Summary/action";
import { addAmountCardPaymentAction } from "share/api/Payment/Type/Card/action";
import formatMoney from "utils/money";

import {
  socket,
  createNewReceiptToScreen,
  sendCustomerInfo,
  updateReceiptToScreen,
  startSocket,
} from "share/socket";
import { connectWebSocketSuccessAction } from "share/socket/action";
import {
  addAmountQrPayAction,
  qrPayFinishAction,
} from "share/api/Payment/Type/QrPay/action";
import { updateBalancePaymentAction } from "share/api/Payment/Type/Balance/action";
import {
  addRemoteReceiptIdAction,
  getRemoteReceiptIdAction,
  POS_RECEIPT,
  resetPosReceiptAction,
} from "share/api/POSReceipt/action";
import { resetPaymentAction } from "share/api/Payment/action";

const { confirm } = Modal;

function ReceiptDashboard() {
  const [isModalPaymentOpen, setIsModalPaymentOpen] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [activeCodeStatus, setActiveCodeStatus] = useState(null);
  const [caculatorNum, setCaculatorNum] = useState("100");
  const [caculatorId, setCaculatorId] = useState(null);
  const [distNum, setDistNum] = useState("0");
  const [distPercent, setDistPercent] = useState("0");
  const [totalRemainer, setTotalRemainer] = useState("0");
  const [giftBlockMessage, setGiftBlockMessage] = useState("");
  const [note, setNote] = useState("");

  const {
    search_data: searchData,
    search_gift: searchGift,
    gifts,
  } = useSelector((state) => state.api.product.searchProduct);
  const {
    data,
    loading,
    totalPrice,
    gift_items,
    expired_items,
    totalItems,
  } = useSelector((state) => state.api.product.productTable);
  const {
    code,
    waiting,
    error: gift_code_error,
    loading: gift_code_loading,
  } = useSelector((state) => state.api.product.gift.gift_code);
  const { store_id, id: user_id, name: user_name, ip: user_ip } = useSelector(
    (state) => state.auth.info.profile
  );
  const { local_receipt_id } = useSelector(
    (state) => state.api.profile.receipt
  );
  const { data: customer, isNewCustomer } = useSelector(
    (state) => state.api.customer.searchCustomer
  );
  const { isOnline, paymentState: isPayment, isStarted } = useSelector(
    (state) => state.mode
  );
  const {
    client_receipt_id,
    remote_receipt_code,
    isLoading: isGetingRemoteReceiptId,
  } = useSelector((state) => state.api.posReceipt);
  const { screen } = useSelector((state) => state.websocket);
  const listReceiptTab = useSelector((state) => state.mode.listReceiptTab);

  const {
    cash: cashAmount,
    charge,
    discount: paymentDiscount,
    totalPrices: paymentTotalPrices,
  } = useSelector((state) => state.api.payment.summary);
  const cardAmount = useSelector((state) => state.api.payment.type.card.amount);
  const qrPayAmount = useSelector(
    (state) => state.api.payment.type.qrPay.amount
  );
  const giftCardAmount = useSelector(
    (state) => state.api.payment.type.giftCard.current
  );
  const { current: currentBalance, balance } = useSelector(
    (state) => state.api.payment.type.balance
  );
  const {
    posReceipt,
    product,
    customer: { searchCustomer },
    payment,
    mode,
  } = useSelector((state) => state.api);

  const { id: client_id } = screen ? screen : { id: "" };

  const dispatch = useDispatch();
  const typingRef = useRef(null);

  // useEffect(() => {
  //   if (data.length === 0) {
  //     setDistNum("0");
  //     setDistPercent("0");
  //     setTotalRemainer("0");

  //     updateReceiptToScreen(client_id, []);
  //     return;
  //   }

  //   setTotalRemainer((totalPrice - distNum).toString());
  // }, [data, distNum, distPercent, client_receipt_id]);

  useEffect(() => {
    setTotalRemainer((totalPrice - distNum).toString());
  }, [distNum, totalPrice]);

  useEffect(() => {
    if (data.length === 0) {
      updateReceiptToScreen(client_id, []);
      setDistPercent("0");
    } else {
      setDistPercent(distPercent);
    }
  });

  useEffect(() => {
    if (isOnline) {
      const items = data.map((item) => {
        return {
          base_price: item.base_price,
          brand_id: item.product_brand_id,
          config: item.product_config,
          discount: item.discount,
          isBabyMomSpaCate: item.isBabyMomSpaCate,
          is_deal: item.is_deal,
          name: item.name,
          price: item.discount_price,
          qty: item.qty,
          rule_id: 0,
          sku: item.sku,
          type: 1,
          expire_date: expired_items[item.sku],
        };
      });

      updateReceiptToScreen(client_id, items);
    }
  }, [data]);

  useEffect(() => {
    if (paymentDiscount === 0 && isPayment) {
      setDistNum("0");
      setDistPercent("0");
      setTotalRemainer(paymentTotalPrices);
    }
  }, [paymentDiscount, paymentTotalPrices]);

  useEffect(() => {
    const totalNoneDiscountProductPrice = [...data].reduce((total, item) => {
      const price = item.discount === 0 ? item.base_price * item.qty : 0;
      return total + price;
    }, 0);
    const price =
      distPercent !== "0"
        ? (distPercent * totalNoneDiscountProductPrice) / 100
        : distNum;
    setDistNum(price);
  }, [distPercent, data]);

  useEffect(() => {
    const cloneData = [...data];
    window.api.updateReceiptById(
      client_receipt_id,
      "customer",
      JSON.stringify({ customer, isNewCustomer })
    );
    if (!customer?.customer_phone) {
      dispatch(giftCodeRemoveAction());
    } else {
      checkRule(cloneData);
    }
    if (customer && isOnline) {
      const {
        customer_id,
        customer_email,
        customer_name,
        customer_phone,
        customer_type,
        customer_init_order_point,
        customer_order_point,
        customer_init_receipt_point,
        customer_receipt_point,
      } = customer;

      const formatCustomer = {
        customer_type: customer_type,
        email: customer_email,
        id: customer_id,
        init_order_point: customer_init_order_point,
        init_receipt_point: customer_init_receipt_point,
        name: customer_name,
        order_point: customer_order_point,
        phone: customer_phone,
        receipt_point: customer_receipt_point,
      };
      sendCustomerInfo(client_id, formatCustomer);
    }
  }, [customer?.customer_phone]);

  useEffect(() => {
    if (data.length !== 0 && waiting === false) {
      checkRule(data);
    }
    //window.api.updateReceiptById(client_receipt_id, 'gift_code', JSON.stringify({ code, waiting, error: gift_code_error, loading: gift_code_loading }));
  }, [code, waiting]);

  useEffect(() => {
    const new_gift_items = gift_items.filter((value) => {
      return (
        gifts.findIndex(
          (item) => item.sku === value.sku && item.qty >= value.qty
        ) !== -1
      );
    });

    dispatch(getProductByRuleRefreshGiftAction(new_gift_items));
    setGiftBlockMessage("");
  }, [gifts]);

  useEffect(async () => {
    if (isOnline) {
      await startSocket();

      socket.on("connect", () => {
        dispatch(connectWebSocketSuccessAction(true));
      });

      socket.on(`screen-to-pos-${client_id}`, (msc) => {
        const dataObject = JSON.parse(msc);
        const { data, action } = dataObject || {};

        switch (action) {
          case "fill-customer":
            const { customer } = data;

            const {
              customer_type,
              id,
              init_order_point,
              init_receipt_point,
              name,
              order_point,
              phone,
              receipt_point,
            } = customer;

            // const customer_point =
            //   init_order_point + init_receipt_point + order_point + receipt_point;

            // const formatData = {
            //   customer_id: id,
            //   customer_phone: phone,
            //   customer_name: name,
            //   customer_point,
            // };
            dispatch(
              searchCustomerRequestAction(phone, false, client_receipt_id)
            );
            break;
          case "payment-qr-cancel": {
            dispatch(
              qrPayFinishAction("User cancel!", false, null, client_receipt_id)
            );
            break;
          }
          case "payment-qr-update": {
            if (data.data.amount) {
              const transaction_id = data.data.transaction_id;
              dispatch(
                qrPayFinishAction(
                  "Payment successfully!",
                  true,
                  transaction_id,
                  client_receipt_id
                )
              );
            } else {
              dispatch(
                qrPayFinishAction(
                  "System error! Try later!",
                  false,
                  null,
                  client_receipt_id
                )
              );
            }
            break;
          }
          default:
            console.log(action);
        }
      });
      socket.on("disconnect", () => {
        dispatch(connectWebSocketSuccessAction(false));
      });
      createNewReceiptToScreen(client_id);
    }
    // if (local_receipt_id === null) {
    //   const localData = {
    //     user_id: user_id,
    //     code: 0,
    //     total_price: 0,
    //     discount: 0,
    //     created_at: dayjs().unix(),
    //     updated_at: dayjs().unix(),
    //   };
    //   dispatch(createNewLocalReceiptRequestAction(localData));
    // }
  }, [screen?.id]);

  //Track price on payment type & re-caculate payment price
  useEffect(() => {
    dispatch(
      caculatePaymentSummaryAction(
        cashAmount,
        cardAmount,
        qrPayAmount,
        giftCardAmount,
        currentBalance,
        client_receipt_id
      )
    );
  }, [
    cashAmount,
    cardAmount,
    giftCardAmount,
    qrPayAmount,
    currentBalance,
    totalPrice,
  ]);

  //When charge amount > 0, show notification
  useEffect(() => {
    if (charge > 0) {
      dispatch(showConfirmModalAction(true, client_receipt_id));
      Modal.info({
        title: "Notification",
        content: (
          <h1>
            Charge: <b className="text-green">{`${formatMoney(charge)}`}</b>
          </h1>
        ),
        onOk: () => {
          dispatch(showConfirmModalAction(false, client_receipt_id));
        },
      });
    }
  }, [charge]);

  useEffect(() => {
    if (isStarted) {
      dispatch(
        createNewLocalReceiptRequestAction({
          user: { user_id, user_name },
          posReceipt,
          product,
          customer: searchCustomer,
          payment,
          mode,
          total_price: 0,
          note: "",
          distNum,
          distPercent,
        })
      );
    }
  }, []);

  useEffect(() => {
    if (remote_receipt_code) {
      setIsModalPaymentOpen(true);
    }
  }, [remote_receipt_code]);

  useEffect(async () => {
    const data = await window.api.getReceptChuckById(client_receipt_id, [
      "note",
      "distNum",
      "distPercent",
    ]);
    setNote(data?.note ?? "");
    setDistNum(data?.distNum ?? "0");
    setTotalRemainer((totalPrice - data?.distNum).toString());
  }, [client_receipt_id]);

  // useEffect(() => {
  //   window.api.updateReceiptById(client_receipt_id, 'gift_items', JSON.stringify(gift_items));
  // }, [gift_items])

  const checkRule = (productData, lastest_sku) => {
    if (productData.length === 0) {
      return;
    }
    const gift_code = !waiting ? code : "";

    const raw = {
      store_id,
      items: productData.map((item) => ({ sku: item.sku, qty: item.qty })),
      customer_phone: customer != null ? customer.customer_phone : "",
      gift_code,
    };

    window.api.updateReceiptById(
      client_receipt_id,
      "gift_code",
      JSON.stringify({
        code: gift_code,
        waiting,
        error: gift_code_error,
        loading: gift_code_loading,
      })
    );

    // const localData = {
    //   user_id,
    //   receipt_id: local_receipt_id,
    //   customer: customer
    //     ? JSON.stringify({
    //       customer_phone: customer.customer_phone,
    //       customer_name: customer.customer_name,
    //       customer_point: customer.customer_point,
    //     })
    //     : null,
    //   updated_at: dayjs().unix(),
    // };

    dispatch(searchProductClearAction(SEARCH_TYPE.SEARCH_DATA));
    if (isOnline) {
      dispatch(
        getProductPricesByRuleRequestAction(raw, lastest_sku, client_receipt_id)
      );
    } else {
      dispatch(
        getProductByRuleOfflineRequestAction(
          raw,
          lastest_sku,
          client_receipt_id
        )
      );
    }
  };

  const handlePaymentClick = () => {
    if (data.length !== 0) {
      dispatch(setPaymentStateModeAction(true));
      dispatch(
        paymentSummaryInitAction(
          totalItems,
          totalPrice,
          totalPrice - distNum,
          distNum,
          client_receipt_id
        )
      );
      window.api.updateReceiptById(client_receipt_id, "status", 1);

      const paymentCustomer =
        customer === null
          ? {
              name: 0,
              phone: 0,
              id: 0,
            }
          : {
              name: customer?.customer_name ?? 0,
              phone: customer?.customer_phone ?? 0,
              id: customer?.customer_id ?? 0,
            };
      const paymentProducts = data.map((item) => {
        return {
          product_sku: item.sku,
          qty: item.qty,
          price: item.base_price,
          discount: item.discount,
          discount_rule: item.rule_id,
        };
      });
      const raw = {
        customer: paymentCustomer,
        products: paymentProducts,
        client_receipt_id,
      };
      if (isOnline) {
        dispatch(getRemoteReceiptIdAction(raw, client_receipt_id));
      } else {
        window.api.updateReceiptById(
          client_receipt_id,
          "remote_receipt_code",
          `Offline: ${client_receipt_id}`
        );
        dispatch(
          addRemoteReceiptIdAction(
            null,
            `Offline: ${client_receipt_id}`,
            null,
            null
          )
        );
      }
    }
  };

  const handleTypingSearch = (value, type) => {
    if (value.length < 4) {
      dispatch(searchProductClearAction(type));
      return;
    }
    if (typingRef.current) {
      clearTimeout(typingRef.current);
    }

    if (isOnline && value !== "" && value.length >= 4) {
      typingRef.current = setTimeout(() => {
        dispatch(searchProductRequestAction(value, type));
      }, 400);
    } else {
      dispatch(searchProductRequestOfflineAction(value, type));
    }
  };

  const handleCheckCustomer = (phone) => {
    if (isOnline) {
      dispatch(searchCustomerRequestAction(phone, false, client_receipt_id));
    } else {
      const localData = {
        receipt_id: local_receipt_id,
      };
      dispatch(searchCustomerOfflineRequestAction(localData, phone));
    }
  };

  const handleGetProducts = (product_sku) => {
    if (isNaN(product_sku)) {
      alert(
        "Product barcode is not existed in system! Please contact to tech for support!"
      );
      return;
    }
    const productData = JSON.parse(JSON.stringify(data));

    const index = productData.findIndex((item) => {
      return +item.sku === +product_sku;
    });

    index === -1
      ? productData.push({ sku: +product_sku, qty: 1 })
      : productData[index].qty++;

    checkRule(productData, product_sku);
  };

  const handleIncreaseQty = (product_sku) => {
    const productData = JSON.parse(JSON.stringify(data));

    const index = productData.findIndex((item) => {
      return item.sku === product_sku;
    });

    productData[index].qty++;

    checkRule(productData, product_sku);
  };

  const handleRemoveProduct = (product_sku) => {
    dispatch(getProductByRuleRemoveExpiredItem(product_sku));

    const productData = JSON.parse(JSON.stringify(data));

    const index = productData.findIndex((item) => {
      return item.sku === product_sku;
    });

    productData.splice(index, 1);

    if (productData.length === 0) {
      dispatch(getProductPricesByRuleClearAction());
      return;
    }

    checkRule(productData, product_sku);
  };

  const handleSearchGift = (product_sku) => {
    setGiftBlockMessage("");
    const giftItemsIndex = gift_items.findIndex((item) => {
      return item.sku == product_sku;
    });
    const giftIndex = gifts.findIndex((item) => {
      return item.sku == product_sku;
    });

    if (
      giftIndex === -1 ||
      (gift_items[giftItemsIndex]?.qty ?? 0) === gifts[giftIndex].qty
    ) {
      alert("Gift is not valid!");
    } else {
      dispatch(
        getProductByRuleAddGiftAction(
          gifts[giftIndex],
          giftItemsIndex,
          client_receipt_id
        )
      );
      setGiftBlockMessage(
        `${gifts[giftIndex].name} (SKU: ${product_sku}) was added successfully!`
      );
    }
    dispatch(searchProductClearAction(SEARCH_TYPE.SEARCH_GIFT));
  };

  const handleShowCaculator = (num, id) => {
    setIsModalVisible(true);
    setCaculatorId(id);
    setCaculatorNum(num);
  };

  const handleDoneCaculator = (num, id) => {
    switch (id) {
      case "distNum": {
        const percent = (100 * num) / totalPrice;
        if (percent > 80) {
          alert("Maximum discount is 80 percent!");
          break;
        }
        if (distPercent === "0") {
          setDistNum(num.toString());
          window.api.updateReceiptById(
            client_receipt_id,
            "distNum",
            num.toString()
          );
        }
        break;
      }
      case "distPercent": {
        if (num > 80) {
          alert("Maximum discount is 80 percent!");
        } else {
          const price = (num * totalPrice) / 100;
          setDistPercent(num.toString());
          window.api.updateReceiptById(
            client_receipt_id,
            "distNum",
            price.toString()
          );
        }
        break;
      }
      case "cashPayment": {
        dispatch(
          paymentSummaryInsertCashAction(+num, totalPrice, client_receipt_id)
        );
        break;
      }
      case "cardPayment": {
        dispatch(addAmountCardPaymentAction(+num, client_receipt_id));
        break;
      }
      case "qrPayment": {
        dispatch(addAmountQrPayAction(+num, client_receipt_id));
        break;
      }
      case "balancePayment": {
        if (+num > currentBalance) {
          dispatch(showConfirmModalAction(true, client_receipt_id));
          Modal.info({
            title: "Notification",
            content: (
              <p>{`Balance amount can not greater than ${formatMoney(
                +balance
              )}`}</p>
            ),
            onOk: () => {
              dispatch(showConfirmModalAction(false, client_receipt_id));
            },
          });
        } else {
          dispatch(updateBalancePaymentAction(+num, client_receipt_id));
        }
        break;
      }
      default: {
        const cloneData = JSON.parse(JSON.stringify(data));
        const index = data.findIndex((item) => item.sku === id);

        cloneData[index].qty = +num;

        checkRule(cloneData, id);
        break;
      }
    }
  };

  const handleActiveCode = (sku, product_name) => {
    setActiveCodeStatus({ sku, product_name });
  };

  const handleScan = (code) => {
    dispatch(
      getProductByRuleAddCodeAction(
        activeCodeStatus.sku,
        +code,
        client_receipt_id
      )
    );
    setActiveCodeStatus(null);
  };

  const handleAddCombo = (combo_sku, combo_details) => {
    const cloneData = JSON.parse(JSON.stringify(data));
    combo_details.forEach((combo_detail) => {
      const data_index = cloneData.findIndex(
        (item) => item.sku === combo_detail.sku
      );
      cloneData[data_index].qty -= combo_detail.qty;
      if (cloneData[data_index].qty === 0) {
        cloneData.splice(data_index, 1);
      }
    });

    const comboIndex = cloneData.findIndex((item) => item.sku === combo_sku);
    if (comboIndex === -1) {
      cloneData.push({ sku: combo_sku, qty: 1 });
    } else {
      cloneData[comboIndex].qty++;
    }
    checkRule(cloneData, combo_sku);
  };

  const handleResetTable = () => {
    confirm({
      title: "Are you sure you want to empty?",
      okText: "Yes",
      cancelText: "No",
      onOk() {
        cleanCustomer();
        dispatch(giftCodeRemoveAction());
        dispatch(getProductPricesByRuleClearAction());
        dispatch(searchProductClearGiftsAction());
        dispatch(setTopBotAreaStateModeAction(null));
        dispatch(setPaymentStateModeAction(false));
        dispatch(searchProductClearAction(SEARCH_TYPE.SEARCH_DATA));
        dispatch(resetPosReceiptAction(POS_RECEIPT.RESET));
        resetPaymentAction(dispatch, client_receipt_id);
        const cloneReceiptTabs = [...listReceiptTab];
        const itemIndex = cloneReceiptTabs.findIndex(
          (item) => item.client_receipt_id === client_receipt_id
        );
        cloneReceiptTabs.splice(itemIndex, 1);
        dispatch(updateReceiptTabAction(cloneReceiptTabs));

        dispatch(
          createNewLocalReceiptRequestAction({
            user: {user_id, user_name},
            posReceipt,
            product,
            customer: searchCustomer,
            payment,
            mode,
            total_price: 0,
            note: "",
            distNum,
            distPercent,
          })
        );
      },
      onCancel() {},
    });
  };

  const removeCustomer = () => {
    const localData = {
      receipt_id: local_receipt_id,
    };
    dispatch(searchCustomerCleanAction(localData, true));
    if (isOnline) {
      createNewReceiptToScreen(client_id);
    }
  };

  const cleanCustomer = () => {
    const localData = {
      receipt_id: local_receipt_id,
    };
    dispatch(searchCustomerCleanAction(localData, false));
    if (isOnline) {
      createNewReceiptToScreen(client_id);
    }
  };

  const handleClickBtnFillInfo = () => {
    if (isOnline) {
      socket.emit(
        "pos-to-screen",
        JSON.stringify({
          action: "customer-fill",
          data: {
            client_id,
          },
        })
      );
    }
  };

  return (
    <Spin spinning={isGetingRemoteReceiptId}>
      <div style={{ marginTop: "-1em" }} className="content">
        <Row gutter={[32, 8]}>
          <Col md={16}>
            <div className="box">
              <div className="box-body">
                <div className="row">
                  <div className="col-xs-9">
                    <ReceiptSearchBar
                      handleSearchProduct={handleTypingSearch}
                      searchData={searchData}
                      handleGetProducts={handleGetProducts}
                      type={SEARCH_TYPE.SEARCH_DATA}
                      isLoading={loading}
                      placeholder={`Enter SKU, Barcode, Name (${HOTKEY.POS_SEARCH_PRODUCT_BAR_FOCUS.key})`}
                      hotkeyFocus={HOTKEY.POS_SEARCH_PRODUCT_BAR_FOCUS.key}
                      isPaymentState={isPayment}
                    />
                  </div>
                  <div className="col-xs-3 input-lg no-padding">
                    <TableCaculateInfo
                      productData={data}
                      gift_items={gift_items}
                    />
                  </div>
                </div>
                <hr />
                {/* <!-- list items --> */}
                <ReceiptItemTable
                  data={data}
                  isLoading={loading}
                  handleIncreaseQty={handleIncreaseQty}
                  handleRemoveProduct={handleRemoveProduct}
                  showModal={handleShowCaculator}
                  handleActiveCode={handleActiveCode}
                  gift_items={gift_items}
                />
                {/* <!-- end list items --> */}
                <br />
                <ReceiptFooter
                  showModal={handleShowCaculator}
                  distNum={distNum}
                  distPercent={distPercent}
                  totalRemainder={totalRemainer}
                  isPaymentState={isPayment}
                />
              </div>
            </div>
            {/* {
              (gifts.length !== 0 && data.length !== 0) && (
                <GiftBlock
                  handleSearchProduct={handleTypingSearch}
                  searchData={searchGift}
                  handleGetProducts={handleSearchGift}
                  message={giftBlockMessage}
                />
              )
            } */}
          </Col>
          <Col md={8}></Col>
        </Row>
      </div>

      <div className="fix-right-box">
        <Row justify="end" gutter={[32, 8]}>
          <Col className="fix-right-box-inside" md={24}>
            <TopBtnBox
              isPayment={isPayment}
              handleResetTable={handleResetTable}
              handleClickBtnFillInfo={handleClickBtnFillInfo}
            />
            {isPayment && (
              <div className="box box-default">
                <div className="box-body">
                  <span style={{ fontSize: "1.5em", fontWeight: "bolder" }}>
                    #{remote_receipt_code}
                  </span>
                </div>
              </div>
            )}
            <div className="box">
              <div className="box-body">
                <CustomerBox
                  isPayment={isPayment}
                  handleCheck={handleCheckCustomer}
                  removeCustomer={removeCustomer}
                />
              </div>
            </div>
            <div className="box">
              <div className="box-body">
                <GiftCodeBox isPayment={isPayment} items={data} />
              </div>
            </div>
            {/* {
              isPayment && (
                <>
                  <PaymentMethodBox />
                  <SummaryBox />
                </>
              )
            } */}
            <PaymentBox
              isPayment={isPayment}
              handlePaymentClick={handlePaymentClick}
              productTable={data}
              handleReopenModal={() => {
                setIsModalPaymentOpen(true);
              }}
              note={note}
              handleChangeNote={(e) => {
                setNote(e.target.value);
                window.api.updateReceiptById(
                  client_receipt_id,
                  "note",
                  e.target.value
                );
              }}
            />
            {/* <InfoBox /> */}
          </Col>
        </Row>
      </div>
      <LeftBotArea
        handleAddCombo={handleAddCombo}
        giftBlockMessage={giftBlockMessage}
        handleSearchGift={handleSearchGift}
        searchGift={searchGift}
        handleTypingSearch={handleTypingSearch}
      />

      {/* Begin Modal Box */}

      <CalculatorModal
        isVisible={isModalVisible}
        setNoneDisplay={() => {
          setIsModalVisible(false);
        }}
        handleDoneCaculator={handleDoneCaculator}
        caculatorNum={caculatorNum}
        caculatorId={caculatorId}
      />

      <CheckExpiredModal />

      <ActiveCodeModal
        isVisible={activeCodeStatus !== null}
        title={
          activeCodeStatus !== null
            ? `Active: ${activeCodeStatus.product_name} #${activeCodeStatus.sku}`
            : ""
        }
        handleCancel={() => {
          setActiveCodeStatus(null);
        }}
        handleScan={handleScan}
      />

      <PaymentModal
        handleShowCaculator={handleShowCaculator}
        isModalPaymentOpen={isModalPaymentOpen}
        handleCancel={() => {
          setIsModalPaymentOpen(false);
        }}
        receipt_discount_percent={+distPercent}
        receipt_discount={+distNum}
        note={note}
      />

      {/* End Modal Box */}
    </Spin>
  );
}

export default ReceiptDashboard;
