import React from "react";
import PropTypes from "prop-types";
import { Col, Row } from "antd";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function StaffBalanceBox(props) {
  return (
    <div className="box box-warning">
      <div className="box-header with-border">
        <h3 className="box-title">Staff Balance</h3>
      </div>
      <div className="box-body">
        <p>
          <strong>Account Code:</strong>
          5FB38AE74D7A0
          <a
            href="http://test.inshasaki.com/accounting/account-balance?account_code=5FB38AE74D7A0"
            className="btn btn-default btn-xs"
          >
            <Space>
              <FontAwesomeIcon icon={faSearch} />
              View Detail
            </Space>
          </a>
        </p>

        <Row>
          <Col md={16} className="text-right">
            <h4>Debit:</h4>
          </Col>
          <Col md={16} className="text-red">
            <h4>-9,500,000</h4>
          </Col>
        </Row>
      </div>
    </div>
  );
}

StaffBalanceBox.propTypes = {};

export default StaffBalanceBox;
