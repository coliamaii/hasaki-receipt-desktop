import { faFileExcel, faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, DatePicker, Form, Input, Row, Select, Space } from "antd";
import _map from "lodash/map";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";

const { Option } = Select;

function SearchForm({ initialValues }) {
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  // const { list } = useSelector((state) => state.api.purchase.vendor);
  // const payment_type_options = Object.keys(payment_methods).map((key) => {
  //   return (
  //     <Option key={key} value={key}>
  //       {payment_methods[key]}
  //     </Option>
  //   );
  // });

  // const status_options = Object.keys(all_status).map((key) => {
  //   return (
  //     <Option key={key} value={key}>
  //       {all_status[key]}
  //     </Option>
  //   );
  // });

  const handleSearchAllVendor = (q) => {
    if (!q) {
      q = null;
    }
    // dispatch(getListVendorRequestAction({ q }));
  };

  useEffect(() => {
    form.submit();
  }, []);

  return (
    <Form form={form} name="paymentSearchForm" initialValues={initialValues}>
      <Row gutter={[32, 16]}>
        <Col md={4}>
          <Form.Item name="from_cdate" style={{ margin: 0 }}>
            <DatePicker
              style={{ width: "100%" }}
              picker="date"
              placeholder="From Date"
            />
          </Form.Item>
        </Col>

        <Col md={4}>
          <Form.Item name="to_cdate" style={{ margin: 0 }}>
            <DatePicker
              style={{ width: "100%" }}
              picker="date"
              placeholder="To Date"
            />
          </Form.Item>
        </Col>

        <Col md={8}>
          <Form.Item name="vendor_id" style={{ margin: 0 }}>
            <Select
              style={{ width: "100%" }}
              showSearch
              placeholder="-All Vendor-"
              onSearch={handleSearchAllVendor}
              suffixIcon={false}
              filterOption={(inputValue, option) => {
                return true;
              }}
            >
              {/* {_map(list, (vendor) => (
                <Option key={vendor.vendor_id} value={vendor.vendor_id}>
                  {vendor.vendor_company} - {vendor.vendor_name} #
                  {vendor.vendor_id}
                </Option>
              ))} */}
            </Select>
          </Form.Item>
        </Col>
        <Col md={4}>
          <Form.Item name="q" style={{ margin: 0 }}>
            <Input type="text" placeholder="Code, PO Code, Transaction" />
          </Form.Item>
        </Col>
        <Col md={4}>
          <div className="input-group input-group">
            <span className="input-group-addon">Verified?</span>
            <Form.Item name="is_verified" style={{ margin: 0 }}>
              <Select>
                <Option value="">-All-</Option>
                <Option value="1">Yes</Option>
                <Option value="2">No</Option>
              </Select>
            </Form.Item>
          </div>
        </Col>
      </Row>
      <Row gutter={[32]}>
        <Col md={4}>
          <Form.Item name="payment_code" style={{ margin: 0 }}>
            <Input type="text" placeholder="Code" />
          </Form.Item>
        </Col>
        <Col md={4}>
          <Form.Item name="payment_transaction" style={{ margin: 0 }}>
            <Input type="text" placeholder="Payment transaction" />
          </Form.Item>
        </Col>

        <Col md={4}>
          <Form.Item name="payment_type" style={{ margin: 0 }}>
            <Select>
              <Option value="">-All payment-</Option>
              {/* {payment_type_options} */}
            </Select>
          </Form.Item>
        </Col>
        <Col md={4}>
          <Form.Item name="status" style={{ margin: 0 }}>
            <Select>
              <Option value="">-All Status-</Option>
              {/* {status_options} */}
            </Select>
          </Form.Item>
        </Col>

        <Col md={4}>
          <Row gutter={[16]}>
            <Col md={12}>
              <Button type="primary" htmlType="submit">
                <Space>
                  <FontAwesomeIcon icon={faSearch} />
                  Search
                </Space>
              </Button>
            </Col>
            <Col md={12}>
              <a
                target="_blank"
                className="btn btn-info"
                href="http://test.inshasaki.com/accounting/ledger?total=0&amp;limit=50&amp;offset=0&amp;payment_code=26012101180005&amp;from_cdate=2021-01-29&amp;to_cdate=2021-01-29&amp;code=26012101180005&amp;btn_export=1"
              >
                <Space>
                  <FontAwesomeIcon icon={faFileExcel} />
                  Export FAST
                </Space>
              </a>
            </Col>
          </Row>
        </Col>
      </Row>
    </Form>
  );
}

SearchForm.propTypes = {};

export default SearchForm;
