import { faBars, faPrint, faUpload } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Row, Space, Table } from "antd";
import React from "react";
import { useSelector } from "react-redux";
import formatMoney from "utils/money";
import PropTypes from "prop-types";
import { css } from "@emotion/css";

EditPaymentTable.propTypes = {
  dataSource: PropTypes.array,
  columns: PropTypes.array,
  rowSelection: PropTypes.object,
};

EditPaymentTable.defaultProps = {
  dataSource: [],
  rowSelection: {},
  columns: [],
};

function EditPaymentTable({ dataSource, columns, rowSelection }) {
  const { isLoading } = useSelector((state) => state.api.accounting.payment);

  const tableCSS = css({
    "& table": {
      borderCollapse: "collapse",
    },
    "& thead > tr > th": {
      fontWeight: "bold",
      textAlign: "center",
    },

    "& thead > tr > th, & tbody > tr > td, & tfoot > tr > td": {
      padding: 8,
    },
  });

  return (
    <Table
      columns={columns}
      dataSource={dataSource}
      rowSelection={rowSelection}
      className={tableCSS}
      loading={isLoading}
      pagination={false}
      // scroll={{
      //   y: '30vw',
      //   x: "100vw",
      // }}
      summary={(pageData) => {
        console.log(pageData);
        let amount_sum = 0;

        pageData.map(({ payment_amount }) => {
          amount_sum += payment_amount;
        });

        return (
          <Table.Summary.Row>
            <Table.Summary.Cell />
            <Table.Summary.Cell />
            <Table.Summary.Cell />
            <Table.Summary.Cell />
            <Table.Summary.Cell />
            <Table.Summary.Cell />
            <Table.Summary.Cell />
            <Table.Summary.Cell />

            <Table.Summary.Cell className="text-center">
              <strong>Sum:</strong>
            </Table.Summary.Cell>
            <Table.Summary.Cell className="text-center">
              <strong>{formatMoney(amount_sum)}</strong>
            </Table.Summary.Cell>
          </Table.Summary.Row>
        );
      }}
    />
  );
}

export default EditPaymentTable;
