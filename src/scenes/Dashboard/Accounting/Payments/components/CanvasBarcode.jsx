import { useBarcode } from "@createnextapp/react-barcode";
import React from "react";
import { useLocation } from "react-router-dom";

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

function CanvasBarcode(props) {
  const query = useQuery();
  const code = query.get("code");
  const { inputRef } = useBarcode({
    value: code,
    options: {
      displayValue: false,
    },
  });

  return (
    <div>
      <h3 align="center">CODE: {code}</h3>
      <p>&nbsp;</p>
      <p className="text-center">
        <canvas ref={inputRef} />
      </p>
    </div>
  );
}

export default CanvasBarcode;
