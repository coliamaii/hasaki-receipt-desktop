import { useBarcode } from "@createnextapp/react-barcode";
import {
  faCalculator,
  faExternalLinkAlt,
  faReply,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Form, Input, Row, Select, Space } from "antd";
import TextArea from "antd/lib/input/TextArea";
import dayjs from "dayjs";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { putUpdatePaymentRequestAction } from "share/api/Accounting/Payment/Update/actions";
import { getListPaymentReceiptRequestAction } from "share/api/Profile/Payment-Receipt/GetList/actions";
import { getLinksUploadRequestAction } from "share/api/Settings/Upload/GetLinks/actions";
import { postFileUploadRequestAction } from "share/api/Settings/Upload/PostFile/actions";
import formatMoney from "utils/money";
import { MEDIA_URL } from "web.config";
import CompletedForm from "./components/CompletedForm";
import VerifiedForm from "./components/VerifiedForm";

const { Option } = Select;
function PaymentEdit(props) {
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const { id } = useSelector((state) => state.auth.info.profile);
  const {
    rows,
    isLoading,
    payment,
    account_balance,
    banks,
    accountBalance,
  } = useSelector((state) => state.api.accounting.payment);
  const detailVendor = useSelector((state) => state.api.purchase.vendor.rows);

  const banpayment_bankaccountks_options = banks.map(({ id, name }) => {
    return <Option value={id}>{name}</Option>;
  });

  const account_balance_id_options = accountBalance.map(({ id, account }) => {
    if (account) {
      return (
        <Option key={id} value={id}>
          {account.account_code} - {account.name}
        </Option>
      );
    }
  });

  const { payment_methods } = useSelector(
    (state) => state.api.profile.paymentReceipt
  );

  const payment_type_options = Object.keys(payment_methods).map((key) => {
    return (
      <Option key={key} value={key}>
        {payment_methods[key]}
      </Option>
    );
  });

  const {
    payment_id,
    payment_code,
    payment_user_id,
    payment_store_id,
    payment_type,
    payment_bankaccount,
    payment_vendor_id,
    payment_po_code,
    payment_transaction,
    payment_amount,
    payment_status,
    payment_note,
    payment_ctime,
    payment_utime,
    reference_code,
    customer_id,
    account_balance_id,
    verified_by,
    verified_at,
    completed_by,
    completed_at,
    staff_id,
    client_name,
  } = payment;

  // Initial Form Values
  const initialEditFormValues = {
    payment_amount: "",
    payment_transaction: "",
    account_balance_id: "0",
    payment_type: "1",
    payment_bankaccount: "0",
    payment_note: "",
    reference_code: "",
    payment_po_code: "",
  };

  const initialUploadValues = {
    object_id: "7926",
    type: "16384",
    link_type: "payment",
    // file: "",
    note: "",
  };

  // Set value Barcode
  const { inputRef } = useBarcode({
    value: payment_code,
    options: {
      displayValue: false,
    },
  });

  // Handle Upload Button Click
  const [visible, setVisible] = useState(false);

  const handleUploadClick = () => {
    setVisible(true);
  };

  const handleModalSaveClick = (formUpload) => {
    formUpload.submit();
  };

  const handleModalCloseClick = () => {
    setVisible(false);
  };

  // Handle Images Button Click
  const [visibleImagesModal, setVisibleImagesModal] = useState(false);

  const handleImagesClick = () => {
    let object_id = payment_id;
    let user_id = id;

    dispatch(getLinksUploadRequestAction({ object_id, user_id }));
    setVisibleImagesModal(true);
  };

  // Handle Images Modal
  const { linksUpload } = useSelector((state) => state.api.accouting.upload);
  const [fileList, setFileList] = useState([]);

  const hanldeCancelBtnImgModal = () => {
    setVisibleImagesModal(false);
  };

  const hanldeOkBtnImgModal = () => {
    setVisibleImagesModal(false);
  };

  useEffect(() => {
    let listData = [];
    linksUpload.rows.map(({ name }, i) => {
      listData.push({
        uid: i,
        name: name,
        status: "done",
        url: `${MEDIA_URL}/${name}`,
      });
    });
    setFileList(listData);
  }, [linksUpload.rows]);

  // handle Form Provider
  const [file, setFile] = useState();
  const handleSubmitFormProvider = (name, { values, forms }) => {
    console.log("form provider");
    console.log(name);
    console.log(values);
    // console.log(forms);
    if (name === "formUploadModal") {
      let data = new FormData();
      Object.keys(values).forEach((key) => {
        data.append(key, values[key]);
      });
      // data.append("file", file);

      console.log("formData");
      console.log(data);
      // const config = {
      //   headers: {
      //     "content-type": "multipart/form-data",
      //   },
      // };
      // axios
      //   .post("/setting/upload", data, config)
      //   .then((request) => {
      //     console.log(request);
      //   })
      //   .catch((err) => {
      //     console.log(err);
      //   });
      // dispatch(postFileUploadRequestAction({ ...values, file: file }));
      dispatch(postFileUploadRequestAction(data));
    }
    if (name === "completedForm") {
      dispatch(putUpdatePaymentRequestAction(payment_id, values));
    }
    if (name === "verifiedForm") {
      dispatch(putUpdatePaymentRequestAction(payment_id, values));
    }
    if (name === "editForm") {
      dispatch(putUpdatePaymentRequestAction(payment_id, values));
    }
  };

  // get Name User completed, verified, create
  const [listUser, setListUser] = useState([]);
  const { user } = useSelector((state) => state.api.accouting.user.detail);

  useEffect(() => {
    [payment_user_id, verified_by, completed_by].map((id) => {
      console.log(user);
    });
  }, [payment_user_id, verified_by, completed_by]);

  useEffect(() => {
    // console.log("user");
    // console.log(user);
    setListUser([...listUser, user]);
  }, [user]);

  useEffect(() => {
    form.setFieldsValue({
      payment_amount: formatMoney(payment.payment_amount),
      payment_transaction: payment.payment_transaction,
      account_balance_id: payment.account_balance_id,
      payment_type: payment.payment_type + "",
      payment_bankaccount:
        payment.payment_bankaccount === null ||
        payment.payment_bankaccount === 0
          ? "0"
          : payment.payment_bankaccount,
      payment_note: payment.payment_note,
      reference_code: payment.reference_code,
      payment_po_code: payment.payment_po_code,
    });
  }, [payment]);

  useEffect(() => {
    dispatch(getListPaymentReceiptRequestAction());
  }, []);

  return (
    <div>
      <section className="content-header">
        <h1>
          Payment
          <small>#{payment_code}</small>
        </h1>
        <ol className="breadcrumb">
          <li>
            <Space>
              <FontAwesomeIcon icon={faCalculator} />
              Accounting
            </Space>
          </li>
          <li>
            <a href="#">Payment</a>
          </li>
        </ol>
      </section>

      <section className="content">
        <Form.Provider onFormFinish={handleSubmitFormProvider}>
          <Row gutter={[32]}>
            <Col xs={24} lg={16}>
              <div className="box box-primary">
                <div className="box-header with-border">
                  <h3 className="box-title" style={{ width: "100%" }}>
                    Edit (Code: {payment_code} )
                    <canvas
                      ref={inputRef}
                      style={{
                        height: 50,
                        width: "50%",
                        verticalAlign: "middle",
                      }}
                    />
                  </h3>
                </div>

                <Form
                  form={form}
                  name="editForm"
                  initialValues={initialEditFormValues}
                >
                  <div className="box-body">
                    <Row gutter={[32]}>
                      <Col md={8}>
                        <div className="form-group">
                          <label className="text-blue">Amount</label>
                          <Form.Item
                            name="payment_amount"
                            style={{ margin: 0 }}
                          >
                            <Input
                              className="form-control text-blue text-right"
                              type="text"
                              placeholder="000,000"
                            />
                          </Form.Item>
                        </div>
                      </Col>

                      {payment_transaction && (
                        <Col md={8}>
                          <div className="form-group">
                            <label>Transaction Code:</label>
                            <Form.Item
                              name="payment_transaction"
                              style={{ margin: 0 }}
                            >
                              <Input className="form-control" type="text" />
                            </Form.Item>
                          </div>
                        </Col>
                      )}
                    </Row>

                    <Row gutter={[32]}>
                      <Col md={8}>
                        <div className="form-group">
                          <label>
                            Account (AccBalanceID: {account_balance_id})
                          </label>
                          <Form.Item
                            name="account_balance_id"
                            style={{ margin: 0 }}
                          >
                            <Select>
                              <Option value="0">-Select Account-</Option>
                              {account_balance_id_options}
                            </Select>
                          </Form.Item>
                        </div>
                      </Col>

                      <Col md={8}>
                        <div className="form-group">
                          <label>Cash</label>
                          <Form.Item name="payment_type" style={{ margin: 0 }}>
                            <Select>{payment_type_options}</Select>
                          </Form.Item>
                        </div>
                      </Col>

                      <Col md={8}>
                        <div className="form-group">
                          <label>Bank</label>
                          <Form.Item
                            name="payment_bankaccount"
                            style={{ margin: 0 }}
                          >
                            <Select>
                              <Option value="0">-Select-</Option>
                              {banpayment_bankaccountks_options}
                            </Select>
                          </Form.Item>
                        </div>
                      </Col>
                    </Row>

                    {client_name && <h4>Client: {client_name}</h4>}

                    {payment_vendor_id !== null && payment_vendor_id !== 0 && (
                      <h4>
                        Vendor:{" "}
                        <strong>
                          {detailVendor.length > 0 && detailVendor[0].fast_code}
                        </strong>{" "}
                        -{" "}
                        {detailVendor.length > 0 &&
                          detailVendor[0].vendor_company}{" "}
                        <small>(VenID: {payment_vendor_id})</small>
                      </h4>
                    )}

                    {staff_id !== null && staff_id !== 0 && (
                      <h4>
                        Staff: <strong>18213</strong>- Phạm Thị Như Phúc
                        <small>(StaffID: {staff_id})</small>
                      </h4>
                    )}

                    {payment_po_code !== 0 && payment_po_code !== null && (
                      <Row>
                        <Col xs={8}>
                          <div class="form-group">
                            <label for="">Invoice </label>

                            <div class="input-group">
                              <Form.Item
                                name="payment_po_code"
                                style={{ margin: 0 }}
                              >
                                <Input
                                  readonly
                                  class="form-control"
                                  type="text"
                                  placeholder="PO Code"
                                />
                              </Form.Item>

                              <span class="input-group-addon">
                                <a href="#">
                                  <FontAwesomeIcon icon={faExternalLinkAlt} />
                                </a>
                              </span>
                            </div>
                          </div>
                        </Col>
                      </Row>
                    )}

                    <Row gutter={[32]}>
                      <Col md={16}>
                        <div className="form-group">
                          <label>Note:</label>
                          <Form.Item name="payment_note" style={{ margin: 0 }}>
                            <TextArea />
                          </Form.Item>
                        </div>
                      </Col>

                      <Col md={8}>
                        <div className="form-group">
                          <label>Reference Code:</label>
                          <Form.Item
                            name="reference_code"
                            style={{ margin: 0 }}
                          >
                            <Input type="text" placeholder="Reference Code" />
                          </Form.Item>
                        </div>
                      </Col>
                    </Row>

                    <Row>
                      <Col md={24}>
                        Created at:
                        {dayjs(payment_ctime * 1000).format(
                          "YYYY-MM-DD HH:ss"
                        )}{" "}
                        - Updated at:
                        {dayjs(payment_utime * 1000).format(
                          "YYYY-MM-DD HH:ss"
                        )}{" "}
                        - UserID: {payment_user_id} - PayID: {payment_id} -
                        Status:
                        {payment_status}
                      </Col>
                    </Row>
                  </div>

                  <div className="box-footer">
                    <Row>
                      <Col md={8}>
                        <Link to="/payment" className="btn btn-default btn-sm">
                          <Space>
                            <FontAwesomeIcon icon={faReply} />
                            Back
                          </Space>
                        </Link>
                      </Col>
                    </Row>
                  </div>
                </Form>
              </div>

              <div className="box box-default">
                <div className="box-body">
                  <Row>
                    {completed_by !== null ? (
                      <Col md={8} className="text-center">
                        <strong>Completed By</strong>
                        <br />
                        <span>
                          {listUser.map(({ id, name }) => {
                            if (id === completed_by) return name;
                          })}{" "}
                          #{completed_by}
                        </span>
                        <br /> (
                        {dayjs(completed_at * 1000).format("YYYY-MM-DD HH:ss")}{" "}
                        )
                      </Col>
                    ) : (
                      <Col md={8} className="text-center">
                        <CompletedForm />
                      </Col>
                    )}

                    <Col md={8} className="text-center check-hide-action">
                      {verified_by !== null ? (
                        <>
                          <strong>Verified By</strong>
                          <br />
                          <span>
                            {listUser.map(({ id, name }) => {
                              if (id === verified_by) return name;
                            })}{" "}
                            #{verified_by}
                          </span>
                          <br /> ({" "}
                          {dayjs(verified_at * 1000).format("YYYY-MM-DD HH:ss")}{" "}
                          )
                        </>
                      ) : (
                        !completed_by && <VerifiedForm />
                      )}
                    </Col>

                    <Col md={8} className="text-center">
                      <strong>Created By</strong>
                      <br />
                      <span>
                        {listUser.map(({ id, name }) => {
                          if (id === payment_user_id) return name;
                        })}{" "}
                        #{payment_user_id}
                      </span>
                      <br />({" "}
                      {dayjs(payment_ctime * 1000).format("YYYY-MM-DD HH:ss")} )
                    </Col>
                  </Row>
                </div>
              </div>
            </Col>
          </Row>
        </Form.Provider>
      </section>
    </div>
  );
}

PaymentEdit.propTypes = {};

export default PaymentEdit;
