import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { MEDIA_URL } from "web.config";
import { Image, Modal, Upload } from "antd";

ImagesModal.propTypes = {
  // onCancel: PropTypes.,
  // onOk,
  // visible,
  // fileListData
};

function ImagesModal({ onCancel, onOk, visible, fileList }) {
  return (
    <Modal title="Image" onCancel={onCancel} onOk={onOk} visible={visible}>
      <Upload
        listType="picture-card"
        fileList={fileList}
        itemRender={(originNode, file, fileList) => {
          // console.log("file")
          // console.log(file)
          // console.log("fileList")
          // console.log(fileList)
          // console.log("originNode")
          // console.log(originNode)
          return <Image width="100%" src={`${MEDIA_URL}/${file.name}`} />;
        }}
      />
    </Modal>
  );
}

export default ImagesModal;
