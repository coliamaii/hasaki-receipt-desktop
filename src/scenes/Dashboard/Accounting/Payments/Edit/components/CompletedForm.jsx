import React from "react";
import PropTypes from "prop-types";
import { Button, Form } from "antd";

function CompletedForm(props) {
  const initialValues = {
    is_completed: 1,
  };
  return (
    <Form initialValues={initialValues} name="completedForm">
      <strong>Completed By</strong>

      <Form.Item style={{ margin: 0 }} name="is_completed">
        <Button type="primary" htmlType="submit">
          COMPLETE
        </Button>
      </Form.Item>
    </Form>
  );
}

CompletedForm.propTypes = {};

export default CompletedForm;
