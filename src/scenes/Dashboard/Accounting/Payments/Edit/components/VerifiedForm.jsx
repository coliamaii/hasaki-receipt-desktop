import React from "react";
import PropTypes from "prop-types";
import { Button, Form } from "antd";

function VerifiedForm(props) {
  const initialValues = {
    is_verified: 1,
  };
  return (
    <Form initialValues={initialValues} name="verifiedForm">
      <strong>Verified By</strong>

      <Form.Item style={{ margin: 0 }} name="is_verified">
        <Button type="primary" htmlType="submit">
          VERIFIED
        </Button>
      </Form.Item>
    </Form>
  );
}

VerifiedForm.propTypes = {};

export default VerifiedForm;
