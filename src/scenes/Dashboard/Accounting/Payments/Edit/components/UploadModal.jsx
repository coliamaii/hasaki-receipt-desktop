import React from "react";
import PropTypes from "prop-types";
import { Button, Form, Input, Modal, Upload } from "antd";
import TextArea from "antd/lib/input/TextArea";
import Icon from "@ant-design/icons";

function UploadModal({
  onCancel,
  onOk,
  visible,
  initialUploadValues,
  setFile,
}) {
  const [form] = Form.useForm();

  const handleUploadChange = (e) => {
    // let files = e.target.files;
    //   form.setFieldsValue({
    //     file: e.target.files,
    //   });
    console.log(e.target.files[0]);
    setFile(e.target.files[0]);
    console.log("change");
    console.log(e);
    // console.log(e.target.files[0])
    // console.log(files);
    //   console.log(e.target.files[0]);
  };
  return (
    <Modal
      title="Upload"
      cancelText="Close"
      onCancel={onCancel}
      okText="Save"
      onOk={() => onOk(form)}
      visible={visible}
    >
      <Form
        form={form}
        name="formUploadModal"
        initialValues={initialUploadValues}
      >
        <Form.Item noStyle name="object_id">
          <Input type="hidden" />
        </Form.Item>
        <Form.Item noStyle name="type">
          <Input type="hidden" />
        </Form.Item>
        <Form.Item noStyle name="link_type">
          <Input type="hidden" />
        </Form.Item>
        <div className="form-horizontal">
          <div className="form-group">
            <label for="url" className="col-sm-3 control-label">
              Image
            </label>
            <div className="col-md-8">
              <Form.Item style={{ margin: 0 }} name="file">
              <Input
                type="file"
                // id="file"
                accept="image/*, application/pdf"
                // onChange={(e) => handleUploadChange(e)}
              />
              {/* <Upload
                  accept="image/*, application/pdf"
                  onChange={(e) => handleUploadChange(e)}
                >
                  <Button>
                    <Icon type="upload" /> Click to Upload
                  </Button>
                </Upload> */}
              </Form.Item>
            </div>
          </div>
          <div className="form-group">
            <label for="url" className="col-sm-3 control-label">
              Note
            </label>
            <div className="col-md-8">
              <Form.Item noStyle name="note">
                <TextArea rows="5"></TextArea>
              </Form.Item>
            </div>
          </div>
        </div>
      </Form>
    </Modal>
  );
}

UploadModal.propTypes = {};

export default UploadModal;
