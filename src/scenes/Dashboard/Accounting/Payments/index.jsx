import {
  faCalculator,
  faEdit,
  faPlus,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, Form, Menu, Row, Space } from "antd";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory, useLocation } from "react-router-dom";
import { getDetailPaymentRequestAction } from "share/api/Accounting/Payment/GetDetail/actions";
import { getListPaymentRequestAction } from "share/api/Accounting/Payment/GetList/actions";
import formatMoney from "utils/money";
import EditPaymentTable from "./components/EditPaymentTable";
import SearchForm from "./components/SearchForm";

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

function Payment(props) {
  const dispatch = useDispatch();
  const history = useHistory();
  let query = useQuery();

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [page, setPage] = useState(1);

  const user_id = useSelector((state) => state.auth.info.profile.id);
  const { list: editPaymentTblDataSource } = useSelector(
    (state) => state.api.accounting.payment
  );

  const initialPaymentSearchFormValues = {
    from_cdate: moment().subtract(7, "d"),
    to_cdate: moment(),
    vendor_id: null,
    q: "",
    is_verified: "",
    payment_code: query.get("code"),
    payment_type: "",
    payment_transaction: "",
    status: "",
  };

  const editPaymentTblRowSelection = {
    selectedRowKeys,
    onChange: (selectedRowKeys, selectedRows) => {
      setSelectedRowKeys(selectedRowKeys);
    },
  };

  const editPaymentTblColumns = [
    {
      title: "No.",
      key: "1",
      align: "center",
      width: "1%",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{index + 1}</>,
        };
      },
    },
    {
      title: "Code",
      key: "2",
      align: "center",
      width: "9%",
      render: ({ payment_code, payment_id }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              <a href="#" onClick={() => handleClickEditBtn(payment_code)}>
                {payment_code}
              </a>
              <br />
              (ID: {payment_id})
            </>
          ),
        };
      },
    },
    {
      title: "Date",
      key: "3",
      align: "center",
      width: "10%",
      render: ({ payment_ctime }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{moment(payment_ctime * 1000).format("YYYY-MM-DD")}</>,
        };
      },
    },
    {
      title: "Content",
      key: "4",
      width: "37%",
      render: ({
        payment_note,
        reference_code,
        payment_user_id,
        payment_ctime,
        payment_utime,
      }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              Note: {payment_note}
              <br />
              Reference Code: {reference_code}
              <br />
              Created by{" "}
              <span>
                {payment_user_id} #{payment_user_id}
              </span>
              (Uid: {payment_user_id}) - Created:{" "}
              {moment(payment_ctime * 1000).format("YYYY-MM-DD HH:ss")}&nbsp;-
              Updated: {moment(payment_utime * 1000).format("YYYY-MM-DD HH:ss")}
            </>
          ),
        };
      },
    },
    {
      title: "Method",
      key: "5",
      align: "center",
      width: "7%",
      render: ({ payment_type }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <span className="text-nowrap text-center">{payment_type}</span>
          ),
        };
      },
    },
    {
      title: "Status",
      key: "6",
      align: "center",
      width: "7%",
      render: ({ payment_status }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{payment_status}</>,
        };
      },
    },
    {
      title: "Verified",
      key: "7",
      align: "center",
      width: "7%",
      render: ({ verified_by, verified_at }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {verified_by ? (
                <>
                  <span>
                    {verified_by} #{verified_by}
                  </span>
                  <br />({moment(verified_at * 1000).format("YYYY-MM-DD")})
                </>
              ) : (
                <>-</>
              )}
            </>
          ),
        };
      },
    },
    {
      title: "Completed",
      key: "8",
      align: "center",
      width: "10%",
      render: ({ verified_by, verified_at }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {verified_by ? (
                <>
                  <span>
                    {verified_by} #{verified_by}
                  </span>
                  <br />({moment(verified_at * 1000).format("YYYY-MM-DD")})
                </>
              ) : (
                <>-</>
              )}
            </>
          ),
        };
      },
    },
    {
      title: "Amount",
      key: "9",
      align: "center",
      width: "7%",
      render: ({ payment_amount }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{formatMoney(payment_amount)}</>,
        };
      },
    },
    // {
    //   title: null,
    //   key: "10",
    //   align: "center",
    //   width: "3%",
    //   render: ({ payment_id }) => {
    //     return {
    //       props: {
    //         style: { verticalAlign: "top" },
    //       },
    //       children: (
    //         <Button onClick={() => handleClickEditBtn(payment_id)}>
    //           <FontAwesomeIcon icon={faEdit} />
    //         </Button>
    //       ),
    //     };
    //   },
    // },
  ];

  const onPaymentFormFinish = (formName, { values, forms }) => {
    if (formName === "paymentSearchForm") {
      dispatch(
        getListPaymentRequestAction({
          ...values,
          from_cdate: values.from_cdate.unix(),
          to_cdate: values.to_cdate.unix(),
        })
      );
    }
  };

  const handleClickEditBtn = (id) => {
    dispatch(getDetailPaymentRequestAction(id, history.push));
  };

  useEffect(() => {
    setPage(1);
  }, []);

  return (
    <Form.Provider onFormFinish={onPaymentFormFinish}>
      <section className="content-header">
        <h1>
          Payments
          <small></small>
          <Link
            to="/support-request/create"
            className="btn btn-default btn-sm text-light-blue"
          >
            <Space>
              <FontAwesomeIcon icon={faPlus} />
              Add new
            </Space>
          </Link>
        </h1>

        <ol className="breadcrumb">
          <li>
            <Space>
              <FontAwesomeIcon icon={faCalculator} />
              Accounting
            </Space>
          </li>
          <li>Payments</li>
        </ol>
      </section>

      <section className="content">
        <Row>
          <Col md={24}>
            <div className="box">
              <Menu mode="horizontal" selectedKeys={["0"]}>
                <Menu.Item key="0">
                  <Link to="/accounting/payment">Payment</Link>
                </Menu.Item>
              </Menu>
              <div className="box-body">
                <SearchForm initialValues={initialPaymentSearchFormValues} />
              </div>
            </div>
          </Col>
        </Row>

        <Row>
          <Col md={24}>
            <div className="box">
              <div className="box-body">
                <EditPaymentTable
                  columns={editPaymentTblColumns}
                  rowSelection={editPaymentTblRowSelection}
                  dataSource={editPaymentTblDataSource}
                />
              </div>
            </div>
          </Col>
        </Row>
      </section>
    </Form.Provider>
  );
}

Payment.propTypes = {};

export default Payment;
