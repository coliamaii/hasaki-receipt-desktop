import Payment from "./Payments";
import PaymentEdit from "./Payments/Edit";

export default [
  {
    key: "accounting.payment",
    name: "Payments",
    component: Payment,
    path: "/accounting/payment",
    template: "main",
    exact: true,
    children: [
      {
        key: "dashboard.payment",
        name: "Payments",
        component: Payment,
        path: "/accounting/payment",
        hide: true,
        template: "main",
      },
      {
        key: "payment.edit",
        name: "Payment Edit",
        component: PaymentEdit,
        path: "/accounting/payment/edit",
        hide: true,
        template: "main",
      },
      {
        key: "payment.create",
        name: "Payment Edit",
        component: PaymentEdit,
        path: "/accounting/payment/create",
        hide: true,
        template: "main",
      },
    ],
  },
];
