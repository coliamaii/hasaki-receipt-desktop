import { Col, Form, Menu, message, Row } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { getStoreConfigInfoRequestAction } from "share/api/Settings/User-Store/GetConfigInfo/actions";
import { socket, startSocket, stopSocket } from "share/socket";
import {
  connectWebSocketSuccessAction,
  webSocketSetPosServerAction,
  webSocketSetScreenAction,
} from "share/socket/action";
import POS from "./component/POS";
import POSChangeModel from "./component/POSChangeModel";

function Setting(props) {
  const dispatch = useDispatch();

  const [visibleSettingPOSModal, setVisibleSettingPOSModal] = useState(false);

  const { store_id } = useSelector((state) => state.auth.info.profile);
  const { screen: screen_list, pos_server: pos_server_list } = useSelector(
    (state) => state.api.setting.store.config
  );

  const initialChangePosServerFormValues = {
    pos_server: "0",
  };

  const handleClickChangeBtn = () => {
    setVisibleSettingPOSModal(true);
  };

  const onSettingFormFinish = (formName, { values, forms }) => {
    if (formName === "changePosServerForm") {
      const { pos_server } = values;
      let data = null;

      if (pos_server === "0") {
        data = {
          ip: null,
          name: "Turn Off",
        };
      } else {
        pos_server_list.map((pos) => {
          if (pos.ip === pos_server) {
            data = pos;
            return;
          }
        });
      }
      dispatch(webSocketSetPosServerAction(data));
      setVisibleSettingPOSModal(false);
    }
  };

  const handleClickTestBtn = async (changePosServerForm) => {
    const key = "testBtn";
    const pos_server = changePosServerForm.getFieldValue("pos_server");

    await stopSocket();
    if (pos_server == 0) {
      message.loading({ content: "Socket disconnecting", key });
      await socket.on("disconnect", () => {
        dispatch(connectWebSocketSuccessAction(false));
        message.success({ content: "Socket disconnect success", key });
      });
    } else {
      message.loading({ content: "Socket connecting", key });
      await startSocket();
      await socket.on("connect", () => {
        dispatch(connectWebSocketSuccessAction(true));
        message.success({ content: "Socket connect success", key });
      });
    }
  };

  const handleClickCancelBtn = () => {
    setVisibleSettingPOSModal(false);
  };

  const handleScreenSelect = (screen_id) => {
    let data = null;
    if (screen_id == "0") {
      data = {
        id: null,
        name: "Turn off",
      };
    } else {
      screen_list.map((obj) => {
        if (obj.id === screen_id) {
          data = obj;
          return;
        }
      });
    }

    dispatch(webSocketSetScreenAction(data));
  };

  useEffect(() => {
    let data = {
      ip: null,
      name: "Turn Off",
    };
    dispatch(getStoreConfigInfoRequestAction(store_id));
    dispatch(webSocketSetPosServerAction(data));
  }, []);

  return (
    <Form.Provider onFormFinish={onSettingFormFinish}>
      <section className="content-header">
        <h1>
          Setting
          <small></small>
        </h1>
      </section>

      <section className="content">
        <Row>
          <Col md={24}>
            <div className="box">
              <Menu mode="horizontal" selectedKeys={["0"]}>
                <Menu.Item key="0">
                  <Link to="/setting/pos">POS</Link>
                </Menu.Item>
              </Menu>
            </div>
          </Col>
        </Row>

        <Row>
          <Col md={24}>
            <div className="box">
              <div className="box-body">
                <POS
                  changeBtn_handleClick={handleClickChangeBtn}
                  handleOnSelect={handleScreenSelect}
                />
              </div>
            </div>
          </Col>
        </Row>
      </section>
      <POSChangeModel
        initialValues={initialChangePosServerFormValues}
        visible={visibleSettingPOSModal}
        handleTestBtn={handleClickTestBtn}
        handleCancelBtn={handleClickCancelBtn}
      />
    </Form.Provider>
  );
}

export default Setting;
