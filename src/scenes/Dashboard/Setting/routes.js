import Setting from "./index";

export default [
  {
    key: "dashboard.setting",
    name: "POS setting",
    component: Setting,
    path: "/setting/pos",
    template: "main",
    exact: true,
  },
];
