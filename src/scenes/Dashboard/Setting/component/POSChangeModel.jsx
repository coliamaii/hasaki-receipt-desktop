import React from "react";
import PropTypes from "prop-types";
import { Modal, Button, Row, Col, Select, Form, Space } from "antd";
import { useSelector } from "react-redux";

const { Option } = Select;

function POSChangeModel({ visible, initialValues, handleTestBtn, handleCancelBtn }) {
  const [form] = Form.useForm();
  const { screen, pos_server, ip: posIpList } = useSelector(
    (state) => state.api.setting.store.config
  );

  return (
    <Modal
      title=" "
      visible={visible}
      onCancel={handleCancelBtn}
      footer={false}
    >
      <Form
        form={form}
        name="changePosServerForm"
        initialValues={initialValues}
      >
        <Row>
          <Col sm={24} md={12}>
            POS SERVER:
          </Col>
          <Col sm={24} md={12}>
            <Form.Item name="pos_server">
              <Select
                style={{ width: "100%" }}
              >
                <Option value="0">Turn Off</Option>
                {pos_server?.map(({ ip, name }) => {
                  return <Option value={ip}>{name}</Option>;
                })}
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col sm={24} className="text-right">
            <Space>
              <Button type="primary" onClick={() => handleTestBtn(form)}>Test</Button>
              <Button type="default" htmlType="submit">
                OK
              </Button>
            </Space>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
}

POSChangeModel.propTypes = {};

export default POSChangeModel;
