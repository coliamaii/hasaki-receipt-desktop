import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Button, Col, Row, Select, Space } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { webSocketSetClientIdAction } from "share/socket/action";

const { Option } = Select;

function POS({ changeBtn_handleClick, handleOnSelect }) {
  const dispatch = useDispatch();

  const [screenSelected, setScreenSelected] = useState("0");
  const { profile } = useSelector((state) => state.auth.info);
  const { screen: screen_list, pos_server, ip: posIpList } = useSelector(
    (state) => state.api.setting.store.config
  );
  const { name: pos_server_name } = useSelector(
    (state) => state.websocket.pos_server
  );
  const socket_selected = useSelector((state) => state.websocket.socket);
  const {
    ip,
    store: { store_name },
  } = profile;

  useEffect(() => {
    if (socket_selected) {
      setScreenSelected(socket_selected.id);
    } else {
      setScreenSelected("0");
    }
  }, [socket_selected]);

  return (
    <Row>
      <Col sm={24} md={12}>
        <Row gutter={[{ xs: 0 }, { xs: 8 }]}>
          <Col sm={12}>Store:</Col>
          <Col sm={12}>
            <strong>{store_name}</strong>
          </Col>
        </Row>

        <Row gutter={[{ xs: 0 }, { xs: 8 }]}>
          <Col sm={12}>IP:</Col>
          <Col sm={12}>
            <strong>{ip}</strong>
          </Col>
        </Row>

        <Row gutter={[{ xs: 0 }, { xs: 8 }]}>
          <Col sm={12}>Screen:</Col>
          <Col sm={12}>
            <Select defaultValue="0" onSelect={handleOnSelect}>
              <Option value="0">Turn Off</Option>
              {screen_list?.map(({ id, name }) => {
                return <Option value={id}>{name}</Option>;
              })}
            </Select>
          </Col>
        </Row>

        <Row gutter={[{ xs: 0 }, { xs: 8 }]}>
          <Col sm={12}>POS Server:</Col>
          <Col sm={12}>
            <Space>
              {pos_server_name}
              <Button
                type="primary"
                danger
                onClick={() => changeBtn_handleClick()}
              >
                Change
              </Button>
            </Space>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}

POS.propTypes = {};

export default POS;
