import { faReply } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, Form, Input, Row, Select, Space } from "antd";
import TextArea from "antd/lib/input/TextArea";
import _map from "lodash/map";
import PropTypes from "prop-types";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import {
  options_return_product_payment_method,
  options_return_product_reason,
} from "share/options/return_product_options";
import formatMoney from "utils/money";

const { Option } = Select;

ReturnProductEditForm.propTypes = {
  initialValues: PropTypes.object,
  handleBackClick: PropTypes.func,
};

ReturnProductEditForm.defaultProps = {
  initialValues: {},
  handleBackClick: () => {
    return;
  },
};

function ReturnProductEditForm({ initialValues, handleBackClick }) {
  const [form] = Form.useForm();

  const { stores } = useSelector((state) => state.auth.info);

  const {
    return_id,
    return_store_id,
    return_source_id,
    return_customer_id,
    return_customer_name,
    return_customer_phone,
    return_item_sku,
    return_item_qty,
    return_refund,
    return_reason,
    note,
    return_payment_method,
  } = useSelector((state) => state.api.returnProduct.detail);

  useEffect(() => {
    form.setFieldsValue({
      store_id: return_store_id,
      source_id: return_source_id,
      customer_id: return_customer_id,
      customer_name: return_customer_name,
      customer_phone: return_customer_phone,
      item_sku: return_item_sku,
      item_qty: return_item_qty,
      refund: formatMoney(return_refund),
      reason: return_reason ? return_reason : "changed-mind",
      payment_method: return_payment_method,
      note,
    });
  }, [return_id]);

  return (
    <Form form={form} name="returnProductFrm" initialValues={initialValues}>
      <div className="box box-default">
        <div className="box-body">
          <Row gutter={[32]}>
            <Col xs={6}>
              <div className="form-group">
                <label>Store</label>

                <Form.Item name="store_id" style={{ margin: 0 }}>
                  <Select placeholder="Store">
                    {_map(stores, ({ store_id, store_name }) => {
                      if (return_store_id === store_id) {
                        return (
                          <Option key={store_id} value={store_id}>
                            {store_name}
                          </Option>
                        );
                      }
                    })}
                  </Select>
                </Form.Item>
              </div>
            </Col>
            <Col xs={6}>
              <div className="form-group">
                <label>ReceiptID/OrderID</label>
                <Form.Item name="source_id" style={{ margin: 0 }}>
                  <Input placeholder="ReceiptID/OrderID" />
                </Form.Item>
              </div>
            </Col>
          </Row>

          <Row gutter={[32]}>
            <Col xs={6}>
              <div className="form-group">
                <label>Customer ID</label>
                <Form.Item name="customer_id" style={{ margin: 0 }}>
                  <Input placeholder="Customer ID" />
                </Form.Item>
              </div>
            </Col>
            <Col xs={6}>
              <div className="form-group">
                <label>Customer Name</label>
                <Form.Item name="customer_name" style={{ margin: 0 }}>
                  <Input placeholder="Customer Name" />
                </Form.Item>
              </div>
            </Col>

            <Col xs={6}>
              <div className="form-group">
                <label>Customer Phone</label>
                <Form.Item name="customer_phone" style={{ margin: 0 }}>
                  <Input placeholder="Customer Phone" />
                </Form.Item>
              </div>
            </Col>
          </Row>

          <Row gutter={[32]}>
            <Col xs={6}>
              <div className="form-group">
                <label>SKU</label>
                <Form.Item name="item_sku" style={{ margin: 0 }}>
                  <Input placeholder="SKU" />
                </Form.Item>
              </div>
            </Col>
            <Col xs={6}>
              <div className="form-group">
                <label>Qty</label>
                <Form.Item name="item_qty" style={{ margin: 0 }}>
                  <Input placeholder="Qty" />
                </Form.Item>
              </div>
            </Col>

            <Col xs={6}>
              <div className="form-group">
                <label>Refund</label> (<span>{formatMoney(return_refund)}</span>
                )
                <Form.Item name="refund" style={{ margin: 0 }}>
                  <Input placeholder="Refund" />
                </Form.Item>
              </div>
            </Col>
          </Row>
          <Row gutter={[32]}>
            <Col xs={6}>
              <div className="form-group">
                <label>Reason</label>
                <Form.Item name="reason" style={{ margin: 0 }}>
                  <Select placeholder="Reason">
                    {_map(options_return_product_reason, ({ value, label }) => {
                      return (
                        <Option key={value} value={value}>
                          {label}
                        </Option>
                      );
                    })}
                  </Select>
                </Form.Item>
              </div>
            </Col>

            <Col xs={6}>
              <div className="form-group">
                <label>Payment method</label>
                <Form.Item name="payment_method" style={{ margin: 0 }}>
                  <Select placeholder="Payment method">
                    {_map(
                      options_return_product_payment_method,
                      ({ value, label }) => {
                        if (value === return_payment_method) {
                          return (
                            <Option key={value} value={value}>
                              {label}
                            </Option>
                          );
                        }
                      }
                    )}
                  </Select>
                </Form.Item>
              </div>
            </Col>
          </Row>
          <Row>
            <Col xs={16}>
              <div className="form-group">
                <label>Note</label>
                <Form.Item name="note" style={{ margin: 0 }}>
                  <TextArea rows="5" maxLength="250"></TextArea>
                </Form.Item>
              </div>
            </Col>
          </Row>
        </div>

        <div className="box-footer text-right">
          <Space>
            {/* <Button
              type="primary"
              htmlType="submit"
              icon={<FontAwesomeIcon icon={faPlus} />}
            >
              &nbsp;Update
            </Button> */}
            <Button
              icon={<FontAwesomeIcon icon={faReply} />}
              onClick={handleBackClick}
            >
              &nbsp;Back
            </Button>
          </Space>
        </div>
      </div>
    </Form>
  );
}

export default ReturnProductEditForm;
