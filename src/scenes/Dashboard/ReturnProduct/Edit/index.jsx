import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Form, Row, Space } from "antd";
import React from "react";
import { useHistory } from "react-router";
import ReturnProductEditForm from "./components/ReturnProductEditForm";

function ReturnProductEdit(props) {
  const history = useHistory();

  const returnProductFrmInitialValues = {
    store_id: "",
    source_id: "",
    customer_id: "",
    customer_name: "",
    customer_phone: "",
    item_sku: "",
    item_qty: "",
    refund: "",
    reason: "",
    payment_method: "",
    note: "",
  };

  const handleBackClick = () => {
    history.push("/product/return");
  };

  const onFormFinishReturnProductForm = (formName, { values, forms }) => {
    const { returnProductFrm } = forms;
    if (formName === "returnProductFrm") {
      // update return product detail
      // dispatch()
    }
  };

  return (
    <Form.Provider onFormFinish={onFormFinishReturnProductForm}>
      <section className="content-header">
        <h1>
          Return Product
          <small>Edit</small>
        </h1>
        <ol className="breadcrumb">
          <li>
            <Space>
              <FontAwesomeIcon icon={faShoppingCart} />
              Sale
            </Space>
          </li>
          <li>Product Out Of store</li>
        </ol>
      </section>

      <section className="content">
        <Row>
          <Col xs={24}>
            <ReturnProductEditForm
              initialValues={returnProductFrmInitialValues}
              handleBackClick={handleBackClick}
            />
          </Col>
        </Row>
      </section>
    </Form.Provider>
  );
}

export default ReturnProductEdit;
