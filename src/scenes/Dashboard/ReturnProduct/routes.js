import ReturnProduct from ".";
import ReturnProductEdit from "./Edit";
import ReturnProductCreate from "./Create";

export default [
  {
    key: "dashboard.return-product",
    name: "Return Product",
    component: ReturnProduct,
    path: "/product/return",
    template: "main",
    exact: true,
    children: [
      {
        key: "return-product.create",
        name: "Return Product Create",
        component: ReturnProductCreate,
        path: "/product/return/create",
        hide: true,
        template: "main",
      },
            {
        key: "return-product.edit",
        name: "Return Product Edit",
        component: ReturnProductEdit,
        path: "/product/return/edit",
        hide: true,
        template: "main",
      },
    ],
  },
];
