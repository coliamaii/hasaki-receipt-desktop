import {
  faPencilAlt,
  faPlus,
  faShoppingCart,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, Form, Row, Space } from "antd";
import dayjs from "dayjs";
import _map from "lodash/map";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import { getDetailReturnProductRequestAction } from "share/api/ReturnProduct/GetDetail/actions";
import { getListReturnProductRequestAction } from "share/api/ReturnProduct/GetList/actions";
import formatMoney from "utils/money";
import ReturnProductSearchForm from "./components/ReturnProductSearchForm";
import ReturnProductTable from "./components/ReturnProductTable";

function ReturnProduct(props) {
  const dispatch = useDispatch();
  const history = useHistory();

  const [page, setPage] = useState(1);

  const { isLoading, list: returnProductTblDataSource, arrUsers } = useSelector(
    (state) => state.api.returnProduct
  );
  const { stores } = useSelector((state) => state.auth.info);

  const initialReturnProductSearchFormValues = {
    store_id: "",
    from_date: moment().subtract(7, "d"),
    to_date: moment(),
    sku: "",
    source_id: "",
    type: "",
    status: "",
    payment_method: "",
  };

  const returnProductTblColumns = [
    {
      title: "No.",
      key: "0",
      align: "center",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{(page - 1) * 10 + index + 1}</>,
        };
      },
    },
    {
      title: "ID",
      key: "1",
      align: "center",
      render: ({ return_id }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>#{return_id}</>,
        };
      },
    },
    {
      title: "Store",
      key: "2",
      align: "center",
      render: ({ return_store_id }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {_map(stores, ({ store_id, store_name }) => {
                if (store_id === return_store_id) return store_name;
              })}
            </>
          ),
        };
      },
    },
    {
      title: "Code",
      key: "3",
      align: "center",
      render: ({ return_source_id }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{return_source_id}</>,
        };
      },
    },
    {
      title: "Location",
      key: "4",
      render: ({ type }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{type == 1 ? "On Store" : "At Home"}</>,
        };
      },
    },
    {
      title: "Product",
      key: "5",
      render: ({ product }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              #{product.product_id} - {product.product_name}
            </>
          ),
        };
      },
    },
    {
      title: "QTY",
      key: "6",
      align: "center",
      render: ({ return_item_qty }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{return_item_qty}</>,
        };
      },
    },
    {
      title: "Refund",
      key: "7",
      align: "center",
      render: ({ return_refund }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{formatMoney(return_refund)}</>,
        };
      },
    },
    {
      title: "Method",
      key: "8",
      align: "center",
      render: ({ status, return_payment_method }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {return_payment_method == 1 ? (
                "Cash"
              ) : return_payment_method == 2 ? (
                status == 1 ? (
                  <>
                    Balance
                    <br />
                    <span className="text-sm text-green">(Success)</span>
                  </>
                ) : (
                  "Balance"
                )
              ) : (
                "Vnpay"
              )}
            </>
          ),
        };
      },
    },
    {
      title: "Reason",
      key: "9",
      align: "center",
      render: ({ return_reason }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{return_reason}</>,
        };
      },
    },
    {
      title: "User",
      key: "10",
      align: "center",
      render: ({ return_user_id }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {_map(arrUsers, ({ id, name }) => {
                if (return_user_id === id) {
                  return name;
                }
              })}
            </>
          ),
        };
      },
    },
    {
      title: "Date",
      key: "11",
      align: "center",
      render: ({ return_ctime }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>{dayjs(return_ctime * 1000).format("YYYY-MM-DD HH:mm")}</>
          ),
        };
      },
    },
    {
      title: "Status",
      key: "12",
      align: "center",
      render: ({ status }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{status == 1 ? "Approved" : "Pending"}</>,
        };
      },
    },
    {
      title: "Action",
      key: "13",
      align: "center",
      render: ({ return_id }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <Button
              onClick={() => handleClickEditBtn(return_id)}
              icon={<FontAwesomeIcon icon={faPencilAlt} />}
            >
              &nbsp; Edit
            </Button>
          ),
        };
      },
    },
  ];

  const onReturnProductFormProviderFinish = (formName, { values, forms }) => {
    const { returnProductSearchForm } = forms;
    if (formName === "returnProductSearchForm") {
      const params = {
        store_id: values.store_id,
        from_date: values.from_date.format("YYYY-MM-DD"),
        to_date: values.to_date.format("YYYY-MM-DD"),
        sku: values.sku,
        source_id: values.source_id,
        type: values.type,
        status: values.status,
        payment_method: values.payment_method,
      };

      dispatch(getListReturnProductRequestAction(params));
    }
  };

  const handleClickEditBtn = (return_id) => {
    dispatch(getDetailReturnProductRequestAction(return_id, history.push));
  };

  const handlePaginationChange = (current) => {
    setPage(current);
  };

  useEffect(() => {
    setPage(1);
  }, [returnProductTblDataSource]);

  return (
    <Form.Provider onFormFinish={onReturnProductFormProviderFinish}>
      <section className="content-header">
        <h1>
          Return Product &nbsp;
          <Link to="/product/return/create">
            <Button>
              <Space>
                <FontAwesomeIcon icon={faPlus} />
                Add new
              </Space>
            </Button>
          </Link>
        </h1>

        <ol className="breadcrumb">
          <li>
            <a href="#">
              <Space>
                <FontAwesomeIcon icon={faShoppingCart} />
                Sale
              </Space>
            </a>
          </li>
          <li>
            <a href="#"> Products</a>
          </li>
          <li>Brands</li>
        </ol>
      </section>

      <section className="content">
        <ReturnProductSearchForm
          initialValues={initialReturnProductSearchFormValues}
        />

        <Row>
          <Col xs={24}>
            <div className="box">
              <ReturnProductTable
                columns={returnProductTblColumns}
                dataSource={returnProductTblDataSource}
                handlePaginationChange={handlePaginationChange}
              />
            </div>
          </Col>
        </Row>
      </section>
    </Form.Provider>
  );
}

export default ReturnProduct;
