import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, DatePicker, Form, Input, Row, Select } from "antd";
import PropTypes from "prop-types";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import {
  options_return_product_payment_method,
  options_return_product_status,
  options_return_product_type,
} from "share/options/return_product_options";

const { Option } = Select;

ReturnProductSearchForm.propTypes = {
  initialValues: PropTypes.object,
};

ReturnProductSearchForm.defaultProps = {
  initialValues: [],
};

function ReturnProductSearchForm({ initialValues }) {
  const [form] = Form.useForm();

  const { isLoading } = useSelector((state) => state.api.returnProduct);
  const { stores } = useSelector((state) => state.auth.info);

  useEffect(() => {
    form.submit();
  }, []);

  return (
    <Form
      form={form}
      name="returnProductSearchForm"
      initialValues={initialValues}
    >
      <Row>
        <Col xs={24}>
          <div className="box">
            <div className="box-body">
              <Row
                gutter={[
                  { sm: 16, md: 32 },
                  { xs: 8, sm: 16, md: 16 },
                ]}
              >
                <Col xs={24} sm={12} md={8} lg={5}>
                  <Form.Item name="from_date" style={{ margin: 0 }}>
                    <DatePicker
                      style={{ width: "100%" }}
                      format="YYYY-MM-DD"
                      placeholder="From Date"
                    />
                  </Form.Item>
                </Col>
                <Col xs={24} sm={12} md={8} lg={5}>
                  <Form.Item name="to_date" style={{ margin: 0 }}>
                    <DatePicker
                      style={{ width: "100%" }}
                      format="YYYY-MM-DD"
                      placeholder="To Date"
                    />
                  </Form.Item>
                </Col>
                <Col xs={24} sm={12} md={8} lg={5}>
                  <Form.Item name="sku" style={{ margin: 0 }}>
                    <Input style={{ width: "100%" }} placeholder="SKU" />
                  </Form.Item>
                </Col>
                <Col xs={24} sm={12} md={8} lg={5}>
                  <Form.Item name="source_id" style={{ margin: 0 }}>
                    <Input
                      style={{ width: "100%" }}
                      placeholder="Order, Receipt Code"
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row
                gutter={[
                  { sm: 16, md: 32 },
                  { xs: 8, sm: 16, md: 16, lg: 0 },
                ]}
              >
                <Col xs={24} sm={12} md={8} lg={5}>
                  <Form.Item name="store_id" style={{ margin: 0 }}>
                    <Select style={{ width: "100%" }}>
                      <Option value="">--All store--</Option>
                      {stores?.map(({ store_id, store_name }) => {
                        return (
                          <Option key={store_id} value={store_id}>
                            {store_name}
                          </Option>
                        );
                      })}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={24} sm={12} md={8} lg={5}>
                  <Form.Item name="type" style={{ margin: 0 }}>
                    <Select style={{ width: "100%" }}>
                      <Option value="">--All Type--</Option>
                      {options_return_product_type.map(({ value, label }) => {
                        return (
                          <Option key={value} value={value}>
                            {label}
                          </Option>
                        );
                      })}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={24} sm={12} md={8} lg={5}>
                  <Form.Item name="status" style={{ margin: 0 }}>
                    <Select style={{ width: "100%" }}>
                      <Option value="">--All Status--</Option>
                      {options_return_product_status.map(({ value, label }) => {
                        return (
                          <Option key={value} value={value}>
                            {label}
                          </Option>
                        );
                      })}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={24} sm={12} md={8} lg={5}>
                  <Form.Item name="payment_method" style={{ margin: 0 }}>
                    <Select style={{ width: "100%" }}>
                      <Option value="">--All Payment Method--</Option>
                      {options_return_product_payment_method.map(
                        ({ value, label }) => {
                          return (
                            <Option key={value} value={value}>
                              {label}
                            </Option>
                          );
                        }
                      )}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={24} sm={12} md={8} lg={4}>
                  <Button
                    htmlType="submit"
                    type="primary"
                    loading={isLoading}
                    icon={<FontAwesomeIcon icon={faSearch} />}
                  >
                    &nbsp;Search
                  </Button>
                </Col>
              </Row>
            </div>
          </div>
        </Col>
      </Row>
    </Form>
  );
}

export default ReturnProductSearchForm;
