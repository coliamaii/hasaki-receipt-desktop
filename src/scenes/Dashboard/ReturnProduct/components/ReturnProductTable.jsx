import { css } from "@emotion/css";
import { Table } from "antd";
import PropTypes from "prop-types";
import React from "react";
import { useSelector } from "react-redux";

ReturnProductTable.propTypes = {
  dataSource: PropTypes.array,
  columns: PropTypes.array,
  handleClickEditBtn: PropTypes.func,
};

ReturnProductTable.defaultProps = {
  dataSource: [],
  columns: [],
  handleClickEditBtn: () => {
    return;
  },
};

function ReturnProductTable({ dataSource, columns, handlePaginationChange }) {
  const { isLoading } = useSelector((state) => state.api.returnProduct);
  const tableCSS = css({
    marginBottom: 20,

    "& table": {
      borderCollapse: "collapse",
    },
    "& thead > tr > th": {
      fontWeight: "bold",
      textAlign: "center",
    },

    "& .ant-table-placeholder": {
      display: "none",
    },

    "& thead > tr > th, & tbody > tr > td, & tfoot > tr > td": {
      padding: 8,
    },

    "& .ant-table-tbody > tr.ant-table-row:hover > td": {
      background: "#1890ff",
    },
  });

  let locale = {
    emptyText: " ",
  };

  return (
    <Table
      loading={isLoading}
      locale={locale}
      className={tableCSS}
      dataSource={dataSource}
      columns={columns}
      pagination={{
        onChange(current) {
          handlePaginationChange(current);
        },
        itemRender: (current, type, originalElement) => {
          if (type === "prev") {
            return <a>Prev</a>;
          }
          if (type === "next") {
            return <a>Next</a>;
          }
          return originalElement;
        },
      }}
    />
  );
}

export default ReturnProductTable;
