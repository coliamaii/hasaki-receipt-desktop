import { css } from "@emotion/css";
import { Table } from "antd";
import React from "react";
import PropType from "prop-types";

ItemsDetailTable.propTypes = {
  columns: PropType.array,
  dataSource: PropType.array,
};

ItemsDetailTable.defaultProps = {
  columns: [],
  dataSource: [],
};

function ItemsDetailTable({ columns, dataSource }) {
  const tableCSS = css({
    marginBottom: 20,

    "& table": {
      borderCollapse: "collapse",
    },
    "& thead > tr > th": {
      fontWeight: "bold",
      textAlign: "center",
    },

    "& .ant-table-placeholder": {
      display: "none",
    },

    "& thead > tr > th, & tbody > tr > td, & tfoot > tr > td": {
      padding: 8,
    },
  });

  let locale = {
    emptyText: " ",
  };

  return (
    <Table
      className={tableCSS}
      locale={locale}
      dataSource={dataSource}
      columns={columns}
      pagination={false}
      footer={false}
    />
  );
}

export default ItemsDetailTable;
