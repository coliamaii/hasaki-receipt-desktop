import { css } from "@emotion/css";
import { Table } from "antd";
import PropTypes from "prop-types";
import React from "react";

PaymentInfoTable.propTypes = {
  dataSource: PropTypes.array,
  columns: PropTypes.array,
};

PaymentInfoTable.propTypes = {
  dataSource: [],
  columns: [],
};

function PaymentInfoTable({ dataSource, columns }) {
  const tableCSS = css({
    marginBottom: 20,

    "& .ant-table-tbody > tr:hover > td": {
      backgroundColor: "unset",
    },
    "& table": {
      borderCollapse: "collapse",
    },
    "& thead > tr > th": {
      fontWeight: "bold",
      textAlign: "center",
    },

    "& .ant-table-placeholder": {
      display: "none",
    },

    "& thead > tr > th, & tbody > tr > td, & tfoot > tr > td": {
      padding: 8,
    },
  });

  let locale = {
    emptyText: " ",
  };

  return (
    <Table
      className={tableCSS}
      locale={locale}
      dataSource={dataSource}
      columns={columns}
      pagination={false}
      footer={false}
      rowClassName={(record, index) =>
        record.payment_method === 5 && "bg-yellow"
      }
    />
  );
}

export default PaymentInfoTable;
