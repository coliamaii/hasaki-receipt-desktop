import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, Form, Input, Row } from "antd";
import PropTypes from "prop-types";
import React from "react";
import { useSelector } from "react-redux";

SearchForm.propTypes = {
  initialValues: PropTypes.object,
};

SearchForm.defaultProps = {
  initialValues: {},
};

function SearchForm({ initialValues }) {
  const [form] = Form.useForm();

  const { isLoading } = useSelector((state) => state.api.returnProduct);

  return (
    <Form form={form} name="searchForm" initialValues={initialValues}>
      <Row gutter={32}>
        <Col xs={8}>
          <Form.Item name="code" style={{ margin: 0 }}>
            <Input placeholder="ReceiptCode,OrderCode" />
          </Form.Item>
        </Col>
        <Col xs={6}>
          <Form.Item name="code" style={{ margin: 0 }}>
            <Button
              type="primary"
              htmlType="submit"
              loading={isLoading}
              icon={<FontAwesomeIcon icon={faSearch} />}
            >
              &nbsp; Search
            </Button>
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
}

export default SearchForm;
