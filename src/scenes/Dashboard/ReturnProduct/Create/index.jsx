import { faSearch, faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Col, Form, Row, Space } from "antd";
import dayjs from "dayjs";
import _map from "lodash/map";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { createReturnProductRequestAction } from "share/api/ReturnProduct/Create/actions";
import { insertReturnProductRequestAction } from "share/api/ReturnProduct/Insert/actions";
import formatMoney from "utils/money";
import ItemsDetailTable from "./components/ItemsDetailTable";
import PaymentInfoTable from "./components/PaymentInfoTable";
import SearchForm from "./components/SearchForm";

const PAYMENT_METHOD_CASH = 1;
const PAYMENT_METHOD_BANKTRANSFER = 2;
const COMPANY_VNPAY_ID = 4;

function ReturnProductCreate(props) {
  const dispatch = useDispatch();
  const history = useHistory();

  const [isSearch, setIsSearch] = useState(false);
  const [paymentInfoTableDataSource, setPaymentInfoTableDataSource] = useState(
    []
  );
  const [itemsDetailTableDataSource, setItemsDetailTableDataSource] = useState(
    []
  );
  const [isAllowReturnProduct, setIsAllowReturnProduct] = useState(false);
  const [showWarning, setShowWarning] = useState(false);
  const [isCashPayment, setIsCashPayment] = useState(false);
  const [isVnpayPayment, setIsVnpayPayment] = useState(false);
  const [indexPayment, setIndexPayment] = useState(1);
  const [cashAmount, setCashAmount] = useState(0);

  const { store_id } = useSelector((state) => state.auth.info.profile);
  const {
    arrReturnItem,
    arrCustomerInfo,
    arrPaymentReceipts,
    isSearchStatus,
    totalCash,
    mess,
    sourceStoreId,
  } = useSelector((state) => state.api.returnProduct);

  const searchFormInitialValues = {
    code: "",
  };

  const paymentInfoTableColumns = [
    {
      title: "No.",
      key: "0",
      align: "center",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{index + 1}</>,
        };
      },
    },
    {
      title: "Type",
      key: "1",
      align: "center",
      render: ({ payment_method }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: (
            <>
              {arrPaymentReceipts
                ? _map(
                    arrPaymentReceipts["payment_methods"],
                    ({ key, name }) => {
                      if (payment_method === key) {
                        return name;
                      }
                    }
                  )
                : ""}
            </>
          ),
        };
      },
    },
    {
      title: "Amount",
      key: "2",
      render: ({ payment_amount }) => {
        return {
          props: {
            style: { verticalAlign: "top", textAlign: "right" },
          },
          children: <>{formatMoney(payment_amount)}</>,
        };
      },
    },
  ];

  const itemsDetailTableColumns = [
    {
      title: "No.",
      key: "0",
      align: "center",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{index + 1}</>,
        };
      },
    },
    {
      title: "SKU",
      key: "1",
      align: "center",
      render: ({ sku }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{sku}</>,
        };
      },
    },
    {
      title: "Name",
      key: "2",
      render: ({ name }) => {
        return {
          props: {
            style: { verticalAlign: "top" },
          },
          children: <>{name}</>,
        };
      },
    },
    {
      title: "Price",
      key: "3",
      render: ({ price }) => {
        return {
          props: {
            style: { verticalAlign: "top", textAlign: "right" },
          },
          children: <>{formatMoney(price)}</>,
        };
      },
    },
    {
      title: "Discount",
      key: "4",
      render: ({ discount }) => {
        return {
          props: {
            style: { verticalAlign: "top", textAlign: "right" },
          },
          children: <>{formatMoney(discount)}</>,
        };
      },
    },
    {
      title: "Qty",
      key: "5",
      render: ({ qty }) => {
        return {
          props: {
            style: { verticalAlign: "top", textAlign: "right" },
          },
          children: <>{qty}</>,
        };
      },
    },
    {
      title: null,
      key: "6",
      render: (text, record, index) => {
        return {
          props: {
            style: { verticalAlign: "top", textAlign: "right" },
          },
          children: (
            <Button
              onClick={() => handleClickAddReturnBtn(record, history.push)}
              className="alert alert-warning"
              style={{ margin: 0 }}
            >
              Add Return
            </Button>
          ),
        };
      },
    },
  ];

  const onReturnProductCreateFormFinish = (formName, { values, forms }) => {
    const { searchForm } = forms;
    if (formName === "searchForm") {
      const params = {
        code: values.code,
      };
      dispatch(createReturnProductRequestAction(params));
      searchForm.setFieldsValue(searchFormInitialValues);
      setIsSearch(true);
    }
  };

  const handleClickViewBtn = () => {
    history.push("/print/pos");
  };

  const handleClickAddReturnBtn = (values) => {
    const params = {
      store_id: 4,
      source_store_id: sourceStoreId,
      source_id: arrReturnItem["code"],
      customer_id: arrReturnItem["customer_id"],
      item_sku: values.sku,
      item_qty: values.qty,
      reason: "",
    };
    console.log("params");
    console.log(params);
    dispatch(insertReturnProductRequestAction(params, history.push));
  };

  useEffect(() => {
    if (arrPaymentReceipts.rows) {
      let temp = [];
      let index_temp = indexPayment;
      _map(arrPaymentReceipts.rows, (obj) => {
        if (obj["payment_method"] === 5) {
          setShowWarning(true);
        }
        if (obj["payment_method"] === PAYMENT_METHOD_CASH) {
          setIsCashPayment(true);
          setCashAmount(obj["payment_amount"]);
        }
        if (
          obj["payment_method"] === PAYMENT_METHOD_BANKTRANSFER &&
          obj["company_id"] === COMPANY_VNPAY_ID
        ) {
          setIsVnpayPayment(true);
        }

        temp.push({
          key: obj.id,
          no: index_temp++,
          ...obj,
        });
      });
      setIndexPayment(index_temp);
      setPaymentInfoTableDataSource(temp);
    }
  }, [arrPaymentReceipts]);

  useEffect(() => {
    if (arrReturnItem.items && Object.keys(arrReturnItem.items).length !== 0) {
      let temp = [];
      _map(Object.keys(arrReturnItem.items), (obj) => {
        temp.push({
          key: arrReturnItem.items[obj].sku,
          ...arrReturnItem.items[obj],
        });
      });
      setItemsDetailTableDataSource(temp);
    }
  }, [arrReturnItem]);

  useEffect(() => {
    if (arrReturnItem["cdate"] > dayjs().unix() - 15 * 86400) {
      setIsAllowReturnProduct(true);
    }
  }, [arrReturnItem]);

  return (
    <Form.Provider onFormFinish={onReturnProductCreateFormFinish}>
      <section className="content-header">
        <h1>
          Return Product
          <small>Add new</small>
        </h1>
        <ol className="breadcrumb">
          <li>
            <a href="#">
              <Space>
                <FontAwesomeIcon icon={faShoppingCart} />
                Sale
              </Space>
            </a>
          </li>
          <li>Product Out Of store</li>
        </ol>
      </section>

      <section className="content">
        <Row>
          <Col xs={24}>
            <div className="box">
              <div className="box-body">
                <SearchForm initialValues={searchFormInitialValues} />
              </div>
            </div>
          </Col>
        </Row>

        {isSearchStatus && (
          <>
            <Row>
              <Col xs={24}>
                <div className="box">
                  <div className="box-header with-border">
                    <h3 className="box-title">
                      Search:{arrReturnItem["code"]}
                    </h3>
                  </div>
                </div>
              </Col>
            </Row>
            <Row>
              <Col xs={24}>
                <div className="box">
                  <div className="box-header with-border">
                    <h2 className="box-title">Order/Receipt information:</h2>
                  </div>
                  <div className="box-body">
                    <Row>
                      <Col xs={8}>
                        <h4>
                          Code:&nbsp;{arrReturnItem["code"]} &nbsp;
                          <Button
                            icon={<FontAwesomeIcon icon={faSearch} />}
                            onClick={handleClickViewBtn}
                          >
                            &nbsp;View
                          </Button>
                        </h4>
                      </Col>
                      <Col xs={8}>
                        <h4>
                          Date:{" "}
                          {dayjs(arrReturnItem["date"] * 1000).format(
                            "YYYY-MM-DD HH:mm"
                          )}{" "}
                        </h4>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs={12}>
                        <h4>
                          Customer: {arrCustomerInfo["customer_name"]}
                          {arrCustomerInfo["customer_phone"] && (
                            <>
                              &nbsp;-&nbsp;{arrCustomerInfo["customer_phone"]}
                            </>
                          )}
                        </h4>
                      </Col>
                    </Row>

                    {arrReturnItem["discount"] ? (
                      <Row>
                        <Col md={12}>
                          <h2 className="text-red">
                            *** Order/Receipt Discount:&nbsp;
                            {formatMoney(arrReturnItem["discount"])}
                          </h2>
                        </Col>
                      </Row>
                    ) : (
                      ""
                    )}

                    <Row>
                      <Col xs={12}>
                        <h5>
                          {arrReturnItem["cdate"] >
                          dayjs().unix() - 15 * 86400 ? (
                            <strong className="text-red">
                              Cho phép đổi trả hàng
                            </strong>
                          ) : (
                            <strong
                              className="text-red"
                              style={{ fontSize: 20 }}
                            >
                              Quá hạn đổi trả hàng
                            </strong>
                          )}
                        </h5>
                      </Col>
                    </Row>

                    {paymentInfoTableDataSource.length > 0 ? (
                      <Row>
                        <Col xs={8}>
                          <h4>Payment info</h4>
                          <PaymentInfoTable
                            dataSource={paymentInfoTableDataSource}
                            columns={paymentInfoTableColumns}
                          />
                        </Col>
                      </Row>
                    ) : (
                      ""
                    )}

                    {isAllowReturnProduct &&
                      (arrCustomerInfo["customer_id"] > 0 &&
                      arrCustomerInfo["customer_id"] ? (
                        isCashPayment ? (
                          <strong className="text-red">
                            Cho phép trả tiền mặt. Tối đa&nbsp;
                            {formatMoney(
                              cashAmount > totalCash
                                ? cashAmount - totalCash
                                : 0
                            )}
                            &#8363;. Nếu số tiền hoàn trả vượt quá số tiền tối
                            đa có thể trả thì tiền sẽ được trả vào balance cho
                            khách!
                          </strong>
                        ) : isVnpayPayment && indexPayment < 3 ? (
                          <strong className="text-red">
                            Không cho phép trả tiền mặt, add balance hoặc tiền
                            sẽ được hoàn trả bởi ngân hàng trong vòng 3-8 ngày
                            làm việc!
                          </strong>
                        ) : (
                          <strong className="text-red">
                            Không cho phép trả tiền mặt. Tiền sẽ được trả vào
                            balance của khách!
                          </strong>
                        )
                      ) : (
                        <strong className="text-red">
                          Không thể add balance cho khách vãng lai, cho phép trả
                          tiền
                        </strong>
                      ))}
                  </div>
                </div>
              </Col>
            </Row>
            {mess && (
              <p className="alert alert-warning alert-dismissible">{mess}</p>
            )}
            <Row>
              <Col xs={24}>
                <div className="box">
                  <div className="box-header with-border">
                    <h3 className="box-title">Items detail</h3>
                  </div>
                  <div className="box-body">
                    <ItemsDetailTable
                      columns={itemsDetailTableColumns}
                      dataSource={itemsDetailTableDataSource}
                    />
                  </div>
                </div>
              </Col>
            </Row>
          </>
        )}
      </section>
    </Form.Provider>
  );
}

export default ReturnProductCreate;
