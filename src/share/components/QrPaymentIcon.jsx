import React from 'react';
import { faQrcode } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function QrPaymentIcon() {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <div>
        <FontAwesomeIcon size="lg" className="text-red" icon={faQrcode} />
      </div>
      <div>
        <b className="text-red">QR PAY</b>
      </div>
    </div>
  )
}

export default QrPaymentIcon;