import React from 'react';
import { faMoneyBillAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function CashPaymentIcon() {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <div>
        <FontAwesomeIcon size="lg" className="text-orange" icon={faMoneyBillAlt} />
      </div>
      <div>
        <b className="text-orange">CASH</b>
      </div>
    </div>
  )
}

export default CashPaymentIcon;