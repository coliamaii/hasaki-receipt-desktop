import { Alert } from 'antd';
import React from 'react';
import { useSelector } from 'react-redux';

function CheckStore() {
  const { ip } = useSelector(state => state.auth.info.profile);

  return (
    <Alert
      className="bg-warning text-white"
      type="error"
      showIcon
      message={
        <b className="text-white">STORE INVALID</b>
      }
      banner
      description={`Your account does have permission to access this store. (${ip})`}
    />
  )
}

export default CheckStore;