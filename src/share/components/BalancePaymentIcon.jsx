import { faWallet } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

function BalancePaymentIcon() {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <div>
        <FontAwesomeIcon size="lg" className="text-warning" icon={faWallet} />
      </div>
      <div>
        <b className="text-warning">BALANCE</b>
      </div>
    </div>
  )
}

export default BalancePaymentIcon;