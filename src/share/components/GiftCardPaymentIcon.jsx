import { faGift } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

function GiftCardPaymentIcon() {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <div>
        <FontAwesomeIcon size="lg" className="text-green" icon={faGift} />
      </div>
      <div>
        <b className="text-green">GIFT CARD</b>
      </div>
    </div>
  )
}

export default GiftCardPaymentIcon;