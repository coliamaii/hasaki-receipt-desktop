const appButtonColor = {
  NEW: '#207398',
  RECONCILE: '#BF3325',
  GIFTS: '#02B290',
  COMBOS: '#F4BE2C',
  RECEIPTS: '#3C9667',
}

export default appButtonColor;