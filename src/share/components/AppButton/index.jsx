import { Badge, Button } from 'antd';
import React from 'react';
import PropTypes from 'prop-types';

import './app-button.scss';
import { useHotkeys } from 'react-hotkeys-hook';

AppButton.propTypes = {
  content: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  icon: PropTypes.any.isRequired,
  handleClick: PropTypes.func.isRequired,
  isDisabled: PropTypes.bool.isRequired,
  ariaLabel: PropTypes.string,
  hotKey: PropTypes.string,
  innerRef: PropTypes.object,
  counter: PropTypes.number,
}

function AppButton({
  content,
  color,
  icon,
  handleClick,
  isDisabled,
  ariaLabel,
  hotKey,
  innerRef,
  counter,
}) {
  useHotkeys(hotKey, () => {
    handleClick();
  }, {
    enableOnTags: ['INPUT', 'SELECT', 'TEXTAREA',],
  })

  return (
    <>
      <Button
        type="primary"
        className={`app-button ${isDisabled && 'app-button--disabled'} disabled`}
        style={{ backgroundColor: color }}
        block
        onClick={handleClick}
        ref={innerRef}
        disabled={isDisabled}
        aria-label={ariaLabel}
      >
        <div className="app-button__content">
          <span className="app-button__content__badge">
            <Badge count={counter} />
          </span>
          {icon}
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <span>{content}</span>
            {
              hotKey && <span>{`(${hotKey})`}</span>
            }
          </div>
        </div>
      </Button>
    </>
  )
}

export default AppButton;