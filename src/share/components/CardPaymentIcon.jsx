import React from 'react';
import { faCreditCard } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function CardPaymentIcon() {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <div>
        <FontAwesomeIcon size="lg" className="text-blue" icon={faCreditCard} />
      </div>
      <div>
        <b className="text-blue">CARD</b>
      </div>
    </div>
  )
}

export default CardPaymentIcon;