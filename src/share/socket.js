import SocketIO from "socket.io-client";

const ENDPOINT = "wss://test-partner.hasaki.vn";

export const socket = SocketIO(ENDPOINT, {
  reconnection: true,
  reconnectionDelayMax: 5000,
  reconnectionDelay: 1000,
  pingInterval: 1000,
  secure: true,
  forceNew: true,
  rejectUnauthorized: false,
});

export const startSocket = () => {
  socket.connect();
};

export const stopSocket = () => {
  socket.disconnect();
};

export const sendCustomerInfo = (client_id, customer) => {
  socket.emit(
    "pos-to-screen",
    JSON.stringify({
      action: "customer-update",
      data: {
        client_id,
        customer,
      },
    })
  );
};

export const createNewReceiptToScreen = (client_id) => {
  socket.emit(
    "pos-to-screen",
    JSON.stringify({
      action: "new-receipt",
      data: {
        client_id,
      },
    })
  );
};

export const updateReceiptToScreen = (client_id, items) => {
  socket.emit(
    "pos-to-screen",
    JSON.stringify({
      action: "update-receipt",
      data: {
        client_id,
        items,
      },
    })
  );
};

export const sendQrCodeToScreen = (qr_data, amount, client_id, source_code) => {
  socket.emit(
    "pos-to-screen",
    JSON.stringify({
      action: 'payment-qr-new',
      data: {
        client_id,
        qr_data,
        amount,
        source_code,
      }
    })
  )
}
