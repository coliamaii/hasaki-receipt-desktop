export const WEBSOCKET = {
  CONNECT_REQUEST: "WEBSOCKET_CONNECT_REQUEST",
  CONNECT_SUCCESS: "WEBSOCKET_CONNECT_SUCCESS",
  CONNECT_ERROR: "WEBSOCKET_CONNECT_ERROR",

  SEND_REQUEST: "WEBSOCKET_SEND_REQUEST",
  SEND_SUCCESS: "WEBSOCKET_SEND_SUCCESS",
  SEND_ERROR: "WEBSOCKET_SEND_ERROR",

  DISCONECT_REQUEST: "WEBSOCKET_DISCONECT_REQUEST",
  DISCONECT_SUCCESS: "WEBSOCKET_DISCONECT_SUCCESS",
  DISCONECT_ERROR: "WEBSOCKET_DISCONECT_ERROR",

  SET_SCREEN: "WEBSOCKET_SET_SCREEN",
  SET_POS_SERVER: "WEBSOCKET_SET_POS_SERVER",
};

export const webSocketSetPosServerAction = (pos_server) => {
  return {
    type: WEBSOCKET.SET_POS_SERVER,
    pos_server,
  };
};

export const webSocketSetScreenAction = (screen) => {
  return {
    type: WEBSOCKET.SET_SCREEN,
    screen,
  };
};

export const connectWebSocketRequestAction = () => {
  return {
    type: WEBSOCKET.CONNECT_REQUEST,
  };
};

export const connectWebSocketSuccessAction = (isConneted) => {
  return {
    type: WEBSOCKET.CONNECT_SUCCESS,
    isConneted,
  };
};

export const connectWebSocketErrorAction = () => {
  return {
    type: WEBSOCKET.CONNECT_ERROR,
  };
};

export const disconnectWebSocketRequestAction = () => {
  return {
    type: WEBSOCKET.DISCONECT_REQUEST,
  };
};

export const disconnectWebSocketSuccessAction = () => {
  return {
    type: WEBSOCKET.DISCONECT_SUCCESS,
  };
};

export const disconnectWebSocketErrorAction = () => {
  return {
    type: WEBSOCKET.DISCONECT_ERROR,
  };
};
