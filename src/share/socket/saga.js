import { all, put, takeLatest } from "redux-saga/effects";
import { WEBSOCKET } from "./action";
import SocketIO from "socket.io-client";

const ENDPOINT = "wss://test-partner.hasaki.vn";

function* WebSocketSetClientIdSaga() {

}

export default function* () {
  yield all([takeLatest(WEBSOCKET.SEND_REQUEST, WebSocketSetClientIdSaga)]);
}
