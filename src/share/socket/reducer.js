import { WEBSOCKET } from "./action";

const initialState = {
  isConneted: false,
  screen: {},
  pos_server: {},
};

function socketReducer(state = initialState, action) {
  switch (action.type) {
    case WEBSOCKET.SET_SCREEN: {
      const { screen } = action;
      return {
        ...state,
        screen,
      };
    }
    case WEBSOCKET.SET_POS_SERVER: {
      return {
        ...state,
        pos_server: action.pos_server,
      };
    }
    case WEBSOCKET.CONNECT_REQUEST: {
      return {
        ...state,
      };
    }
    case WEBSOCKET.CONNECT_SUCCESS: {
      const { isConneted } = action;
      return {
        ...state,
        isConneted,
      };
    }
    default: {
      return {
        ...state,
      };
    }
  }
}

export default socketReducer;
