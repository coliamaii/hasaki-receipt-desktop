import isReachable from "is-reachable"
import { all, call, put, takeLatest, delay } from "redux-saga/effects";
import { verify, verify_offline } from "scenes/Auth/actions";
import { searchCustomerSuccessAction } from "share/api/Customer/SearchCustomer/actions";
import { getProductByRuleOfflineRequestAction, getProductByRuleRefreshCodeAction, getProductByRuleRefreshExpiredItem, getProductByRuleRefreshGiftAction, getProductPricesByRuleRequestAction, getProductPricesByRuleSuccessAction } from "share/api/Product/GetProductsByRule/actions";
import { refreshPosReceiptAction, setPosReceiptLoadingAction } from "share/api/POSReceipt/action"
import { MODE, setIsOnlineModeAction, setPaymentStateModeAction } from "./action";
import { giftCodeRemoveAction, refreshGiftCodeAction } from "share/api/Product/Gift/action";
import { refreshPaymentSummaryAction } from "share/api/Payment/Summary/action";
import { refreshGiftCardAction } from "share/api/Payment/Type/GiftCard/action";
import { refreshCardPaymentAction } from "share/api/Payment/Type/Card/action";
import { qrPayRefreshAction } from "share/api/Payment/Type/QrPay/action";
import { refreshBalancePaymentAction } from "share/api/Payment/Type/Balance/action";
import { refreshVatAction } from "share/api/Payment/VAT/action";

const CHECKING_URL = 'google.com:443';

function* checkOnlineSaga() {
  const isOnline = yield isReachable(CHECKING_URL);
  if (isOnline) {
    yield put(setIsOnlineModeAction(true));
    yield put(verify());
  } else {
    yield put(setIsOnlineModeAction(false));
    yield put(verify_offline());
  }
}

function* fetchReceiptBaseOnTabSaga({ client_receipt_id, store_id, isOnline }) {
  yield put(setPosReceiptLoadingAction(true))
  try {
    const { receipt, receipt_details } = yield window.api.fetchAllReceiptInfoById(client_receipt_id);
    const formatData = receipt_details.map((item) => {
      return {
        sku: item.sku,
        qty: item.qty,
      };
    });
    const customer = JSON.parse(receipt.customer);
    const giftCode = JSON.parse(receipt.gift_code);
    const gift_items = JSON.parse(receipt.gift_items);
    const expired_items = JSON.parse(receipt.expired_items);
    const codes = JSON.parse(receipt.codes);
    const summary = JSON.parse(receipt.payment_summary);
    const gift_card = JSON.parse(receipt.gift_card_payment);
    const card = JSON.parse(receipt.card_payment);
    const qrpay = JSON.parse(receipt.qr_payment);
    const balance = JSON.parse(receipt.balance_payment)
    const vat = JSON.parse(receipt.vat);

    yield put(getProductByRuleRefreshExpiredItem(expired_items));

    yield put(refreshPosReceiptAction({
      client_receipt_id: client_receipt_id,
      remote_receipt_id: receipt.remote_receipt_id,
      remote_receipt_code: receipt.remote_receipt_code,
      receipt_udate: receipt.remote_udate,
      receipt_cdate: receipt.remote_cdate,
      isLoading: false,
      isComplete: receipt.status === 2,
    }));
    yield put(setPaymentStateModeAction(receipt.status === 1))

    yield put(searchCustomerSuccessAction(customer.customer, customer.isNewCustomer));

    if (formatData.length !== 0) {
      yield put(refreshGiftCodeAction(giftCode));
      const raw = {
        store_id,
        items: formatData,
        customer_phone: customer?.customer_phone ? customer.customer_phone : '',
        gift_code: giftCode.code,
      };

      if (isOnline) {
        yield put(getProductPricesByRuleRequestAction(raw, null, null));
      } else {
        yield put(getProductByRuleOfflineRequestAction(raw, null, null));
      }
    } else {
      yield put(getProductPricesByRuleSuccessAction([], []));
      yield put(giftCodeRemoveAction());
    }

    yield put(getProductByRuleRefreshGiftAction(gift_items));
    yield put(getProductByRuleRefreshCodeAction(codes));

    yield put(refreshPaymentSummaryAction(summary))
    yield put(refreshGiftCardAction(gift_card))
    yield put(refreshCardPaymentAction(card));
    yield put(qrPayRefreshAction(qrpay));
    yield put(refreshBalancePaymentAction(balance));
    yield put(refreshVatAction(vat));
  } catch (err) {
    console.log(err);
    //yield put(setPosReceiptLoadingAction(false))
  }
}

export default function* () {
  yield all([
    takeLatest(MODE.CHECK_ONLINE, checkOnlineSaga),
    takeLatest(MODE.GET_RECEIPT_STATE, fetchReceiptBaseOnTabSaga),
  ])
}