const { MODE } = require("./action");

const initialState = {
  isStarted: true,//if true, auto create new recepipt
  isOnline: true,
  topBotAreaState: null,
  paymentState: false,
  updateStatus: null,
  listReceiptTab: [],
}

function modeReducer(state = initialState, action) {
  switch (action.type) {
    case MODE.SET_IS_ONLINE: {
      const { isOnline } = action;
      return {
        ...state,
        isOnline,
      }
    }
    case MODE.SET_TOP_BOT_AREA_STATE: {
      const { areaState: topBotAreaState } = action;
      return {
        ...state,
        topBotAreaState,
      }
    }
    case MODE.SET_PAYMENT_STATE: {
      const { isPayment: paymentState } = action;
      return {
        ...state,
        paymentState,
      }
    }
    case MODE.SET_UPDATE_STATUS: {
      const { updateStatus } = action;
      return {
        ...state,
        updateStatus,
      }
    }
    case MODE.UPDATE_RECEIPT_TAB: {
      const { listReceiptTab } = action;
      return {
        ...state,
        listReceiptTab,
      }
    }
    case MODE.SET_IS_STARTED: {
      const { isStarted } = action;

      return {
        ...state,
        isStarted,
      }
    }
    default: {
      return {
        ...state,
      }
    }
  }
}

export default modeReducer;