export const MODE = {
  CHECK_ONLINE: 'CHECK_ONLINE_MODE',
  SET_IS_ONLINE: 'SET_IS_ONLINE_MODE',
  SET_TOP_BOT_AREA_STATE: 'SET_TOP_BOT_AREA_STATE_MODE',
  SET_PAYMENT_STATE: 'SET_PAYMENT_STATE_MODE',
  SET_UPDATE_STATUS: 'SET_UPDATE_STATUS_MODE',
  UPDATE_RECEIPT_TAB: 'UPDATE_RECEIPT_TAB_MODE',
  GET_RECEIPT_STATE: 'GET_RECEIPT_STATE_BASE_ON_TAB',
  SET_IS_STARTED: 'SET_IS_STARTED_MODE',
}

export const checkOnlineModeAction = () => {
  return {
    type: MODE.CHECK_ONLINE,
  }
}

export const setIsOnlineModeAction = (isOnline) => {
  return {
    type: MODE.SET_IS_ONLINE,
    isOnline,
  }
}

export const setTopBotAreaStateModeAction = (areaState) => {
  return {
    type: MODE.SET_TOP_BOT_AREA_STATE,
    areaState,
  }
}

export const setPaymentStateModeAction = (isPayment) => {
  return {
    type: MODE.SET_PAYMENT_STATE,
    isPayment,
  }
}

export const setUpdateStatusAction = (updateStatus) => {
  return {
    type: MODE.SET_UPDATE_STATUS,
    updateStatus,
  }
}

export const updateReceiptTabAction = (listReceiptTab) => {
  return {
    type: MODE.UPDATE_RECEIPT_TAB,
    listReceiptTab,
  }
}

export const getReceiptStateBaseOnTabAction = (client_receipt_id, store_id, isOnline) => {
  return {
    type: MODE.GET_RECEIPT_STATE,
    client_receipt_id,
    store_id,
    isOnline,
  }
}

export const setIsStartedAction = (isStarted) => {
  return {
    type: MODE.SET_IS_STARTED,
    isStarted,
  }
}