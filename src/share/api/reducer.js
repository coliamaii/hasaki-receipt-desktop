import { combineReducers } from "redux";
import accountingReducer from "./Accounting/reducer";
import customerReducer from "./Customer/reducer";
import lockScreenReducer from "./LockScreen/reducer";
import paymentReducer from "./Payment/reducer";
import posReceiptReducer from "./POSReceipt/reducer";
import productReducer from "./Product/reducer";
import profileReducer from "./Profile/reducer";
import returnProductReducer from "./ReturnProduct/reducer";
import settingReducer from "./Settings/reducer";
import supportRequestReducer from "./SupportRequest/reducer";

const reducer = combineReducers({
  accounting: accountingReducer,
  posReceipt: posReceiptReducer,
  product: productReducer,
  customer: customerReducer,
  profile: profileReducer,
  supportRequest: supportRequestReducer,
  setting: settingReducer,
  payment: paymentReducer,
  returnProduct: returnProductReducer,
  lockScreen: lockScreenReducer,
});

export default reducer;
