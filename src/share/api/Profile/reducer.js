import { combineReducers } from "redux";
import consultantReducer from "./Consultant/reducer";
import notificationReducer from "./Notification/reducer";
import paymentReceiptReducer from "./Payment-Receipt/reducer";
import receiptReducer from "./Receipt/reducer";
import reconcileReducer from "./Reconcile/reducer";
import timesheetReducer from "./TimeSheet/reducer";

const accountingReducer = combineReducers({
  reconcile: reconcileReducer,
  receipt: receiptReducer,
  timesheet: timesheetReducer,
  consultant: consultantReducer,
  paymentReceipt: paymentReceiptReducer,
  notification: notificationReducer,
});

export default accountingReducer;
