import { all } from "redux-saga/effects";
import consultantSaga from "./Consultant/saga";
import notificationSaga from "./Notification/GetList/saga";
import paymentReceiptSaga from "./Payment-Receipt/saga";
import receiptSaga from "./Receipt/saga";
import reconcileSaga from "./Reconcile/saga";
import timesheetSaga from "./TimeSheet/saga";

export default function* () {
  yield all([
    reconcileSaga(),
    receiptSaga(),
    timesheetSaga(),
    paymentReceiptSaga(),
    consultantSaga(),
    notificationSaga(),
  ]);
}
