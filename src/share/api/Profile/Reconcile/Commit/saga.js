import { message } from "antd";
import { getList as getBalanceTransList } from "apis/accounting/balance-transaction";
import { commit as commitReconcile } from "apis/accounting/reconcile";
import { getList as getSupReqList } from "apis/hr/support-request";
import { getDetail as getUserDetail } from "apis/setting/user";
import moment from "moment";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  commitReconcileErrorAction,
  commitReconcileSuccessAction,
  COMMIT_RECONCILE,
} from "./actions";

const key = "commit_reconcile";

function* commitReconcileSaga(action) {
  message.loading({ content: "Commiting reconcile...", key });

  try {
    const responseCommit = yield call(commitReconcile, action.id, action.data);

    if (responseCommit.status) {
      console.log("responseCommit");
      console.log(responseCommit);
      let detail = {};
      let reconcile_user = {};
      let sup_req_tbl = [];
      let balance_trans_tbl = [];

      if (responseCommit.data.reconcile) {
        const {
          reconcile_user_id,
          properties,
          reconcile_start_time,
          reconcile_code,
        } = responseCommit.data.reconcile;

        const { voucher, esteem_gift } = properties
          ? JSON.parse(properties)
          : {
              voucher: {
                five_hundreds: 0,
                two_hundreds: 0,
                one_hundreds: 0,
                fifties: 0,
                twenties: 0,
                tens: 0,
                ones: 0,

                total_count: 0,
              },
              esteem_gift: {
                five_hundreds: 0,
                two_hundreds: 0,
                one_hundreds: 0,
                fifties: 0,
                twenties: 0,
                tens: 0,
                ones: 0,

                total_count: 0,
              },
            };
        // get user name of receipt
        const responseUserDetail = yield call(getUserDetail, reconcile_user_id);
        if (responseUserDetail.status) {
          const { id, name } = responseUserDetail.data.user;
          reconcile_user = {
            id,
            name,
          };
        }
        detail = {
          ...responseCommit.data.reconcile,
          reconcile_user,
          properties: { voucher, esteem_gift },
        };

        // get list suppport request
        let from_create = (reconcile_start_time
          ? moment(reconcile_start_time * 1000)
          : moment()
        ).format("YYYY-MM-DD");

        const getSupReqList_params = {
          from_create,
          to_create: from_create,
          user_id: reconcile_user_id,
        };

        const responseSupReq = yield call(getSupReqList, getSupReqList_params);
        if (responseSupReq.status) {
          let check = null;
          sup_req_tbl = responseSupReq.data.rows.filter((data) => {
            if (data.supreq_content !== check) {
              check = data.supreq_content;
              return true;
            }
            return false;
          });
        }
        // get Balance Transaction List
        const getBalanceTransList_params = {
          ref_code: reconcile_code,
          data_accounts: 1,
        };

        const responsegBalanceTrans = yield call(
          getBalanceTransList,
          getBalanceTransList_params
        );

        if (responsegBalanceTrans.status) {
          balance_trans_tbl = [...responsegBalanceTrans.data.rows];
        }

        yield put(
          commitReconcileSuccessAction({
            detail,
            sup_req_tbl,
            balance_trans_tbl,
          })
        );
        message.success({ content: "Commit reconcile done!", key });
      } else {
        yield put(commitReconcileErrorAction());
        message.error({ content: "Commit reconcile Error", key });
      }
    } else {
      yield put(commitReconcileErrorAction());
      message.error({ content: "Commit reconcile Error", key });
    }
  } catch (err) {
    yield put(commitReconcileErrorAction());
    message.error({ content: "Commit reconcile Error", key });
  }
}

function* commitReconcileWatcher() {
  yield takeLatest(COMMIT_RECONCILE.REQUEST, commitReconcileSaga);
}
export default commitReconcileWatcher;
