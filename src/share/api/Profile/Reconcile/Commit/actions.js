export const COMMIT_RECONCILE = {
  REQUEST: "API_COMMIT_RECONCILE_REQUEST",
  SUCCESS: "API_COMMIT_RECONCILE_SUCCESS",
  ERROR: "API_COMMIT_RECONCILE_ERROR",
};

export function commitReconcileRequestAction(id, data) {
  return {
    type: COMMIT_RECONCILE.REQUEST,
    id,
    data,
  };
}

export function commitReconcileSuccessAction(data) {
  return {
    type: COMMIT_RECONCILE.SUCCESS,
    data,
  };
}

export function commitReconcileErrorAction() {
  return {
    type: COMMIT_RECONCILE.ERROR,
  };
}
