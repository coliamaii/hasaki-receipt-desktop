export const INSERT_RECONCILE = {
  REQUEST: "API_INSERT_RECONCILE_REQUEST",
  SUCCESS: "API_INSERT_RECONCILE_SUCCESS",
  ERROR: "API_INSERT_RECONCILE_ERROR",
};

export function insertReconcileRequestAction(data) {
  return {
    type: INSERT_RECONCILE.REQUEST,
    data,
  };
}

export function insertReconcileSuccessAction(data) {
  return {
    type: INSERT_RECONCILE.SUCCESS,
    data,
  };
}

export function insertReconcileErrorAction() {
  return {
    type: INSERT_RECONCILE.ERROR,
  };
}
