import { getList as getBalanceTransList } from "apis/accounting/balance-transaction";
import { getDetail as getDetailReconcile } from "apis/accounting/reconcile";
import { getList as getSupReqList } from "apis/hr/support-request";
import { getDetail as getUserDetail } from "apis/setting/user";
import { getList as getListAcount } from "apis/accounting/account";
import moment from "moment";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  getDetailReconcileErrorAction,
  getDetailReconcileSuccessAction,
  GET_DETAIL_RECONCILE,
} from "./actions";

function* getDetailReconcileSaga(action) {
  try {
    const responseGetDetail = yield call(getDetailReconcile, action.id);

    if (responseGetDetail.status) {
      let detail = {};
      let reconcile_user = {};
      let sup_req_tbl = [];
      let balance_trans_tbl = [];
      let accountList = [];

      const {
        reconcile_user_id,
        properties,
        reconcile_start_time,
        reconcile_code,
      } = responseGetDetail.data.reconcile;

      const { voucher, esteem_gift } = properties
        ? JSON.parse(properties)
        : {
            voucher: {
              five_hundreds: 0,
              two_hundreds: 0,
              one_hundreds: 0,
              fifties: 0,
              twenties: 0,
              tens: 0,
              ones: 0,

              total_count: 0,
            },
            esteem_gift: {
              five_hundreds: 0,
              two_hundreds: 0,
              one_hundreds: 0,
              fifties: 0,
              twenties: 0,
              tens: 0,
              ones: 0,

              total_count: 0,
            },
          };
      // get user name of receipt
      const responseUserDetail = yield call(getUserDetail, reconcile_user_id);
      if (responseUserDetail.status) {
        const { id, name } = responseUserDetail.data.user;
        reconcile_user = {
          id,
          name,
        };
      }
      detail = {
        ...responseGetDetail.data.reconcile,
        reconcile_user,
        properties: { voucher, esteem_gift },
      };
      // get list suppport request
      let from_create = (reconcile_start_time
        ? moment(reconcile_start_time * 1000)
        : moment()
      ).format("YYYY-MM-DD");

      const getSupReqList_params = {
        from_create,
        to_create: from_create,
        user_id: reconcile_user_id,
      };

      const responseSupReq = yield call(getSupReqList, getSupReqList_params);
      if (responseSupReq.status) {
        let check = null;
        sup_req_tbl = responseSupReq.data.rows.filter((data) => {
          if (data.supreq_content !== check) {
            check = data.supreq_content;
            return true;
          }
          return false;
        });
      }
      // get Balance Transaction List
      const getBalanceTransList_params = {
        ref_code: reconcile_code,
        data_accounts: 1,
      };

      const responsegBalanceTrans = yield call(
        getBalanceTransList,
        getBalanceTransList_params
      );

      if (responsegBalanceTrans.status) {
        balance_trans_tbl = [...responsegBalanceTrans.data.rows];
      }

      // get Account List
      const accountListParams = {
        location_id: 1,
        sort: "type",
        dir: "desc",
      };

      const responsegAccountList = yield call(getListAcount, accountListParams);

      if (responsegAccountList.status) {
        accountList = responsegAccountList.data.rows;
      }

      yield put(
        getDetailReconcileSuccessAction({
          detail,
          sup_req_tbl,
          balance_trans_tbl,
          accountList,
        })
      );
    } else {
      yield put(getDetailReconcileErrorAction());
    }
  } catch (err) {
    yield put(getDetailReconcileErrorAction());
  }
}

function* getDetailReconcileWatcher() {
  yield takeLatest(GET_DETAIL_RECONCILE.REQUEST, getDetailReconcileSaga);
}
export default getDetailReconcileWatcher;
