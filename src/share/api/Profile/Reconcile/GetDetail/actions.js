export const GET_DETAIL_RECONCILE = {
  REQUEST: "API_GET_DETAIL_RECONCILE_REQUEST",
  SUCCESS: "API_GET_DETAIL_RECONCILE_SUCCESS",
  ERROR: "API_GET_DETAIL_RECONCILE_ERROR",
};

export function getDetailReconcileRequestAction(id) {
  return {
    type: GET_DETAIL_RECONCILE.REQUEST,
    id,
  };
}

export function getDetailReconcileSuccessAction(data) {
  return {
    type: GET_DETAIL_RECONCILE.SUCCESS,
    data,
  };
}

export function getDetailReconcileErrorAction(err) {
  return {
    type: GET_DETAIL_RECONCILE.ERROR,
    err,
  };
}
