import { all } from "redux-saga/effects";
import commitReconcileWatcher from "./Commit/saga";
import insertReconcileWatcher from "./Insert/saga";
import updateReconcileWatcher from "./Update/saga";
import getListReconcileWatcher from "./GetList/saga";
import getDetailReconcileWatcher from "./GetDetail/saga";

export default function* () {
  yield all([
    insertReconcileWatcher(),
    updateReconcileWatcher(),
    commitReconcileWatcher(),
    getListReconcileWatcher(),
    getDetailReconcileWatcher(),
  ]);
}
