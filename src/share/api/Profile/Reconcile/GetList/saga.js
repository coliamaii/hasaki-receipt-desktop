import { getList as getListReconcile } from "apis/report/reconcile";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  getListReconcileErrorAction,
  getListReconcileSuccessAction,
  GET_LIST_RECONCILE,
} from "./actions";

function* getListReconcileSaga(action) {
  try {
    const responseGetList = yield call(getListReconcile, action.params);

    if (responseGetList.status) {
      let list = [];

      list = responseGetList.data.rows.map((obj) => {
        return {
          key: obj.reconcile_id,
          ...obj,
        };
      });

      yield put(getListReconcileSuccessAction({ list }));
    } else {
      yield put(getListReconcileErrorAction());
    }
  } catch (err) {
    yield put(getListReconcileErrorAction());
  }
}

function* getListReconcileWatcher() {
  yield takeLatest(GET_LIST_RECONCILE.REQUEST, getListReconcileSaga);
}
export default getListReconcileWatcher;
