export const GET_LIST_RECONCILE = {
  REQUEST: "API_GET_LIST_RECONCILE_REQUEST",
  SUCCESS: "API_GET_LIST_RECONCILE_SUCCESS",
  ERROR: "API_GET_LIST_RECONCILE_ERROR",
};

export function getListReconcileRequestAction(params) {
  return {
    type: GET_LIST_RECONCILE.REQUEST,
    params,
  };
}

export function getListReconcileSuccessAction(data) {
  return {
    type: GET_LIST_RECONCILE.SUCCESS,
    data,
  };
}

export function getListReconcileErrorAction() {
  return {
    type: GET_LIST_RECONCILE.ERROR,
  };
}
