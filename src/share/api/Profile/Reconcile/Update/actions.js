export const UPDATE_RECONCILE = {
  REQUEST: "API_UPDATE_RECONCILE_REQUEST",
  SUCCESS: "API_UPDATE_RECONCILE_SUCCESS",
  ERROR: "API_UPDATE_RECONCILE_ERROR",
};

export function updateReconcileRequestAction(id, data) {
  return {
    type: UPDATE_RECONCILE.REQUEST,
    id,
    data,
  };
}

export function updateReconcileSuccessAction(data) {
  return {
    type: UPDATE_RECONCILE.SUCCESS,
    data,
  };
}

export function updateReconcileErrorAction() {
  return {
    type: UPDATE_RECONCILE.ERROR,
  };
}
