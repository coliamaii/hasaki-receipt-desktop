import { message } from "antd";
import { getList as getBalanceTransList } from "apis/accounting/balance-transaction";
import { update as updateReconcile } from "apis/accounting/reconcile";
import { getDetail as getUserDetail } from "apis/setting/user";
import { call, put, takeLatest } from "redux-saga/effects";
import filterNumber from "utils/filterNumber";
import {
  updateReconcileErrorAction,
  updateReconcileSuccessAction,
  UPDATE_RECONCILE,
} from "./actions";

const key = "update_reconcile";

function* updateReconcileSaga(action) {
  message.loading({ content: "Updating reconcile...", key });
  try {
    const formatBody = {
      reconcile_five_hundreds: filterNumber(
        action.data.reconcile_five_hundreds
      ),
      reconcile_two_hundreds: filterNumber(action.data.reconcile_two_hundreds),
      reconcile_one_hundreds: filterNumber(action.data.reconcile_one_hundreds),
      reconcile_fifties: filterNumber(action.data.reconcile_fifties),
      reconcile_twenties: filterNumber(action.data.reconcile_twenties),
      reconcile_tens: filterNumber(action.data.reconcile_tens),
      reconcile_ones: filterNumber(action.data.reconcile_ones),
      reconcile_note: action.data.reconcile_note,
      reconcile_total_card_checkout: filterNumber(
        action.data.reconcile_total_card_checkout
      ),
      properties: JSON.stringify({
        voucher: action.data.voucher,
        esteem_gift: action.data.esteem_gift,
      }),
    };

    const responseUpdateReconcile = yield call(
      updateReconcile,
      action.id,
      formatBody
    );
    if (responseUpdateReconcile.status) {
      let detail = {};
      let reconcile_user = {};
      let balance_trans_tbl = [];

      const {
        reconcile_user_id,
        properties,
        reconcile_start_time,
        reconcile_code,
      } = responseUpdateReconcile.data.reconcile;

      const { voucher, esteem_gift } = properties
        ? JSON.parse(properties)
        : {
            voucher: {
              five_hundreds: 0,
              two_hundreds: 0,
              one_hundreds: 0,
              fifties: 0,
              twenties: 0,
              tens: 0,
              ones: 0,

              total_count: 0,
            },
            esteem_gift: {
              five_hundreds: 0,
              two_hundreds: 0,
              one_hundreds: 0,
              fifties: 0,
              twenties: 0,
              tens: 0,
              ones: 0,

              total_count: 0,
            },
          };
      // get user name of receipt
      const responseUserDetail = yield call(getUserDetail, reconcile_user_id);
      if (responseUserDetail.status) {
        const { id, name } = responseUserDetail.data.user;
        reconcile_user = {
          id,
          name,
        };
      }
      detail = {
        ...responseUpdateReconcile.data.reconcile,
        reconcile_user,
        properties: { voucher, esteem_gift },
      };

      // get Balance Transaction List
      const getBalanceTransList_params = {
        ref_code: reconcile_code,
        data_accounts: 1,
      };

      const responsegBalanceTrans = yield call(
        getBalanceTransList,
        getBalanceTransList_params
      );

      if (responsegBalanceTrans.status) {
        balance_trans_tbl = [...responsegBalanceTrans.data.rows];
      }

      yield put(
        updateReconcileSuccessAction({
          detail,
          balance_trans_tbl,
        })
      );
      message.success({ content: "Update reconcile done!", key });
    } else {
      message.error({ content: "Update reconcile Error", key });
      yield put(updateReconcileErrorAction());
    }
  } catch (err) {
    yield put(updateReconcileErrorAction());
    message.error({ content: "Update reconcile Error", key });
  }
}

function* updateReconcileWatcher() {
  yield takeLatest(UPDATE_RECONCILE.REQUEST, updateReconcileSaga);
}
export default updateReconcileWatcher;
