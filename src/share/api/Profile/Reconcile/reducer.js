import { COMMIT_RECONCILE } from "./Commit/actions";
import { GET_DETAIL_RECONCILE } from "./GetDetail/actions";
import { INSERT_RECONCILE } from "./Insert/actions";
import { UPDATE_RECONCILE } from "./Update/actions";
import { GET_LIST_RECONCILE } from "./GetList/actions";

const initialState = {
  isLoading: false,
  isCommit: false,
  showViewHistory: false,

  detail: {},
  list: [],
  sup_req_tbl: [],
  balance_trans_tbl: [],
  accountList: [],

  err: "",
};

export function reconcileReducer(state = initialState, action) {
  switch (action.type) {
    case INSERT_RECONCILE.REQUEST: {
      localStorage.removeItem("reconcile");
      return {
        ...state,
        isLoading: true,
      };
    }
    case INSERT_RECONCILE.SUCCESS: {
      localStorage.setItem("reconcile", JSON.stringify(action.data));
      return {
        ...state,
        detail: action.data.detail,
        sup_req_tbl: action.data.sup_req_tbl,
        balance_trans_tbl: action.data.balance_trans_tbl,
        accountList: action.data.accountList,
        isLoading: false,
      };
    }
    case INSERT_RECONCILE.ERROR: {
      return {
        ...state,
        isLoading: false,
      };
    }

    case UPDATE_RECONCILE.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case UPDATE_RECONCILE.SUCCESS: {
      localStorage.setItem("reconcile", JSON.stringify(action.data));
      return {
        ...state,
        detail: action.data.detail,
        balance_trans_tbl: action.data.balance_trans_tbl,
        isLoading: false,
      };
    }

    case UPDATE_RECONCILE.ERROR: {
      return {
        ...state,
        isLoading: false,
      };
    }

    case COMMIT_RECONCILE.REQUEST: {
      return {
        ...state,
        isCommit: true,
      };
    }

    case COMMIT_RECONCILE.SUCCESS: {
      localStorage.setItem("reconcile", JSON.stringify(action.data));
      return {
        ...state,
        detail: action.data.detail,
        sup_req_tbl: action.data.sup_req_tbl,
        balance_trans_tbl: action.data.balance_trans_tbl,
        isCommit: false,
      };
    }

    case COMMIT_RECONCILE.ERROR: {
      return {
        ...state,
        isCommit: false,
      };
    }

    case GET_LIST_RECONCILE.REQUEST: {
      return {
        ...state,
        showViewHistory: true,
      };
    }

    case GET_LIST_RECONCILE.SUCCESS: {
      return {
        ...state,
        list: action.data.list,
      };
    }

    case GET_LIST_RECONCILE.ERROR: {
      return {
        ...state,
      };
    }

    case GET_DETAIL_RECONCILE.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case GET_DETAIL_RECONCILE.SUCCESS: {
      localStorage.setItem("reconcile", JSON.stringify(action.data));
      return {
        ...state,
        detail: action.data.detail,
        sup_req_tbl: action.data.sup_req_tbl,
        balance_trans_tbl: action.data.balance_trans_tbl,
        accountList: action.data.accountList,
        showViewHistory: false,
        isLoading: false,
      };
    }

    case GET_DETAIL_RECONCILE.ERROR: {
      return {
        ...state,
        isLoading: false,
        err: action.err,
      };
    }

    default: {
      return {
        ...state,
      };
    }
  }
}

export default reconcileReducer;
