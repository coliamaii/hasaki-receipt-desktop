import { getList } from "apis/report/payment-receipt";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  getListPaymentReceiptErrorAction,
  getListPaymentReceiptSuccessAction,
  GET_LIST_PAYMENT_RECEIPT,
} from "./actions";

function* getListPaymentReceiptSaga(action) {
  try {
    const responsePayRec = yield call(getList, action.params);

    if (responsePayRec.status) {
      let list = [];

      list = responsePayRec.data.rows.map((obj) => ({
        ...obj,
        key: obj.id,
      }));

      yield put(getListPaymentReceiptSuccessAction({ list }));
    } else {
      yield put(getListPaymentReceiptErrorAction());
    }
  } catch (err) {
    yield put(getListPaymentReceiptErrorAction());
  }
}

function* getListPaymentReceiptWatcher() {
  yield takeLatest(GET_LIST_PAYMENT_RECEIPT.REQUEST, getListPaymentReceiptSaga);
}
export default getListPaymentReceiptWatcher;
