export const GET_LIST_PAYMENT_RECEIPT = {
  REQUEST: "API_GET_LIST_PAYMENT_RECEIPT_REQUEST",
  SUCCESS: "API_GET_LIST_PAYMENT_RECEIPT_SUCCESS",
  ERROR: "API_GET_LIST_PAYMENT_RECEIPT_ERROR",
};

export function getListPaymentReceiptRequestAction(params) {
  return {
    type: GET_LIST_PAYMENT_RECEIPT.REQUEST,
    params,
  };
}

export function getListPaymentReceiptSuccessAction(data) {
  return {
    type: GET_LIST_PAYMENT_RECEIPT.SUCCESS,
    data,
  };
}

export function getListPaymentReceiptErrorAction() {
  return {
    type: GET_LIST_PAYMENT_RECEIPT.ERROR,
  };
}
