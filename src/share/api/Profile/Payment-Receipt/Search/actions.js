export const GET_SEARCH_RC_PAYMENT_RECEIPT = {
  REQUEST: "API_GET_SEARCH_RC_PAYMENT_RECEIPT_REQUEST",
  SUCCESS: "API_GET_SEARCH_RC_PAYMENT_RECEIPT_SUCCESS",
  ERROR: "API_GET_SEARCH_RC_PAYMENT_RECEIPT_ERROR",
};

export function getSearchRCPaymentReceiptRequestAction(params) {
  return {
    type: GET_SEARCH_RC_PAYMENT_RECEIPT.REQUEST,
    params
  };
}

export function getSearchRCPaymentReceiptSuccessAction(data) {
  return {
    type: GET_SEARCH_RC_PAYMENT_RECEIPT.SUCCESS,
    data,
  };
}

export function getSearchRCPaymentReceiptErrorAction() {
  return {
    type: GET_SEARCH_RC_PAYMENT_RECEIPT.ERROR,
  };
}
