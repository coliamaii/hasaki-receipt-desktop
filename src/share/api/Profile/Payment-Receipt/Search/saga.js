import { searchReferenceCode } from "apis/report/payment-receipt";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  getSearchRCPaymentReceiptErrorAction,
  getSearchRCPaymentReceiptSuccessAction,
  GET_SEARCH_RC_PAYMENT_RECEIPT,
} from "./actions";

function* getSearchRCPaymentReceiptSaga(action) {
  try {
    const { status, data } = yield call(searchReferenceCode, action.params);

    if (status === 1) {
      yield put(getSearchRCPaymentReceiptSuccessAction(data));
    } else {
      yield put(getSearchRCPaymentReceiptErrorAction());
    }
  } catch (err) {
    yield put(getSearchRCPaymentReceiptErrorAction());
  }
}

function* getSearchRCPaymentReceiptWatcher() {
  yield takeLatest(
    GET_SEARCH_RC_PAYMENT_RECEIPT.REQUEST,
    getSearchRCPaymentReceiptSaga
  );
}
export default getSearchRCPaymentReceiptWatcher;
