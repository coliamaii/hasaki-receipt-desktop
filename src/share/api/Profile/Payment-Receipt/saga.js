import { all } from "redux-saga/effects";
import getSearchRCPaymentReceiptWatcher from "./Search/saga";
import getListPaymentReceiptWatcher from "./GetList/saga"

export default function* () {
  yield all([
    getListPaymentReceiptWatcher(),
    getSearchRCPaymentReceiptWatcher(),
  ]);
}