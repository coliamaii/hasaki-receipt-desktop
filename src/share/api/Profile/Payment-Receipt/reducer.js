import { GET_SEARCH_RC_PAYMENT_RECEIPT } from "./Search/actions";
import { GET_LIST_PAYMENT_RECEIPT } from "./GetList/actions";

const initialState = {
  isLoading: false,
  list: [],
  payment_methods: {},
  sum_payment_amount: 0,
  all_status: {},
};

export function paymentReceiptReducer(state = initialState, action) {
  switch (action.type) {
    case GET_SEARCH_RC_PAYMENT_RECEIPT.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_SEARCH_RC_PAYMENT_RECEIPT.SUCCESS: {
      return {
        ...state,
        list: action.data.list,
        isLoading: false,
      };
    }

    case GET_LIST_PAYMENT_RECEIPT.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_LIST_PAYMENT_RECEIPT.SUCCESS: {
      return {
        ...state,
        isLoading: false,
        ...action.data,
      };
    }

    default: {
      return {
        ...state,
      };
    }
  }
}

export default paymentReceiptReducer;
