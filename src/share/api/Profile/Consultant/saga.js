import { all } from "redux-saga/effects";
import getListDailyConsultantWatcher from "./Daily/saga";
import getListDetailConsultantWatcher from "./Detail/saga";
import getSummaryConsultantWatcher from "./Summary/saga";

export default function* () {
  yield all([
    getListDailyConsultantWatcher(),
    getListDetailConsultantWatcher(),
    getSummaryConsultantWatcher(),
  ]);
}