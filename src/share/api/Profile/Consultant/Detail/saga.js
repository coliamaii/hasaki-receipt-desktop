import { getListDetail } from "apis/report/consultant";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  getListDetailConsultantErrorAction,
  getListDetailConsultantSuccessAction,
  GET_LIST_DETAIL_CONSULTANT,
} from "./actions";

function* getListDetailConsultantSaga(action) {
  try {
    const { status, data } = yield call(getListDetail, action.params);

    if (status === 1) {
      yield put(getListDetailConsultantSuccessAction(data));
    } else {
      yield put(getListDetailConsultantErrorAction());
    }
  } catch (err) {
    yield put(getListDetailConsultantErrorAction());
  }
}

function* getListDetailConsultantWatcher() {
  yield takeLatest(
    GET_LIST_DETAIL_CONSULTANT.REQUEST,
    getListDetailConsultantSaga
  );
}
export default getListDetailConsultantWatcher;
