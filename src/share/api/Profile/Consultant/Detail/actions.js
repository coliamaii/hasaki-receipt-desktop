export const GET_LIST_DETAIL_CONSULTANT = {
  REQUEST: "API_GET_LIST_DETAIL_CONSULTANT_REQUEST",
  SUCCESS: "API_GET_LIST_DETAIL_CONSULTANT_SUCCESS",
  ERROR: "API_GET_LIST_DETAIL_CONSULTANT_ERROR",
};

export function getListDetailConsultantRequestAction(params) {
  return {
    type: GET_LIST_DETAIL_CONSULTANT.REQUEST,
    params
  };
}

export function getListDetailConsultantSuccessAction(data) {
  return {
    type: GET_LIST_DETAIL_CONSULTANT.SUCCESS,
    data,
  };
}

export function getListDetailConsultantErrorAction() {
  return {
    type: GET_LIST_DETAIL_CONSULTANT.ERROR,
  };
}
