  import { GET_LIST_DAILY_CONSULTANT } from "./Daily/actions";
import { GET_LIST_DETAIL_CONSULTANT } from "./Detail/actions";
import { GET_SUMMARY_CONSULTANT } from "./Summary/actions";

const initialState = {
  isLoading: false,
  rows: [],
  stores: [],
  summary: "0",
};

export function consultantReducer(state = initialState, action) {
  switch (action.type) {
    case GET_LIST_DAILY_CONSULTANT.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case GET_LIST_DAILY_CONSULTANT.SUCCESS: {
      return {
        ...state,
        rows: action.data.rows,
        stores: action.data.stores,
        isLoading: false,
      };
    }

    case GET_LIST_DETAIL_CONSULTANT.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_LIST_DETAIL_CONSULTANT.SUCCESS: {
      return {
        ...state,
        ...action.data,
        isLoading: false,
      };
    }

    case GET_SUMMARY_CONSULTANT.REQUEST: {
      return {
        ...state,
      };
    }
    case GET_SUMMARY_CONSULTANT.SUCCESS: {
      return {
        ...state,
        summary: action.data.rows[0].total,
      };
    }

    default: {
      return {
        ...state,
      };
    }
  }
}

export default consultantReducer;
