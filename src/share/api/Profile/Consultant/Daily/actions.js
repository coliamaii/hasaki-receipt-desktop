export const GET_LIST_DAILY_CONSULTANT = {
  REQUEST: "API_GET_LIST_DAILY_CONSULTANT_REQUEST",
  SUCCESS: "API_GET_LIST_DAILY_CONSULTANT_SUCCESS",
  ERROR: "API_GET_LIST_DAILY_CONSULTANT_ERROR",
};

export function getListDailyConsultantRequestAction(params) {
  return {
    type: GET_LIST_DAILY_CONSULTANT.REQUEST,
    params
  };
}

export function getListDailyConsultantSuccessAction(data) {
  return {
    type: GET_LIST_DAILY_CONSULTANT.SUCCESS,
    data,
  };
}

export function getListDailyConsultantErrorAction() {
  return {
    type: GET_LIST_DAILY_CONSULTANT.ERROR,
  };
}
