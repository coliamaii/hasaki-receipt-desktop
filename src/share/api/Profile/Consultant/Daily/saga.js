import { getListDaily } from "apis/report/consultant";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  getListDailyConsultantErrorAction,
  getListDailyConsultantSuccessAction,
  GET_LIST_DAILY_CONSULTANT,
} from "./actions";

function* getListDailyConsultantSaga(action) {
  try {
    const { status, data } = yield call(getListDaily, action.params);

    if (status === 1) {
      yield put(getListDailyConsultantSuccessAction(data));
    } else {
      yield put(getListDailyConsultantErrorAction());
    }
  } catch (err) {
    yield put(getListDailyConsultantErrorAction());
  }
}

function* getListDailyConsultantWatcher() {
  yield takeLatest(
    GET_LIST_DAILY_CONSULTANT.REQUEST,
    getListDailyConsultantSaga
  );
}
export default getListDailyConsultantWatcher;
