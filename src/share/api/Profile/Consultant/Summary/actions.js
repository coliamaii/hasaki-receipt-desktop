export const GET_SUMMARY_CONSULTANT = {
  REQUEST: "API_GET_SUMMARY_CONSULTANT_REQUEST",
  SUCCESS: "API_GET_SUMMARY_CONSULTANT_SUCCESS",
  ERROR: "API_GET_SUMMARY_CONSULTANT_ERROR",
};

export function getSummaryConsultantRequestAction(params) {
  return {
    type: GET_SUMMARY_CONSULTANT.REQUEST,
    params
  };
}

export function getSummaryConsultantSuccessAction(data) {
  return {
    type: GET_SUMMARY_CONSULTANT.SUCCESS,
    data,
  };
}

export function getSummaryConsultantErrorAction() {
  return {
    type: GET_SUMMARY_CONSULTANT.ERROR,
  };
}
