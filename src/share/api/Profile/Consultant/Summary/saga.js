import { getSummary } from "apis/report/consultant";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  GET_SUMMARY_CONSULTANT,
  getSummaryConsultantErrorAction,
  getSummaryConsultantSuccessAction,
} from "./actions";

function* getSummaryConsultantSaga(action) {
  try {
    const { status, data } = yield call(getSummary, action.params);

    if (status === 1) {
      yield put(getSummaryConsultantSuccessAction(data));
    } else {
      yield put(getSummaryConsultantErrorAction());
    }
  } catch (err) {
    yield put(getSummaryConsultantErrorAction());
  }
}

function* getSummaryConsultantWatcher() {
  yield takeLatest(GET_SUMMARY_CONSULTANT.REQUEST, getSummaryConsultantSaga);
}
export default getSummaryConsultantWatcher;
