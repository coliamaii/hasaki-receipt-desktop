import { GET_LIST_TIMESHEET } from "./GetList/actions";

const initialState = {
  isLoading: false,
  list: [],
};

export function timesheetReducer(state = initialState, action) {
  switch (action.type) {
    case GET_LIST_TIMESHEET.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_LIST_TIMESHEET.SUCCESS: {
      console.log(action)
      return {
        ...state,
        list: action.data.list,
        isLoading: false,
      };
    }

    default: {
      return {
        ...state,
      };
    }
  }
}

export default timesheetReducer;
