import { all } from "redux-saga/effects";
import getListTimeSheetWatcher from "./GetList/saga"

export default function* () {
  yield all([
    getListTimeSheetWatcher(),
  ]);
}