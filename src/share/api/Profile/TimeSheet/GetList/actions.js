export const GET_LIST_TIMESHEET = {
  REQUEST: "API_GET_LIST_TIMESHEET_REQUEST",
  SUCCESS: "API_GET_LIST_TIMESHEET_SUCCESS",
  ERROR: "API_GET_LIST_TIMESHEET_ERROR",
};

export function getListTimeSheetRequestAction(params) {
  return {
    type: GET_LIST_TIMESHEET.REQUEST,
    params,
  };
}

export function getListTimeSheetSuccessAction(data) {
  return {
    type: GET_LIST_TIMESHEET.SUCCESS,
    data,
  };
}

export function getListTimeSheetErrorAction() {
  return {
    type: GET_LIST_TIMESHEET.ERROR,
  };
}
