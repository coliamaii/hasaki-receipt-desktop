import { getList as getListTimesheet } from "apis/hr/timesheet";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  getListTimeSheetErrorAction,
  getListTimeSheetSuccessAction,
  GET_LIST_TIMESHEET,
} from "./actions";

function* getListTimeSheetSaga(action) {
  try {
    const responseTimesheet = yield call(getListTimesheet, action.params);

    if (responseTimesheet.status) {
      let list = [];

      list = responseTimesheet.data.rows.map((obj) => {
        return { key: obj.id, ...obj };
      });
      yield put(getListTimeSheetSuccessAction({ list }));
    } else {
      yield put(getListTimeSheetErrorAction());
    }
  } catch (err) {
    yield put(getListTimeSheetErrorAction());
  }
}

function* getListTimeSheetWatcher() {
  yield takeLatest(GET_LIST_TIMESHEET.REQUEST, getListTimeSheetSaga);
}
export default getListTimeSheetWatcher;
