export const GET_DETAIL_RECEIPT = {
  REQUEST: "API_GET_DETAIL_RECEIPT_REQUEST",
  SUCCESS: "API_GET_DETAIL_RECEIPT_SUCCESS",
  ERROR: "API_GET_DETAIL_RECEIPT_ERROR",

  OFFLINE_REQUEST: "API_GET_DETAIL_RECEIPT_OFFLINE_REQUEST",
};
export function getDetailOnlineReceiptRequestAction(params, historyPush) {

  return {
    type: GET_DETAIL_RECEIPT.REQUEST,
    params,
    historyPush,
  };
}

export function getDetailReceiptSuccessAction(data) {
  return {
    type: GET_DETAIL_RECEIPT.SUCCESS,
    data,
  };
}

export function getDetailReceiptErrorAction() {
  return {
    type: GET_DETAIL_RECEIPT.ERROR,
  };
}

export function getDetailOfflineReceiptRequestAction(params, historyPush) {
  return {
    type: GET_DETAIL_RECEIPT.OFFLINE_REQUEST,
    params,
    historyPush,
  };
}
