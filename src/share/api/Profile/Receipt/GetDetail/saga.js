import { getDetail as getDetailReceipt } from "apis/accounting/receipt";
import { getList as getListReturnProduct } from "apis/sales/return";
import _map from "lodash/map";
import { call, put, select, takeLatest } from "redux-saga/effects";
import {
  searchCustomerCleanAction,
  searchCustomerRequestAction,
} from "share/api/Customer/SearchCustomer/actions";
import { getProductPricesByRuleSuccessAction } from "share/api/Product/GetProductsByRule/actions";
import {
  getDetailReceiptErrorAction,
  getDetailReceiptSuccessAction,
  GET_DETAIL_RECEIPT,
} from "./actions";
import { getReceiptStateBaseOnTabAction} from 'share/mode/action'

export const getStoreId = (state) => state.auth.info.profile.store_id;

function* getDetailReceiptInOnlineSaga(action) {
  try {
    const responseReceipt = yield call(getDetailReceipt, action.params);
    console.log("responseReceipt");
    console.log(responseReceipt);

    if (responseReceipt.status) {
      let detail = {};
      let arrReturn = [];

      const {
        arrPaymentReceipts,
        products,
        receipt,
        user,
        customer,
        services,
      } = responseReceipt.data;

      // yield put(
      //   createNewLocalReceiptSuccessAction({
      //     receipt_id: receipt["receipt_id"],
      //   })
      // );

      if (receipt["receipt_status"] === 2) {
        // get list return product
        const responseReturnProduct = yield call(getListReturnProduct, {
          source_id: responseReceipt.data.receipt.receipt_code,
        });

        if (responseReturnProduct.status) {
          arrReturn = _map(responseReturnProduct.data.arrReturn, (obj) => ({
            key: obj.return_id,
            ...obj,
          }));
          arrReturn = arrReturn.sort((a, b) => {
            return a.return_ctime - b.return_ctime;
          });
        }

        let temp_receipt = receipt.items.map((obj) => ({
          key: obj.receiptdt_id,
          ...obj,
        }));

        temp_receipt = temp_receipt.sort((a, b) => {
          return JSON.parse(a.receiptdt_sku) - JSON.parse(b.receiptdt_sku);
        });

        detail = {
          ...responseReceipt.data,
          receipt: {
            ...receipt,
            items: temp_receipt,
          },
          arrReturn,
        };
        yield put(getDetailReceiptSuccessAction({ detail }));
        action.historyPush("/print/receipt");
      } else {
        detail = receipt;

        const formatData = receipt.items.map((item) => {
          const sku = item.receiptdt_sku;
          const name = products
            ? products.map(({ product_sku, product_name }) => {
                if (item.receiptdt_sku === product_sku) {
                  return product_name;
                }
              })
            : services.map(({ service_sku, service_name }) => {
                if (item.receiptdt_sku === service_sku) {
                  return service_name;
                }
              });
          const base_price = item.receiptdt_price;
          const discount = item.receiptdt_discount;
          const discount_price = base_price - discount;
          const qty = item.receiptdt_qty;
          const product_config = 0;
          const product_brand_id = 0;
          const lineTotal = qty * discount_price;

          return {
            sku,
            name,
            base_price,
            discount_price,
            discount,
            qty,
            product_config,
            lineTotal,
            product_brand_id,
          };
        });

        yield put(getProductPricesByRuleSuccessAction(formatData, []));

        yield put(getDetailReceiptSuccessAction({ detail }));

        if (customer) {
          const { customer_phone } = customer;
          yield put(searchCustomerRequestAction(customer_phone));
        } else {
          yield put(searchCustomerCleanAction());
        }

        yield put(getReceiptStateBaseOnTabAction(83, 6, true));

        action.historyPush("/");
      }
    } else {
      yield put(getDetailReceiptErrorAction());
    }
  } catch (err) {}
}

function* getDetailReceiptInOfflineSaga(action) {
  console.log("getDetailReceiptInOfflineSaga");
  console.log(action);
  try {
    const responseReceipt = yield call(
      window.api.getDetailReceipt,
      action.params
    );
    console.log("responseReceipt");
    console.log(responseReceipt);

    if (responseReceipt.length === 1) {
      const localReceipt = responseReceipt[0];
      console.log("localReceipt");
      console.log(localReceipt);
      let detail = {};
      let arrReturn = [];

      if (localReceipt["status"] === 2) {
        let receipt = {};
        let items = [];
        let products = [];
        let customer = {};
        let user = {};
        let arrPaymentReceipts = {};
        const storeId = yield select(getStoreId);

        responseReceipt.map((obj) => {
          items.push({
            key: obj.sku,
            receiptdt_sku: obj.sku,
            receiptdt_price: obj.base_price,
            receiptdt_discount: obj.discount_price,
            receiptdt_qty: obj.qty,
          });
          products.push({
            product_id: obj.product_sku,
            product_sku: obj.product_sku,
            product_barcode: obj.product_barcode,
            product_name: obj.product_name,
            product_price: obj.base_price,
          });
        });

        console.log("items");
        console.log(items);

        console.log("products");
        console.log(products);

        receipt = {
          receipt_id: localReceipt.id,
          receipt_code: localReceipt.id, //
          receipt_user_id: JSON.parse(localReceipt.user).user_id, //
          receipt_customer_id: JSON.parse(localReceipt.customer).customer
            ?.customer_id,
          receipt_cdate: localReceipt.local_created_at, //
          receipt_subtotal: JSON.parse(localReceipt.payment_summary)
            .subTotalPrice, //
          receipt_discount: JSON.parse(localReceipt.payment_summary).discount, //
          receipt_total: JSON.parse(localReceipt.payment_summary).totalPrices, //
          receipt_store_id: storeId, //
          items,
        };
        console.log("receipt");
        console.log(receipt);

        customer = {
          customer_id: JSON.parse(localReceipt.customer).customer.customer_id,
          customer_name: JSON.parse(localReceipt.customer).customer
            .customer_name, //
          customer_phone: JSON.parse(localReceipt.customer).customer
            .customer_phone, //
          customer_email: JSON.parse(localReceipt.customer).customer
            .customer_email, //
          customer_order_point: JSON.parse(localReceipt.customer).customer
            .customer_order_point, //
          customer_receipt_point: JSON.parse(localReceipt.customer).customer
            .customer_receipt_point, //
          customer_init_order_point: JSON.parse(localReceipt.customer).customer
            .customer_init_order_point, //
          customer_init_receipt_point: JSON.parse(localReceipt.customer)
            .customer.customer_init_receipt_point, //
          used_point: JSON.parse(localReceipt.customer).customer.used_point, //
        };

        console.log("customer");
        console.log(customer);

        user = {
          id: JSON.parse(localReceipt.user).user_id,
          name: JSON.parse(localReceipt.user).user_name,
        };

        console.log("user");
        console.log(user);

        detail = {
          receipt,
          products,
          customer,
          user,
          arrPaymentReceipts,
        };

        console.log("detail");
        console.log(detail);

        yield put(getDetailReceiptSuccessAction({ detail }));
        action.historyPush("/print/receipt");
      } else {
        // detail = receipt;
        // const formatData = receipt.items.map((item) => {
        //   const sku = item.receiptdt_sku;
        //   const name = products
        //     ? products.map(({ product_sku, product_name }) => {
        //         if (item.receiptdt_sku === product_sku) {
        //           return product_name;
        //         }
        //       })
        //     : services.map(({ service_sku, service_name }) => {
        //         if (item.receiptdt_sku === service_sku) {
        //           return service_name;
        //         }
        //       });
        //   const base_price = item.receiptdt_price;
        //   const discount = item.receiptdt_discount;
        //   const discount_price = base_price - discount;
        //   const qty = item.receiptdt_qty;
        //   const product_config = 0;
        //   const product_brand_id = 0;
        //   const lineTotal = qty * discount_price;
        //   return {
        //     sku,
        //     name,
        //     base_price,
        //     discount_price,
        //     discount,
        //     qty,
        //     product_config,
        //     lineTotal,
        //     product_brand_id,
        //   };
        // });
        // yield put(getProductPricesByRuleSuccessAction(formatData, []));
        // yield put(getDetailReceiptSuccessAction({ detail }));
        // if (customer) {
        //   const { customer_phone } = customer;
        //   yield put(searchCustomerRequestAction(customer_phone));
        // } else {
        //   yield put(searchCustomerCleanAction());
        // }
        // action.historyPush("/");
      }
      // const formatData = responseReceipt.map((item) => {
      //   const sku = item.sku;
      //   const name = item.sku;
      //   const base_price = 100000;
      //   const discount = 0;
      //   const discount_price = base_price - discount;
      //   const qty = item.qty;
      //   const product_config = 0;
      //   const product_brand_id = 0;
      //   const lineTotal = qty * discount_price;
      //   const customer = JSON.parse(item.customer);
      //   return {
      //     sku,
      //     name,
      //     base_price,
      //     discount_price,
      //     discount,
      //     qty,
      //     product_config,
      //     lineTotal,
      //     product_brand_id,
      //     customer,
      //   };
      // });
      // if (responseReceipt) {
      //   yield put(getProductPricesByRuleSuccessAction(formatData, []));
      //   yield put(createNewLocalReceiptSuccessAction({ receipt_id }));
      //   yield put(searchCustomerSuccessAction(formatData[0].customer, false));
      //   action.historyPush("/");
      // } else {
      // }
    }
  } catch (err) {}
}

function* getDetailReceiptWatcher() {
  yield takeLatest(GET_DETAIL_RECEIPT.REQUEST, getDetailReceiptInOnlineSaga);
  yield takeLatest(
    GET_DETAIL_RECEIPT.OFFLINE_REQUEST,
    getDetailReceiptInOfflineSaga
  );
}
export default getDetailReceiptWatcher;
