import { GET_LIST_RECEIPT } from "./GetList/actions";
import { CREATE_NEW_LOCAL_RECEIPT } from "./CreateNewLocal/actions";
import { GET_DETAIL_RECEIPT } from "./GetDetail/actions";
import { CREATE_EMPTY_RECEIPT } from "./Empty/actions";

const initialState = {
  local_receipt_id: null,
  detail: {},
  list: [],
  customers: [],
  isLoading: false,
};

export function receiptReducer(state = initialState, action) {
  switch (action.type) {
    case CREATE_EMPTY_RECEIPT.REQUEST: {
      return {
        ...state,
      };
    }

    case CREATE_EMPTY_RECEIPT.SUCCESS: {
      localStorage.removeItem("receipt_detail");
      return {
        ...state,
        detail: {},
      };
    }

    case GET_LIST_RECEIPT.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case GET_LIST_RECEIPT.OFFLINE_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case GET_DETAIL_RECEIPT.SUCCESS: {
      localStorage.setItem(
        "receipt_detail",
        JSON.stringify(action.data.detail)
      );
      return {
        ...state,
        detail: action.data.detail,
        isLoading: false,
      };
    }

    case GET_LIST_RECEIPT.SUCCESS: {
      return {
        ...state,
        list: action.data.list,
        customers: action.data.customers,
        isLoading: false,
      };
    }

    case GET_LIST_RECEIPT.ERROR: {
      return {
        ...state,
        isLoading: false,
      };
    }

    case CREATE_NEW_LOCAL_RECEIPT.REQUEST: {
      return {
        ...state,
      };
    }

    case CREATE_NEW_LOCAL_RECEIPT.SUCCESS: {
      const { receipt_id } = action.data;

      return {
        ...state,
        local_receipt_id: receipt_id,
      };
    }

    default: {
      return {
        ...state,
      };
    }
  }
}

export default receiptReducer;
