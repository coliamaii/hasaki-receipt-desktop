import { all } from "redux-saga/effects";
import createNewLocalReceiptWatcher from "./CreateNewLocal/saga";
import createEmptyReceiptWatcher from "./Empty/saga";
import getDetailReceiptWatcher from "./GetDetail/saga";
import getListReceiptWatcher from "./GetList/saga";

export default function* () {
  yield all([
    getListReceiptWatcher(),
    createNewLocalReceiptWatcher(),
    getDetailReceiptWatcher(),
    createEmptyReceiptWatcher(),
  ]);
}
