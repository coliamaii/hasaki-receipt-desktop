export const CREATE_EMPTY_RECEIPT = {
  REQUEST: "API_CREATE_EMPTY_RECEIPT_REQUEST",
  SUCCESS: "API_CREATE_EMPTY_RECEIPT_SUCCESS",
  ERROR: "API_CREATE_EMPTY_RECEIPT_ERROR",
};
export function createEmptyReceiptRequestAction(historyPush, dispatch) {
  return {
    type: CREATE_EMPTY_RECEIPT.REQUEST,
    historyPush,
    dispatch,
  };
}

export function createEmptyReceiptSuccessAction(data) {
  return {
    type: CREATE_EMPTY_RECEIPT.SUCCESS,
    data,
  };
}

export function createEmptyReceiptErrorAction() {
  return {
    type: CREATE_EMPTY_RECEIPT.ERROR,
  };
}
