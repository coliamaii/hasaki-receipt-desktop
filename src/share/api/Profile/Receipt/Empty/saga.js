import { create as createEmptyReceipt } from "apis/accounting/receipt";
import { call, put, select, takeLatest } from "redux-saga/effects";
import {
  searchCustomerCleanAction,
  searchCustomerSuccessAction,
} from "share/api/Customer/SearchCustomer/actions";
import { resetPaymentAction } from "share/api/Payment/action";
import {
  POS_RECEIPT,
  resetPosReceiptAction,
} from "share/api/POSReceipt/action";
import { getProductPricesByRuleClearAction } from "share/api/Product/GetProductsByRule/actions";
import { giftCodeRemoveAction } from "share/api/Product/Gift/action";
import {
  searchProductClearAction,
  searchProductClearGiftsAction,
} from "share/api/Product/SearchProduct/actions";
import { SEARCH_TYPE } from "share/api/Product/SearchProduct/search-type";
import {
  setPaymentStateModeAction,
  setTopBotAreaStateModeAction,
  updateReceiptTabAction,
} from "share/mode/action";
import { createNewReceiptToScreen } from "share/socket";
import {
  createEmptyReceiptErrorAction,
  createEmptyReceiptSuccessAction,
  CREATE_EMPTY_RECEIPT,
} from "./actions";

export const getLocalReceiptId = (state) => state.api.profile.receipt;
export const getIsOnline = (state) => state.mode;
export const getListReceiptTab = (state) => state.mode.listReceiptTab;
export const getScreen = (state) => state.websocket;

function* createEmptyReceiptSaga(action) {
  try {
    // const responseReceipt = yield call(createEmptyReceipt);

    // if (responseReceipt.status) {
    let detail = {};

    const localReceiptId = yield select(getLocalReceiptId);
    const isOnline = yield select(getIsOnline);
    const listReceiptTab = yield select(getListReceiptTab);
    const screen = yield select(getScreen);
    const localData = {
      receipt_id: localReceiptId,
    };
    const { id: client_id } = screen ? screen : { id: "" };

    yield put(searchCustomerCleanAction(localData, false));
    if (isOnline) {
      createNewReceiptToScreen(client_id);
    }
    yield put(giftCodeRemoveAction());
    yield put(getProductPricesByRuleClearAction());
    yield put(searchProductClearGiftsAction());
    yield put(setTopBotAreaStateModeAction(null));
    yield put(setPaymentStateModeAction(false));
    yield put(searchProductClearAction(SEARCH_TYPE.SEARCH_DATA));
    yield put(resetPosReceiptAction(POS_RECEIPT.RESET));
    resetPaymentAction(action.dispatch, localReceiptId);
    const cloneReceiptTabs = [...listReceiptTab];
    const itemIndex = cloneReceiptTabs.findIndex(
      (item) => item.client_receipt_id === localReceiptId
    );
    cloneReceiptTabs.splice(itemIndex, 1);
    yield put(updateReceiptTabAction(cloneReceiptTabs));

    yield put(createEmptyReceiptSuccessAction({ detail }));
    action.historyPush("/");
    // } else {
    //   yield put(createEmptyReceiptErrorAction());
    // }
  } catch (err) {
    yield put(createEmptyReceiptErrorAction());
  }
}

function* createEmptyReceiptWatcher() {
  yield takeLatest(CREATE_EMPTY_RECEIPT.REQUEST, createEmptyReceiptSaga);
}
export default createEmptyReceiptWatcher;
