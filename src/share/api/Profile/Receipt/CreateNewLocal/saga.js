import { all, call, put, takeLatest } from "redux-saga/effects";
import { newPOSReceiptAction, setPosReceiptLoadingAction } from "share/api/POSReceipt/action";
import { setIsStartedAction } from "share/mode/action";
import {
  createNewLocalReceiptErrorAction,
  createNewLocalReceiptSuccessAction,
  CREATE_NEW_LOCAL_RECEIPT,
} from "./actions";

function* createNewLocalReceiptSaga(action) {
  try {
    yield (put(setPosReceiptLoadingAction(true)));
    const receipt_id = yield call(window.api.createNewReceipt, action.data);

    yield put(
      createNewLocalReceiptSuccessAction({ receipt_id })
    );
    yield put(setIsStartedAction(false));
    yield (put(setPosReceiptLoadingAction(false)));
  } catch (err) {
    yield put(createNewLocalReceiptErrorAction());
  }
}

function* insertPOSReceiptSaga({ data: { receipt_id } }) {
  yield put(newPOSReceiptAction(receipt_id));
}

function* createNewLocalReceiptWatcher() {
  yield all([
    takeLatest(CREATE_NEW_LOCAL_RECEIPT.REQUEST, createNewLocalReceiptSaga),
    takeLatest(CREATE_NEW_LOCAL_RECEIPT.SUCCESS, insertPOSReceiptSaga),
  ])
}
export default createNewLocalReceiptWatcher;
