export const CREATE_NEW_LOCAL_RECEIPT = {
  REQUEST: "API_CREATE_NEW_LOCAL_RECEIPT_REQUEST",
  SUCCESS: "API_CREATE_NEW_LOCAL_RECEIPT_SUCCESS",
  ERROR: "API_CREATE_NEW_LOCAL_RECEIPT_ERROR",
};

export function createNewLocalReceiptRequestAction(data) {
  return {
    type: CREATE_NEW_LOCAL_RECEIPT.REQUEST,
    data,
  };
}

export function createNewLocalReceiptSuccessAction(data) {
  return {
    type: CREATE_NEW_LOCAL_RECEIPT.SUCCESS,
    data,
  };
}

export function createNewLocalReceiptErrorAction() {
  return {
    type: CREATE_NEW_LOCAL_RECEIPT.ERROR,
  };
}
