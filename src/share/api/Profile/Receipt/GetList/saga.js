import { getList as getListReceipt } from "apis/accounting/receipt";
import { getList as getListCustomer } from "apis/sales/customer";
import { call, put, select, takeLatest } from "redux-saga/effects";
import {
    getListReceiptErrorAction,
    getListReceiptSuccessAction,
    GET_LIST_RECEIPT,
} from "./actions";

export const getStoreId = (state) => state.auth.info.profile.store_id;

function* getListReceiptSaga(action) {
    try {
        const responseReceipt = yield call(getListReceipt, action.params);

        if (responseReceipt.status) {
            let list = [];
            let customers = [];
            let ids = new Set();

            list = responseReceipt.data.rows.map((obj) => {
                if (obj.receipt_customer_id > 0) {
                    ids.add(obj.receipt_customer_id);
                }

                return {
                    key: obj.receipt_id,
                    ...obj,
                };
            });

            ids = [...ids].toString().replaceAll("[]", "");
            const responseCustomer = yield call(getListCustomer, { ids });
            if (responseCustomer.status) {
                customers = responseCustomer.data.rows;
            }

            yield put(getListReceiptSuccessAction({ list, customers }));
        } else {
            yield put(getListReceiptErrorAction());
        }
    } catch (err) {
        yield put(getListReceiptErrorAction());
    }
}

function* getListReceiptInOfflineSaga(action) {
    console.log("getListReceiptInOfflineSaga");
    console.log(action);
    try {
        const responseReceipt = yield call(
            window.api.getListReceipt,
            action.params
        );
        console.log("receipts");
        console.log(responseReceipt);

        if (responseReceipt) {
            let list = [];
            let customers = [];
            let store_id = yield select(getStoreId);

            responseReceipt.map((obj) => {
                const payment_summary = JSON.parse(obj.payment_summary);
                if (payment_summary.items > 0) {
                    const customer = JSON.parse(obj.customer);

                    const user = JSON.parse(obj.user);

                    list.push({
                        key: obj.id,
                        receipt_id: obj.id,
                        receipt_code: obj.id,
                        // ? obj.remote_receipt_code
                        // : obj.id,
                        receipt_customer_id: customer.customer ?
                            customer.customer.customer_id :
                            0,
                        receipt_cdate: obj.local_created_at,
                        receipt_udate: obj.local_updated_at,
                        receipt_desc: obj.note,
                        receipt_total_item: payment_summary.items,
                        receipt_total: payment_summary.totalPrices,
                        receipt_store_id: store_id,
                        receipt_status: obj.status,
                        receipt_type: 2,
                        receipt_balance: JSON.parse(obj.balance_payment).current,
                        user: {
                            id: user.user_id,
                            name: user.user_name,
                        },
                    });
                    if (customer.customer) {
                        customers.push(customer.customer);
                    }
                }
            });

            console.log("list: ");
            console.log(list);
            console.log("customers");
            console.log(customers);

            yield put(getListReceiptSuccessAction({ list, customers }));
        } else {
            yield put(getListReceiptErrorAction());
        }
    } catch (err) {
        yield put(getListReceiptErrorAction());
    }
}

function* getListReceiptWatcher() {
    yield takeLatest(GET_LIST_RECEIPT.REQUEST, getListReceiptSaga);
    yield takeLatest(
        GET_LIST_RECEIPT.OFFLINE_REQUEST,
        getListReceiptInOfflineSaga
    );
}
export default getListReceiptWatcher;