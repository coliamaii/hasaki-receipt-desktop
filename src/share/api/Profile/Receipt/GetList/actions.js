export const GET_LIST_RECEIPT = {
  REQUEST: "API_GET_LIST_RECEIPT_REQUEST",
  SUCCESS: "API_GET_LIST_RECEIPT_SUCCESS",
  ERROR: "API_GET_LIST_RECEIPT_ERROR",

  OFFLINE_REQUEST: "API_GET_LIST_RECEIPT_OFFLINE_REQUEST",
};

export function getListReceiptRequestAction(params) {
  return {
    type: GET_LIST_RECEIPT.REQUEST,
    params,
  };
}

export function getListReceiptSuccessAction(data) {
  return {
    type: GET_LIST_RECEIPT.SUCCESS,
    data,
  };
}

export function getListReceiptErrorAction() {
  return {
    type: GET_LIST_RECEIPT.ERROR,
  };
}

export function getListOfflineReceiptRequestAction(params) {
  return {
    type: GET_LIST_RECEIPT.OFFLINE_REQUEST,
    params
  };
}
