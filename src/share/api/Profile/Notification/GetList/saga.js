import { getList as getListNotification } from "apis/setting/notification";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  getListNotificationErrorAction,
  getListNotificationSuccessAction,
  GET_LIST_NOTIFICATION,
} from "./actions";

function* getListNotificationSaga(action) {
  try {
    const responseNotification = yield call(getListNotification, action.params);

    if (responseNotification.status) {
      let list = [];
      
      list = responseNotification.data.rows;

      yield put(getListNotificationSuccessAction({ list }));
    } else {
      yield put(getListNotificationErrorAction());
    }
  } catch (err) {
    yield put(getListNotificationErrorAction());
  }
}

function* getListNotificationWatcher() {
  yield takeLatest(GET_LIST_NOTIFICATION.REQUEST, getListNotificationSaga);
}
export default getListNotificationWatcher;
