export const GET_LIST_NOTIFICATION = {
  REQUEST: "API_GET_LIST_NOTIFICATION_REQUEST",
  SUCCESS: "API_GET_LIST_NOTIFICATION_SUCCESS",
  ERROR: "API_GET_LIST_NOTIFICATION_ERROR",
};

export function getListNotificationRequestAction(params) {
  return {
    type: GET_LIST_NOTIFICATION.REQUEST,
    params,
  };
}

export function getListNotificationSuccessAction(data) {
  return {
    type: GET_LIST_NOTIFICATION.SUCCESS,
    data,
  };
}

export function getListNotificationErrorAction() {
  return {
    type: GET_LIST_NOTIFICATION.ERROR,
  };
}
