import { GET_LIST_NOTIFICATION } from "./GetList/actions";

const initialState = {
  isLoading: false,
  list: [],
};

export function notificationReducer(state = initialState, action) {
  switch (action.type) {
    case GET_LIST_NOTIFICATION.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_LIST_NOTIFICATION.SUCCESS: {
      return {
        ...state,
        list: action.data.list,
        isLoading: false,
      };
    }

    default: {
      return {
        ...state,
      };
    }
  }
}

export default notificationReducer;
