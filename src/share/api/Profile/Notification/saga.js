import { all } from "redux-saga/effects";
import getListNotificationWatcher from "./GetList/saga";

export default function* () {
  yield all([
    getListNotificationWatcher(),
  ]);
}
