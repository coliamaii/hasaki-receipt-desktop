import { CREATE_RETURN_PRODUCT } from "./Create/actions";
import { GET_DETAIL_RETURN_PRODUCT } from "./GetDetail/actions";
import { GET_LIST_RETURN_PRODUCT } from "./GetList/actions";
import { INSERT_RETURN_PRODUCT } from "./Insert/actions";

const initialState = {
  isLoading: false,
  isSearchStatus: false,
  detail: {},
  list: [],
  arrUsers: [],
  arrReturnItem: {},
  arrCustomerInfo: {},
  totalCash: 0,
  arrPaymentReceipts: {},
  mess: "",
  sourceStoreId: null,
};

export function returnProductReducer(state = initialState, action) {
  switch (action.type) {
    case GET_LIST_RETURN_PRODUCT.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_LIST_RETURN_PRODUCT.SUCCESS: {
      return {
        ...state,
        isLoading: false,
        list: action.data.list,
        arrUsers: action.data.arrUsers,
      };
    }
    case GET_LIST_RETURN_PRODUCT.ERROR: {
      return {
        ...state,
        isLoading: false,
      };
    }

    case GET_DETAIL_RETURN_PRODUCT.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_DETAIL_RETURN_PRODUCT.SUCCESS: {
      return {
        ...state,
        isLoading: false,
        detail: action.data.detail,
        mess: "",
      };
    }
    case GET_DETAIL_RETURN_PRODUCT.ERROR: {
      return {
        ...state,
        isLoading: false,
      };
    }

    case CREATE_RETURN_PRODUCT.REQUEST: {
      return {
        ...state,
        isLoading: true,
        mess: "",
      };
    }
    case CREATE_RETURN_PRODUCT.SUCCESS: {
      return {
        ...state,
        isLoading: false,
        isSearchStatus: true,
        arrReturnItem: action.data.arrReturnItem,
        arrCustomerInfo: action.data.arrCustomerInfo,
        totalCash: action.data.totalCash,
        arrPaymentReceipts: action.data.arrPaymentReceipts,
        sourceStoreId: action.data.sourceStoreId,
      };
    }
    case CREATE_RETURN_PRODUCT.ERROR: {
      return {
        ...state,
        isLoading: false,
      };
    }

    case INSERT_RETURN_PRODUCT.REQUEST: {
      return {
        ...state,
      };
    }
    case INSERT_RETURN_PRODUCT.SUCCESS: {
      console.log("success action")
      console.log(action)
      return {
        ...state,
        detail: action.data.detail,
      };
    }
    case INSERT_RETURN_PRODUCT.ERROR: {
      return {
        ...state,
        mess: action.error.mess,
      };
    }

    default: {
      return {
        ...state,
      };
    }
  }
}

export default returnProductReducer;
