export const INSERT_RETURN_PRODUCT = {
  REQUEST: "API_INSERT_RETURN_PRODUCT_REQUEST",
  SUCCESS: "API_INSERT_RETURN_PRODUCT_SUCCESS",
  ERROR: "API_INSERT_RETURN_PRODUCT_ERROR",
};

export function insertReturnProductRequestAction(params, callback) {
  return {
    type: INSERT_RETURN_PRODUCT.REQUEST,
    params,
    callback,
  };
}

export function insertReturnProductSuccessAction(data) {
  return {
    type: INSERT_RETURN_PRODUCT.SUCCESS,
    data,
  };
}

export function insertReturnProductErrorAction(error) {
  return {
    type: INSERT_RETURN_PRODUCT.ERROR,
    error,
  };
}

