import {
  insert as insertReturnProduct,
  getDetail as getDetailReturnProduct,
} from "apis/sales/return";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  insertReturnProductErrorAction,
  insertReturnProductSuccessAction,
  INSERT_RETURN_PRODUCT,
} from "./actions";

function* insertReturnProductSaga(action) {
  try {
    const responseReturnProduct = yield call(
      insertReturnProduct,
      action.params
    );
    console.log("responseReturnProduct insert");
    console.log(responseReturnProduct);

    if (responseReturnProduct.status) {
      let detail = {};

      const responseDetailReturnProduct = yield call(
        getDetailReturnProduct,
        responseReturnProduct.data
      );

      console.log("responseDetailReturnProduct");
      console.log(responseDetailReturnProduct);

      detail = responseDetailReturnProduct.data.arrReturn;

      console.log("detail")
      console.log(detail)
      yield put(insertReturnProductSuccessAction({ detail }));
      action.callback("/product/return/edit");
    } else {
      if (responseReturnProduct.code === 422) {
        yield put(
          insertReturnProductErrorAction({
            mess: responseReturnProduct.message,
          })
        );
      }
    }
  } catch (err) {
    yield put(
      insertReturnProductErrorAction({
        mess: err,
      })
    );
  }
}

function* insertReturnProductWatcher() {
  yield takeLatest(INSERT_RETURN_PRODUCT.REQUEST, insertReturnProductSaga);
}
export default insertReturnProductWatcher;
