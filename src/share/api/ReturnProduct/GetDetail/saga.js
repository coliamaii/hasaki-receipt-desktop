import { getDetail as getDetailReturnProduct } from "apis/sales/return";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  getDetailReturnProductErrorAction,
  getDetailReturnProductSuccessAction,
  GET_DETAIL_RETURN_PRODUCT,
} from "./actions";

function* getDetailReturnProductSaga(action) {
  try {
    const responseReturnProduct = yield call(getDetailReturnProduct, action.id);
    console.log("responseReturnProduct")
    console.log(responseReturnProduct)

    if (responseReturnProduct.status) {
      let detail = {};

      detail = responseReturnProduct.data.arrReturn;

      yield put(getDetailReturnProductSuccessAction({ detail }));
      action.callback("/product/return/edit")
    } else {
      yield put(getDetailReturnProductErrorAction());
    }
  } catch (err) {
    yield put(getDetailReturnProductErrorAction());
  }
}

function* getDetailReturnProductWatcher() {
  yield takeLatest(
    GET_DETAIL_RETURN_PRODUCT.REQUEST,
    getDetailReturnProductSaga
  );
}
export default getDetailReturnProductWatcher;
