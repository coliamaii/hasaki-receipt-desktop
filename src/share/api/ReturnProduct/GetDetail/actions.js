export const GET_DETAIL_RETURN_PRODUCT = {
  REQUEST: "API_GET_DETAIL_RETURN_PRODUCT_REQUEST",
  SUCCESS: "API_GET_DETAIL_RETURN_PRODUCT_SUCCESS",
  ERROR: "API_GET_DETAIL_RETURN_PRODUCT_ERROR",
};

export function getDetailReturnProductRequestAction(id, callback) {
  return {
    type: GET_DETAIL_RETURN_PRODUCT.REQUEST,
    id,
    callback,
  };
}

export function getDetailReturnProductSuccessAction(data) {
  return {
    type: GET_DETAIL_RETURN_PRODUCT.SUCCESS,
    data,
  };
}

export function getDetailReturnProductErrorAction() {
  return {
    type: GET_DETAIL_RETURN_PRODUCT.ERROR,
  };
}
