import { all } from "redux-saga/effects";
import getListReturnProductWatcher from "./GetList/saga";
import getDetailReturnProductWatcher from "./GetDetail/saga";
import createReturnProductWatcher from "./Create/saga";
import insertReturnProductWatcher from "./Insert/saga";

export default function* () {
  yield all([
    getListReturnProductWatcher(),
    getDetailReturnProductWatcher(),
    createReturnProductWatcher(),
    insertReturnProductWatcher(),
  ]);
}
