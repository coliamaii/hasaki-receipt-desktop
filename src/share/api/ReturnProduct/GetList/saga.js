import { getList as getListReturnProduct } from "apis/sales/return";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  getListReturnProductErrorAction,
  getListReturnProductSuccessAction,
  GET_LIST_RETURN_PRODUCT,
} from "./actions";

function* getListReturnProductSaga(action) {
  try {
    const responseReturnProduct = yield call(
      getListReturnProduct,
      action.params
    );

    if (responseReturnProduct.status) {
      let list = [];
      let arrUsers = [];

      list = responseReturnProduct.data.arrReturn.map((obj) => {
        return {
          key: obj.return_id,
          ...obj,
          product: responseReturnProduct.data.arrProducts[obj.return_item_sku],
        };
      });

      arrUsers = responseReturnProduct.data.arrUsers;

      yield put(getListReturnProductSuccessAction({ list, arrUsers }));
    } else {
      yield put(getListReturnProductErrorAction());
    }
  } catch (err) {
    yield put(getListReturnProductErrorAction());
  }
}

function* getListReturnProductWatcher() {
  yield takeLatest(GET_LIST_RETURN_PRODUCT.REQUEST, getListReturnProductSaga);
}
export default getListReturnProductWatcher;
