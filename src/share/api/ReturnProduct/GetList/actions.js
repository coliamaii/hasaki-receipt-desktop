export const GET_LIST_RETURN_PRODUCT = {
  REQUEST: "API_GET_LIST_RETURN_PRODUCT_REQUEST",
  SUCCESS: "API_GET_LIST_RETURN_PRODUCT_SUCCESS",
  ERROR: "API_GET_LIST_RETURN_PRODUCT_ERROR",
};

export function getListReturnProductRequestAction(params) {
  return {
    type: GET_LIST_RETURN_PRODUCT.REQUEST,
    params,
  };
}

export function getListReturnProductSuccessAction(data) {
  return {
    type: GET_LIST_RETURN_PRODUCT.SUCCESS,
    data,
  };
}

export function getListReturnProductErrorAction() {
  return {
    type: GET_LIST_RETURN_PRODUCT.ERROR,
  };
}
