export const CREATE_RETURN_PRODUCT = {
  REQUEST: "API_CREATE_RETURN_PRODUCT_REQUEST",
  SUCCESS: "API_CREATE_RETURN_PRODUCT_SUCCESS",
  ERROR: "API_CREATE_RETURN_PRODUCT_ERROR",
};

export function createReturnProductRequestAction(params) {
  return {
    type: CREATE_RETURN_PRODUCT.REQUEST,
    params,
  };
}

export function createReturnProductSuccessAction(data) {
  return {
    type: CREATE_RETURN_PRODUCT.SUCCESS,
    data,
  };
}

export function createReturnProductErrorAction() {
  return {
    type: CREATE_RETURN_PRODUCT.ERROR,
  };
}
