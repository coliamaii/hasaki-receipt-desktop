import { create as createReturnProduct } from "apis/sales/return";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  createReturnProductErrorAction,
  createReturnProductSuccessAction,
  CREATE_RETURN_PRODUCT,
} from "./actions";

function* createReturnProductSaga(action) {
  try {
    const responseReturnProduct = yield call(
      createReturnProduct,
      action.params
    );

    if (responseReturnProduct.status) {
      let arrReturnItem = {};
      let arrCustomerInfo = {};
      let totalCash = 0;
      let arrPaymentReceipts = {};
      let sourceStoreId = null;

      arrReturnItem = responseReturnProduct.data.arrReturnItem;
      arrCustomerInfo = responseReturnProduct.data.arrCustomerInfo
        ? responseReturnProduct.data.arrCustomerInfo
        : {
            customer_name: "Guest",
            customer_phone: "",
          };
      totalCash = responseReturnProduct.data.totalCash;
      arrPaymentReceipts = responseReturnProduct.data.arrPaymentReceipts;
      sourceStoreId = responseReturnProduct.data.sourceStoreId;

      yield put(
        createReturnProductSuccessAction({
          arrReturnItem,
          arrCustomerInfo,
          totalCash,
          arrPaymentReceipts,
          sourceStoreId,
        })
      );
    } else {
      yield put(createReturnProductErrorAction());
    }
  } catch (err) {
    yield put(createReturnProductErrorAction());
  }
}

function* createReturnProductWatcher() {
  yield takeLatest(CREATE_RETURN_PRODUCT.REQUEST, createReturnProductSaga);
}
export default createReturnProductWatcher;
