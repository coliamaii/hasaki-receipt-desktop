import { all } from "redux-saga/effects";
import accountingSaga from "./Accounting/saga";
import customerSaga from "./Customer/saga";
import lockScreenSaga from "./LockScreen/saga";
import paymentSaga from "./Payment/saga";
import posReceiptSaga from "./POSReceipt/saga";
import productSaga from "./Product/saga";
import profileSaga from "./Profile/saga";
import returnProductSaga from "./ReturnProduct/saga";
import settingSaga from "./Settings/saga";
import supportRequestSaga from "./SupportRequest/saga";

export default function* () {
  yield all([
    accountingSaga(),
    productSaga(),
    customerSaga(),
    profileSaga(),
    supportRequestSaga(),
    settingSaga(),
    paymentSaga(),
    returnProductSaga(),
    posReceiptSaga(),
    lockScreenSaga(),
  ]);
}
