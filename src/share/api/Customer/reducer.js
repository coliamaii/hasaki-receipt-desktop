import { combineReducers } from 'redux'
import searchCustomerReducer from './SearchCustomer/reducer'
import getDetailCustomerReducer from './GetDetail/reducer'

const customerReducer = combineReducers({
  getDetail: getDetailCustomerReducer,
  searchCustomer: searchCustomerReducer,
});

export default customerReducer;