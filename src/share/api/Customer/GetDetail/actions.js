export const GET_DETAIL_CUSTOMER = {
  REQUEST: "API_GET_DETAIL_CUSTOMER_REQUEST",
  SUCCESS: "API_GET_DETAIL_CUSTOMER_SUCCESS",
  ERROR: "API_GET_DETAIL_CUSTOMER_ERROR",
};

export function getDetailCustomerRequestAction(id) {
  return {
    type: GET_DETAIL_CUSTOMER.REQUEST,
    id,
  };
}

export function getDetailCustomerSuccessAction(data) {
  return {
    type: GET_DETAIL_CUSTOMER.SUCCESS,
    data,
  };
}

export function getDetailCustomerErrorAction() {
  return {
    type: GET_DETAIL_CUSTOMER.ERROR,
  };
}
