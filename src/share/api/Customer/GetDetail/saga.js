import { getDetail } from "apis/sales/customer";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  getDetailCustomerErrorAction,
  getDetailCustomerSuccessAction,
  GET_DETAIL_CUSTOMER,
} from "./actions";

function* getDetailCustomerSaga(action) {
  try {
    const {
      status,
      data: {
        customer: { customer_id, customer_name, customer_phone },
      },
    } = yield call(getDetail, action.id);

    if (status === 1) {
      yield put(
        getDetailCustomerSuccessAction({
          customer_id,
          customer_name,
          customer_phone,
        })
      );
    } else {
      yield put(getDetailCustomerErrorAction());
    }
  } catch (err) {
    yield put(getDetailCustomerErrorAction());
  }
}

function* getDetailCustomerWatcher() {
  yield takeLatest(GET_DETAIL_CUSTOMER.REQUEST, getDetailCustomerSaga);
}

export default getDetailCustomerWatcher;
