import { GET_DETAIL_CUSTOMER } from "./actions";

const initialState = {
  rows: [],
};

function getDetailCustomerReducer(state = initialState, action) {
  switch (action.type) {
    case GET_DETAIL_CUSTOMER.REQUEST: {
      return {
        ...state,
      };
    }

    case GET_DETAIL_CUSTOMER.SUCCESS: {
      return {
        ...state,
        rows: action.data,
      };
    }

    default: {
      return {
        ...state,
      };
    }
  }
}

export default getDetailCustomerReducer;
