import { all } from "redux-saga/effects";
import getDetailCustomerWatcher from "./GetDetail/saga";
import searchCustomerWatcher from "./SearchCustomer/saga";

export default function* () {
  yield all([
    getDetailCustomerWatcher(),
    searchCustomerWatcher(),
  ]);
}