export const SEARCH_CUSTOMER = {
  CANCEL: "API_SEARCH_CUSTOMER_CANCEL",
  REQUEST: "API_SEARCH_CUSTOMER_REQUEST",
  OFFLINE_REQUEST: "API_SEARCH_CUSTOMER_OFFLINE_REQUEST",
  SUCCESS: "API_SEARCH_CUSTOMER_SUCCESS",
  ERROR: "API_SEARCH_CUSTOMER_ERROR",
  CLEAN: "API_SEARCH_CUSTOMER_CLEAN",
  STORE_NEW: "SEARCH_CUSTOMER_STORE_NEW",
  IS_SEARCHING: "IS_SEARCHING_CUSTOMER",
};

export function searchCustomerRequestAction(phone, isNewCustomer, client_receipt_id) {
  return {
    type: SEARCH_CUSTOMER.REQUEST,
    phone,
    isNewCustomer,
    client_receipt_id,
  };
}

export function searchCustomerOfflineRequestAction(localData, phone) {
  return {
    type: SEARCH_CUSTOMER.OFFLINE_REQUEST,
    localData,
    phone,
  };
}

export function searchCustomerSuccessAction(data, isNewCustomer) {
  return {
    type: SEARCH_CUSTOMER.SUCCESS,
    data,
    isNewCustomer,
  };
}

export function searchCustomerErrorAction() {
  return {
    type: SEARCH_CUSTOMER.ERROR,
  };
}

export function searchCustomerCleanAction(localData, isSearching) {
  return {
    type: SEARCH_CUSTOMER.CLEAN,
    localData,
    isSearching,
  };
}

export function searchCustomerStoreNewAction(customerName, customerEmail) {
  return {
    type: SEARCH_CUSTOMER.STORE_NEW,
    customerName,
    customerEmail,
  };
}

export function isSearchingCustomerAction(isSearching) {
  return {
    type: SEARCH_CUSTOMER.IS_SEARCHING,
    isSearching,
  }
}
