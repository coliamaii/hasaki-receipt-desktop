import { searchByPhoneNumber } from "apis/sales/customer";
import { all, call, put, takeLatest } from "redux-saga/effects";
import { initBalancePaymentAction } from "share/api/Payment/Type/Balance/action";
import {
  searchCustomerErrorAction,
  searchCustomerSuccessAction,
  SEARCH_CUSTOMER,
} from "./actions";

function* searchCustomerSaga(action) {
  try {
    let { phone, client_receipt_id } = action;
    const { status, data } = yield call(searchByPhoneNumber, { phone });
    if (status === 1) {
      if (data.customer) {
        const {
          customer_order_point,
          customer_receipt_point,
          customer_init_order_point,
          customer_init_receipt_point,
          used_point
        } = data.customer;
        yield put(searchCustomerSuccessAction({
          ...data.customer,
          customer_point: (customer_order_point + customer_receipt_point + customer_init_order_point + customer_init_receipt_point) - used_point,
        }, false));

        if (data.account_balance) {
          const balance = data.account_balance.balance;
          const credit = balance.find(item => item.balance_type === 1).credit;
          yield put(initBalancePaymentAction(credit, true, client_receipt_id));
        } else {
          yield put(initBalancePaymentAction(0, false, client_receipt_id));
        }
      } else {
        yield put(searchCustomerSuccessAction({ customer_phone: phone, customer_point: 0 }, true));
      }
    } else {
      yield put(searchCustomerErrorAction());
    }
  } catch (err) {
    yield put(searchCustomerErrorAction());
  }
}

function* searchCustomerInOfflineSaga(action) {
  try {
    let { phone, localData } = action;
    const { receipt_id } = localData;
    const customer = JSON.stringify({ customer_phone: phone });

    // yield call(window.api.updateDataByIdFromReceiptTable, {
    //   receipt_id,
    //   customer,
    // });

    yield put(searchCustomerSuccessAction({ customer_phone: phone }, false));
  } catch (err) {
    yield put(searchCustomerErrorAction());
  }
}

function* searchCustomerCancelSaga(action) {
  try {
    let { localData } = action;
    const { receipt_id } = localData;
    const customer = null;

    // yield call(window.api.updateDataByIdFromReceiptTable, {
    //   receipt_id,
    //   customer,
    // });

    yield put(searchCustomerSuccessAction(null, false));
  } catch (err) {
    yield put(searchCustomerErrorAction());
  }
}

function* searchCustomerWatcher() {
  yield all([
    takeLatest(SEARCH_CUSTOMER.REQUEST, searchCustomerSaga),
    takeLatest(
      SEARCH_CUSTOMER.OFFLINE_REQUEST,
      searchCustomerInOfflineSaga
    ),
    takeLatest(SEARCH_CUSTOMER.CLEAN, searchCustomerCancelSaga),
  ])
}

export default searchCustomerWatcher;
