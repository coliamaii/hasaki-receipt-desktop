import {
  SEARCH_CUSTOMER,
} from './actions';

const initialState = {
  data: null,
  isNewCustomer: null,
  loading: false,
  isSearching: false,//if true, always focus to customer searchbar
}

function searchCustomerReducer(state = initialState, action) {
  switch (action.type) {
    case SEARCH_CUSTOMER.REQUEST: {
      return {
        ...state,
        loading: true,
        isSearching: true,
      }
    }
    case SEARCH_CUSTOMER.SUCCESS: {
      const { data, isNewCustomer } = action;
      return {
        ...state,
        loading: false,
        data,
        isNewCustomer,
      }
    }
    case SEARCH_CUSTOMER.CLEAN: {
      const { isSearching } = action;
      return {
        loading: false,
        data: null,
        isNewCustomer: null,
        isSearching,
      }
    }
    case SEARCH_CUSTOMER.STORE_NEW: {
      const { customerName, customerEmail } = action;

      return {
        ...state,
        data: {
          ...state.data,
          customer_name: customerName,
          customer_email: customerEmail,
        }
      }
    }
    case SEARCH_CUSTOMER.IS_SEARCHING: {
      const { isSearching } = action;
      return {
        ...state,
        isSearching,
      }
    }
    default: {
      return {
        ...state,
      }
    }
  }
}

export default searchCustomerReducer;