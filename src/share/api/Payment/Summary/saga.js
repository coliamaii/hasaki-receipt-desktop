import { all, call, put, takeLatest } from "@redux-saga/core/effects";
import { staffConsultant } from "apis/hr/consultant";
import { paymentSummaryUpdateConsultantAction, PAYMENT_SUMMARY } from "./action";

function* addConsultantSaga({ code, client_receipt_id }) {
  try {
    const { status, data: { rows } } = yield call(staffConsultant, code);
    if (status === 1) {
      if (rows.length === 0) {
        yield put(paymentSummaryUpdateConsultantAction({
          isDone: false,
        }, client_receipt_id))
        return;
      }
      const staff = rows.find(item => item.code === code);
      yield put(paymentSummaryUpdateConsultantAction({
        id: staff.staff_id,
        code: staff.code,
        name: staff.staff_name,
        isDone: true,
      }, client_receipt_id))
    }
  } catch (err) {
    console.log({ err });
  }
}

function* paymentSummaryWatcher() {
  yield all([
    takeLatest(PAYMENT_SUMMARY.ADD_CONSULTANT, addConsultantSaga),
  ])
}

export default paymentSummaryWatcher;