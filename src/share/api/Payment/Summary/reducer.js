import { PAYMENT_SUMMARY } from "./action";

export const initialState = {
  items: 0,
  subTotalPrice: 0,
  totalPrices: 0,
  cash: 0,
  discount: 0,
  charge: 0,
  remainder: 0,
  isConfirmModalOpen: false,
  consultant: null,
}

function SummaryPaymentReducer(state = initialState, action) {
  switch (action.type) {
    case PAYMENT_SUMMARY.INIT: {
      const { items, subTotalPrice, totalPrices, discount, client_receipt_id } = action;
      const newState = {
        ...state,
        items,
        subTotalPrice,
        totalPrices,
        discount,
        remainder: totalPrices,
        isConfirmModalOpen: false,
      };
      window.api.updateReceiptById(client_receipt_id, 'payment_summary', JSON.stringify(newState));
      return newState;
    }
    case PAYMENT_SUMMARY.CACULATE: {
      const { cash, card, qrPay, gift, balance, client_receipt_id } = action;
      let charge = 0;
      let remainder = state.totalPrices - cash - card - qrPay - gift - balance;

      if (remainder < 0) {
        charge = -remainder;
        remainder = 0
      }
      if (gift >= state.totalPrices) {
        remainder = 0;
        charge = 0;
      }
      if ((gift + cash > state.totalPrices) && (gift > 0)) {
        charge = cash;
      }

      const newState = {
        ...state,
        remainder,
        charge,
      }

      window.api.updateReceiptById(client_receipt_id, 'payment_summary', JSON.stringify(newState));
      return newState;
    }
    case PAYMENT_SUMMARY.ADD_CASH: {
      const { cash, client_receipt_id } = action;
      window.api.updateReceiptById(client_receipt_id, 'payment_summary', {
        ...state,
        cash,
      });

      return {
        ...state,
        cash,
      }
    }
    case PAYMENT_SUMMARY.SHOW_CONFIRM_MODAL: {
      const { isConfirmModalOpen, client_receipt_id } = action;
      window.api.updateReceiptById(client_receipt_id, 'payment_summary', JSON.stringify({
        ...state,
        isConfirmModalOpen,
      }));
      return {
        ...state,
        isConfirmModalOpen,
      }
    }
    case PAYMENT_SUMMARY.UPDATE_CONSULTANT: {
      const { consultant, client_receipt_id } = action;
      window.api.updateReceiptById(client_receipt_id, 'payment_summary', JSON.stringify({
        ...state,
        consultant,
      }));
      return {
        ...state,
        consultant,
      }
    }
    case PAYMENT_SUMMARY.RESET: {
      const { client_receipt_id } = action;
      window.api.updateReceiptById(client_receipt_id, 'payment_summary', JSON.stringify({
        ...initialState,
      }));
      return {
        ...initialState,
      }
    }
    case PAYMENT_SUMMARY.REFRESH: {
      const { data } = action;
      return data;
    }
    default: {
      return { ...state };
    }
  }
}

export default SummaryPaymentReducer;