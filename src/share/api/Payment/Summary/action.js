export const PAYMENT_SUMMARY = {
  INIT: 'PAYMENT_SUMMARY_INIT',
  CACULATE: 'PAYMENT_SUMMARY_CACULATE',
  ADD_CASH: 'PAYMENT_SUMMARY_ADD_CASH',
  SHOW_CONFIRM_MODAL: 'PAYMENT_SUMMARY_SHOW_CONFIRM_MODAL',
  ADD_CONSULTANT: 'PAYMENT_SUMMARY_ADD_CONSULTANT',
  UPDATE_CONSULTANT: 'PAYMENT_SUMMARY_UPDATE_CONSULTANT',
  RESET: 'RESET_PAYMENT_SUMMARY',
  REFRESH: 'REFRESH_PAYMENT_SUMMARY',
}

export const showConfirmModalAction = (isConfirmModalOpen, client_receipt_id) => {
  return {
    type: PAYMENT_SUMMARY.SHOW_CONFIRM_MODAL,
    isConfirmModalOpen,
    client_receipt_id,
  }
}

export const caculatePaymentSummaryAction = (cash, card, qrPay, gift, balance, client_receipt_id) => {
  return {
    type: PAYMENT_SUMMARY.CACULATE,
    cash,
    card,
    qrPay,
    gift,
    balance,
    client_receipt_id
  }
}

export const paymentSummaryInitAction = (items, subTotalPrice, totalPrices, discount, client_receipt_id) => {
  return {
    type: PAYMENT_SUMMARY.INIT,
    items,
    subTotalPrice,
    totalPrices,
    discount,
    client_receipt_id,
  }
}

export const paymentSummaryInsertCashAction = (cash, client_receipt_id) => {
  return {
    type: PAYMENT_SUMMARY.ADD_CASH,
    cash,
    client_receipt_id
  }
}

export const paymentSummaryAddConsultantAction = (code, client_receipt_id) => {
  return {
    type: PAYMENT_SUMMARY.ADD_CONSULTANT,
    code,
    client_receipt_id,
  }
}

export const paymentSummaryUpdateConsultantAction = (consultant, client_receipt_id) => {
  return {
    type: PAYMENT_SUMMARY.UPDATE_CONSULTANT,
    consultant,
    client_receipt_id
  }
}

export const resetPaymentSummaryAction = (client_receipt_id) => {
  return {
    type: PAYMENT_SUMMARY.RESET,
    client_receipt_id,
  }
}

export const refreshPaymentSummaryAction = (data) => {
  return {
    type: PAYMENT_SUMMARY.REFRESH,
    data,
  }
}