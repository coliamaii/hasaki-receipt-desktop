import { CARD_PAYMENT } from "./action";

export const initalState = {
  amount: 0,
  message: null,
  isPaymentRequest: false,
  isLoading: false,
  isDone: false,
  isError: false,
  partner: null,
  transaction_id: null,
}

const CardPaymentReducer = (state = initalState, action) => {
  switch (action.type) {
    case CARD_PAYMENT.ADD_AMOUNT: {
      const { amount, client_receipt_id } = action;
      window.api.updateReceiptById(client_receipt_id, 'card_payment', JSON.stringify({
        ...state,
        amount,
      }));
      return {
        ...state,
        amount,
      }
    }
    case CARD_PAYMENT.CANCEL: {
      const { client_receipt_id } = action;

      const newState = {
        ...state,
        message: null,
        isPaymentRequest: false,
        isLoading: false,
        isDone: false,
        isError: false,
        partner: null,
      }
      window.api.updateReceiptById(client_receipt_id, 'card_payment', JSON.stringify(newState));
      return newState;
    }
    case CARD_PAYMENT.SUCCESS: {
      const { client_receipt_id } = action;
      window.api.updateReceiptById(client_receipt_id, 'card_payment', JSON.stringify({
        ...state,
        message: null,
        isPaymentRequest: false,
      }));
      return {
        ...state,
        message: null,
        isPaymentRequest: false,
      }
    }
    case CARD_PAYMENT.VCB_REQUEST: {
      const { client_receipt_id } = action;

      const newState = {
        ...state,
        partner: 'Vietcombank',
        isPaymentRequest: true,
        isLoading: true,
        message: 'Connecting.....',
        isDone: false,
        isError: false,
      }
      window.api.updateReceiptById(client_receipt_id, 'card_payment', JSON.stringify(newState));
      return newState;
    }
    case CARD_PAYMENT.UPDATE_STATUS: {
      const { message, is_complete, client_receipt_id } = action;

      const newState = {
        ...state,
        message,
        isDone: Boolean(is_complete),
      }

      window.api.updateReceiptById(client_receipt_id, 'card_payment', JSON.stringify(newState));
      return newState;
    }
    case CARD_PAYMENT.FINISH: {
      const { message, is_complete, transaction_id, client_receipt_id } = action;

      const newState = {
        ...state,
        message,
        transaction_id,
        isLoading: false,
        isDone: is_complete,
        isError: !is_complete,
      };

      window.api.updateReceiptById(client_receipt_id, 'card_payment', JSON.stringify(newState));
      return newState;
    }
    case CARD_PAYMENT.BIDV_REQUEST: {
      const { client_receipt_id } = action;

      const newState = {
        ...state,
        partner: 'BIDV',
        isPaymentRequest: true,
        isLoading: true,
        message: 'Connecting.....',
        isDone: false,
        isError: false,
      };
      window.api.updateReceiptById(client_receipt_id, 'card_payment', JSON.stringify(newState));
      return newState;
    }
    case CARD_PAYMENT.RESET: {
      const { client_receipt_id } = action;
      window.api.updateReceiptById(client_receipt_id, 'card_payment', JSON.stringify({
        ...initalState,
      }));
      return {
        ...initalState,
      }
    }
    case CARD_PAYMENT.REFRESH: {
      const { data } = action;
      return data;
    }
    default: {
      return {
        ...state,
      }
    }
  }
}

export default CardPaymentReducer;