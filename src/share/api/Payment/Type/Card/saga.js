import { all, call, delay, put, takeLatest } from "@redux-saga/core/effects";
import { CARD_PAYMENT, cardPaymentFinishAction, cardPaymentUpdateStatusAction } from "./action";
import { vcbRequestPayment, vcbCheckPaymentStatus } from 'apis/partner/vcb';
import { bidvPaymentRequest, bidvSavePaymentResult } from "apis/partner/bidv";

function* vcbPaymentRequestSaga({ source_code, store_id, amount, ip_pos, client_receipt_id }) {
  const retryCounter = 100;
  try {
    const { status, data } = yield call(vcbRequestPayment, source_code, store_id, amount, ip_pos);
    if (status === 1) {
      const { message, is_complete, log_id } = data;
      yield put(cardPaymentUpdateStatusAction(message, is_complete, client_receipt_id));
      let isDone = false;
      let result;
      let messageResponse;
      for (let i = 1; i < retryCounter; i++) {
        result = yield vcbCheckPaymentStatus(source_code, store_id, amount, log_id);
        const { message, is_complete } = data;
        if (is_complete) {
          const transaction_id = result?.data?.transaction_id
          if (transaction_id) {
            yield put(cardPaymentFinishAction(message, true, transaction_id, client_receipt_id));
            isDone = true;
          }
          messageResponse = message;
          break;
        }
        yield delay(2000);
      }
      if (!isDone) {
        yield put(cardPaymentFinishAction(messageResponse, false, null, client_receipt_id));
      }
    }
    else {
      //show err
    }
  } catch (err) {
    console.log(err);
  }
}

function* bidvPaymentRequestSaga({ source_code, store_id, amount, ip_pos, client_receipt_id }) {
  try {
    const { status, data: { url } } = yield call(bidvPaymentRequest, source_code, store_id, amount, ip_pos);
    if (status === 1) {
      yield put(cardPaymentUpdateStatusAction('Waiting for payment...', false, client_receipt_id));
      yield call(window.api.bidvCheckPaymentStatus, { url });
    }
  } catch (err) {
    yield put(cardPaymentFinishAction('Error occurred!', false, null, client_receipt_id));
  }
}

function* bidvHandleResponseSaga({ bidvResponse, client_receipt_id }) {
  const { status, transInfo } = bidvResponse;
  if (status === 'CANCELED') {
    yield put(cardPaymentFinishAction('User canceled!', false, null, client_receipt_id));
  } else if (status === 'DONE') {
    if (transInfo) {
      if (transInfo.transStatus === 'DECLINE') {
        let message;
        switch (transInfo.returnCode) {
          case '19':
            message = '[19] Re-enter transaction';
            break;
          case '01':
          case '02':
            message = '[01] Refer to card issuer';
            break;
          case '03':
          case '3':
            message = '[03] Invalid Merchant';
            break;
          case '04':
            message = '[04] Pickup card';
            break;
          case '07':
            message = '[07] Pickup card, special condition';
            break;
          case '41':
            message = '[41] Pickup card (lost card)';
            break;
          case '43':
            message = '[43] Pickup card (stolen card)';
            break;
          case '05':
          case '5':
            message = '[05] Do not honour';
            break;
          case '12':
            message = '[12] Invalid transaction';
            break;
          case '13':
            message = '[13] Invalid amount - currency conversion filed overflow or amount exceed maximum for card program';
            break;
          case '14':
            message = '[14] Invalid account number (no such number)';
            break;
          case '30':
            message = '[30] Format error';
            break;
          case '31':
            message = '[31] Transaction denined';
            break;
          case '51':
            message = '[51] Insufficient funds/over credit limits';
            break;
          case '61':
            message = '[61] Exceeds withdrawal amount limit';
            break;
          case '65':
            message = '[65] Exceed withdrawal count limit';
            break;
          case '54':
            message = '[54] Expired card';
            break;
          case '55':
            message = '[55] Incorrect personal identification number';
            break;
          case '87':
            message = '[87] PIN cryptographic error found';
            break;
          case '57':
            message = '[57] Transaction not permitted to cardholder';
            break;
          case '58':
            message = '[58] Transaction not permitted to acquirer/terminal';
            break;
          case '59':
            message = '[59] Suspected fraud';
            break;
          case '62':
            message = '[62] Restricted card';
            break;
          case '75':
            message = '[75] Allowable number of PIN tried exceeded';
            break;
          case '79':
            message = '[75] Declined for CVV2 failure';
            break;
          case '81':
            message = '[81] Invalid PIN block';
            break;
          case '91':
            message = '[91] Issuer unavaiable or switch inoperative';
            break;
          case '93':
            message = '[93] Transaction cannot be completed; violaiton of law';
            break;
          case '96':
            message = '[96] System malfunction';
            break;
          default:
            message = `[${transInfo.returnCode}] Payment declined!`;
        }
        yield put(cardPaymentFinishAction(message, false, null, client_receipt_id));
      }
      else if (transInfo.transStatus === 'APPROVE') {
        //Done here
        yield put(cardPaymentFinishAction('Success!', true, transInfo.refNo, client_receipt_id));
      }
      const res = yield call(bidvSavePaymentResult, bidvResponse, transInfo.refNo);
      console.log({ res });
    }
  } else if (bidvResponse.message) {
    if (bidvResponse.message.indexOf('connect ETIMEDOUT') > -1) {
      yield put(cardPaymentFinishAction('Can not connected to POS!', false, null, client_receipt_id));
    } else {
      yield put(cardPaymentFinishAction(bidvResponse.message, false, null, client_receipt_id));
    }
  }
}

function* cardPaymentWatcher() {
  yield all([
    takeLatest(CARD_PAYMENT.VCB_REQUEST, vcbPaymentRequestSaga),
    takeLatest(CARD_PAYMENT.BIDV_REQUEST, bidvPaymentRequestSaga),
    takeLatest(CARD_PAYMENT.BIDV_HANDLE, bidvHandleResponseSaga),
  ]);
}

export default cardPaymentWatcher;