export const CARD_PAYMENT = {
  ADD_AMOUNT: 'ADD_AMOUNT_CARD_PAYMENT',
  CANCEL: 'CANCEL_CARD_PAYMENT',
  SUCCESS: 'SUCCESS_CARD_PAYMENT',
  UPDATE_STATUS: 'CARD_PAYMENT_UPDATE_STATUS',
  FINISH: 'CARD_PAYMENT_FINISH',
  VCB_REQUEST: 'VCB_CARD_PAYMENT_REQUEST',
  BIDV_REQUEST: 'BIDV_REQUEST_CARD_PAYMENT',
  BIDV_HANDLE: 'BIDV_HANDLE_CARD_PAYMENT',
  RESET: 'RESET_CARD_PAYMENT',
  REFRESH: 'REFRESH_CARD_PAYMENT',
}

export const addAmountCardPaymentAction = (amount, client_receipt_id) => {
  return {
    type: CARD_PAYMENT.ADD_AMOUNT,
    amount,
    client_receipt_id,
  }
}

export const cancelCardPaymentAction = (client_receipt_id) => {
  return {
    type: CARD_PAYMENT.CANCEL,
    client_receipt_id,
  }
}

export const cardPaymentSuccessAction = (client_receipt_id) => {
  return {
    type: CARD_PAYMENT.SUCCESS,
    client_receipt_id,
  }
}

export const cardPaymentUpdateStatusAction = (message, is_complete, client_receipt_id) => {
  return {
    type: CARD_PAYMENT.UPDATE_STATUS,
    message,
    is_complete,
    client_receipt_id,
  }
}

export const cardPaymentFinishAction = (message, is_complete, transaction_id, client_receipt_id) => {
  return {
    type: CARD_PAYMENT.FINISH,
    message,
    is_complete,
    transaction_id,
    client_receipt_id,
  }
}

export const vcbCardPaymentRequestAction = (source_code, store_id, amount, ip_pos, client_receipt_id) => {
  return {
    type: CARD_PAYMENT.VCB_REQUEST,
    source_code,
    store_id,
    amount,
    ip_pos,
    client_receipt_id,
  };
}

export const bidvCardPaymentRequestAction = (source_code, store_id, amount, ip_pos, client_receipt_id) => {
  return {
    type: CARD_PAYMENT.BIDV_REQUEST,
    source_code,
    store_id,
    amount,
    ip_pos,
    client_receipt_id,
  }
}

export const handleBidvCardPaymentAction = (bidvResponse, client_receipt_id) => {
  return {
    type: CARD_PAYMENT.BIDV_HANDLE,
    bidvResponse,
    client_receipt_id,
  }
}

export const resetCardPaymentAction = (client_receipt_id) => {
  return {
    type: CARD_PAYMENT.RESET,
    client_receipt_id,
  }
}

export const refreshCardPaymentAction = (data) => {
  return {
    type: CARD_PAYMENT.REFRESH,
    data,
  }
}