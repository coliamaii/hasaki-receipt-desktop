import { combineReducers } from "redux";
import balancePaymentReducer from "./Balance/reducer";
import CardPaymentReducer from "./Card/reducer";
import GiftCardTypeReducer from "./GiftCard/reducer";
import qrPayReducer from "./QrPay/reducer";

const PaymentTypeReducer = combineReducers({
  giftCard: GiftCardTypeReducer,
  card: CardPaymentReducer,
  qrPay: qrPayReducer,
  balance: balancePaymentReducer,
});

export default PaymentTypeReducer;