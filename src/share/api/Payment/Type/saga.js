import { all } from "@redux-saga/core/effects";
import giftCard from './GiftCard/saga';
import card from './Card/saga';
import qrPay from './QrPay/saga';
import balance from './Balance/saga';

export default function* () {
  yield all([
    giftCard(),
    card(),
    qrPay(),
    balance(),
  ]);
}