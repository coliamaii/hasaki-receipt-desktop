import { call, put, takeLatest } from "@redux-saga/core/effects";
import { getQrCode } from "apis/partner/vnpay";
import { qrPayFinishAction, qrPayUpdateCodeAction, qrPayUpdateStatusAction, QR_PAYMENT } from "./action";

function* qrPayRequestSaga({ store_id, source_code, amount, terminal_id, client_receipt_id }) {
  const { status, message, data: { code } } = yield call(getQrCode, store_id, source_code, amount, terminal_id);
  if (status === 1) {
    yield put(qrPayUpdateStatusAction('Waiting for customer scan...', false, client_receipt_id));
    yield put(qrPayUpdateCodeAction(code, client_receipt_id));
  } else {
    yield put(qrPayFinishAction(message, false, null, client_receipt_id));
  }
}

function* qrPayWatcher() {
  yield takeLatest(QR_PAYMENT.REQUEST, qrPayRequestSaga);
}

export default qrPayWatcher;