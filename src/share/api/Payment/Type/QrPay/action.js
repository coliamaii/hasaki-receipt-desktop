export const QR_PAYMENT = {
  ADD_AMOUNT: 'ADD_AMOUNT_QRPAY',
  REQUEST: 'QR_PAYMENT_REQUEST',
  UPDATE_STATUS: 'QR_PAY_UPDATE_STATUS',
  UPDATE_CODE: 'QR_PAY_UPDATE_CODE',
  FINISH: 'QR_PAY_FINISH',
  CANCEL: 'QR_PAY_CANCEL',
  RESET: 'QR_PAY_RESET',
  REFRESH: 'QR_PAY_REFRESH',
}

export const addAmountQrPayAction = (amount, client_receipt_id) => {
  return {
    type: QR_PAYMENT.ADD_AMOUNT,
    amount,
    client_receipt_id,
  }
}

export const qrPaymentRequestAction = (store_id, source_code, amount, terminal_id, client_receipt_id) => {
  return {
    type: QR_PAYMENT.REQUEST,
    store_id,
    source_code,
    amount,
    terminal_id,
    client_receipt_id,
  }
}

export const qrPayUpdateStatusAction = (message, is_complete, client_receipt_id) => {
  return {
    type: QR_PAYMENT.UPDATE_STATUS,
    message,
    is_complete,
    client_receipt_id,
  }
}

export const qrPayUpdateCodeAction = (code, client_receipt_id) => {
  return {
    type: QR_PAYMENT.UPDATE_CODE,
    code,
    client_receipt_id,
  }
}

export const qrPayFinishAction = (message, is_complete, transaction_id, client_receipt_id) => {
  return {
    type: QR_PAYMENT.FINISH,
    message,
    is_complete,
    transaction_id,
    client_receipt_id,
  }
}

export const qrPayCancelAction = (client_receipt_id) => {
  return {
    type: QR_PAYMENT.CANCEL,
    client_receipt_id,
  }
}

export const qrPayResetAction = (client_receipt_id) => {
  return {
    type: QR_PAYMENT.RESET,
    client_receipt_id
  }
}

export const qrPayRefreshAction = (data) => {
  return {
    type: QR_PAYMENT.REFRESH,
    data,
  }
}