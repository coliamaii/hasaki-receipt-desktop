import { QR_PAYMENT } from "./action";

export const initalState = {
  amount: 0,
  code: null,
  message: null,
  isPaymentRequest: false,
  isLoading: false,
  isDone: false,
  isError: false,
  transaction_id: null,
}

const qrPayReducer = (state = initalState, action) => {
  switch (action.type) {
    case QR_PAYMENT.ADD_AMOUNT: {
      const { amount, client_receipt_id } = action;

      window.api.updateReceiptById(client_receipt_id, 'qr_payment', JSON.stringify({
        ...state,
        amount,
      }));
      return {
        ...state,
        amount,
      }
    }
    case QR_PAYMENT.REQUEST: {
      const { client_receipt_id } = action;

      const newState = {
        ...state,
        message: 'Get QR info...',
        isPaymentRequest: true,
        isLoading: true,
        isDone: false,
        isError: false,
      }

      window.api.updateReceiptById(client_receipt_id, 'qr_payment', JSON.stringify(newState));
      return newState;
    }
    case QR_PAYMENT.UPDATE_STATUS: {
      const { message, is_complete, client_receipt_id } = action;

      const newState = {
        ...state,
        message,
        isDone: is_complete,
      }

      window.api.updateReceiptById(client_receipt_id, 'qr_payment', JSON.stringify(newState));
      return newState;
    }
    case QR_PAYMENT.UPDATE_CODE: {
      const { code, client_receipt_id } = action;

      window.api.updateReceiptById(client_receipt_id, 'qr_payment', JSON.stringify({
        ...state,
        code,
      }));
      return {
        ...state,
        code,
      }
    }
    case QR_PAYMENT.FINISH: {
      const { message, is_complete, transaction_id, client_receipt_id } = action;

      const newState = {
        ...state,
        code: null,
        message,
        isLoading: false,
        isDone: is_complete,
        isError: !is_complete,
        transaction_id,
      }

      window.api.updateReceiptById(client_receipt_id, 'qr_payment', JSON.stringify(newState));
      return newState;
    }
    case QR_PAYMENT.CANCEL: {
      const { client_receipt_id } = action;

      const newState = {
        ...state,
        code: null,
        message: null,
        isPaymentRequest: false,
        isLoading: false,
        isDone: false,
        isError: false,
        transaction_id: null,
      }
      window.api.updateReceiptById(client_receipt_id, 'qr_payment', JSON.stringify(newState));
      return newState;
    }
    case QR_PAYMENT.RESET: {
      const { client_receipt_id } = action;
      window.api.updateReceiptById(client_receipt_id, 'qr_payment', JSON.stringify({
        ...initalState,
      }));
      return {
        ...initalState,
      }
    }
    case QR_PAYMENT.REFRESH: {
      const { data } = action;
      return data;
    }
    default: {
      return {
        ...state,
      }
    }
  }
}

export default qrPayReducer;