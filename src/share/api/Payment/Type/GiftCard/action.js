export const GIFT_CARD_CHECK = {
  REQUEST: 'API_GIFT_CARD_CHECK_REQUEST',
  ADD_CODE: 'GIFT_CARD_CHECK_ADD_CODE',
  UPDATE_CODE: 'GIFT_CARD_CHECK_ADD_UPDATE_CODE',
  BEGIN_REDEEM: 'BEGIN_REDEEM_GIFT_CARD',
  FINISH_REDEEM: 'FINISH_REDEEM_GIFT_CARD',
  RESET: 'RESET_GIFT_CARD_CHECK',
  REFRESH: 'REFRESH_GIFT_CARD',
}

export const giftCardCheckRequestAction = (serial_number, store_id, giftCardType, client_receipt_id) => {
  return {
    type: GIFT_CARD_CHECK.REQUEST,
    serial_number,
    store_id,
    giftCardType,
    client_receipt_id,
  }
}

export const giftCardCheckAddCodeAction = (serial_number, giftCardType, client_receipt_id) => {
  return {
    type: GIFT_CARD_CHECK.ADD_CODE,
    serial_number,
    giftCardType,
    client_receipt_id,
  }
}

export const giftCardCheckUpdateCodeAction = (serial_number, code, value, status, client_receipt_id) => {
  return {
    type: GIFT_CARD_CHECK.UPDATE_CODE,
    serial_number,
    code,
    value,
    status,
    client_receipt_id,
  }
}

export const beginRedeemGiftsAction = (validGiftCodes, receipt_code, receipt_id, store_id, client_receipt_id) => {
  return {
    type: GIFT_CARD_CHECK.BEGIN_REDEEM,
    validGiftCodes,
    receipt_code,
    receipt_id,
    store_id,
    client_receipt_id,
  }
}

export const finishRedeemGiftAction = (validRedeemGifts, client_receipt_id) => {
  return {
    type: GIFT_CARD_CHECK.FINISH_REDEEM,
    validRedeemGifts,
    client_receipt_id,
  }
}

export const resetGiftCardCheckAction = (client_receipt_id) => {
  return {
    type: GIFT_CARD_CHECK.RESET,
    client_receipt_id,
  }
}

export const refreshGiftCardAction = (data) => {
  return {
    type: GIFT_CARD_CHECK.REFRESH,
    data,
  }
}