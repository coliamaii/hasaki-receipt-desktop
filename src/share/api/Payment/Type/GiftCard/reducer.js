import { GIFT_CARD_CHECK } from "./action";

export const initialState = {
  current: 0,
  totalPrice: 0,
  listCode: [],
  isLoading: false,
}

const GiftCardTypeReducer = (state = initialState, action) => {
  switch (action.type) {
    case GIFT_CARD_CHECK.ADD_CODE: {
      const { serial_number, giftCardType, client_receipt_id } = action;
      const codeInfo = { serial_number, code: null, type: giftCardType, value: null, status: null };
      const newState = {
        ...state,
        listCode: [...state.listCode].concat(codeInfo),
      }
      window.api.updateReceiptById(client_receipt_id, 'gift_card_payment', JSON.stringify(newState));
      return newState;
    }
    case GIFT_CARD_CHECK.UPDATE_CODE: {
      const { serial_number, code, value, status, client_receipt_id } = action;
      const index = state.listCode.findIndex(item => item.serial_number === serial_number);
      const codeInfo = { serial_number, code, type: state.listCode[index].type, value, status, isRedeem: false, transaction_id: null };
      const cloneListCode = [...state.listCode];
      cloneListCode[index] = codeInfo;

      const newState = {
        ...state,
        totalPrice: state.totalPrice + (status === 0 ? 0 : value),
        listCode: cloneListCode,
      }
      window.api.updateReceiptById(client_receipt_id, 'gift_card_payment', JSON.stringify(newState));

      return newState;
    }
    case GIFT_CARD_CHECK.BEGIN_REDEEM: {
      const { client_receipt_id } = action;
      const cloneArr = [...state.listCode];
      const listCode = cloneArr.filter(item => item.status).map(item => { return { ...item, isRedeem: true } });
      const totalPrice = listCode.reduce((initvalue, item) => {
        const val = item.isRedeem ? 0 : item.value;
        return val + initvalue;
      }, 0);

      const newState = {
        ...state,
        isLoading: true,
        listCode,
        totalPrice,
      }
      window.api.updateReceiptById(client_receipt_id, 'gift_card_payment', JSON.stringify(newState));
      return newState;
    }
    case GIFT_CARD_CHECK.FINISH_REDEEM: {
      const { validRedeemGifts, client_receipt_id } = action;
      const cloneData = [...state.listCode];
      let amount = 0;
      validRedeemGifts.forEach((validGift) => {
        const index = cloneData.findIndex(item => item.serial_number === validGift.serial_number);
        cloneData[index].transaction_id = validGift.transaction_id;
        amount += cloneData[index].value;
      })
      const newState = {
        ...state,
        listCode: cloneData,
        current: state.current + amount,
      }
      window.api.updateReceiptById(client_receipt_id, 'gift_card_payment', JSON.stringify(newState));
      return newState;
    }
    case GIFT_CARD_CHECK.RESET: {
      const { client_receipt_id } = action;
      window.api.updateReceiptById(client_receipt_id, 'gift_card_payment', JSON.stringify({ ...initialState }));
      return {
        ...initialState,
      }
    }
    case GIFT_CARD_CHECK.REFRESH: {
      const { data } = action;
      return data;
    }
    default: {
      return { ...state };
    }
  }
}

export default GiftCardTypeReducer;