import { all, call, put, takeLatest } from "@redux-saga/core/effects";
import { checkGoTitGift, redeemGotItGift } from "apis/partner/gotit";
import { checkHasakiGift, redeemHasakiGift } from "apis/partner/hasaki";
import { checkSodexoGift, redeemSodexoGift } from "apis/partner/sodexo";
import { finishRedeemGiftAction, giftCardCheckAddCodeAction, giftCardCheckUpdateCodeAction, GIFT_CARD_CHECK } from "./action";
import GIFT_CARD_TYPE from "./type";

function* giftCardCheckSaga({ serial_number, store_id, giftCardType, client_receipt_id }) {
  console.log(giftCardType);
  yield put(giftCardCheckAddCodeAction(serial_number, giftCardType, client_receipt_id));
  let checkGiftApi;
  switch (giftCardType) {
    case GIFT_CARD_TYPE.HASAKI_GIFT: {
      checkGiftApi = checkHasakiGift;
      break;
    }
    case GIFT_CARD_TYPE.SODEXO: {
      checkGiftApi = checkSodexoGift;
      break;
    }
    case GIFT_CARD_TYPE.GOT_IT: {
      checkGiftApi = checkGoTitGift;
      break;
    }
  }

  const { status, data } = yield call(checkGiftApi, serial_number, store_id);
  if (status === 1) {
    if (giftCardType === GIFT_CARD_TYPE.SODEXO) {

    } else {
      const { value, valid, code } = Object.values(data.rows)[0];
      yield put(giftCardCheckUpdateCodeAction(serial_number, code, value, valid, client_receipt_id));
    }
  }
}

function* redeemGiftsSaga({ validGiftCodes, receipt_code, receipt_id, store_id, client_receipt_id }) {
  const multipleRedeemRequest = validGiftCodes.map(item => {
    switch (item.type) {
      case GIFT_CARD_TYPE.HASAKI_GIFT: {
        //return call(redeemHasakiGift, item.serial_number, receipt_code, store_id);
        return Promise.resolve(
          {
            "message": "All voucher redeem",
            "status": 1,
            "code": 200,
            "data": {
              "4244652892": {
                "code": "4e7efe64",
                "value": 100000,
                "valid": 1,
                "serial_number": "4244652892"
              }
            }
          }
        )
      }
      case GIFT_CARD_TYPE.GOT_IT: {
        //return call(redeemGotItGift, item.serial_number, receipt_id, receipt_code, store_id);
        return Promise.resolve({
          "message": "Used successfully.",
          "status": 1,
          "code": 200,
          "data": {
            "transaction_id": "ec74f2c630e711c2995ee47863bb1f72",
            "transaction_value": 100000,
            "valid": true
          }
        })
      }
      case GIFT_CARD_TYPE.SODEXO: {
        //return call(redeemSodexoGift, item.serial_number, receipt_id, receipt_code, store_id);
      }
    }
  });

  const listResponse = yield all(multipleRedeemRequest);
  const validRedeemGifts = listResponse.map((item, index) => {
    if (validGiftCodes[index].type === GIFT_CARD_TYPE.SODEXO) {
      if (item.data.valid) {
        return {
          serial_number: validGiftCodes[index].serial_number,
          transaction_id: item.data.transaction_id,
        }
      }
    } else if (validGiftCodes[index].type === GIFT_CARD_TYPE.HASAKI_GIFT) {
      if (item.status === 1) {
        return {
          serial_number: validGiftCodes[index].serial_number,
          transaction_id: '',
        }
      }
    } else if (validGiftCodes[index].type === GIFT_CARD_TYPE.GOT_IT) {
      if (item.status === 1) {
        return {
          serial_number: validGiftCodes[index].serial_number,
          transaction_id: item.data.transaction_id,
        }
      }
    }
  }).filter(item => item);
  yield put(finishRedeemGiftAction(validRedeemGifts, client_receipt_id));
}

function* giftCardWatcher() {
  yield all([
    takeLatest(GIFT_CARD_CHECK.REQUEST, giftCardCheckSaga),
    takeLatest(GIFT_CARD_CHECK.BEGIN_REDEEM, redeemGiftsSaga),
  ]);
}

export default giftCardWatcher;