import { all, put, takeLatest } from "@redux-saga/core/effects";
import { balancePaymentDone, BALANCE_PAYMENT } from "./action";

function* sendOptBalanceSaga({ opt }) {

  //Mock
  console.log({ opt });
}

function* balancePaymentWatcher() {
  yield all([
    takeLatest(BALANCE_PAYMENT.SEND, sendOptBalanceSaga),
  ]);
}

export default balancePaymentWatcher;