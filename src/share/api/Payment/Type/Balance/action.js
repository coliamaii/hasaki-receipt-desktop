export const BALANCE_PAYMENT = {
  INIT: 'INIT_BALANCE_PAYMENT',
  UPDATE: 'UPDATE_BALANCE_PAYMENT',
  SEND: 'SEND_OPT_BALANCE',
  DONE: 'BALANCE_PAYMENT_DONE',
  RESET: 'RESET_BALANCE_PAYMENT',
  REFRESH: 'REFRESH_BALANCE_PAYMENT',
}

export const initBalancePaymentAction = (amount, have_account_balance, client_receipt_id) => {
  return {
    type: BALANCE_PAYMENT.INIT,
    amount,
    have_account_balance,
    client_receipt_id,
  }
}

export const updateBalancePaymentAction = (amount, client_receipt_id) => {
  return {
    type: BALANCE_PAYMENT.UPDATE,
    amount,
    client_receipt_id,
  }
}

export const sendOptBalanceAction = (opt, client_receipt_id) => {
  return {
    type: BALANCE_PAYMENT.SEND,
    opt,
    client_receipt_id,
  }
}

export const balancePaymentDone = (transaction_id, client_receipt_id) => {
  return {
    type: BALANCE_PAYMENT.DONE,
    transaction_id,
    client_receipt_id,
  }
}

export const resetBalancePaymentAction = (client_receipt_id) => {
  return {
    type: BALANCE_PAYMENT.RESET,
    client_receipt_id,
  }
}

export const refreshBalancePaymentAction = (data) => {
  return {
    type: BALANCE_PAYMENT.REFRESH,
    data,
  }
}