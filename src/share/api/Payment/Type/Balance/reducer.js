import { BALANCE_PAYMENT } from "./action";

export const initialState = {
  amount: 0,
  balance: 0,
  current: 0,
  opt: null,
  transaction_id: null,
  isSend: false,
  isDone: false,
  have_account_balance: null,
}

const balancePaymentReducer = (state = initialState, action) => {
  switch (action.type) {
    case BALANCE_PAYMENT.INIT: {
      const { amount, have_account_balance, client_receipt_id } = action;

      const newState = {
        ...state,
        amount,
        balance: amount,
        have_account_balance
      };

      window.api.updateReceiptById(client_receipt_id, 'balance_payment', JSON.stringify(newState))
      return newState
    }
    case BALANCE_PAYMENT.UPDATE: {
      const { amount, client_receipt_id } = action;

      window.api.updateReceiptById(client_receipt_id, 'qr_payment', JSON.stringify({
        ...state,
        amount,
      }))
      return {
        ...state,
        amount,
      }
    }
    case BALANCE_PAYMENT.SEND: {
      const { opt, client_receipt_id } = action;

      const newState = {
        ...state,
        isSend: true,
        opt,
      }

      window.api.updateReceiptById(client_receipt_id, 'qr_payment', JSON.stringify(newState))
      return newState;
    }
    case BALANCE_PAYMENT.DONE: {
      const { transaction_id, client_receipt_id } = action;
      const newState = {
        ...state,
        transaction_id,
        isDone: true,
        current: state.amount,
      }
      window.api.updateReceiptById(client_receipt_id, 'qr_payment', JSON.stringify(newState))
      return newState;
    }
    case BALANCE_PAYMENT.RESET: {
      const { client_receipt_id } = action;

      window.api.updateReceiptById(client_receipt_id, 'qr_payment', JSON.stringify({
        ...initialState,
      }))
      return {
        ...initialState,
      }
    }
    case BALANCE_PAYMENT.REFRESH: {
      const { data } = action;
      return data;
    }
    default: {
      return {
        ...state,
      }
    }
  }
}

export default balancePaymentReducer;