import { all } from "@redux-saga/core/effects";
import paymentType from './Type/saga';
import paymentVAT from './VAT/saga';
import paymentSummary from './Summary/saga';

export default function* () {
  yield all([
    paymentSummary(),
    paymentType(),
    paymentVAT(),
  ]);
}