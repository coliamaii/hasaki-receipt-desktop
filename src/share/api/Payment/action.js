import { resetPaymentSummaryAction } from './Summary/action';
import { resetBalancePaymentAction } from './Type/Balance/action';
import { resetCardPaymentAction } from './Type/Card/action';
import { resetGiftCardCheckAction } from './Type/GiftCard/action';
import { qrPayResetAction } from './Type/QrPay/action';
import { resetVatAction } from './VAT/action';

export const resetPaymentAction = (dispatch, client_receipt_id) => {
  dispatch(resetBalancePaymentAction(client_receipt_id));
  dispatch(resetCardPaymentAction(client_receipt_id));
  dispatch(resetGiftCardCheckAction(client_receipt_id));
  dispatch(qrPayResetAction(client_receipt_id));
  dispatch(resetVatAction(client_receipt_id));
  dispatch(resetPaymentSummaryAction(client_receipt_id));
}