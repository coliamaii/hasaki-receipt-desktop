import { combineReducers } from "redux";
import PaymentTypeReducer from "./Type/reducer";
import SummaryPaymentReducer from "./Summary/reducer";
import vatReducer from "./VAT/reducer";

const paymentReducer = combineReducers({
  summary: SummaryPaymentReducer,
  type: PaymentTypeReducer,
  vat: vatReducer,
});

export default paymentReducer;