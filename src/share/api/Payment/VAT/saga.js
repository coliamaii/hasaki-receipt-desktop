import { call, put, takeLatest } from "@redux-saga/core/effects";
import { parseRegistrationNumber } from "apis/sales/customer";
import { vatPaymentSuccessAction, VAT_PAYMENT } from './action';

function* checkVATSaga({ regCode, client_receipt_id }) {
  try {
    const { status, data: { info } } = yield call(parseRegistrationNumber, regCode);
    if (status === 1) {
      const { company_name, contact_name, address } = info;
      yield put(vatPaymentSuccessAction(regCode, company_name, contact_name, address, client_receipt_id));
    }
  } catch (err) {

  }
}

function* VATPaymentWatcher() {
  yield takeLatest(VAT_PAYMENT.CHECK, checkVATSaga);
}

export default VATPaymentWatcher;