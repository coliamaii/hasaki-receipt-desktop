export const VAT_PAYMENT = {
  CHECK: 'VAT_PAYMENT_CHECK',
  SUCCESS: 'VAT_PAYMENT_SUCCESS',
  DONE: 'VAT_PAYMENT_DONE',
  RESET: 'VAT_RESET',
  REFRESH: 'VAT_REFRESH',
}

export const checkVATPaymentAction = (regCode, amount, client_receipt_id) => {
  return {
    type: VAT_PAYMENT.CHECK,
    regCode,
    amount,
    client_receipt_id,
  }
}

export const vatPaymentSuccessAction = (regCode, companyName, contactName, address, client_receipt_id) => {
  return {
    type: VAT_PAYMENT.SUCCESS,
    regCode,
    companyName,
    contactName,
    address,
    client_receipt_id,
  }
}

export const vatPaymentDoneAction = (regCode, companyName, address, email, client_receipt_id) => {
  return {
    type: VAT_PAYMENT.DONE,
    regCode,
    companyName,
    address,
    email,
    client_receipt_id,
  }
}

export const resetVatAction = (client_receipt_id) => {
  return {
    type: VAT_PAYMENT.RESET,
    client_receipt_id,
  }
}

export const refreshVatAction = (data) => {
  return {
    type: VAT_PAYMENT.REFRESH,
    data,
  }
}