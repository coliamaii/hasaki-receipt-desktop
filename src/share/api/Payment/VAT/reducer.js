import { VAT_PAYMENT } from "./action";

export const initialState = {
  regCode: '',
  companyName: '',
  contactName: '',
  address: '',
  email: '',
  amount: 0,
  isLoading: false,
  isDone: false,
}

const vatReducer = (state = initialState, action) => {
  switch (action.type) {
    case VAT_PAYMENT.CHECK: {
      const { amount, client_receipt_id } = action;

      const newState = {
        ...state,
        isLoading: true,
        amount,
      }

      window.api.updateReceiptById(client_receipt_id, 'vat', JSON.stringify(newState));
      return newState;
    }
    case VAT_PAYMENT.SUCCESS: {
      const { regCode, companyName, contactName, address, client_receipt_id } = action;
      const newState = {
        ...state,
        regCode,
        companyName,
        contactName,
        address,
        isLoading: false,
      }
      window.api.updateReceiptById(client_receipt_id, 'vat', JSON.stringify(newState));
      return newState;
    }
    case VAT_PAYMENT.DONE: {
      const { regCode, companyName, address, email, client_receipt_id } = action;
      const newState = {
        ...state,
        regCode,
        companyName,
        address,
        email,
        isDone: true,
      }

      window.api.updateReceiptById(client_receipt_id, 'vat', JSON.stringify(newState));
      return newState;
    }
    case VAT_PAYMENT.RESET: {
      const { client_receipt_id } = action;
      window.api.updateReceiptById(client_receipt_id, 'vat', JSON.stringify({
        ...initialState,
      }));
      return {
        ...initialState,
      }
    }
    case VAT_PAYMENT.REFRESH: {
      const { data } = action;
      return data;
    }
    default: {
      return {
        ...state,
      }
    }
  }
}

export default vatReducer;