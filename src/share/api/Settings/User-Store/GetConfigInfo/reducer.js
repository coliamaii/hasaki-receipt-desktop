import { GET_STORE_CONFIG_INFO } from "./actions";

const initialState = {};

export function storeConfigInfoReducer(state = initialState, action) {
  switch (action.type) {
    case GET_STORE_CONFIG_INFO.REQUEST: {
      return {
        ...state,
      };
    }
    case GET_STORE_CONFIG_INFO.SUCCESS: {
      return {
        ...state,
        ...action.data,
      };
    }

    default: {
      return {
        ...state,
      };
    }
  }
}

export default storeConfigInfoReducer;
