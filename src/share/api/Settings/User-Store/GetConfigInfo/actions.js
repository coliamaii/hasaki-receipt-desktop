export const GET_STORE_CONFIG_INFO = {
  REQUEST: "API_GET_STORE_CONFIG_INFO_REQUEST",
  SUCCESS: "API_GET_STORE_CONFIG_INFO_SUCCESS",
  ERROR: "API_GET_STORE_CONFIG_INFO_ERROR",
};

export function getStoreConfigInfoRequestAction(id) {
  return {
    type: GET_STORE_CONFIG_INFO.REQUEST,
    id,
  };
}

export function getStoreConfigInfoSuccessAction(data) {
  return {
    type: GET_STORE_CONFIG_INFO.SUCCESS,
    data,
  };
}

export function getStoreConfigInfoErrorAction() {
  return {
    type: GET_STORE_CONFIG_INFO.ERROR,
  };
}
