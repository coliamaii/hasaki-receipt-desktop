import { getStoreConfigInfo } from "apis/setting/store";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  getStoreConfigInfoErrorAction,
  getStoreConfigInfoSuccessAction,
  GET_STORE_CONFIG_INFO,
} from "./actions";

function* getStoreConfigInfoSaga(action) {
  try {
    const { status, data } = yield call(getStoreConfigInfo, action.id);

    console.log("getStoreConfigInfoSaga")
    console.log(data)
    if (status === 1) {
      yield put(getStoreConfigInfoSuccessAction(data));
    } else {
      yield put(getStoreConfigInfoErrorAction());
    }
  } catch (err) {
    yield put(getStoreConfigInfoErrorAction());
  }
}

function* getStoreConfigInfoWatcher() {
  yield takeLatest(GET_STORE_CONFIG_INFO.REQUEST, getStoreConfigInfoSaga);
}
export default getStoreConfigInfoWatcher;
