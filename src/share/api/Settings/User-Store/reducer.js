import { combineReducers } from "redux";
import storeListReducer from "./GetList/reducer";
import storeConfigInfoReducer from "./GetConfigInfo/reducer";

const userReducer = combineReducers({
  list: storeListReducer,
  config: storeConfigInfoReducer,
});

export default userReducer;
