import { all } from "redux-saga/effects";
import getStoreListWatcher from "./GetList/saga";
import getStoreConfigInfoWatcher from "./GetConfigInfo/saga";

export default function* () {
  yield all([
    getStoreListWatcher(),
    getStoreConfigInfoWatcher(),
  ]);
}
