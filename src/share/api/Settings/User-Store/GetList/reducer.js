import { GET_LIST_STORE } from "./actions";

const initialState = {
  rows: [],
};

export function storeListReducer(state = initialState, action) {
  switch (action.type) {
    case GET_LIST_STORE.REQUEST: {
      return {
        ...state,
      };
    }
    case GET_LIST_STORE.SUCCESS: {
      return {
        ...state,
        rows: action.data.rows,
      };
    }

    default: {
      return {
        ...state,
      };
    }
  }
}

export default storeListReducer;