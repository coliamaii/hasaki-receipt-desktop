export const GET_LIST_STORE = {
  REQUEST: "API_GET_LIST_STORE_REQUEST",
  SUCCESS: "API_GET_LIST_STORE_SUCCESS",
  ERROR: "API_GET_LIST_STORE_ERROR",
};

export function getListStoreRequestAction(params) {
  return {
    type: GET_LIST_STORE.REQUEST,
    params,
  };
}

export function getListStoreSuccessAction(data) {
  return {
    type: GET_LIST_STORE.SUCCESS,
    data,
  };
}

export function getListStoreErrorAction() {
  return {
    type: GET_LIST_STORE.ERROR,
  };
}
