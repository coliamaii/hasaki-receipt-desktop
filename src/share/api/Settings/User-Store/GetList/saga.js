import { getList } from "apis/setting/store";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  getListStoreErrorAction,
  getListStoreSuccessAction,
  GET_LIST_STORE,
} from "./actions";

function* getListStoreSaga(action) {
  try {
    const { status, data } = yield call(getList, action.params);

    if (status === 1) {
      yield put(getListStoreSuccessAction(data));
    } else {
      yield put(getListStoreErrorAction());
    }
  } catch (err) {
    yield put(getListStoreErrorAction());
  }
}

function* getListStoreWatcher() {
  yield takeLatest(GET_LIST_STORE.REQUEST, getListStoreSaga);
}
export default getListStoreWatcher;
