import { combineReducers } from "redux";
import searchUserReducer from "./SearchUser/reducer";
import uploadReducer from "./Upload/reducer";
import storeReducer from "./User-Store/reducer";

const accountingReducer = combineReducers({
  searchUser: searchUserReducer,
  store: storeReducer,
  upload: uploadReducer,
});

export default accountingReducer;
