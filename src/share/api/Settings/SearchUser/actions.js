export const GET_SEARCH_LIST_USER = {
  REQUEST: "API_GET_SEARCH_LIST_USER_REQUEST",
  SUCCESS: "API_GET_SEARCH_LIST_USER_SUCCESS",
  ERROR: "API_GET_SEARCH_LIST_USER_ERROR",
};

export function getSearchListUserRequestAction(params) {
  return {
    type: GET_SEARCH_LIST_USER.REQUEST,
    params,
  };
}

export function getSearchListUserSuccessAction(data) {
  return {
    type: GET_SEARCH_LIST_USER.SUCCESS,
    data,
  };
}

export function getSearchListUserErrorAction() {
  return {
    type: GET_SEARCH_LIST_USER.ERROR,
  };
}
