import { GET_SEARCH_LIST_USER } from "./actions";

const initialState = {
  rows: [],
};

export function searchUserReducer(state = initialState, action) {
  switch (action.type) {
    case GET_SEARCH_LIST_USER.REQUEST: {
      return {
        ...state,
      };
    }
    case GET_SEARCH_LIST_USER.SUCCESS: {
      return {
        ...state,
        rows: action.data.rows,
      };
    }

    default: {
      return {
        ...state,
      };
    }
  }
}

export default searchUserReducer;