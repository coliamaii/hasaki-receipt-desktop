import { getList } from "apis/setting/user";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  getSearchListUserErrorAction,
  getSearchListUserSuccessAction,
  GET_SEARCH_LIST_USER,
} from "./actions";

function* getSearchListUserSaga(action) {
  try {
    const { status, data } = yield call(getList, action.params);

    if (status === 1) {
      yield put(getSearchListUserSuccessAction(data));
    } else {
      yield put(getSearchListUserErrorAction());
    }
  } catch (err) {
    yield put(getSearchListUserErrorAction());
  }
}

function* getSearchListUserWatcher() {
  yield takeLatest(GET_SEARCH_LIST_USER.REQUEST, getSearchListUserSaga);
}
export default getSearchListUserWatcher;
