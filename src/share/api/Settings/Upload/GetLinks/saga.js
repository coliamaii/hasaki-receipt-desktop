import { getLinks } from "apis/setting/upload";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  getLinksUploadErrorAction,
  getLinksUploadSuccessAction,
  GET_LINKS_UPLOAD,
} from "./actions";

function* getLinksUploadSaga(action) {
  try {
    const { status, data } = yield call(getLinks, action.params);

    if (status === 1) {
      yield put(getLinksUploadSuccessAction(data));
    } else {
      yield put(getLinksUploadErrorAction());
    }
  } catch (err) {
    yield put(getLinksUploadErrorAction());
  }
}

function* getLinksUploadWatcher() {
  yield takeLatest(GET_LINKS_UPLOAD.REQUEST, getLinksUploadSaga);
}
export default getLinksUploadWatcher;
