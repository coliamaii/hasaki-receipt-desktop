export const GET_LINKS_UPLOAD = {
  REQUEST: "API_GET_LINKS_UPLOAD_REQUEST",
  SUCCESS: "API_GET_LINKS_UPLOAD_SUCCESS",
  ERROR: "API_GET_LINKS_UPLOAD_ERROR",
};

export function getLinksUploadRequestAction(params) {
  return {
    type: GET_LINKS_UPLOAD.REQUEST,
    params,
  };
}

export function getLinksUploadSuccessAction(data) {
  return {
    type: GET_LINKS_UPLOAD.SUCCESS,
    data,
  };
}

export function getLinksUploadErrorAction() {
  return {
    type: GET_LINKS_UPLOAD.ERROR,
  };
}
