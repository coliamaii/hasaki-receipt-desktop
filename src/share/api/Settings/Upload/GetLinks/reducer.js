import { GET_LINKS_UPLOAD } from "./actions";

const initialState = {
  isLoading: false,
  rows: [],
};

export function linksUploadReducer(state = initialState, action) {
  switch (action.type) {
    case GET_LINKS_UPLOAD.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_LINKS_UPLOAD.SUCCESS: {
      return {
        ...state,
        isLoading: false,
        ...action.data,
      };
    }

    default: {
      return {
        ...state,
      };
    }
  }
}

export default linksUploadReducer;