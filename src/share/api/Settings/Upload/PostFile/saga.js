import { postFile } from "apis/setting/upload";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  postFileUploadErrorAction,
  postFileUploadSuccessAction,
  POST_FILE_UPLOAD,
} from "./actions";

function* postFileUploadSaga(action) {
  console.log("upload");
  console.log(action);
  try {
    const { status, data } = yield call(postFile, action.formData);
    console.log("status")
    console.log(status);
    console.log("data")
    console.log(data);
    if (status === 1) {
      yield put(postFileUploadSuccessAction(data));
    } else {
      yield put(postFileUploadErrorAction());
    }
  } catch (err) {
    yield put(postFileUploadErrorAction());
  }
}

function* postFileUploadWatcher() {
  yield takeLatest(POST_FILE_UPLOAD.REQUEST, postFileUploadSaga);
}
export default postFileUploadWatcher;
