export const POST_FILE_UPLOAD = {
  REQUEST: "API_POST_FILE_UPLOAD_REQUEST",
  SUCCESS: "API_POST_FILE_UPLOAD_SUCCESS",
  ERROR: "API_POST_FILE_UPLOAD_ERROR",
};

export function postFileUploadRequestAction(formData) {
  return {
    type: POST_FILE_UPLOAD.REQUEST,
    formData,
  };
}

export function postFileUploadSuccessAction(data) {
  return {
    type: POST_FILE_UPLOAD.SUCCESS,
    data,
  };
}

export function postFileUploadErrorAction() {
  return {
    type: POST_FILE_UPLOAD.ERROR,
  };
}
