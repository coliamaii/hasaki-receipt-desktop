import { POST_FILE_UPLOAD } from "./actions";

const initialState = {
  isLoading: false,
  rows: {},
};

export function fileUploadReducer(state = initialState, action) {
  switch (action.type) {
    case POST_FILE_UPLOAD.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case POST_FILE_UPLOAD.SUCCESS: {
      return {
        ...state,
        isLoading: false,
        // rows: {...action.data},
        ...action.data
      };
    }

    default: {
      return {
        ...state,
      };
    }
  }
}

export default fileUploadReducer;