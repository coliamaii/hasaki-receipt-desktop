import { all } from "redux-saga/effects";
import getLinksUploadWatcher from "./GetLinks/saga";
import postFileUploadWatcher from './PostFile/saga'

export default function* () {
  yield all([
    getLinksUploadWatcher(),
    postFileUploadWatcher(),
  ]);
}
