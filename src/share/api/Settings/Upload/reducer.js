import { combineReducers } from "redux";
import linksUploadReducer from "./GetLinks/reducer";
import fileUploadReducer from "./PostFile/reducer";

const uploadReducer = combineReducers({
  linksUpload: linksUploadReducer,
  fileUpload: fileUploadReducer,
});

export default uploadReducer;
