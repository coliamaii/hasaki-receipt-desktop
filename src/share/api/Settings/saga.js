import { all } from "redux-saga/effects";
import getSearchListUserWatcher from "./SearchUser/saga";
import uploadWatcher from "./Upload/saga";
import getListStoreWatcher from "./User-Store/saga";

export default function* () {
  yield all([
    getSearchListUserWatcher(),
    getListStoreWatcher(),
    uploadWatcher(),
  ]);
}
