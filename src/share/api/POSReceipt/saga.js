import { all, call, delay, put, takeLatest } from "@redux-saga/core/effects";
import { completePOS, insertPOS } from "apis/sales/payment";
import { getDetailOnlineReceiptRequestAction } from "../Profile/Receipt/GetDetail/actions";
import {
  addRemoteReceiptIdAction,
  finishPosReceiptAction,
  POS_RECEIPT,
} from "./action";
//import { previewReceiptRequestAction } from "../Profile/Receipt/Print/actions";
import { Modal } from "antd";

function* getRemoteReceiptIdSaga({ raw, client_receipt_id }) {
  try {
    const { status, data } = yield call(insertPOS, raw);
    if (status === 1) {
      const {
        receipt: { receipt_code, receipt_id, receipt_cdate, receipt_udate },
      } = data;

      yield window.api.updateReceiptById(client_receipt_id, 'remote_receipt_id', receipt_id);
      yield window.api.updateReceiptById(client_receipt_id, 'remote_receipt_code', receipt_code);
      yield window.api.updateReceiptById(client_receipt_id, 'remote_cdate', receipt_cdate);
      yield window.api.updateReceiptById(client_receipt_id, 'remote_udate', receipt_udate);

      yield put(
        addRemoteReceiptIdAction(
          receipt_id,
          receipt_code,
          receipt_cdate,
          receipt_udate
        )
      );
    }
  } catch (err) {
    console.log(err);
  }
}

function* paymentSaga({ completePaymentData, callbackRedirect }) {
  try {
    console.log("completePaymentData");
    console.log(completePaymentData);
    const { status, data, message } = yield call(completePOS, completePaymentData);
    if (status === 1) {
      yield delay(2000);
      yield put(
        getDetailOnlineReceiptRequestAction(
          { code: data.receipt.receipt_code },
          callbackRedirect
        )
      );
      yield put(finishPosReceiptAction());
    }
    else {
      Modal.warning({
        title: 'Notification',
        content: message,
      })
    }
  } catch (err) {
    console.log(err);
  }
}

function* posReceiptWatcher() {
  yield all([
    takeLatest(POS_RECEIPT.GET_REMOTE_ID, getRemoteReceiptIdSaga),
    takeLatest(POS_RECEIPT.PAYMENT, paymentSaga),
  ]);
}

export default posReceiptWatcher;
