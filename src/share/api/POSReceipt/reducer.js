import { POS_RECEIPT } from "./action";

export const initialState = {
  client_receipt_id: null,
  remote_receipt_id: null,
  remote_receipt_code: null,
  receipt_udate: null,
  receipt_cdate: null,
  isLoading: false,
  isComplete: false,
  isPrint: false,
};

const receiptReducer = (state = initialState, action) => {
  switch (action.type) {
    case POS_RECEIPT.PAYMENT: {
      return {
        ...state,
        isPrint: true,
      };
    }
    case POS_RECEIPT.FINISH: {
      return {
        ...state,
        isPrint: false,
      };
    }
    case POS_RECEIPT.NEW: {
      const { client_receipt_id } = action;
      return {
        ...state,
        client_receipt_id,
      };
    }
    case POS_RECEIPT.GET_REMOTE_ID: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case POS_RECEIPT.ADD_REMOTE_ID: {
      const {
        remote_receipt_id,
        remote_receipt_code,
        receipt_cdate,
        receipt_udate,
      } = action;
      return {
        ...state,
        remote_receipt_id,
        remote_receipt_code,
        receipt_cdate,
        receipt_udate,
        isLoading: false,
      };
    }
    case POS_RECEIPT.FINISH: {
      return {
        ...state,
        isComplete: true,
      };
    }
    case POS_RECEIPT.RESET: {
      return {
        ...initialState,
      };
    }
    case POS_RECEIPT.SET_LOADING: {
      const { isLoading } = action;
      return {
        ...state,
        isLoading,
      }
    }
    case POS_RECEIPT.REFRESH: {
      const { receipt } = action;
      return receipt;
    }
    default: {
      return {
        ...state,
      };
    }
  }
};

export default receiptReducer;
