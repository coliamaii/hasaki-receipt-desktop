export const POS_RECEIPT = {
  NEW: 'NEW_POS_RECEIPT',
  GET_REMOTE_ID: 'GET_REMOTE_ID_RECEIPT',
  ADD_REMOTE_ID: 'ADD_REMOTE_ID_RECEIPT',
  PAYMENT: 'PAYMENT_POS_RECEIPT',
  FINISH: 'FINISH_POS_RECEIPT',
  RESET: 'RESET_POS_RECEIPT',
  SET_LOADING: 'SET_LOADING_POS_RECEIPT',
  REFRESH: 'REFRESH_POS_RECEIPT',
}

export const newPOSReceiptAction = (client_receipt_id) => {
  return {
    type: POS_RECEIPT.NEW,
    client_receipt_id,
  }
}

export const getRemoteReceiptIdAction = (raw, client_receipt_id) => {
  return {
    type: POS_RECEIPT.GET_REMOTE_ID,
    raw,
    client_receipt_id,
  }
}

export const addRemoteReceiptIdAction = (remote_receipt_id, remote_receipt_code, receipt_cdate, receipt_udate) => {
  return {
    type: POS_RECEIPT.ADD_REMOTE_ID,
    remote_receipt_id,
    remote_receipt_code,
    receipt_cdate,
    receipt_udate,
  }
}

export const paymentPosReceiptAction = (completePaymentData, callbackRedirect) => {
  return {
    type: POS_RECEIPT.PAYMENT,
    completePaymentData,
    callbackRedirect,
  }
}

export const finishPosReceiptAction = () => {
  return {
    type: POS_RECEIPT.FINISH,
  }
}

export const resetPosReceiptAction = () => {
  return {
    type: POS_RECEIPT.RESET,
  }
}

export const setPosReceiptLoadingAction = (isLoading) => {
  return {
    type: POS_RECEIPT.SET_LOADING,
    isLoading,
  }
}

export const refreshPosReceiptAction = (receipt) => {
  return {
    type: POS_RECEIPT.REFRESH,
    receipt,
  }
}