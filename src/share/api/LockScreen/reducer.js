import { CHECK_USER } from "./CheckUser/actions";

const initialState = {
  isLoading: false,
  isLocked: false,
  checkedUser: {},
  err: null,
};

export function lockScreenReducer(state = initialState, action) {
  switch (action.type) {
    case CHECK_USER.REQUEST: {
      return {
        ...state,
        isLoading: true,
        err: null,
      };
    }

    case CHECK_USER.SUCCESS: {
      localStorage.setItem("isLocked", JSON.stringify(action.data.isLocked));
      return {
        ...state,
        isLoading: false,
        isLocked: action.data.isLocked,
        err: null,
      };
    }

    case CHECK_USER.ERROR: {
      return {
        ...state,
        isLoading: false,
        err: action.errors.err,
      };
    }

    default: {
      return {
        ...state,
      };
    }
  }
}

export default lockScreenReducer;
