import { all } from "redux-saga/effects";
import checkUserWatcher from "./CheckUser/saga";

export default function* () {
  yield all([
    checkUserWatcher(),
  ]);
}
