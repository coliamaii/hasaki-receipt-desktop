export const CHECK_USER = {
  REQUEST: "API_CHECK_USER_REQUEST",
  SUCCESS: "API_CHECK_USER_SUCCESS",
  ERROR: "API_CHECK_USER_ERROR",
};

export function checkUserRequestAction(params) {
  return {
    type: CHECK_USER.REQUEST,
    params,
  };
}

export function checkUserSuccessAction(data) {
  return {
    type: CHECK_USER.SUCCESS,
    data,
  };
}

export function checkUserErrorAction(errors) {
  return {
    type: CHECK_USER.ERROR,
    errors,
  };
}
