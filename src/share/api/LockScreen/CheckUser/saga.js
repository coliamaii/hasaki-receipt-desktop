import { login } from "apis/auth";
import { call, put, select, takeLatest } from "redux-saga/effects";
import {
  checkUserErrorAction,
  checkUserSuccessAction,
  CHECK_USER,
} from "./actions";

export const getUserId = (state) => state.auth.info.profile.id;

function* checkUserSaga(action) {
  try {
    const responseUser = yield call(login, action.params);

    if (responseUser.status) {
      let user_id = yield select(getUserId);
      if (user_id === responseUser.data.user_id) {
        yield put(checkUserSuccessAction({ isLocked: false }));
      } else {
        yield put(checkUserErrorAction({ err: "Sai mật khẩu" }));
      }
    } else {
      yield put(checkUserErrorAction({ err: responseUser.message }));
    }
  } catch (err) {
    yield put(checkUserErrorAction({ err }));
  }
}
function* checkUserWatcher() {
  yield takeLatest(CHECK_USER.REQUEST, checkUserSaga);
}
export default checkUserWatcher;
