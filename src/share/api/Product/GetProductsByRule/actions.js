export const GET_PRODUCTS_BY_RULE = {
  REQUEST: "API_GET_PRODUCT_BY_RULE_REQUEST",
  LOADING: "API_GET_PRODUCT_BY_RULE_LOADING",
  SUCCESS: "API_GET_PRODUCT_BY_RULE_SUCCESS",
  CLEAR: "GET_PRODUCT_BY_RULE_CLEAR",
  ERROR: "API_GET_PRODUCT_BY_RULE_ERROR",
  CHECK_EXPIRED: "API_GET_PRODUCT_BY_RULE_CHECK_EXPIRED",
  ADD_GIFT: "API_GET_PRODUCT_BY_RULE_ADD_GIFT",
  REFRESH_GIFT: "API_GET_PRODUCT_BY_RULE_REFRESH_GIFT",
  ADD_EXPIRED_ITEM: "GET_PRODUCT_BY_RULE_ADD_EXPIRED_ITEM",
  REMOVE_EXPIRED_ITEM: "GET_PRODUCT_BY_RULE_REMOVE_EXPIRED_ITEM",
  REFRESH_EXPIRED_ITEM: "GET_PRODUCT_BY_RULE_REFRESH_EXPIRED_ITEM",
  ADD_CODE: "GET_PRODUCT_BY_RULE_ADD_CODE",
  REFRESH_CODE: "GET_PRODUCT_BY_RULE_REFRESH_CODE",
  OFFLINE_REQUEST: "API_GET_PRODUCT_BY_RULE_OFFLINE_REQUEST",
  // OFFLINE_VIEW_DETAIL_REQUEST: 'API_GET_PRODUCT_BY_RULE_OFFLINE_VIEW_DETAIL_REQUEST',
};

export const getProductPricesByRuleRequestAction = (
  raw,
  lastest_sku,
  client_receipt_id
) => {
  return {
    type: GET_PRODUCTS_BY_RULE.REQUEST,
    raw,
    lastest_sku,
    client_receipt_id,
  };
};

export const getProductPricesByRuleLoadingAction = (isLoading) => {
  return {
    type: GET_PRODUCTS_BY_RULE.LOADING,
    isLoading,
  };
};

export const getProductPricesByRuleSuccessAction = (data, combos) => {
  return {
    type: GET_PRODUCTS_BY_RULE.SUCCESS,
    data,
    combos,
  };
};

export const getProductPricesByRuleClearAction = () => {
  return {
    type: GET_PRODUCTS_BY_RULE.CLEAR,
  };
};

export const getProductByRuleCheckExpiredAction = (sku) => {
  return {
    type: GET_PRODUCTS_BY_RULE.CHECK_EXPIRED,
    sku,
  };
};

export const getProductByRuleAddGiftAction = (gift, giftIndex, client_receipt_id) => {
  return {
    type: GET_PRODUCTS_BY_RULE.ADD_GIFT,
    gift,
    giftIndex,
    client_receipt_id,
  };
};

export const getProductByRuleRefreshGiftAction = (gift_items) => {
  return {
    type: GET_PRODUCTS_BY_RULE.REFRESH_GIFT,
    gift_items,
  };
};

export const getProductByRuleAddExpiredItem = (sku, expired_date, client_receipt_id) => {
  return {
    type: GET_PRODUCTS_BY_RULE.ADD_EXPIRED_ITEM,
    sku,
    expired_date,
    client_receipt_id,
  };
};

export const getProductByRuleRemoveExpiredItem = (sku) => {
  return {
    type: GET_PRODUCTS_BY_RULE.REMOVE_EXPIRED_ITEM,
    sku,
  };
};

export const getProductByRuleRefreshExpiredItem = (expired_items) => {
  return {
    type: GET_PRODUCTS_BY_RULE.REFRESH_EXPIRED_ITEM,
    expired_items,
  }
}

export const getProductByRuleAddCodeAction = (sku, code, client_receipt_id) => {
  return {
    type: GET_PRODUCTS_BY_RULE.ADD_CODE,
    sku,
    code,
    client_receipt_id,
  };
};

export const getProductByRuleRefreshCodeAction = (codes) => {
  return {
    type: GET_PRODUCTS_BY_RULE.REFRESH_CODE,
    codes,
  }
}

export const getProductByRuleOfflineRequestAction = (
  raw,
  lastest_sku,
  client_receipt_id
) => {
  return {
    type: GET_PRODUCTS_BY_RULE.OFFLINE_REQUEST,
    raw,
    lastest_sku,
    client_receipt_id,
  };
};
