import { Modal } from "antd";
import { product_rule } from "apis/sales/rule";
import { all, call, put, takeLatest } from "redux-saga/effects";
import { setPosReceiptLoadingAction } from "share/api/POSReceipt/action";
import {
  searchProductAddGiftsAction,
  searchProductClearGiftsAction,
  searchProductClearAction,
} from "../SearchProduct/actions";
import { SEARCH_TYPE } from "../SearchProduct/search-type";
import {
  getProductByRuleCheckExpiredAction,
  getProductPricesByRuleLoadingAction,
  getProductPricesByRuleSuccessAction,
  GET_PRODUCTS_BY_RULE,
} from "./actions";

function* checkExpired(lastest_sku, formatData) {
  if (lastest_sku) {
    const lastestItemIndex = yield formatData.findIndex((item) => {
      return item.sku == lastest_sku;
    });
    const lastest_product = yield formatData[lastestItemIndex].product_config;
    if ((lastest_product & 2) === 2) {
      yield put(getProductByRuleCheckExpiredAction(lastest_sku));
    }
  }
}

function* getProductSaga({ raw, lastest_sku, client_receipt_id }) {
  try {
    yield put(getProductPricesByRuleLoadingAction(true));
    const { data, status } = yield call(product_rule, { raw });


    if (status === 1) {
      let total_price = 0;
      const formatData = yield data.rows.map((item) => {
        const sku = item.sku;
        if (data.products[sku]?.product_barcode === undefined) {
          alert(
            "Product barcode is not existed in system! Please contact to tech for support!"
          );
          //then break, switch to catch err
        }
        const name = data.products[sku].product_name;
        const base_price = item.price;
        const discount = item.discount;
        const discount_price = base_price - discount;
        const qty = item.qty;
        const product_config = data.products[sku].product_config;
        const product_brand_id = data.products[sku].product_brand_id;
        const lineTotal = qty * discount_price;
        const rule_id = item.rule_id;
        const isBabyMomSpaCate = item.is_baby_mom_spa_cate === 1;
        const is_deal = item.is_deal === 1;

        total_price += discount_price * qty;

        return {
          sku,
          name,
          base_price,
          discount_price,
          discount,
          qty,
          product_config,
          lineTotal,
          product_brand_id,
          isBabyMomSpaCate,
          is_deal,
          rule_id,
        };
      });


      const empty_price_index = yield formatData.findIndex((item) => {
        return item.base_price === null;
      });
      if (empty_price_index !== -1) {
        alert(
          `Sản phẩm ${formatData[empty_price_index].name} (${formatData[empty_price_index].sku}) không thể bán do chưa có giá. Vui lòng giữ lại sp và liên hệ quản lý!`
        );
        formatData.splice(empty_price_index, 1);
      }

      const combos = data.combos
        .filter(combo => !isNaN(combo.sku))
        .map((combo) => ({
          combo_sku: combo.sku,
          combo_price: combo.price,
          combo_details: combo.combo_details,
          combo_name: data.products[combo.sku].product_name,
        }));

      yield put(getProductPricesByRuleSuccessAction(formatData, combos));

      if (!Array.isArray(data.gifts)) {
        const formatGifts = Object.entries(data.gifts).map((item) => {
          const sku = item[0];
          const name = data.products[sku].product_name;
          const unit_cost = data.gifts[sku].price;
          const qty = data.gifts[sku].qty;
          return { sku, name, unit_cost, qty };
        });
        yield put(searchProductAddGiftsAction(formatGifts));
      } else {
        yield put(searchProductClearGiftsAction());
      }

      yield checkExpired(lastest_sku, formatData);

      // insert product list data to "receipt_details" talble in localDB
      if (client_receipt_id) {
        yield call(window.api.addNewDataIntoReceiptDetailTable, {
          receipt_id: client_receipt_id,
          products: formatData,
        });
      }

      // update data by id to "receipts" talble in localDB
      // yield call(window.api.updateDataByIdFromReceiptTable, {
      //   ...localData,
      //   discount: formatData.discount_price,
      //   total_price,
      //   status: 1,
      // });

      yield put(searchProductClearAction(SEARCH_TYPE.SEARCH_DATA));
      yield put(setPosReceiptLoadingAction(false))
    } else {
      alert("Error! Please contact technical for supporting!");
      yield put(getProductPricesByRuleLoadingAction(false));
      yield put(searchProductClearAction(SEARCH_TYPE.SEARCH_DATA));
    }
  } catch (err) {
    Modal.error({
      title: 'Có lỗi xảy ra!',
    })
    yield put(getProductPricesByRuleLoadingAction(false));
    yield put(searchProductClearAction(SEARCH_TYPE.SEARCH_DATA));
  }
}

function* getProductInOfflineSaga({ raw, lastest_sku, client_receipt_id }) {
  try {
    const { items } = raw;
    yield put(getProductPricesByRuleLoadingAction(true));

    const { products, combos } = yield call(window.api.getProductPriceByRule, {
      items: items,
    });

    const formatData = products.map((item) => {
      const sku = item.product_sku;
      const name = item.product_name;
      const base_price = 100000;
      const discount = 0;
      const discount_price = base_price - discount;
      const qty = item.qty;
      const product_config = item.product_config;
      const product_brand_id = item.product_brand;
      const lineTotal = qty * discount_price;

      return {
        sku,
        name,
        base_price,
        discount_price,
        discount,
        qty,
        product_config,
        lineTotal,
        product_brand_id,
      };
    });

    yield put(getProductPricesByRuleSuccessAction(formatData, combos));

    // add Product to receipt_details talble in localDB

    yield call(window.api.addNewDataIntoReceiptDetailTable, {
      receipt_id: client_receipt_id,
      products: formatData,
    });


    yield checkExpired(lastest_sku, formatData);

    yield put(searchProductClearAction(SEARCH_TYPE.SEARCH_DATA));
    yield put(getProductPricesByRuleLoadingAction(false));
  } catch (err) {
    yield put(getProductPricesByRuleLoadingAction(false));
    yield put(searchProductClearAction(SEARCH_TYPE.SEARCH_DATA));
  }
}

function* getProductsWatcher() {
  yield all([
    takeLatest(GET_PRODUCTS_BY_RULE.REQUEST, getProductSaga),
    takeLatest(GET_PRODUCTS_BY_RULE.OFFLINE_REQUEST, getProductInOfflineSaga),
  ]);
}

export default getProductsWatcher;
