import { GET_PRODUCTS_BY_RULE } from "./actions";

const initialState = {
  data: [],
  totalPrice: 0,
  totalItems: 0,
  gift_items: [],
  combo_suggestion: [],
  expired_items: {},
  codes: {},
  check_expired_items: null,
  loading: false,
};

function getProductByRuleReducer(state = initialState, action) {
  switch (action.type) {
    case GET_PRODUCTS_BY_RULE.LOADING: {
      const { isLoading } = action;
      return {
        ...state,
        loading: isLoading,
      };
    }
    case GET_PRODUCTS_BY_RULE.SUCCESS: {
      const { data, combos } = action;
      return {
        ...state,
        loading: false,
        totalPrice: data.reduce((total, item) => {
          return total + item.discount_price * item.qty;
        }, 0),
        totalItems: data.reduce((total, item) => {
          return total + item.qty;
        }, 0),
        data: JSON.parse(JSON.stringify(data)),
        combo_suggestion: combos,
      };
    }
    case GET_PRODUCTS_BY_RULE.CLEAR: {
      return {
        data: [],
        totalPrice: 0,
        totalItems: 0,
        gift_items: [],
        combo_suggestion: [],
        expired_items: {},
        loading: false,
        check_expired_items: null,
        codes: {},
      }
    }
    case GET_PRODUCTS_BY_RULE.CHECK_EXPIRED: {
      const { sku } = action;
      return {
        ...state,
        check_expired_items: sku,
      };
    }
    case GET_PRODUCTS_BY_RULE.ADD_GIFT: {
      const { gift, giftIndex, client_receipt_id } = action;
      const cloneData = [...state.gift_items];
      if (giftIndex === -1) {
        const gift_items_data = cloneData.concat({
          sku: gift.sku,
          name: gift.name,
          base_price: gift.unit_cost,
          discount_price: gift.unit_cost,
          discount: gift.unit_cost,
          qty: 1,
          lineTotal: gift.unit_cost,
        })
        window.api.updateReceiptById(client_receipt_id, 'gift_items', JSON.stringify(gift_items_data));
        return {
          ...state,
          gift_items: gift_items_data
        };
      } else {
        cloneData[giftIndex].qty++;
        window.api.updateReceiptById(client_receipt_id, 'gift_items', JSON.stringify(cloneData));
        return {
          ...state,
          gift_items: cloneData,
        };
      }
    }
    case GET_PRODUCTS_BY_RULE.REFRESH_GIFT: {
      const { gift_items } = action;
      return {
        ...state,
        gift_items,
      };
    }
    case GET_PRODUCTS_BY_RULE.ADD_EXPIRED_ITEM: {
      const { sku, expired_date, client_receipt_id } = action;
      const clone_expired_items = JSON.parse(
        JSON.stringify(state.expired_items)
      );
      !clone_expired_items[sku]
        ? (clone_expired_items[sku] = [expired_date])
        : clone_expired_items[sku].push(expired_date);
      window.api.updateReceiptById(client_receipt_id, 'expired_items', JSON.stringify(clone_expired_items));
      return {
        ...state,
        expired_items: clone_expired_items,
      };
    }
    case GET_PRODUCTS_BY_RULE.REFRESH_EXPIRED_ITEM: {
      const { expired_items } = action;
      return {
        ...state,
        expired_items,
      }
    }
    case GET_PRODUCTS_BY_RULE.REMOVE_EXPIRED_ITEM: {
      const { sku } = action;
      const clone_expired_items = JSON.parse(
        JSON.stringify(state.expired_items)
      );
      delete clone_expired_items[sku];
      return {
        ...state,
        expired_items: clone_expired_items,
      };
    }
    case GET_PRODUCTS_BY_RULE.ADD_CODE: {
      const { sku, code, client_receipt_id } = action;
      const cloneCodes = JSON.parse(JSON.stringify(state.codes));
      !cloneCodes[sku]
        ? (cloneCodes[sku] = [code])
        : cloneCodes[sku].push(code);
      window.api.updateReceiptById(client_receipt_id, 'codes', JSON.stringify(cloneCodes));
      return {
        ...state,
        codes: cloneCodes,
      };
    }
    case GET_PRODUCTS_BY_RULE.REFRESH_CODE: {
      const { codes } = action;
      return {
        ...state,
        codes,
      }
    }
    default: {
      return {
        ...state,
      };
    }
  }
}

export default getProductByRuleReducer;
