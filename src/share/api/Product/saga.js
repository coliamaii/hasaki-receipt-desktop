import { all } from "redux-saga/effects";
import searchProduct from './SearchProduct/saga';
import getProductRule from './GetProductsByRule/saga';
import giftCode from './Gift/saga';

export default function* () {
  yield all([
    searchProduct(),
    getProductRule(),
    giftCode(),
  ]);
}