import { combineReducers } from 'redux';
import getProductByRuleReducer from './GetProductsByRule/reducer';
import giftReducer from './Gift/reducer';
import searchProductReducer from './SearchProduct/reducer';

const productReducer = combineReducers({
  searchProduct: searchProductReducer,
  productTable: getProductByRuleReducer,
  gift: giftReducer,
});

export default productReducer;