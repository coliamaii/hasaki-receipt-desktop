import { search } from "apis/sales/product";
import { all, call, put, takeLatest } from "redux-saga/effects";
import {
  SEARCH_PRODUCT,
  searchProductSuccessAction,
  searchProductErrorAction,
  searchProductLoadingAction,
  searchProductClearAction
} from "./actions";

function* searchSaga({ keyword, search_type }) {
  try {
    const { status, data } = yield call(search, { q: keyword });
    if (status === 1) {
      yield put(searchProductSuccessAction(data.rows, search_type));
    } else {
      yield put(searchProductErrorAction());
    }
  } catch (err) {
    yield put(searchProductErrorAction());
  }
}

function* searchOfflineSaga({ keyword, search_type }) {
  try {
    yield put(searchProductLoadingAction(true));
    const data = yield window.api.searchProducts({ search_text: keyword });
    yield put(searchProductSuccessAction(data, search_type));
  } catch (err) {
    yield put(searchProductErrorAction());
  }
}

function* searchWatcher() {
  yield all([
    takeLatest(SEARCH_PRODUCT.REQUEST, searchSaga),
    takeLatest(SEARCH_PRODUCT.REQUEST_OFFLINE, searchOfflineSaga),
  ]);
}

export default searchWatcher;