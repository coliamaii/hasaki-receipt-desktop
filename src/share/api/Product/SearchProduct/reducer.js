import {
  SEARCH_PRODUCT,
} from './actions';

const initialState = {
  search_data: [],
  search_gift: [],
  gifts: [],
  loading: false,
}

function searchProductReducer(state = initialState, action) {
  switch (action.type) {
    case SEARCH_PRODUCT.REQUEST: {
      return {
        ...state,
        loading: true,
      }
    }
    case SEARCH_PRODUCT.SUCCESS: {
      const { data, search_type } = action;
      return {
        ...state,
        loading: false,
        [search_type]: [...data],
      }
    }
    case SEARCH_PRODUCT.CLEAR: {
      const { search_type } = action;
      return {
        ...state,
        loading: false,
        [search_type]: [],
      }
    }
    case SEARCH_PRODUCT.ADD_GIFTS: {
      const { gifts } = action;
      return {
        ...state,
        gifts,
      }
    }
    case SEARCH_PRODUCT.CLEAR_GIFTS: {
      return {
        ...state,
        gifts: [],
      }
    }
    default: {
      return {
        ...state,
      }
    }
  }
}

export default searchProductReducer;