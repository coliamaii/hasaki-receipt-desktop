export const SEARCH_PRODUCT = {
  CLEAR: 'API_SEARCH_PRODUCT_CLEAR',
  REQUEST: 'API_SEARCH_PRODUCT_REQUEST_ONLINE',
  REQUEST_OFFLINE: 'API_SEARCH_PRODUCT_REQUEST_OFFLINE',
  LOADING: 'API_SEARCH_PRODUCT_LOADING',
  SUCCESS: 'API_SEARCH_PRODUCT_SUCCESS',
  ERROR: 'API_SEARCH_PRODUCT_ERROR',
  ADD_GIFTS: 'API_SEARCH_PRODUCT_ADD_GIFTS',
  CLEAR_GIFTS: 'API_SEARCH_PRODUCT_CLEAR_GIFTS',
}

export function searchProductRequestAction(keyword, search_type) {
  return {
    type: SEARCH_PRODUCT.REQUEST,
    keyword,
    search_type,
  }
}

export function searchProductRequestOfflineAction(keyword, search_type) {
  return {
    type: SEARCH_PRODUCT.REQUEST_OFFLINE,
    keyword,
    search_type,
  }
}

export function searchProductLoadingAction(isLoading) {
  return {
    type: SEARCH_PRODUCT.LOADING,
    isLoading,
  }
}

export function searchProductSuccessAction(data, search_type) {
  return {
    type: SEARCH_PRODUCT.SUCCESS,
    data,
    search_type,
  }
}

export function searchProductClearAction(search_type) {
  return {
    type: SEARCH_PRODUCT.CLEAR,
    search_type
  }
}

export function searchProductErrorAction() {
  return {
    type: SEARCH_PRODUCT.ERROR,
  }
}

export const searchProductAddGiftsAction = (gifts) => {
  return {
    type: SEARCH_PRODUCT.ADD_GIFTS,
    gifts,
  }
}

export const searchProductClearGiftsAction = () => {
  return {
    type: SEARCH_PRODUCT.CLEAR_GIFTS,
  }
}