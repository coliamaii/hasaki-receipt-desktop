export const GIFT_CODE = {
  REQUEST: 'GIFT_CODE_REQUEST',
  LOADING: 'GIFT_CODE_LOADING',
  SUCCESS: 'GIFT_CODE_SUCCESS',
  FAILED: 'GIFT_CODE_FAILED',
  REMOVE: 'GIFT_CODE_REMOVE',
  REFRESH: 'GIFT_CODE_REFRESH',
}

export const giftCodeRequestAction = (code, customer_id, items, store_id) => {
  return {
    type: GIFT_CODE.REQUEST,
    code,
    customer_id,
    items,
    store_id,
  }
}

export const giftCodeLoadingAction = (loading) => {
  return {
    type: GIFT_CODE.LOADING,
    loading,
  }
}

export const giftCodeSuccessAction = (code, waiting) => {
  return {
    type: GIFT_CODE.SUCCESS,
    code,
    waiting,
  }
}

export const giftCodeFailedAction = (msg) => {
  return {
    type: GIFT_CODE.FAILED,
    msg,
  }
}

export const giftCodeRemoveAction = () => {
  return {
    type: GIFT_CODE.REMOVE,
  }
}

export const refreshGiftCodeAction = (gift_code) => {
  return {
    type: GIFT_CODE.REFRESH,
    gift_code,
  }
}