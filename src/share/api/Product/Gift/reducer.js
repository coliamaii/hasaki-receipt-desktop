import {
  GIFT_CODE
} from './action';

const initialState = {
  gift_code: {
    error: '',
    code: '',
    loading: false,
    waiting: false,
  },
}

const giftReducer = (state = initialState, action) => {
  switch (action.type) {
    case GIFT_CODE.LOADING: {
      const { loading } = action;
      return {
        ...state,
        gift_code: {
          ...state.gift_code,
          loading,
        }
      }
    }
    case GIFT_CODE.SUCCESS: {
      const { code, waiting } = action;
      return {
        ...state,
        gift_code: {
          ...state.gift_code,
          loading: false,
          code,
          error: '',
          waiting,
        }
      }
    }
    case GIFT_CODE.FAILED: {
      const { msg } = action;
      return {
        ...state,
        gift_code: {
          ...state.gift_code,
          loading: false,
          error: msg,
          code: '',
          waiting: false,
        }
      }
    }
    case GIFT_CODE.REMOVE: {
      return {
        ...state,
        gift_code: {
          error: '',
          code: '',
          loading: false,
          waiting: false,
        }
      }
    }
    case GIFT_CODE.REFRESH: {
      const { gift_code } = action;
      return {
        ...state,
        gift_code,
      }
    }
    default: {
      return {
        ...state,
      }
    }
  }
}

export default giftReducer;