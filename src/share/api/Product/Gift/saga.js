import { call, put, takeLatest } from "redux-saga/effects";
import { validateGiftCode } from 'apis/sales/gift';

import {
  giftCodeFailedAction,
  giftCodeLoadingAction,
  giftCodeSuccessAction,
  GIFT_CODE,
} from './action'

function* giftSaga({ code, customer_id, items, store_id }) {
  try {
    yield put(giftCodeLoadingAction(true));
    const { data: { error, coupon_id } } = yield call(validateGiftCode, { code, customer_id, items, store_id });
    if (error === 0) {
      yield put(giftCodeSuccessAction(code, false));
    }
    if (error === 1 && coupon_id === 0) {
      yield put(giftCodeFailedAction(`Your gift code is not valid (${code})`));
    }
  } catch (err) {

  }
}

function* giftWatcher() {
  yield takeLatest(GIFT_CODE.REQUEST, giftSaga);
}

export default giftWatcher;