export const INSERT_SUPREQ = {
  REQUEST: "API_INSERT_SUPREQ_REQUEST",
  SUCCESS: "API_INSERT_SUPREQ_SUCCESS",
  ERROR: "API_INSERT_SUPREQ_ERROR",
};

export function insertSupReqRequestAction(data, callback) {
  return {
    type: INSERT_SUPREQ.REQUEST,
    data,
    callback,
  };
}

export function insertSupReqSuccessAction(data) {
  return {
    type: INSERT_SUPREQ.SUCCESS,
    data,
  };
}

export function insertSupReqErrorAction() {
  return {
    type: INSERT_SUPREQ.ERROR,
  };
}
