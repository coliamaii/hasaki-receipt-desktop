import { insert as insertSupReq } from "apis/hr/support-request";
import { getList as getListAccountObject } from "apis/accounting/account-object";
import { getList as getListDepartment } from "apis/setting/department";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  insertSupReqErrorAction,
  insertSupReqSuccessAction,
  INSERT_SUPREQ,
} from "./actions";

function* insertSupReqSaga(action) {
  try {
    const responseSupReq = yield call(insertSupReq, action.data);

    if (responseSupReq.status) {
      let detail = {};
      let accountingCode = [];
      let departments = [];

      detail = responseSupReq.data.support_request;

      // get list Account Object
      const accObjParams = {
        object_type: "3",
        data_json: "1",
      };

      const responseAccObj = yield call(getListAccountObject, accObjParams);
      if (responseAccObj.status) {
        accountingCode = responseAccObj.data.rows;
      }

      // get list department
      const responseDepartment = yield call(getListDepartment);
      if (responseDepartment.status) {
        departments = responseDepartment.data.rows;
      }

      yield put(
        insertSupReqSuccessAction({ detail, accountingCode, departments })
      );
      action.callback("/support-request/edit");
    } else {
      yield put(insertSupReqErrorAction());
    }
  } catch (err) {
    yield put(insertSupReqErrorAction());
  }
}

function* insertSupReqWatcher() {
  yield takeLatest(INSERT_SUPREQ.REQUEST, insertSupReqSaga);
}
export default insertSupReqWatcher;
