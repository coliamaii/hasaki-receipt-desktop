import { all } from "redux-saga/effects";
import getListSupReqWatcher from "./GetList/saga";
import insertSupReqWatcher from "./Insert/saga";
import getDetailSupReqWatcher from "./GetDetail/saga";
import putCancelSupReqWatcher from "./Cancel/saga";
import updateSupReqWatcher from "./Update/saga";
import postCommentSupReqWatcher from "./PostComment/saga";

export default function* () {
  yield all([
    getListSupReqWatcher(),
    getDetailSupReqWatcher(),
    updateSupReqWatcher(),
    putCancelSupReqWatcher(),
    postCommentSupReqWatcher(),
    insertSupReqWatcher(),
  ]);
}
