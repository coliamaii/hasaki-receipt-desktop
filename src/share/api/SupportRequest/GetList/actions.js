export const GET_LIST_SUP_REQ = {
  REQUEST: "API_GET_LIST_SUP_REQ_REQUEST",
  SUCCESS: "API_GET_LIST_SUP_REQ_SUCCESS",
  ERROR: "API_GET_LIST_SUP_REQ_ERROR",
};

export function getListSupReqRequestAction(params) {
  return {
    type: GET_LIST_SUP_REQ.REQUEST,
    params,
  };
}

export function getListSupReqSuccessAction(data) {
  return {
    type: GET_LIST_SUP_REQ.SUCCESS,
    data,
  };
}

export function getListSupReqErrorAction() {
  return {
    type: GET_LIST_SUP_REQ.ERROR,
  };
}
