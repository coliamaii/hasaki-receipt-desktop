import { getList as getListSupReq } from "apis/hr/support-request";
import { getList as getListUser } from "apis/setting/user";
import { getList as getListAccountObject } from "apis/accounting/account-object";
import { getList as getListDepartment } from "apis/setting/department";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  getListSupReqErrorAction,
  getListSupReqSuccessAction,
  GET_LIST_SUP_REQ,
} from "./actions";

function* getListSupReqSaga(action) {
  try {
    const responseSupReq = yield call(getListSupReq, action.params);

    if (responseSupReq.status === 1) {
      let list = [];
      let users = [];
      let accountingCode = [];
      let departments = [];
      let ids = new Set();

      const { rows, support_status, support_type } = responseSupReq.data;

      for (let i = 0; i < rows.length; i++) {
        let obj = rows[i];
        if (obj.feedback_by) {
          ids.add(obj.feedback_by);
        }
        if (obj.supreq_user_id) {
          ids.add(obj.supreq_user_id);
        }

        list.push({ key: obj.supreq_id, ...obj });
      }

      ids = [...ids].toString().replaceAll("[]", "");
      const responseListUser = yield call(getListUser, { ids });
      if (responseListUser.status) {
        users = responseListUser.data.rows;
      }

      // get list Account Object
      const accObjParams = {
        object_type: "3",
        data_json: "1",
      };

      const responseAccObj = yield call(getListAccountObject, accObjParams);
      if (responseAccObj.status) {
        accountingCode = responseAccObj.data.rows;
      }

      // get list department
      const responseDepartment = yield call(getListDepartment);
      if (responseDepartment.status) {
        departments = responseDepartment.data.rows;
      }

      yield put(
        getListSupReqSuccessAction({
          list,
          users,
          support_status,
          support_type,
          accountingCode,
          departments,
        })
      );
    } else {
      yield put(getListSupReqErrorAction());
    }
  } catch (err) {
    yield put(getListSupReqErrorAction());
  }
}

function* getListSupReqWatcher() {
  yield takeLatest(GET_LIST_SUP_REQ.REQUEST, getListSupReqSaga);
}
export default getListSupReqWatcher;
