import { getList as getListAccountObject } from "apis/accounting/account-object";
import { getList as getListComment } from "apis/hr/comment";
import {
  getDetail as getDetailSupreq,
  getList as getListSupReq,
} from "apis/hr/support-request";
import { getList as getListUser } from "apis/setting/user";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  getDetailSupReqErrorAction,
  getDetailSupReqSuccessAction,
  GET_DETAIL_SUP_REQ,
} from "./actions";

function* getDetailSupReqSaga(action) {
  try {
    const responseSupReqDetail = yield call(getDetailSupreq, action.id);

    if (responseSupReqDetail.status === 1) {
      let detail = {};
      let list = [];
      let accouting_code = [];
      let comments = [];
      let users = [];
      let ids = new Set();

      detail = responseSupReqDetail.data.support_request;
      const {
        supreq_code,
        supreq_id,
        code,
        supreq_user_id,
      } = responseSupReqDetail.data.support_request;

      // get list support request
      const getListSupReq_params = {
        code: supreq_code,
      };
      const responseSupReq = yield call(getListSupReq, getListSupReq_params);

      if (responseSupReq.status) {
        list = responseSupReq.data.rows.filter(
          (item) => item.supreq_id !== supreq_id
        );
      }
      // accounting code
      const getListAccObj_params = {
        object_type: "3",
        data_json: "1",
      };
      const responseAccObj = yield call(
        getListAccountObject,
        getListAccObj_params
      );
      if (responseAccObj.status) {
        accouting_code = [...responseAccObj.data.rows];
      }
      // get list comment
      const getListComment_params = {
        ref_code: code,
      };

      const responseComment = yield call(getListComment, getListComment_params);
      if (responseComment.status) {
        responseComment.data.rows.map((obj) => {
          comments.push({
            key: obj.id,
            ...obj,
          });
        });
      }

      // get list user
      ids.add(supreq_user_id);
      ids = [...ids].toString().replaceAll("[]", "");
      const responseListUser = yield call(getListUser, { ids });
      if (responseListUser.status) {
        users = responseListUser.data.rows;
      }

      yield put(
        getDetailSupReqSuccessAction({
          detail,
          list,
          users,
          accouting_code,
          comments,
        })
      );
      action.callback("/support-request/edit");
    } else {
      yield put(getDetailSupReqErrorAction());
    }
  } catch (err) {
    yield put(getDetailSupReqErrorAction());
  }
}
function* getDetailSupReqWatcher() {
  yield takeLatest(GET_DETAIL_SUP_REQ.REQUEST, getDetailSupReqSaga);
}
export default getDetailSupReqWatcher;
