export const GET_DETAIL_SUP_REQ = {
  REQUEST: "API_GET_DETAIL_SUP_REQ_REQUEST",
  SUCCESS: "API_GET_DETAIL_SUP_REQ_SUCCESS",
  ERROR: "API_GET_DETAIL_SUP_REQ_ERROR",
};

export function getDetailSupReqRequestAction(id, callback) {
  return {
    type: GET_DETAIL_SUP_REQ.REQUEST,
    id,
    callback,
  };
}

export function getDetailSupReqSuccessAction(data) {
  return {
    type: GET_DETAIL_SUP_REQ.SUCCESS,
    data,
  };
}

export function getDetailSupReqErrorAction() {
  return {
    type: GET_DETAIL_SUP_REQ.ERROR,
  };
}
