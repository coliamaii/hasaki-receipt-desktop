import {
  getList as getListComment,
  postCmt as postComment,
} from "apis/hr/comment";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  postCommentSupReqErrorAction,
  postCommentSupReqSuccessAction,
  POST_COMMENT_SUP_REQ,
} from "./actions";

function* postCommentSupReqSaga(action) {
  try {
    const responseComment = yield call(postComment, action.body);

    if (responseComment.status === 1) {
      let comments = [];
      // get list comment
      const getListComment_params = {
        ref_code: action.body.ref_code,
      };
      const responseComment = yield call(getListComment, getListComment_params);
      if (responseComment.status) {
        comments = [...responseComment.data.rows];
      }

      yield put(postCommentSupReqSuccessAction({ comments }));
    } else {
      yield put(postCommentSupReqErrorAction());
    }
  } catch (err) {
    yield put(postCommentSupReqErrorAction());
  }
}

function* postCommentSupReqWatcher() {
  yield takeLatest(POST_COMMENT_SUP_REQ.REQUEST, postCommentSupReqSaga);
}
export default postCommentSupReqWatcher;
