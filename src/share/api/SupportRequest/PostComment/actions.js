export const POST_COMMENT_SUP_REQ = {
  REQUEST: "API_POST_COMMENT_SUP_REQ_REQUEST",
  SUCCESS: "API_POST_COMMENT_SUP_REQ_SUCCESS",
  ERROR: "API_POST_COMMENT_SUP_REQ_ERROR",
};

export function postCommentSupReqRequestAction(body) {
  return {
    type: POST_COMMENT_SUP_REQ.REQUEST,
    body,
  };
}

export function postCommentSupReqSuccessAction(data) {
  return {
    type: POST_COMMENT_SUP_REQ.SUCCESS,
    data,
  };
}

export function postCommentSupReqErrorAction() {
  return {
    type: POST_COMMENT_SUP_REQ.ERROR,
  };
}
