import { getList as getListComment } from "apis/hr/comment";
import { update as updateSupReq } from "apis/hr/support-request";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  putUpdateSupReqErrorAction,
  putUpdateSupReqSuccessAction,
  PUT_UPDATE_SUPREQ,
} from "./actions";

function* updateSupReqSaga(action) {
  try {
    const responseSupReq = yield call(updateSupReq, action.id, action.data);

    if (responseSupReq.status === 1) {
      let detail = {};
      let comments = [];
      detail = responseSupReq.data.support_request;
      // get list comment
      const getListComment_params = {
        ref_code: detail.code,
      };
      const responseComment = yield call(getListComment, getListComment_params);
      if (responseComment.status) {
        comments = [...responseComment.data.rows];
      }
      yield put(putUpdateSupReqSuccessAction({ detail, comments }));
    } else {
      yield put(putUpdateSupReqErrorAction());
    }
  } catch (err) {
    yield put(putUpdateSupReqErrorAction());
  }
}
function* updateSupReqWatcher() {
  yield takeLatest(PUT_UPDATE_SUPREQ.REQUEST, updateSupReqSaga);
}
export default updateSupReqWatcher;
