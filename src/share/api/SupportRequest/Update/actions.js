export const PUT_UPDATE_SUPREQ = {
  REQUEST: "API_PUT_UPDATE_SUPREQ_REQUEST",
  SUCCESS: "API_PUT_UPDATE_SUPREQ_SUCCESS",
  ERROR: "API_PUT_UPDATE_SUPREQ_ERROR",
};

export function putUpdateSupReqRequestAction(id, data) {
  return {
    type: PUT_UPDATE_SUPREQ.REQUEST,
    id,
    data,
  };
}

export function putUpdateSupReqSuccessAction(data) {
  return {
    type: PUT_UPDATE_SUPREQ.SUCCESS,
    data,
  };
}

export function putUpdateSupReqErrorAction() {
  return {
    type: PUT_UPDATE_SUPREQ.ERROR,
  };
}
