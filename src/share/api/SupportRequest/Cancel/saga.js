import { getList as getListComment } from "apis/hr/comment";
import { cancel as cancelSupReq } from "apis/hr/support-request";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  putCancelSupReqErrorAction,
  putCancelSupReqSuccessAction,
  PUT_CANCEL_SUPREQ,
} from "./actions";

function* putCancelSupReqSaga(action) {
  try {
    const responseCancelSupReq = yield call(
      cancelSupReq,
      action.id,
      action.data
    );

    if (responseCancelSupReq.status === 1) {
      let comments = [];
      const { support_request } = responseCancelSupReq.data;
      // get list comment
      const getListComment_params = {
        ref_code: support_request.code,
      };
      const responseComment = yield call(getListComment, getListComment_params);
      if (responseComment.status) {
        comments = [...responseComment.data.rows];
      }
      yield put(putCancelSupReqSuccessAction({ support_request, comments }));
    } else {
      yield put(putCancelSupReqErrorAction());
    }
  } catch (err) {
    yield put(putCancelSupReqErrorAction());
  }
}

function* putCancelSupReqWatcher() {
  yield takeLatest(PUT_CANCEL_SUPREQ.REQUEST, putCancelSupReqSaga);
}
export default putCancelSupReqWatcher;
