export const PUT_CANCEL_SUPREQ = {
  REQUEST: "API_PUT_CANCEL_SUPREQ_REQUEST",
  SUCCESS: "API_PUT_CANCEL_SUPREQ_SUCCESS",
  ERROR: "API_PUT_CANCEL_SUPREQ_ERROR",
};

export function putCancelSupReqRequestAction(id, data) {
  return {
    type: PUT_CANCEL_SUPREQ.REQUEST,
    id,
    data,
  };
}

export function putCancelSupReqSuccessAction(data) {
  return {
    type: PUT_CANCEL_SUPREQ.SUCCESS,
    data,
  };
}

export function putCancelSupReqErrorAction() {
  return {
    type: PUT_CANCEL_SUPREQ.ERROR,
  };
}