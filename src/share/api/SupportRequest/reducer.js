import { PUT_CANCEL_SUPREQ } from "./Cancel/actions";
import { GET_DETAIL_SUP_REQ } from "./GetDetail/actions";
import { GET_LIST_SUP_REQ } from "./GetList/actions";
import { INSERT_SUPREQ } from "./Insert/actions";
import { POST_COMMENT_SUP_REQ } from "./PostComment/actions";
import { PUT_UPDATE_SUPREQ } from "./Update/actions";

const initialState = {
  isLoading: false,
  detail: {},
  list: [],
  users: [],
  support_status: {},
  support_type: {},
  accountingCode: [],
  departments: [],
  comments: [],
};

export function supportRequestReducer(state = initialState, action) {
  switch (action.type) {
    case GET_LIST_SUP_REQ.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case GET_LIST_SUP_REQ.SUCCESS: {
      return {
        ...state,
        list: action.data.list,
        users: action.data.users,
        support_status: action.data.support_status,
        support_type: action.data.support_type,
        accountingCode: action.data.accountingCode,
        departments: action.data.departments,
        isLoading: false,
      };
    }

    case GET_DETAIL_SUP_REQ.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case GET_DETAIL_SUP_REQ.SUCCESS: {
      return {
        ...state,
        detail: action.data.detail,
        list: action.data.list,
        users: action.data.users,
        accouting_code: action.data.accouting_code,
        comments: action.data.comments,
      };
    }

    case PUT_CANCEL_SUPREQ.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case PUT_CANCEL_SUPREQ.SUCCESS: {
      return {
        ...state,
        ...action.data,
        isLoading: false,
      };
    }

    case POST_COMMENT_SUP_REQ.SUCCESS: {
      return {
        ...state,
        comments: action.data.comments,
      };
    }

    case PUT_UPDATE_SUPREQ.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case PUT_UPDATE_SUPREQ.SUCCESS: {
      return {
        ...state,
        detail: action.data.detail,
        comments: action.data.comments,
        isLoading: false,
      };
    }

    case INSERT_SUPREQ.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case INSERT_SUPREQ.SUCCESS: {
      return {
        ...state,
        isLoading: false,
        detail: action.data.detail,
        accountingCode: action.data.accountingCode,
        departments: action.data.departments,
      };
    }

    default: {
      return {
        ...state,
      };
    }
  }
}

export default supportRequestReducer;
