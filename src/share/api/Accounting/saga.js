import { all } from "redux-saga/effects";
import paymentSaga from "./Payment/saga";

export default function* () {
  yield all([
    paymentSaga(),
  ]);
}
