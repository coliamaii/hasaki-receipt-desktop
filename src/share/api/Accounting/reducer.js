import { combineReducers } from "redux";
import paymentReducer from "./Payment/reducer";

const accountingReducer = combineReducers({
  payment: paymentReducer,
});

export default accountingReducer;
