import { all } from "redux-saga/effects";
import getListPaymentWatcher from "./GetList/saga";
import getDetailPaymentWatcher from "./GetDetail/saga";
import putUpdatePaymentWatcher from "./Update/saga";

export default function* () {
  yield all([
    getListPaymentWatcher(),
    getDetailPaymentWatcher(),
    putUpdatePaymentWatcher(),
  ]);
}