export const PUT_UPDATE_PAYMENT = {
  REQUEST: "API_PUT_UPDATE_PAYMENT_REQUEST",
  SUCCESS: "API_PUT_UPDATE_PAYMENT_SUCCESS",
  ERROR: "API_PUT_UPDATE_PAYMENT_ERROR",
};

export function putUpdatePaymentRequestAction(id, data) {
  return {
    type: PUT_UPDATE_PAYMENT.REQUEST,
    id,
    data,
  };
}

export function putUpdatePaymentSuccessAction(data) {
  return {
    type: PUT_UPDATE_PAYMENT.SUCCESS,
    data,
  };
}

export function putUpdatePaymentErrorAction() {
  return {
    type: PUT_UPDATE_PAYMENT.ERROR,
  };
}
