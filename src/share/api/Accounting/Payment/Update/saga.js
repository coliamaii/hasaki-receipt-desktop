import { update } from "apis/accounting/payment";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  putUpdatePaymentErrorAction,
  putUpdatePaymentSuccessAction,
  PUT_UPDATE_PAYMENT,
} from "./actions";

function* putUpdatePaymentSaga(action) {
  try {
    const { status, data } = yield call(update, action.id, action.data);

    if (status === 1) {
      yield put(putUpdatePaymentSuccessAction(data));
    } else {
      yield put(putUpdatePaymentErrorAction());
    }
  } catch (err) {
    yield put(putUpdatePaymentErrorAction());
  }
}

function* putUpdatePaymentWatcher() {
  yield takeLatest(PUT_UPDATE_PAYMENT.REQUEST, putUpdatePaymentSaga);
}
export default putUpdatePaymentWatcher;
