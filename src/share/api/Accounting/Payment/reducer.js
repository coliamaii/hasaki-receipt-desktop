import { GET_LIST_PAYMENT } from "./GetList/actions";
import { GET_DETAIL_PAYMENT } from "./GetDetail/actions";
import { PUT_UPDATE_PAYMENT } from "./Update/actions";

const initialState = {
  isLoading: false,
  list: [],
  banks: [],
  accountBalance: [],
};

export function paymentReducer(state = initialState, action) {
  switch (action.type) {
    case GET_LIST_PAYMENT.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_LIST_PAYMENT.SUCCESS: {
      return {
        ...state,
        list: action.data.list,
        isLoading: false,
      };
    }

    case GET_DETAIL_PAYMENT.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_DETAIL_PAYMENT.SUCCESS: {
      return {
        ...state,
        isLoading: false,
      };
    }

    case PUT_UPDATE_PAYMENT.REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case PUT_UPDATE_PAYMENT.SUCCESS: {
      return {
        ...state,
        isLoading: false,
      };
    }

    default: {
      return {
        ...state,
      };
    }
  }
}

export default paymentReducer;
