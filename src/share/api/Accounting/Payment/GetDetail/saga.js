import { getDetail } from "apis/accounting/payment";
import { getBanks } from "apis/accounting/payment-transaction";
import { getList as getListAccountBalance } from "apis/accounting/account-balance";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  getDetailPaymentErrorAction,
  getDetailPaymentSuccessAction,
  GET_DETAIL_PAYMENT,
} from "./actions";

function* getDetailPaymentSaga(action) {
  try {
    const { status, data } = yield call(getDetail, action.params);

    if (status === 1) {
      let banks = [];
      let accountBalance = [];

      // get bank list
      const responseBank = yield call(getBanks);

      if (responseBank.status) {
        banks = Object.values(responseBank.data);
      }

      // get Account Balance list
      const responseAccountBalance = yield call(getListAccountBalance, {
        location: 1,
      });

      if (responseAccountBalance.status) {
        accountBalance = responseAccountBalance.data.rows;
      }

      yield put(getDetailPaymentSuccessAction(data));
      action.callback("/payment/edit");
    } else {
      yield put(getDetailPaymentErrorAction());
    }
  } catch (err) {
    yield put(getDetailPaymentErrorAction());
  }
}

function* getDetailPaymentWatcher() {
  yield takeLatest(GET_DETAIL_PAYMENT.REQUEST, getDetailPaymentSaga);
}
export default getDetailPaymentWatcher;
