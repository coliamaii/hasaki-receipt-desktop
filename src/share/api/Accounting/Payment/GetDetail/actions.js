export const GET_DETAIL_PAYMENT = {
  REQUEST: "API_GET_DETAIL_PAYMENT_REQUEST",
  SUCCESS: "API_GET_DETAIL_PAYMENT_SUCCESS",
  ERROR: "API_GET_DETAIL_PAYMENT_ERROR",
};

export function getDetailPaymentRequestAction(params, callback) {
  return {
    type: GET_DETAIL_PAYMENT.REQUEST,
    params,
    callback
  };
}

export function getDetailPaymentSuccessAction(data) {
  return {
    type: GET_DETAIL_PAYMENT.SUCCESS,
    data,
  };
}

export function getDetailPaymentErrorAction() {
  return {
    type: GET_DETAIL_PAYMENT.ERROR,
  };
}
