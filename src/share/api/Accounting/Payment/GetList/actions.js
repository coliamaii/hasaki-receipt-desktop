export const GET_LIST_PAYMENT = {
  REQUEST: "API_GET_LIST_PAYMENT_REQUEST",
  SUCCESS: "API_GET_LIST_PAYMENT_SUCCESS",
  ERROR: "API_GET_LIST_PAYMENT_ERROR",
};

export function getListPaymentRequestAction(params) {
  return {
    type: GET_LIST_PAYMENT.REQUEST,
    params
  };
}

export function getListPaymentSuccessAction(data) {
  return {
    type: GET_LIST_PAYMENT.SUCCESS,
    data,
  };
}

export function getListPaymentErrorAction() {
  return {
    type: GET_LIST_PAYMENT.ERROR,
  };
}
