import { getList as getListPayment } from "apis/accounting/payment";
import { call, put, takeLatest } from "redux-saga/effects";
import {
  getListPaymentErrorAction,
  getListPaymentSuccessAction,
  GET_LIST_PAYMENT,
} from "./actions";

function* getListPaymentSaga(action) {
  try {
    const responsePayment = yield call(getListPayment, action.params);

    if (responsePayment.status) {
      let list = [];

      list = responsePayment.data.rows.map((obj) => {
        return {
          key: obj.payment_id,
          ...obj,
        };
      });

      yield put(getListPaymentSuccessAction({ list }));
    } else {
      yield put(getListPaymentErrorAction());
    }
  } catch (err) {
    yield put(getListPaymentErrorAction());
  }
}

function* getListPaymentWatcher() {
  yield takeLatest(GET_LIST_PAYMENT.REQUEST, getListPaymentSaga);
}
export default getListPaymentWatcher;
