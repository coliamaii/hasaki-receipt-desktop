import React, { useEffect, useState } from 'react';

function useNetworkConnectivity() {
  const [isOnline, setIsOnline] = useState(typeof navigator.onLine === 'boolean' ? navigator.onLine : true);

  useEffect(() => {
    window.addEventListener('online', goOnline);
    window.addEventListener('offline', goOffline);

    return () => {
      window.removeEventListener('online', goOnline);
      window.removeEventListener('offline', goOffline);
    };
  }, []);

  const goOnline = () => {
    setIsOnline(true);
  };

  const goOffline = () => {
    setIsOnline(false);
  };

  return isOnline;
}

export default useNetworkConnectivity;