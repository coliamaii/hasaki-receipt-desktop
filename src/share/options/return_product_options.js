import React from "react";
import Trans from "utils/Trans";

export const options_all_types = [
  { value: "1", label: <Trans value="CASH" /> },
  { value: "2", label: <Trans value="BANKTRANSFER" /> },
  { value: "3", label: <Trans value="CARD" /> },
  { value: "4", label: <Trans value="VOUCHER" /> },
  { value: "5", label: <Trans value="GIFTCARD" /> },
  { value: "6", label: <Trans value="BALANCE" /> },
  { value: "7", label: <Trans value="GIFT CHEQUE" /> },
  { value: "8", label: <Trans value="MOBILE CODE" /> },
];

export const options_return_product_type = [
  { value: "1", label: <Trans value="On Store" /> },
  { value: "2", label: <Trans value="At Home" /> },
];
export const options_return_product_status = [
  { value: "2", label: <Trans value="Pending" /> },
  { value: "1", label: <Trans value="Approved" /> },
];

export const options_return_product_payment_method = [
  { value: 1, label: <span>Cash</span> },
  { value: 2, label: <span>Balance</span> },
  { value: 3, label: <span>Vnpay</span> },
];

export const options_return_product_reason = [
  { value: "changed-mind", label: "changed-mind" },
  { value: "faulty", label: "faulty" },
  { value: "incorrect", label: "incorrect" },
];
