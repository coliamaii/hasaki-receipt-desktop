import React from "react";
import Trans from "utils/Trans";
import dayjs from "dayjs";
const dateFormat = "YYYY-MM-DD";

export const options_type = [
  { value: 1, label: <Trans value="ADJUSTMENT" /> },
  { value: 2, label: <Trans value="DISCOUNT PRODUCT" /> },
  { value: 3, label: <Trans value="DISCOUNT SERVICE" /> },
  { value: 4, label: <Trans value="FEEDBACK" /> },
  { value: 5, label: <Trans value="PAYMENT" /> },
  { value: 6, label: <Trans value="ADVANCE" /> },
  { value: 7, label: <Trans value="VAT" /> },
  { value: 8, label: <Trans value="CUSTOMER REQUEST" /> },
];

export const options_payment_types = [
  { value: "0", label: <Trans value="-All Payment-" /> },
  { value: 1, label: <Trans value="CASH" /> },
  { value: 2, label: <Trans value="BANKTRANSFER" /> },
  { value: 3, label: <Trans value="CARD" /> },
  { value: 4, label: <Trans value="VOUCHER" /> },
  { value: 5, label: <Trans value="GIFTCARD" /> },
  { value: 6, label: <Trans value="BALANCE" /> },
  { value: 7, label: <Trans value="GIFT CHEQUE" /> },
  { value: 8, label: <Trans value="MOBILE CODE" /> },
];

export const options_status = [
  { value: 0, label: <Trans value="-All Status-" /> },
  { value: 1, label: <Trans value="Pending" /> },
  { value: 2, label: <Trans value="Completed" /> },
  { value: 3, label: <Trans value="Send back" /> },
  { value: 4, label: <Trans value="Cancel" /> },
  { value: 5, label: <Trans value="Verified" /> },
  { value: 11, label: <Trans value="Request payment" /> },
  { value: 12, label: <Trans value="Pending (Not Assign)" /> },
];

export const options_reference_type = [
  { value: 0, label: <Trans value="-All Object-" /> },
  { value: "reconcile", label: <Trans value="RECONCILE" /> },
  { value: "receipt", label: <Trans value="RECEIPT" /> },
  { value: "po", label: <Trans value="PO" /> },
];

export const options_adjustment_reason = [
  { value: 1, label: <Trans value="System Error" /> },
  { value: 2, label: <Trans value="Persional mistake" /> },
];

export const options_from_due_date = [
  { value: 0, label: <Trans value="Select Due Date" /> },
  { value: dayjs().format(dateFormat), label: <Trans value="Over Due" /> },
];
