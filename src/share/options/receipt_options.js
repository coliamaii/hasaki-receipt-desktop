import React from "react";
import Trans from "utils/Trans";

export const options_status = [
  { value: 0, label: <Trans value="-All Status-" /> },
  { value: 1, label: <Trans value="Pending" /> },
  { value: 2, label: <Trans value="Completed" /> },
];
