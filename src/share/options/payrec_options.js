import React from "react";
import Trans from "utils/Trans";

export const options_payment_method = [
  { value: 1, label: <Trans value="CASH" /> },
  { value: 2, label: <Trans value="BANKTRANSFER" /> },
  { value: 3, label: <Trans value="CARD" /> },
  { value: 4, label: <Trans value="VOUCHER" /> },
  { value: 5, label: <Trans value="GIFTCARD" /> },
  { value: 6, label: <Trans value="BALANCE" /> },
  { value: 7, label: <Trans value="GIFT CHEQUE" /> },
  { value: 8, label: <Trans value="MOBILE CODE" /> },
];

export const options_status = [
  { value: 1, label: <Trans value="Active" /> },
  { value: 2, label: <Trans value="Pending" /> },
  { value: 4, label: <Trans value="Delete" /> },
  { value: 11, label: <Trans value="NOT Verified" /> },
  { value: 12, label: <Trans value="NOT Completed" /> },
];

