import { all } from 'redux-saga/effects';
import authSaga from './scenes/Auth/saga';
import alertSaga from './share/alert/saga';
import apiSaga from './share/api/saga';
import modeSaga from './share/mode/saga';
import websocketSaga from './share/socket/saga'

const rootSaga = function* () {
  yield all([
    authSaga(),
    alertSaga(),
    apiSaga(),
    modeSaga(),
    websocketSaga(),
  ]);
}

export default rootSaga;