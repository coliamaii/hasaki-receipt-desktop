import Auth from 'scenes/Auth/router'
import Dashboard from 'scenes/Dashboard/routes';
import Print from 'scenes/Print/routes';

export default [
  ...Dashboard,
  ...Auth,
  ...Print,
];
