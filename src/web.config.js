export const WS_API = process.env.REACT_APP_ENV === 'production' ? 'https://ws.inshasaki.com/api' : (process.env.REACT_APP_ENV === 'test' ? 'http://test-ws.inshasaki.com/api' : 'http://test-ws.inshasaki.com/api')
export const OLD_INSIDE = process.env.REACT_APP_ENV === 'production' ? 'https://inshasaki.com' : (process.env.REACT_APP_ENV === 'test' ? 'http://test.inshasaki.com' : 'http://test.inshasaki.com')
export const MEDIA_URL = process.env.REACT_APP_ENV === 'production' ? 'https://media.inshasaki.com' : (process.env.REACT_APP_ENV === 'test' ? 'http://media-test.inshasaki.com' : 'http://media-test.inshasaki.com')
export const VOUCHER_GROUP_ID_DEFAULT = '4'
export const REACT_APP_VERSION = process.env.REACT_APP_VERSION
export const API_TOKEN_KEY = 'api_token_key'
export const USER_INFO = 'user_info';
export const ONLINE_RUNNING_MODE = process.env.REACT_APP_ONLINE_MODE ? process.env.REACT_APP_ONLINE_MODE === 'online' : true;
export const PAYMENT_METHOD = {
    CASH: 1,
    BANKTRANSFER: 2,
    CARD: 3,
    VOUCHER: 4,
    GIFTCARD: 5,
    BALANCE: 6,
    SHIP_CHECK: 7,
    MOBILE_CODE: 8,
}
export const PHONE_HOTLINE = '1800 6324'
export const PHONE_COMPLAIN = '1800 6310'
export const MST_HASAKI = '0313612829'
export const SPA_WORKTIME = '10h00 - 20h00'