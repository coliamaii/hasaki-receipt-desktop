import axios from "utils/request";

const prefix = "/report/return-product";

export const getList = (params = {}) => {
    return axios({
        method: "GET",
        url: `${prefix}`,
        params: {
            ...params,
        },
    });
};