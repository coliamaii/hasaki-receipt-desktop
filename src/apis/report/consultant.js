import axios from "utils/request";

const prefix = "/report/consultant";

export const getListDaily = (params = {}) => {
  return axios({
    method: "GET",
    url: `${prefix}/daily`,
    params: {
      ...params,
    },
  });
};

export const getListDetail = (params = {}) => {
  return axios({
    method: "GET",
    url: `${prefix}`,
    params: {
      ...params,
    },
  });
};

export const getSummary = (params = {}) => {
  return axios({
    method: "GET",
    url: `${prefix}/summary`,
    params: {
      ...params,
    },
  });
};
