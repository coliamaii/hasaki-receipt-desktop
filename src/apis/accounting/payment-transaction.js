import axios from "utils/request";

const prefix = "/accounting/payment-transaction";

export const getBanks = () => {
  return axios({
    method: "GET",
    url: `${prefix}/bank`,
  });
};
