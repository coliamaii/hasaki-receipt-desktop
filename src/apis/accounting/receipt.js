import axios from "utils/request";

const prefix = "/accounting/receipt";

export const create = () => {
  return axios({
    method: "GET",
    url: `${prefix}/empty`,
  });
};

export const getList = (params = {}) => {
  return axios({
    method: "GET",
    url: `${prefix}`,
    params: {
      ...params,
    },
  });
};

export const getDetail = ({ code }) => {
  return axios({
    method: "GET",
    url: `${prefix}/detail`,
    params: {
      code,
    },
  });
};
