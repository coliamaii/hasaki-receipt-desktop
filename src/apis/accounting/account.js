import axios from "utils/request";

const prefix = "/accounting/account";

export const getList = (params = {}) => {
  return axios({
    method: "GET",
    url: `${prefix}`,
    params: {
      ...params,
    },
  });
};
