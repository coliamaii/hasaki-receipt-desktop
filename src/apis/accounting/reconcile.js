import axios from "utils/request";

const prefix = "/accounting/reconcile";

export const insert = (data) => {
  return axios({
    method: "POST",
    url: `${prefix}`,
    data,
  });
};

export const update = (id, data) => {
  return axios({
    method: "PUT",
    url: `${prefix}/${id}`,
    data,
  });
};

export const commit = (id) => {
  return axios({
    method: "PUT",
    url: `${prefix}/commit/${id}`,
  });
};

export const getList = (params = {}) => {
  return axios({
    method: "GET",
    url: `${prefix}`,
    params: {
      ...params,
    },
  });
};

export const getDetail = (id) => {
  return axios({
    method: "GET",
    url: `${prefix}/${id}`,
  });
};
