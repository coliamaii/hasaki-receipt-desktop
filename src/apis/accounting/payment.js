import axios from "utils/request";

const prefix = "/accounting/payment";

export const getList = (params = {}) => {
  return axios({
    method: "GET",
    url: `${prefix}`,
    params: {
      ...params,
    },
  });
};

export const getDetail = (id) => {
  return axios({
    method: "GET",
    url: `${prefix}/${id}`,
  });
};

export const update = (id, data) => {
  return axios({
    method: "PUT",
    url: `${prefix}/${id}`,
    data,
  });
};