import axios from "utils/request";

const prefix = "/accounting/account-object";

export const getList = (params = {}) => {
  return axios({
    method: "GET",
    url: `${prefix}`,
    params: {
      ...params,
    },
  });
};
