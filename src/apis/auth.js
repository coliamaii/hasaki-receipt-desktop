import axios from "utils/request";

const prefix = "/auth";

export const login = (data) => {
  return axios({
    method: "POST",
    data,
    url: `${prefix}/login`,
  });
};
