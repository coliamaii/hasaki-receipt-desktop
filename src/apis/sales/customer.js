import axios from "utils/request";

const prefix = "/sales/customer";

export const searchByPhoneNumber = ({ phone }) => {
  return axios({
    method: 'GET',
    url: `${prefix}/detail`,
    params: {
      phone,
    }
  })
}

export const getDetail = (id) => {
  return axios({
    method: "GET",
    url: `${prefix}/${id}`,
  })
}

export const parseRegistrationNumber = (reg_code) => {
  return axios({
    method: 'GET',
    url: `${prefix}/parse-registration-number`,
    params: {
      reg_code,
    }
  })
}

export const getList = ({ ids }) => {
  return axios({
    method: "GET",
    url: `${prefix}`,
    params: {
      ids,
    },
  });
};

