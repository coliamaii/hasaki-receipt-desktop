import axios from 'utils/request';

const prefix = 'sales/vouchergroup';

export const validateGiftCode = ({ code, customer_id, items, store_id }) => {
  return axios({
    method: 'POST',
    url: `${prefix}/validate`,
    data: { code, customer_id, items: JSON.stringify(items), store_id: JSON.stringify(store_id) },
  })
}