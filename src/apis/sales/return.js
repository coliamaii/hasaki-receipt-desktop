import axios from "utils/request";

const prefix = "/sales/return";

export const getList = (params = {}) => {
  return axios({
    method: "GET",
    url: `${prefix}`,
    params: {
      ...params,
    },
  });
};

export const getDetail = (id) => {
  return axios({
    method: "GET",
    url: `${prefix}/${id}`,
  });
};

export const create = ({ code }) => {
  return axios({
    method: "GET",
    url: `${prefix}/create`,
    params: {
      code,
    },
  });
};

export const insert = (params = {}) => {
  return axios({
    method: "POST",
    url: `${prefix}`,
    params: {
      ...params,
    },
  });
};

export const update = (data) => {
  return axios({
    method: "PUT",
    url: `${prefix}`,
    data,
  });
};
