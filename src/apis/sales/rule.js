import axios from 'utils/request';

const prefix = '/sales/rules';

export const product_rule = ({ raw }) => {
  return axios({
    method: 'post',
    url: `${prefix}/product`,
    data: raw,
  })
}