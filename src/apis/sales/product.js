import axios from 'utils/request';

const prefix = '/sales/product';

export const search = ({ q }) => {
  return axios({
    method: 'GET',
    url: `${prefix}/search`,
    params: {
      q,
    }
  })
};