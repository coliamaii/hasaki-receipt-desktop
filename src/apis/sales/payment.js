import axios from 'utils/request';

const prefix = '/accounting/receipt';

export const insertPOS = (raw) => {
  return axios({
    method: 'POST',
    url: `${prefix}/insert-pos`,
    data: raw,
  });
}

export const completePOS = (data) => {
  return axios({
    method: 'POST',
    url: `${prefix}/${data.receipt_id}/complete-pos`,
    data,
  })
}