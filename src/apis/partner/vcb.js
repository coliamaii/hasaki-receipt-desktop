import axios from 'utils/request';

const prefix = 'partner/vietcombank';

export const vcbRequestPayment = (source_code, store_id, amount, ip_pos) => {
  return axios({
    method: 'POST',
    url: `${prefix}/payment`,
    data: {
      source_code,
      store_id,
      amount,
      ip_pos,
    }
  })
}

export const vcbCheckPaymentStatus = (source_code, store_id, amount, log_id) => {
  return axios({
    method: 'PUT',
    url: `${prefix}/${log_id}/check`,
    data: {
      source_code,
      store_id,
      amount,
    }
  })
}