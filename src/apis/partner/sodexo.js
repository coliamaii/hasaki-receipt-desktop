import axios from 'utils/request';

const prefix = '/partner/sodexo';

export const checkSodexoGift = (code, store_id) => {
  return axios({
    method: 'GET',
    url: `${prefix}`,
    params: {
      code,
      store_id,
    }
  });
}

export const redeemSodexoGift = (code, source_id, source_code, store_id) => {
  return axios({
    method: 'POST',
    url: `${prefix}`,
    data: {
      code,
      source_id,
      source_code,
      store_id,
    }
  })
}