import axios from 'utils/request';

const prefix = '/partner/hasaki';

export const checkHasakiGift = (code, store_id) => {
  return axios({
    method: 'GET',
    url: `${prefix}`,
    params: {
      store_id,
      code,
    }
  })
}

export const redeemHasakiGift = (code, used_code, store_id) => {
  return axios({
    method: 'POST',
    url: `${prefix}`,
    data: {
      code,
      used_code,
      store_id,
    }
  });
}