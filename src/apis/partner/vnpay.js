import axios from 'utils/request';

const prefix = '/partner/vn-pay';

export const getQrCode = (store_id, source_code, amount, terminal_id) => {
  return axios({
    method: 'GET',
    url: `${prefix}/qr`,
    params: {
      store_id,
      source_code,
      amount,
      terminal_id,
    }
  })
}