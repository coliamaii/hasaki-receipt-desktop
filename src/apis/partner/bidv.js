import axios from 'utils/request';

const prefix = 'partner/bidv';

export const bidvPaymentRequest = (source_code, store_id, amount, ip_pos) => {
  return axios({
    method: 'POST',
    url: `${prefix}`,
    data: {
      source_code,
      store_id,
      amount,
      ip_pos,
    }
  });
}

export const bidvSavePaymentResult = (result, transaction_id) => {
  return axios({
    method: 'PUT',
    url: `${prefix}/${transaction_id}`,
    data: {
      result,
    }
  })
}