import axios from "utils/request";

const prefix = "/purchase/vendor";

export const getList = (params = {}) => {
  return axios({
    method: "GET",
    url: `${prefix}`,
    params: {
      ...params,
    },
  });
};