import axios from "utils/request";

const prefix = "/hr/timesheet";

export const getList = (params = {}) => {
  return axios({
    method: "GET",
    url: `${prefix}/login-user`,
    params: {
      ...params,
    },
  });
};
