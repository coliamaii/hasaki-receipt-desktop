import axios from "utils/request";

const prefix = "/hr/staff";

export const staffConsultant = (code) => {
  return axios({
    method: 'GET',
    url: `${prefix}`,
    params: {
      code,
    }
  })
}