import axios from "utils/request";

const prefix = "/hr/comment";

export const postCmt = (data) => {
  return axios({
    method: "POST",
    url: `${prefix}`,
    data,
  });
};

export const getList = (params = {}) => {
  return axios({
    method: "GET",
    url: `${prefix}`,
    params: {
      ...params,
    },
  });
};
