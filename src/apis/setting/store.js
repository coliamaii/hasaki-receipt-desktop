import axios from "utils/request";

const prefix = "/setting/store";

export const getList = (params = {}) => {
  return axios({
    method: "GET",
    url: `${prefix}`,
    params: {
      ...params,
    },
  });
};

export const getStoreConfigInfo = (id) => {
  return axios({
    method: "GET",
    url: `${prefix}/${id}/config`,
  });
};

