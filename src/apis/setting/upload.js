import axios from "utils/request";

const prefix = "/setting/upload";

export const getLinks = (params = {}) => {
  return axios({
    method: "GET",
    url: `${prefix}`,
    params: {
      ...params,
    },
  });
};

export const postFile = (formData) => {
  return axios({
    method: "POST",
    url: `${prefix}`,
    data: formData,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  });
};
