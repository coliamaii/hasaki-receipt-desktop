import axios from "utils/request";

const prefix = "/setting/user";

export const getList = (params = {}) => {
  return axios({
    method: "GET",
    params: {
      ...params,
    },
    url: `${prefix}`,
  });
};

export const create = (data) => {
  return axios({
    method: "POST",
    data,
    url: `${prefix}`,
  });
};
export const getDetail = (id) => {
  return axios({
    method: "GET",
    url: `${prefix}/${id}`,
  });
};

export const update = (id, data) => {
  return axios({
    method: "PUT",
    data,
    url: `${prefix}/${id}`,
  });
};

export const destroy = (id) => {
  return axios({
    method: "POST",
    url: `${prefix}/${id}/delete`,
  });
};

// params: q, ids
export const search = (params) => {
  return axios({
    method: "GET",
    url: `${prefix}`,
    params: {
      sort: "id",
      offset: 0,
      limit: 10,
      ...params,
    },
  });
};

export const profile = () => {
  return axios({
    method: "GET",
    url: `${prefix}/profile`,
  });
};
export const ipPublic = () => {
  return axios({
    method: "GET",
    url: `${prefix}/ip-public`,
  });
};
