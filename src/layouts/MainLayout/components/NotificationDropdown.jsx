import React from 'react';
import { faBell } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Badge, Dropdown, Menu } from 'antd';

function NotificationDropdown() {
  return (
    <>
      <Dropdown
        trigger={['click']}
        overlay={
          (<Menu className="rounded-0">
            
          </Menu>)
        }
      >
        <button className="btn btn-bell text-white mr-3" style={{ marginRight: '1em' }}>
          <Badge count={0} showZero>
            <FontAwesomeIcon style={{ fontSize: 20 }} icon={faBell} />
          </Badge>
        </button>
      </Dropdown>
    </>
  )
}

export default NotificationDropdown;