import { UserOutlined } from "@ant-design/icons";
import {
  faCalculator,
  faCog,
  faDollarSign,
  faLifeRing,
  faLock,
  faSignOutAlt,
  faSortDown,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Dropdown, Menu, Space } from "antd";
import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { OLD_INSIDE } from "web.config";

function UserDropdown() {
  const profile = useSelector((state) => state.auth.info.profile);

  return (
    <>
      <Dropdown
        trigger={["click"]}
        overlay={
          <Menu>
            <Menu.Item key="0">
              {/* <a href={OLD_INSIDE + '/common/profile/dashboard'}><FontAwesomeIcon style={{ width: 20 }} icon={faUser} /> Profile</a> */}
              <Link to="/report-profile/dashboard">
                <FontAwesomeIcon style={{ width: 20 }} icon={faUser} /> Report
                Profile{" "}
              </Link>
            </Menu.Item>
            <Menu.Item key="1">
              <Link to="/report-profile/reconcile">
                <FontAwesomeIcon style={{ width: 20 }} icon={faCalculator} />{" "}
                Reconcile
              </Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link to="/report-profile/receipt">
                <FontAwesomeIcon style={{ width: 20 }} icon={faDollarSign} />{" "}
                Receipts
              </Link>
            </Menu.Item>
            <Menu.Item key="3">
              <Link to="/support-request">
                <FontAwesomeIcon style={{ width: 20 }} icon={faLifeRing} />{" "}
                Support Request{" "}
              </Link>
            </Menu.Item>
            <Menu.Item key="4">
              <a href={OLD_INSIDE + "/setting/user/change-password"}>
                <FontAwesomeIcon style={{ width: 20 }} icon={faLock} /> Change
                password
              </a>
            </Menu.Item>
            <Menu.Divider />
            <Menu.Item key="5">
              <Link to="/setting/pos">
                <FontAwesomeIcon style={{ width: 20 }} icon={faCog} />
                Setting
              </Link>
            </Menu.Item>
            <Menu.Item key="6">
              <Link to="/auth/login">
                <FontAwesomeIcon style={{ width: 20 }} icon={faSignOutAlt} />
                Sign Out
              </Link>
            </Menu.Item>
          </Menu>
        }
      >
        <Space style={{ cursor: "pointer", color: "white" }}>
          {/* <Avatar size={20} icon={<UserOutlined/>} /> */}
          <UserOutlined />
          <span className="my-0">{profile.name}</span>
          <FontAwesomeIcon style={{ fontSize: 20 }} icon={faSortDown} />
        </Space>
      </Dropdown>
    </>
  );
}

export default UserDropdown;
