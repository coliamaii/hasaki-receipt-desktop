import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getProductPricesByRuleSuccessAction } from "share/api/Product/GetProductsByRule/actions";
import { giftCodeRemoveAction } from "share/api/Product/Gift/action";
import { createNewLocalReceiptRequestAction } from "share/api/Profile/Receipt/CreateNewLocal/actions";
import {
  getReceiptStateBaseOnTabAction,
  updateReceiptTabAction,
} from "share/mode/action";
import { initialState as posReceiptInitialState } from "share/api/POSReceipt/reducer";
import { initialState as paymentSummaryInitialState } from "share/api/Payment/Summary/reducer";
import { initalState as cardInitialState } from "share/api/Payment/Type/Card/reducer";
import { initialState as balanceInitialState } from "share/api/Payment/Type/Balance/reducer";
import { initialState as giftcardInitialState } from "share/api/Payment/Type/GiftCard/reducer";
import { initalState as qrpayInitialState } from "share/api/Payment/Type/QrPay/reducer";
import { initialState as vatInitialState } from "share/api/Payment/VAT/reducer";
import ReceiptTab from "./ReceiptTab";

function ReceiptTabGroup() {
  const [currentReceiptIndex, setCurrentReceiptIndex] = useState(1);

  const listReceiptTab = useSelector((state) => state.mode.listReceiptTab);
  const client_receipt_id = useSelector(
    (state) => state.api.posReceipt.client_receipt_id
  );
  const { id: user_id, name: user_name } = useSelector(
    (state) => state.auth.info.profile
  );
  const { product, customer: searchCustomer } = useSelector(
    (state) => state.api
  );
  const mode = useSelector((state) => state.mode);
  const { store_id } = useSelector((state) => state.auth.info.profile);
  const { isLoading } = useSelector((state) => state.api.posReceipt);
  const {
    summary,
    type: { giftCard, card, qrPay, balance },
    vat,
  } = useSelector((state) => state.api.payment);

  const dispatch = useDispatch();

  //Check if not exist client_receipt_id, add new one and active the latest tab
  useEffect(() => {
    if (client_receipt_id) {
      if (
        listReceiptTab.findIndex(
          (item) => item.client_receipt_id === client_receipt_id
        ) === -1
      ) {
        const lastestIndex =
          listReceiptTab[listReceiptTab.length - 1]?.index ?? 0;
        dispatch(
          updateReceiptTabAction(
            [...listReceiptTab].concat({
              client_receipt_id,
              index: lastestIndex + 1,
            })
          )
        );
        setCurrentReceiptIndex(lastestIndex + 1);
      }
    }
  }, [client_receipt_id]);

  //If current tab index change, fetch data from local
  useEffect(() => {
    const itemIndex = listReceiptTab.findIndex(
      (item) => item.index === currentReceiptIndex
    );
    if (itemIndex === -1) return;
    dispatch(
      getReceiptStateBaseOnTabAction(
        listReceiptTab[itemIndex].client_receipt_id,
        store_id,
        mode.isOnline
      )
    );
  }, [currentReceiptIndex]);

  const handleSwitchReceipt = (index) => {
    if (!isLoading) {
      dispatch(getProductPricesByRuleSuccessAction([], []));
      dispatch(giftCodeRemoveAction());
      setCurrentReceiptIndex(index);
    }
  };

  const handleAddReceiptTab = () => {
    dispatch(getProductPricesByRuleSuccessAction([], []));
    dispatch(giftCodeRemoveAction());

    product.productTable.expired_items = {};
    product.productTable.codes = {};
    product.productTable.gift_items = [];

    const paymentInitState = {
      summary: paymentSummaryInitialState,
      type: {
        giftCard: giftcardInitialState,
        card: cardInitialState,
        qrPay: qrpayInitialState,
        balance: balanceInitialState,
      },
      vat: vatInitialState,
    };

    dispatch(
      createNewLocalReceiptRequestAction({
        user: { user_id, user_name },
        posReceipt: posReceiptInitialState,
        product,
        customer: searchCustomer,
        payment: paymentInitState,
        mode,
        total_price: 0,
        note: "",
        distNum: 0,
        distPercent: 0,
      })
    );
  };

  const handleCloseReceiptTab = (index) => {
    if (isLoading) return;
    //dispatch(getProductPricesByRuleSuccessAction([], []));
    const cloneReceiptTabs = [...listReceiptTab];
    const itemIndex = cloneReceiptTabs.findIndex(
      (item) => item.index === index
    );

    if (itemIndex === cloneReceiptTabs.length - 1) {
      setCurrentReceiptIndex(
        cloneReceiptTabs[cloneReceiptTabs.length - 2].index
      );
    } else {
      setCurrentReceiptIndex(cloneReceiptTabs[itemIndex + 1].index);
    }

    cloneReceiptTabs.splice(itemIndex, 1);
    dispatch(updateReceiptTabAction(cloneReceiptTabs));
  };

  return (
    <div className="receipt-tab">
      {listReceiptTab.map((receiptTab) => (
        <ReceiptTab
          index={receiptTab.index}
          name={receiptTab.client_receipt_id}
          isCurrent={receiptTab.index === currentReceiptIndex}
          handleClick={handleSwitchReceipt}
          handleClose={handleCloseReceiptTab}
        />
      ))}
      <div onClick={handleAddReceiptTab} className="receipt-tab__plus">
        <FontAwesomeIcon icon={faPlus} />
      </div>
    </div>
  );
}

export default ReceiptTabGroup;
