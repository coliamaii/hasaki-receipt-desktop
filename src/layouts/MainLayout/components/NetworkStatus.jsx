import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Modal, Switch } from "antd";
import { setIsOnlineModeAction } from "share/mode/action";

const { confirm } = Modal;

function NetworkStatus() {
  const { isOnline } = useSelector((state) => state.mode);

  const dispatch = useDispatch();

  const handleChangeStatus = (checked, event) => {
    confirm({
      title: "Do you want to switch mode?",
      okText: "Yes",
      cancelText: "No",
      onOk() {
        dispatch(setIsOnlineModeAction(checked));
      },
      onCancel() {},
    });
  };

  return (
    <Switch
      className="network-status-switch"
      style={{ marginRight: "0.5em" }}
      checkedChildren="Online"
      unCheckedChildren="Offline"
      checked={isOnline}
      onClick={handleChangeStatus}
      size="default"
    />
  );
}

export default NetworkStatus;
