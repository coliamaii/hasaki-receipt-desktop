import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { Space } from 'antd';

ReceiptTab.propTypes = {
  index: PropTypes.number.isRequired,
  name: PropTypes.number.isRequired,
  isCurrent: PropTypes.bool.isRequired,
  handleClick: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
}

function ReceiptTab({ index, name, isCurrent, handleClick, handleClose }) {
  return (
    <div onClick={() => { handleClick(index) }} className={`receipt-tab__item receipt-tab__item--${isCurrent ? 'active' : ''}`}>
      <div>Receipt {name}</div>
      <div
        style={{ display: 'flex', alignItems: 'center' }}
      >
        <FontAwesomeIcon
          onClick={(e) => { e.stopPropagation(); handleClose(index) }}
          className="receipt-tab__item__close" icon={faTimesCircle}
        />
      </div>
    </div>
  )
}

export default ReceiptTab;