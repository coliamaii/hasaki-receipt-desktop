import { ClockCircleTwoTone } from "@ant-design/icons";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Badge, Dropdown, Menu } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { setUpdateStatusAction } from "share/mode/action";
import UPDATE_STATUS from "utils/update-status";

function SettingDropdown() {
  const { updateStatus } = useSelector((state) => state.mode);

  const [icon, setIcon] = useState(null);

  const dispatch = useDispatch();

  useEffect(async () => {
    await window.api.listenUpdateMessage((e, message) => {
      switch (message) {
        case UPDATE_STATUS.UPDATE_NOT_AVAILABLE: {
          alert("There are currently no updates available!");
          dispatch(setUpdateStatusAction(null));
          break;
        }
        case UPDATE_STATUS.UPDATE_DOWNLOADED: {
          dispatch(setUpdateStatusAction(UPDATE_STATUS.UPDATE_DOWNLOADED));
          break;
        }
        case UPDATE_STATUS.UPDATE_ERROR: {
          dispatch(setUpdateStatusAction(UPDATE_STATUS.UPDATE_ERROR));
          break;
        }
      }
    });
  }, []);

  useEffect(() => {
    switch (updateStatus) {
      case UPDATE_STATUS.CHECKING: {
        setIcon(<ClockCircleTwoTone twoToneColor="#FF0000" />);
        break;
      }
      default: {
        setIcon(handleBadgeCount());
        break;
      }
    }
  }, [updateStatus]);

  const handleBadgeCount = () => {
    let count = 0;
    if (updateStatus === UPDATE_STATUS.UPDATE_DOWNLOADED) {
      count++;
    }
    return count;
  };

  const handleCheckUpdate = async () => {
    if (updateStatus === UPDATE_STATUS.UPDATE_DOWNLOADED) {
      await window.api.quitAndUpdate();
    } else {
      dispatch(setUpdateStatusAction(UPDATE_STATUS.CHECKING));
      await window.api.checkUpdate();
    }
  };

  return (
    <>
      <Dropdown
        trigger={["click"]}
        placement="bottomCenter"
        overlay={
          <Menu className="rounded-0">
            <Menu.Item
              disabled={updateStatus === UPDATE_STATUS.CHECKING}
              onClick={handleCheckUpdate}
              key="0"
            >
              {updateStatus === UPDATE_STATUS.UPDATE_DOWNLOADED ? (
                <span className="text-red">Restart and update!</span>
              ) : (
                <span>Check for updates...</span>
              )}
            </Menu.Item>
            {/* <Menu.Divider /> */}
            <Menu.Item key="1">
              <Link to="/product/return">Return Product</Link>
            </Menu.Item>
          </Menu>
        }
      >
        <button
          className="btn btn-bell text-white mr-3"
          style={{ marginRight: "1em" }}
        >
          <Badge size="default" count={icon}>
            <FontAwesomeIcon style={{ fontSize: 20 }} icon={faCog} />
          </Badge>
        </button>
      </Dropdown>
    </>
  );
}

export default SettingDropdown;
