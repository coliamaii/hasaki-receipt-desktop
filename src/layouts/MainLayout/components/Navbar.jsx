import React from 'react';
import NotificationDropdown from './NotificationDropdown';
import UserDropdown from './UserDropdown';

import logo from 'assets/images/logo.png';
import { Link, useLocation } from 'react-router-dom';
import NetworkStatus from './NetworkStatus';
import SettingDropdown from './SettingDropdown';
import ReceiptTabGroup from './ReceiptTabGroup';

function Navbar() {
  const location = useLocation();

  return (
    <div className="top-nav-bar">
      <div>
        <ul className="nav navbar-nav">
          <li><img src={logo} /></li>
          <li style={{ marginLeft: '1em' }}>
            <Link to="/">
              <strong style={{ color: 'white' }}>POS</strong>
            </Link>
          </li>
          {
            location.pathname === '/' && (
              <li>
                <ReceiptTabGroup />
              </li>
            )
          }
        </ul>
      </div>
      <div>
        <NetworkStatus />
        <NotificationDropdown />
        <SettingDropdown />
        <UserDropdown />
      </div>
    </div>
  )
}

export default Navbar;