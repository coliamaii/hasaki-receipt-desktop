import React from 'react';
import { Layout } from 'antd';
import Header from './Header';

import './style.scss';

function MainLayout({ children }) {
  return (
    <>
      <Layout
        style={{
          width: '100%',
          minHeight: '100vh'
        }}
      >
        <Layout.Header
          className="fixed-top-header main-header bg-header pl-0 pr-3 box-shadow pro-laylout-header"
        >
          <Header />
        </Layout.Header>
        <Layout.Content style={{ marginTop:50 }}>
          {children}
        </Layout.Content>
      </Layout>
    </>
  )
}

export default MainLayout;