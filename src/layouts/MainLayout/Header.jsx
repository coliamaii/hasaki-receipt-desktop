import React from 'react';
import Navbar from './components/Navbar';

function Header() {
  return (
    <div
      style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}
    >
      <Navbar />
    </div>
  )
}

export default Header;