import { Button, Col, Row } from "antd";
import React, { useState } from "react";
import { useHistory } from "react-router";

function PrintLayout({ children }) {
  const history = useHistory();
  const [isVisiable, setIsVisiable] = useState(true);

  const handleBack = () => {
    history.push("/report-profile/reconcile");
  };
  const handlePrint = async () => {
    await setIsVisiable(false);
    await window.print();
    await setIsVisiable(true);
  };
  return (
    <div
      className="container-fluid"
      // style={{ height: "100vh", minHeight: 450, backgroundColor: "#d2d6de" }}
    >
      {/* {isVisiable && (
        <Row>
          <Col>
            <Button onClick={handleBack}>Back</Button>
          </Col>
          <Col>
            <Button onClick={handlePrint}>Print</Button>
          </Col>
        </Row>
      )} */}

      {children}
    </div>
  );
}

PrintLayout.propTypes = {};

export default PrintLayout;
