import React from 'react';
import logo from 'assets/images/logo_1.png';

import './auth-layout.scss';

function AuthLayout({ children }) {
  return (
    <>
      <div
        className="auth-layout"
        style={{ height: '100vh', minHeight: 450, backgroundColor: '#d2d6de' }}
      >
        <img alt="" src={logo} width="100" />
        {children}
      </div>
    </>
  )
}

export default AuthLayout;