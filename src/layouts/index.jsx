import React from 'react';
import classNames from 'classnames';
import MainLayout from './MainLayout'
import AuthLayout from './AuthLayout'
import PrintLayout from './PrintLayout';

function Layout({ children, template, props }) {
  const getTemplate = () => {
    switch (template) {
      case 'auth':
        return <AuthLayout >
          {children}
        </AuthLayout>
      case 'print':
        return <PrintLayout {...props}>
            {children}
        </PrintLayout>
      case 'blank':
        return children
      default:
        return <MainLayout>
          {children}
        </MainLayout>
    }
  }

  return (
    <>
      {
        getTemplate()
      }
    </>
  )
}

export default Layout;