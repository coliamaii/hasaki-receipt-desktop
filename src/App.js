import { EnterOutlined, UserOutlined } from "@ant-design/icons";
import { css } from "@emotion/css";
import { faSignOutAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Avatar, Button, Col, Form, Input, Modal, Row, Spin } from "antd";
import Layout from "layouts";
import React, { useEffect, useRef } from "react";
import { useTranslation } from "react-i18next";
import IdleTimer from "react-idle-timer";
import { useDispatch, useSelector } from "react-redux";
import { HashRouter, Link, Redirect, Route, Switch } from "react-router-dom";
import routes from "routes";
import { verify_offline } from "scenes/Auth/actions";
import {
  checkUserRequestAction,
  checkUserSuccessAction,
} from "share/api/LockScreen/CheckUser/actions";
import CheckStore from "share/components/CheckStore";
import { checkOnlineModeAction } from "share/mode/action";
import { ONLINE_RUNNING_MODE } from "web.config";

function App({ history }) {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const idleTimer = useRef(null);
  const { i18n } = useTranslation();

  const { checkedUser, isLoading, isLocked, err } = useSelector(
    (state) => state.api.lockScreen
  );
  const { isOnline } = useSelector((state) => state.mode);

  const {
    profile,
    staff_info,
    stores,
    permission: user_permissions,
    logged,
    verified,
  } = useSelector((state) => state.auth.info);

  const { locale, name: user_name, email: user_email, id: user_id } = profile;

  const isLockedLocal = localStorage.getItem("isLocked");
  const locationHref = window.location.href;
  const countAuthLoginStr = (locationHref.match(/\/auth\/login/g) || []).length;
  const countErrorStr = (locationHref.match(/\/errors/g) || []).length;

  const lockModalCss = css({
    "& .ant-modal": {
      top: 0,
      margin: "0 !important",
      width: "100vw !important",
      height: "100vh !important",
      maxWidth: "100vw",
    },
    "& .ant-modal .ant-modal-content": {
      backgroundColor: "#00000099 !important",
    },
  });

  const initialLockScreenFormValues = {
    email: user_email,
    password: "",
  };

  const handleOnAction = (event) => {
    console.log("user did something", event);
  };

  const handleOnActive = (event) => {
    console.log("user is active", event);
    console.log("time remaining", idleTimer.current.getRemainingTime());
  };

  const handleOnIdle = (e) => {
    console.log(locationHref);
    console.log(countAuthLoginStr);
    console.log(countErrorStr);
    if (countAuthLoginStr === 0 && countErrorStr === 0) {
      dispatch(checkUserSuccessAction({ isLocked: true }));
    }
  };

  const handleClickSignOutBtn = (e) => {
    dispatch(checkUserSuccessAction({ isLocked: false }));
  };

  const onLockScreenFormFinish = (params) => {
    if (isOnline) {
      dispatch(checkUserRequestAction(params));
    } else {
      dispatch(checkUserSuccessAction({ isLocked: false }));
    }
  };

  useEffect(() => {
    i18n.changeLanguage(locale);
  }, [locale]);

  useEffect(() => {
    if (isLocked) {
      form.setFieldsValue(initialLockScreenFormValues);
    }
  }, [isLocked]);

  useEffect(() => {
    form.setFieldsValue({ email: user_email });
  }, [user_email]);

  useEffect(() => {
    if (countAuthLoginStr === 0 && countErrorStr === 0) {
      dispatch(checkUserSuccessAction({ isLocked: JSON.parse(isLockedLocal) }));
    }

    if (ONLINE_RUNNING_MODE) {
      dispatch(checkOnlineModeAction());
    } else {
      dispatch(verify_offline());
    }
  }, []);

  return (
    <div className="App">
      <IdleTimer
        ref={idleTimer}
        timeout={1000 * 60 * 10} //10p 1000 * 60 * 10
        // onActive={handleOnActive}
        onIdle={handleOnIdle}
        // onAction={handleOnAction}
        // debounce={250}
      />
      {!verified && (
        <div
          style={{
            height: "100vh",
            width: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Spin size="large" />
        </div>
      )}
      {verified && (
        <HashRouter>
          <Switch>
            {routes.map(({ children, ...rest }) => {
              let components = children ? [rest, ...children] : [rest];
              return components.map(
                ({
                  check_store,
                  component: Content,
                  key,
                  name,
                  path,
                  is_public,
                  permission,
                  template,
                  exact = true,
                }) => (
                  <Route
                    exact={exact}
                    path={path}
                    key={key}
                    render={(route) =>
                      ((!permission ||
                        user_permissions.indexOf(permission) > -1) &&
                        !is_public &&
                        logged) ||
                      is_public ? (
                        <Layout template={template}>
                          {check_store && !profile.ip_allow ? (
                            <CheckStore />
                          ) : (
                            <Content
                              {...route}
                              pageName={name}
                              userInfo={profile}
                              stores={stores}
                              staffInfo={staff_info}
                              checkPermission={(action) =>
                                user_permissions.indexOf(action) > -1
                              }
                            />
                          )}
                        </Layout>
                      ) : logged ? (
                        <Redirect to="/errors/403" />
                      ) : (
                        <Redirect to="/auth/login" />
                      )
                    }
                  />
                )
              );
            })}
          </Switch>
          <Modal
            visible={isLocked}
            // confirmLoading={isLoading}
            okText={false}
            closeIcon={<></>}
            // onCancel={this.handleCancel}
            title={false}
            footer={false}
            wrapClassName={lockModalCss}
            bodyStyle={{ height: "100vh", width: "100vw" }}
          >
            <Row className="text-center">
              <Col
                sm={24}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  textAlign: "center",
                  alignItems: "center",
                  paddingTop: "10%",
                }}
              >
                <Avatar size={200} icon={<UserOutlined />} alt="HASAKI" />
                <strong style={{ color: "white", fontSize: 30 }}>
                  {user_name}
                </strong>
                <Form
                  form={form}
                  name="lockScreenForm"
                  initialValues={initialLockScreenFormValues}
                  onFinish={onLockScreenFormFinish}
                  style={{ margin: 0, display: "flex" }}
                >
                  <Form.Item name="email" noStyle>
                    <Input placeholder="Email" hidden />
                  </Form.Item>
                  <Form.Item name="password" style={{ margin: 0, width: 300 }}>
                    <Input.Password placeholder="Password" />
                  </Form.Item>
                  <Form.Item style={{ margin: 0 }}>
                    <Button
                      htmlType="submit"
                      loading={isLoading}
                      icon={<EnterOutlined />}
                    ></Button>
                  </Form.Item>
                </Form>
                {err ? <div className="text-red">{err}</div> : ""}
                <Link to="/auth/login" onClick={handleClickSignOutBtn}>
                  <FontAwesomeIcon icon={faSignOutAlt} />
                  Sign Out
                </Link>
              </Col>
            </Row>
          </Modal>
        </HashRouter>
      )}
    </div>
  );
}

export default App;
