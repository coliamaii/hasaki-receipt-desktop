export default [
	{location_id: 'store_71', ip: '118.69.77.96', name: '71 Hoàng Hoa Thám'},
	{location_id: 'store_71', ip: '113.161.79.63', name: '71 Hoàng Hoa Thám'},
	//555 3/2
	{location_id: 'store_555', ip: '203.205.27.94', name: '555 3 tháng 2'},
	{location_id: 'store_555', ip: '115.73.218.48', name: '555 3 tháng 2'},
	{location_id: "store_555", ip: '113.161.49.29', name: '555 3 tháng 2'},
	{location_id: "store_555", ip: '14.161.19.94', name: '555 3 tháng 2'},
	{location_id: "store_555", ip: '222.253.79.158', name: '555 3 tháng 2'},
	//176 PLD
	{location_id: 'store_176', ip: '113.161.32.1', name: '176 Phan Đăng Lưu'},
	{location_id: 'store_176', ip: '27.74.248.89', name: '176 Phan Đăng Lưu'},
	//94LVV
	{location_id: 'store_94', ip: '14.161.14.105', name: '94 Lê Văn Việt'},
	{location_id: 'store_94', ip: '115.79.142.135', name: '94 Lê Văn Việt'},
	//119 NGT
	{location_id: 'store_119', ip: '14.241.241.61', name: '119 Nguyễn Gia Trí'},
	{location_id: 'store_119', ip: '27.74.243.244', name: '119 Nguyễn Gia Trí'},
	{location_id: 'store_119', ip: '115.78.7.24', name: '119 Nguyễn Gia Trí'},
	//657b QT
	{location_id: 'store_657', ip: '115.77.184.91', name: '657B Quang Trung'},
	{location_id: 'store_657', ip: '14.161.23.238', name: '657B Quang Trung'},
	//468A NTT
	{location_id: 'store_468', ip: '115.79.42.231', name: '468A Nguyễn Thị Thập'},
	{location_id: 'store_468', ip: '14.241.243.175', name: '468A Nguyễn Thị Thập'},
	//447 PVT
	{location_id: 'store_447', ip: '14.241.239.82', name: '447 Phan Văn Trị'},
	{location_id: 'store_447', ip: '115.77.188.14', name: '447 Phan Văn Trị'},
	//6M NAT
	{location_id: 'store_6m', ip: '14.241.230.132', name: '6M Nguyễn Anh Thủ'},
	{location_id: 'store_6m', ip: '115.79.46.55', name: '6M Nguyễn Anh Thủ'},
	//304 LVQ
	{location_id: 'store_304', ip: '14.241.254.119', name: '304 Lê Văn Quới'},
	{location_id: 'store_304', ip: '115.78.3.220', name: '304 Lê Văn Quới'},
	//104 LTT
	{location_id: 'store_104', ip: '27.74.248.129', name: '104 Lê Trọng Tấn'},
	{location_id: 'store_104', ip: '203.210.208.167', name: '104 Lê Trọng Tấn'},
	//141 NT
	{location_id: 'store_141', ip: '14.241.229.211', name: '141 Nguyễn Trãi'},
	{location_id: 'store_141', ip: '115.78.234.45', name: '141 Nguyễn Trãi'},
	//82 HMT
	{location_id: 'store_81', ip: '14.241.225.57', name: '81 Hồ Tùng Mậu'},
	{location_id: 'store_81', ip: '115.78.224.8', name: '81 Hồ Tùng Mậu'},
	//48 LVS
	{location_id: 'store_48', ip: '14.241.228.31', name: '48 Lê Văn Sỹ'},
	{location_id: 'store_48', ip: '115.79.143.132', name: '48 Lê Văn Sỹ'},
	//15LVV
	{location_id: 'store_15', ip: '115.73.218.84', name: '15 Võ Văn Ngân'},
	{location_id: 'store_15', ip: '203.210.209.56', name: '15 Võ Văn Ngân'},
	//182 Cầu Giấy
	{location_id: 'store_182', ip: '222.252.25.142', name: '182 Cầu Giấy'},
	{location_id: 'store_182', ip: '210.245.48.136', name: '182 Cầu Giấy'},
]

