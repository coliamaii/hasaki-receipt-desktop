const HOTKEY = {
  POS_NEW_CLICK: {
    key: 'F1',
    description: '',
  },
  POS_SEARCH_PRODUCT_BAR_FOCUS: {
    key: 'F3',
    description: '',
  },
  POS_SEARCH_GIFT_FOCUS: {
    key: 'Ctrl+F3',
    description: '',
  },
  POS_SEARCH_CUSTOMER_BY_PHONE_FOCUS: {
    key: 'F4',
    description: '',
  },
  POS_SHOW_GIFTS_CLICK: {
    key: 'F5',
    description: '',
  },
  POS_SHOW_COMBOS_CLICK: {
    key: 'F6',
    description: '',
  },
  POS_PAYMENT_CLICK: {
    key: 'F9',
    description: '',
  },
}

export default HOTKEY;