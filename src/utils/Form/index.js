import RenderInputText from './RenderInputText';
import RenderInputPassword from './RenderInputPassword';

export {
  RenderInputText,
  RenderInputPassword,
}