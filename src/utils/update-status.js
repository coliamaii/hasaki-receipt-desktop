const UPDATE_STATUS = {
  CHECKING: 'checking-update',
  UPDATE_NOT_AVAILABLE: 'update-not-available',
  UPDATE_DOWNLOADED: 'update-downloaded',
  UPDATE_ERROR: 'update-error',
}

export default UPDATE_STATUS;